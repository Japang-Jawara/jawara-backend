$(document).ready(function() {

    // JAWARA LEADS
    if ($('#tbl-jawara').length !== 0) {
        var url = 'jawara/tableJawaraLeds';
        $('#tbl-jawara').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": url,
                "dataSrc": ""
            }
        });

        $('#editkyc').on('show.bs.modal', function(event) {
            var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
            var modal = $(this)

            // Isi nilai pada field
            modal.find('#idx').attr("value", div.data('id'));
            modal.find('#name').attr("value", div.data('name'));
            modal.find('#phone').attr("value", div.data('phone'));
            modal.find('#nik_ktp').attr("value", div.data('nik_ktp'));
            modal.find('#nik_kk').attr("value", div.data('nik_kk'));

            modal.find('#file_ktpx').attr("value", div.data('file_ktp'));
            modal.find('#file_kkx').attr("value", div.data('file_kk'));
            modal.find('#file_storex').attr("value", div.data('file_store'));
            modal.find('#alamat_rumahx').val(div.data('alamat_rumah'));

        });
    }

    if ($('#tbl-jawara-priority').length !== 0) {
        var urll = 'jawara/tableJawaraPriority';
        $('#tbl-jawara-priority').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": urll,
                "dataSrc": ""
            }
        });
    }

    if ($('#tbl-jawara-done').length !== 0) {
        var urll = 'jawara/doneKYC';
        $('#tbl-jawara-done').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": urll,
                "dataSrc": ""
            }
        });
    }

    if ($('#tbl-all-jawara').length !== 0) {
        var url = 'jawara/tableAllJawara';
        $('#tbl-all-jawara').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": url,
                "dataSrc": ""
            }
        });
    }

    $('#addsales').on('show.bs.modal', function(event) {
        var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
        var modal = $(this)

        // Isi nilai pada field
        modal.find('#idx').attr("value", div.data('id'));

    });
    // END JAWARALEADS

    // KYC
    if ($('#tbl-kyc').length !== 0) {
        var urlll = 'kyc/tableKyc';
        $('#tbl-kyc').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": urlll,
                "dataSrc": ""
            }
        });
    }

    if ($('#tbl-kyc-reject').length !== 0) {
        var urlll = 'kyc/tableKycReject';
        $('#tbl-kyc-reject').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": urlll,
                "dataSrc": ""
            }
        });
    }

    if ($('#tbl-kyc-done').length !== 0) {
        var urllll = 'kyc/doneDi';
        $('#tbl-kyc-done').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": urllll,
                "dataSrc": ""
            }
        });
    }

    $('#ubahkyc').on('show.bs.modal', function(event) {
        var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
        var modal = $(this)

        var nama_jawara = '#nama_jawara'+div.data('nama_jawara');
        var delivered_email = '#delivered_email'+div.data('delivered_email');
        var active_phone = '#active_phone'+div.data('active_phone');
        var active_whatsapp = '#active_whatsapp'+div.data('active_whatsapp');
        var correct_ktp = '#correct_ktp'+div.data('correct_ktp');
        var correct_kk = '#correct_kk'+div.data('correct_kk');
        
        var ktp_jelas = '#ktp_jelas'+div.data('ktp_jelas');
        var kk_jelas = '#kk_jelas'+div.data('kk_jelas');
        var store_jelas = '#store_jelas'+div.data('store_jelas');

        var status = '#status'+div.data('status');

        // Isi nilai pada field
        modal.find('#idx').attr("value", div.data('id'));
        modal.find('#name').attr("value", div.data('name'));
        modal.find('#phone').attr("value", div.data('phone'));
        modal.find('#nik_ktp').attr("value", div.data('nik_ktp'));
        modal.find('#nik_kk').attr("value", div.data('nik_kk'));
        modal.find('#bank_code').attr("value", div.data('bank_code'));
        modal.find('#nomor_rekening').attr("value", div.data('nomor_rekening'));

        modal.find('#file_ktpx').attr("value", div.data('file_ktp'));
        modal.find('#file_ktp_pasanganx').attr("value", div.data('file_ktp_pasangan'));
        modal.find('#file_kkx').attr("value", div.data('file_kk'));
        modal.find('#file_storex').attr("value", div.data('file_store'));
        modal.find('#notes').val(div.data('notes'));

        $(nama_jawara).prop("checked", true);
        $(delivered_email).prop("checked", true);
        $(active_phone).prop("checked", true);
        $(active_whatsapp).prop("checked", true);
        $(correct_ktp).prop("checked", true);
        $(correct_kk).prop("checked", true);
        
        $(ktp_jelas).prop("checked", true);
        $(kk_jelas).prop("checked", true);
        $(store_jelas).prop("checked", true);

        $(status).prop("checked", true);

    });

    // END KYC

    // INSPECTION
    if ($('#tbl-inspection').length !== 0) {
        var urllll = 'inspection/tableInspection';
        $('#tbl-inspection').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": urllll,
                "dataSrc": ""
            }
        });
    }

    if ($('#tbl-inspection-rendah').length !== 0) {
        var urllll = 'inspection/tableInspectionrendah';
        $('#tbl-inspection-rendah').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": urllll,
                "dataSrc": ""
            }
        });
    }

    if ($('#tbl-done-ground').length !== 0) {
        var urllll = 'inspection/tableDoneground';
        $('#tbl-done-ground').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": urllll,
                "dataSrc": ""
            }
        });
    }

    $('#digital_inspection').on('show.bs.modal', function(event) {
        var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
        var modal = $(this)

        // Isi nilai pada field
        modal.find('#idx').attr("value", div.data('id'));
        modal.find('#name').attr("value", div.data('name'));
        modal.find('#phone').attr("value", div.data('phone'));
        modal.find('#nik_ktp').attr("value", div.data('nik_ktp'));

    });

    $('#onground').on('show.bs.modal', function(event) {
        var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
        var modal = $(this)

        // Isi nilai pada field
        modal.find('#idx').attr("value", div.data('id'));
        modal.find('#name').attr("value", div.data('name'));
        modal.find('#phone').attr("value", div.data('phone'));
        modal.find('#nik_ktp').attr("value", div.data('nik_ktp'));

    });

    // END INSPECTION

    // GROUND
    if ($('#tbl-ground').length !== 0) {
        var urllll = 'ground/tableGround';
        $('#tbl-ground').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": urllll,
                "dataSrc": ""
            }
        });
    }

    if ($('#tbl-ground-rendah').length !== 0) {
        var urllll = 'ground/tableGroundrendah';
        $('#tbl-ground-rendah').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": urllll,
                "dataSrc": ""
            }
        });
    }
    // END GROUND

    // p2p LEND
    if ($('#tbl-lendinspection').length !== 0) {
        var urllll = 'lend/getAlldata';
        $('#tbl-lendinspection').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": urllll,
                "dataSrc": ""
            }
        });
    }

    if ($('#tbl-lendscore').length !== 0) {
        var urllll = 'lend/getLendground';
        $('#tbl-lendscore').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": urllll,
                "dataSrc": ""
            }
        });
    }

    if ($('#tbl-p2p').length !== 0) {
        var urllll = 'lend/getP2p';
        $('#tbl-p2p').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": urllll,
                "dataSrc": ""
            }
        });
    }

    if ($('#tbl-lend').length !== 0) {
        var urllll = 'p2p/getP2p';
        $('#tbl-lend').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": urllll,
                "dataSrc": ""
            }
        });
    }

    $('#ubahstatus').on('show.bs.modal', function(event) {
        var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
        var modal = $(this)

        // Isi nilai pada field
        modal.find('#id').attr("value", div.data('id'));
    });

    $('#comments').on('show.bs.modal', function(event) {
        var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
        var modal = $(this)

        // Isi nilai pada field
        modal.find('#id').attr("value", div.data('id'));
    });

    if ($('#tbl-lend-approve').length !== 0) {
        var urllll = 'p2p/getP2pApprove';
        $('#tbl-lend-approve').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": urllll,
                "dataSrc": ""
            }
        });
    }

    if ($('#tbl-lend-reject').length !== 0) {
        var urllll = 'p2p/getP2pReject';
        $('#tbl-lend-reject').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": urllll,
                "dataSrc": ""
            }
        });
    }
    // END p2p LEND

    // Disbursement
    if ($('#tbl-disbursement').length !== 0) {
        var url = 'disbursement/getDisbursement';
        $('#tbl-disbursement').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": url,
                "dataSrc": ""
            }
        });
    }

    $('#disbursement').on('show.bs.modal', function(event) {
        var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
        var modal = $(this)

        // Isi nilai pada field
        modal.find('#idx').attr("value", div.data('id'));
        modal.find('#founders').attr("value", div.data('founders'));modal.find('#nik_ktp').attr("value", div.data('nik_ktp'));
        modal.find('#nik_ktp').attr("value", div.data('nik_ktp'));
        modal.find('#name').attr("value", div.data('name'));
        modal.find('#phone').attr("value", div.data('phone'));
        modal.find('#alamat_rumah').val(div.data('alamat_rumah'));
        modal.find('#shop_location').val(div.data('shop_location'));
        modal.find('#shop_name').val(div.data('shop_name'));
        modal.find('#kodepos').attr("value", div.data('kodepos'));
        modal.find('#kecamatan').val(div.data('kecamatan'));
        modal.find('#kab_kota').val(div.data('kab_kota'));
        modal.find('#provinsi').val(div.data('provinsi'));
        modal.find('#code_lend').attr("value", div.data('code_lend'));
        modal.find('#status_lend').attr("value", div.data('status_lend'));
        modal.find('#status_lends').attr("value", div.data('status_lends'));
        
    });

    $('#comment_cancel').on('show.bs.modal', function(event) {
        var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
        var modal = $(this)

        // Isi nilai pada field
        modal.find('#idx').attr("value", div.data('id'));
        
    });

    if ($('#tbl-disbursement-done').length !== 0) {
        var url = 'disbursement/getDisbursementDone';
        $('#tbl-disbursement-done').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": url,
                "dataSrc": ""
            }
        });
    }

    if ($('#tbl-disbursement-cancel').length !== 0) {
        var url = 'disbursement/getDisbursementCancel';
        $('#tbl-disbursement-cancel').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": url,
                "dataSrc": ""
            }
        });
    }
    // End Disbursement

    // Konfirmasi Pencairan
    if ($('#tbl-confirm').length !== 0) {
        var url = 'disbursement/getConfirm';
        $('#tbl-confirm').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": url,
                "dataSrc": ""
            }
        });
    }

    if ($('#tbl-confirm-accept').length !== 0) {
        var url = 'disbursement/getConfirmAccept';
        $('#tbl-confirm-accept').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": url,
                "dataSrc": ""
            }
        });
    }

    $('#confirm').on('show.bs.modal', function(event) {
        var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
        var modal = $(this)

        // Isi nilai pada field
        modal.find('#idx').attr("value", div.data('id'));
        
    });
    // End Konfirmasi Pencairan

    // Deploy
    if ($('#tbl-deploy').length !== 0) {
        var url = 'deploy/getOpenDeploy';
        $('#tbl-deploy').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": url,
                "dataSrc": ""
            }
        });
    }

    if ($('#tbl-deploy-done').length !== 0) {
        var url = 'deploy/getDoneDeploy';
        $('#tbl-deploy-done').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": url,
                "dataSrc": ""
            }
        });
    }

    $('#deploy').on('show.bs.modal', function(event) {
        var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
        var modal = $(this)

        // Isi nilai pada field
        modal.find('#idx').attr("value", div.data('id'));
        
    });

    $('#edit-deploy').on('show.bs.modal', function(event) {
        var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
        var modal = $(this)

        // Isi nilai pada field
        modal.find('#idx').attr("value", div.data('id'));
        modal.find('#date_delivery_dep').val(div.data('date_delivery_dep'));
        modal.find('#time_delivery_dep').val(div.data('time_delivery_dep'));
        modal.find('#link_surat_dep').attr("value", div.data('link_surat_dep'));
        modal.find('#status_dep').val(div.data('status_dep'));
        modal.find('#spanduk_dep').val(div.data('spanduk_dep'));
        modal.find('#freezer_dep').val(div.data('freezer_dep'));
        modal.find('#notes_dep').val(div.data('notes_dep'));
        
    });
    // End Deploy

    // Deploy Japang
    if ($('#tbl-deploy-jap').length !== 0) {
        var url = 'deployjap/getOpenDeploy';
        $('#tbl-deploy-jap').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": url,
                "dataSrc": ""
            }
        });
    }

    if ($('#tbl-deploy-jap-done').length !== 0) {
        var url = 'deployjap/getDoneDeploy';
        $('#tbl-deploy-jap-done').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": url,
                "dataSrc": ""
            }
        });
    }

    $('#deployjap').on('show.bs.modal', function(event) {
        var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
        var modal = $(this)

        // Isi nilai pada field
        modal.find('#idx').attr("value", div.data('id'));
        
    });
    // End Deploy

    // Score
    if ($('#tbl-score-tinggi').length !== 0) {
        var url = 'score/ScoreTinggi';
        $('#tbl-score-tinggi').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": url,
                "dataSrc": ""
            }
        });
    }

    if ($('#tbl-score-rendah').length !== 0) {
        var url = 'score/ScoreRendah';
        $('#tbl-score-rendah').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": url,
                "dataSrc": ""
            }
        });
    }

    if ($('#tbl-potensial-jawara').length !== 0) {
        var url = 'jawara/getPotensial';
        $('#tbl-potensial-jawara').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": url,
                "dataSrc": ""
            }
        });
    }

    if ($('#tbl-all-potensial-jawara').length !== 0) {
        var url = 'jawara/getPotensialDistinct';
        $('#tbl-all-potensial-jawara').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": url,
                "dataSrc": ""
            }
        });
    }
    
    if ($('#tbl-surveyor').length !== 0) {
        var url = 'surveyor/getData';
        $('#tbl-surveyor').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": url,
                "dataSrc": ""
            }
        });
    }

    // Sales
    if ($('#tbl-sales').length !== 0) {
        var url = 'sales/getSales';
        $('#tbl-sales').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": url,
                "dataSrc": ""
            }
        });
    }

    if ($('#tbl-delivery').length !== 0) {
        var url = 'delivery/getDelivery';
        $('#tbl-delivery').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": url,
                "dataSrc": ""
            }
        });
    }

    if ($('#tbl-eligiblee').length !== 0) {
        var url = 'eligible/getAlldata';
        $('#tbl-eligiblee').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": url,
                "dataSrc": ""
            }
        });
    }

    if ($('#tbl-eligible-approve').length !== 0) {
        var url = 'eligible/getApprove';
        $('#tbl-eligible-approve').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": url,
                "dataSrc": ""
            }
        });
    }

    if ($('#tbl-eligible-reject').length !== 0) {
        var url = 'eligible/getReject';
        $('#tbl-eligible-reject').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": url,
                "dataSrc": ""
            }
        });
    }

    if ($('#tbl-eligible-rekap').length !== 0) {
        var url = 'eligible/getRekap';
        $('#tbl-eligible-rekap').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": url,
                "dataSrc": ""
            }
        });
    }

    if ($('#tbl-price').length !== 0) {
        var urllll = 'config/getPrice';
        $('#tbl-price').dataTable({
            dom: 'Bfrtip',
            // dom: '<"row"<"col-sm-5"B><"col-sm-7"fr>>t<"row"<"col-sm-5"i><"col-sm-7"p>>',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": urllll,
                "dataSrc": ""
            }
        });
    }

    // LEADS JAPANG
    if ($('#tbl-leads-japang').length !== 0) {
        var urllll = 'leads_japang';
        $('#tbl-leads-japang').dataTable({
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": urllll,
                "dataSrc": ""
            }
        });
    }

    if ($('#tbl-approve-japang').length !== 0) {
        var urllll = 'leads_japang_approve';
        $('#tbl-approve-japang').dataTable({
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": urllll,
                "dataSrc": ""
            }
        });
    }

    if ($('#tbl-reject-japang').length !== 0) {
        var urllll = 'leads_japang_reject';
        $('#tbl-reject-japang').dataTable({
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": urllll,
                "dataSrc": ""
            }
        });
    }

    if ($('#tbl-reject-japang-fi').length !== 0) {
        var urllll = 'leads_japang_reject_fi';
        $('#tbl-reject-japang-fi').dataTable({
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": urllll,
                "dataSrc": ""
            }
        });
    }

    // DEPLOY LEADS JAPANG
    if ($('#tbl-japang-pending-deploy').length !== 0) {
        var url = 'getOpenDeploy';
        $('#tbl-japang-pending-deploy').dataTable({
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": url,
                "dataSrc": ""
            }
        });
    }

    if ($('#tbl-japang-deploy-done').length !== 0) {
        var url = 'getDoneDeploy';
        $('#tbl-japang-deploy-done').dataTable({
            dom: 'Bfrtip',
            buttons: [
                'excel', 'csv'
            ],
            responsive: true,
            "ajax": {
                "url": url,
                "dataSrc": ""
            }
        });
    }

    $('#deploy-japang').on('show.bs.modal', function(event) {
        var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
        var modal = $(this)

        // Isi nilai pada field
        modal.find('#idx').attr("value", div.data('id'));
        
    });

    $('#edit-deploy-japang').on('show.bs.modal', function(event) {
        var div = $(event.relatedTarget) // Tombol dimana modal di tampilkan
        var modal = $(this)

        // Isi nilai pada field
        modal.find('#idx').attr("value", div.data('id'));
        modal.find('#date_delivery_depjap').val(div.data('date_delivery_depjap'));
        modal.find('#time_delivery_depjap').val(div.data('time_delivery_depjap'));
        modal.find('#link_surat_depjap').attr("value", div.data('link_surat_depjap'));
        modal.find('#status_depjap').val(div.data('status_depjap'));
        modal.find('#spanduk_depjap').val(div.data('spanduk_depjap'));
        modal.find('#freezer_depjap').val(div.data('freezer_depjap'));
        modal.find('#notes_depjap').val(div.data('notes_depjap'));
        
    });
    // END DEPLOY LEADS JAPANG
});