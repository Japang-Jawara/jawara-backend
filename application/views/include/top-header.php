<div id="header" class="header navbar-default">
    <div class="navbar-header">
        <?php if($this->session->userdata('is_login') == NULL){?>
            <a href="#" class="navbar-brand"><span class="navbar-logo"></span> <b>JAPANG</b> JAWARA</a>
        <?php }else{ ?>
            <a href="<?php echo base_url('index.php/home');?>" class="navbar-brand"><span class="navbar-logo"></span> <b>JAPANG</b> JAWARA</a>
        <?php } ?>
        <button type="button" class="navbar-toggle" data-click="sidebar-toggled">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    <ul class="navbar-nav navbar-right">
        <li class="dropdown navbar-user">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<?= $this->session->userdata('profile_picture') ?>" alt=""/> 
                <span class="d-none d-md-inline"><?= $this->session->userdata('first_name') ?></span> <b class="caret"></b>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <div class="dropdown-divider"></div>
                <a href="<?php echo base_url('index.php/user'); ?>" class="dropdown-item">Edit Profil</a>
                <a href="<?php echo base_url('index.php/login/logout'); ?>" class="dropdown-item">Log Out</a>
            </div>     
        </li>
    </ul>
</div>