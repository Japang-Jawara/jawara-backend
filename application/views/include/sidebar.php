<!-- begin #sidebar -->
<div id="sidebar" class="sidebar">
	<div data-scrollbar="true" data-height="100%">
		<ul class="nav">
			<li class="nav-profile <?php echo $this->uri->segment(1) == "profil" ? "active" : ""; ?>">
				<a href="javascript:;" data-toggle="nav-profile">
					<div class="cover with-shadow"></div>
					<?php if($this->session->userdata('is_login') != NULL){?>
						<div class="image">
							<img src="<?= $this->session->userdata('profile_picture') ?>" alt=""/>   
						</div>
						<div class="info">
							<b class="caret pull-right"></b>
							<?php echo $this->session->userdata('first_name'); ?>
							<small><?= $this->session->userdata('last_name') ?></small>
						</div>
					<?php }else if($this->session->userdata('is_p2p') != NULL){ ?>
						<div class="image">
							<img src="<?= $this->session->userdata('profile_picture') ?>" alt=""/>   
						</div>
						<div class="info">
							<b class="caret pull-right"></b>
							<?php echo $this->session->userdata('first_name'); ?>
							<small><?= $this->session->userdata('last_name') ?></small>
						</div>
					<?php }else{ ?>
						<div class="info">
							<b class="caret pull-right"></b>
							Hi JAWARA
							<small>JApang WArung RAkyat</small>
						</div>
					<?php } ?>
				</a>
			</li>
			<?php if($this->session->userdata('is_login') == TRUE){?>
			<li>
				<ul class="nav nav-profile <?php echo $this->uri->segment(1) == "user" ? "active" : ""; ?>">
					<li class="<?php echo $this->uri->segment(1) == "user" ? "active" : ""; ?>"><a href="<?php echo base_url('index.php/user'); ?>"><i class="fa fa-cog"></i> Edit Profile</a></li>
				</ul>
			</li>
			<?php } ?>
		</ul>
		<?php if($this->session->userdata('is_login') == TRUE){?>
		<ul class="nav">
			<li class="nav-header">Utama</li>
			<li class="<?php echo $this->uri->segment(1) == "home" && $this->uri->segment(2) == "" ? "active" : ""; ?> has-sub">
				<a href="<?php echo base_url('index.php/home'); ?>">
					<i class="fa fa-th-large"></i>
					<span>DASHBOARD</span>
				</a>
			</li>
			<li class="<?php echo $this->uri->segment(2) == "active" ? "active" : ""; ?> has-sub">
				<a href="<?php echo base_url('index.php/home/active'); ?>">
					<i class="fa fa-map-pin"></i>
					<span>MAPS JAWARA ACTIVE</span>
				</a>
			</li>
		</ul>
		<ul class="nav">
			<li class="nav-header">Navigation</li>
			<li class="<?php echo $this->uri->segment(1) == "jawara" || $this->uri->segment(2) == "potensialLeads" ? "active" : ""; ?> has-sub">
				<a href="javascript:;">
					<b class="caret"></b>
					<i class="fa fa-users"></i> JAWARA LEADS
				</a>
				<ul class="sub-menu">
					<li class="<?php echo $this->uri->segment(2) == "allJawara" ? "active" : ""; ?>"><a href="<?php echo base_url('index.php/jawara/allJawara'); ?>">ALL JAWARA</a></li>
					<li class="<?php echo $this->uri->segment(1) == "jawara" && $this->uri->segment(2) == "" ? "active" : ""; ?>"><a href="<?php echo base_url('index.php/jawara'); ?>">JAWARA</a></li>
					<li class="<?php echo $this->uri->segment(2) == "potensialLeads" ? "active" : ""; ?>"><a href="<?php echo base_url('index.php/jawara/potensialLeads'); ?>">POTENSIAL JAWARA</a></li>
				</ul>
			</li>
			<li class="<?php echo $this->uri->segment(1) == "kyc" ? "active" : ""; ?> has-sub">
				<a href="<?php echo base_url('index.php/kyc'); ?>">
					<i class="fa fa-sticky-note"></i>
					<span>KYC</span>
				</a>
			</li>
			<li class="<?php echo $this->uri->segment(1) == "inspection" ? "active" : ""; ?> has-sub">
				<a href="<?php echo base_url('index.php/inspection'); ?>">
					<i class="fa fa-poll"></i>
					<span>DIGITAL INSPECTION</span>
				</a>
			</li>
			<li class="<?php echo $this->uri->segment(1) == "ground" ? "active" : ""; ?> has-sub">
				<a href="<?php echo base_url('index.php/ground'); ?>">
					<i class="fa fa-bullhorn"></i>
					<span>SCORE CARD/ON GROUND</span>
				</a>
			</li>

			<li class="<?php echo $this->uri->segment(1) == "lend" || $this->uri->segment(2) == "score" || $this->uri->segment(2) == "p2p" ? "active" : ""; ?> has-sub">
				<a href="javascript:;">
					<b class="caret"></b>
					<i class="fa fa-check"></i> FINANCIAL
				</a>
				<ul class="sub-menu">
					<li class="<?php echo $this->uri->segment(1) == "lend" && $this->uri->segment(2) == "" ? "active" : ""; ?>"><a href="<?php echo base_url('index.php/lend'); ?>">CHECK ELIGIBLE</a></li>
					<li class="<?php echo $this->uri->segment(2) == "p2p" ? "active" : ""; ?>"><a href="<?php echo base_url('index.php/lend/p2p'); ?>">REKAP FI</a></li>
				</ul>
			</li>

			<li class="<?php echo $this->uri->segment(1) == "disbursement" ? "active" : ""; ?> has-sub">
				<a href="javascript:;">
					<b class="caret"></b>
					<i class="fa fa-money"></i> DISBURSEMENT
				</a>
				<ul class="sub-menu">
					<li class="<?php echo $this->uri->segment(1) == "disbursement" && $this->uri->segment(2) == "" ? "active" : ""; ?>"><a href="<?php echo base_url('index.php/disbursement'); ?>">PRODUCT REQUEST</a></li>
					<li class="<?php echo $this->uri->segment(2) == "confirm" ? "active" : ""; ?>"><a href="<?php echo base_url('index.php/disbursement/confirm'); ?>">DISBURSEMENT CONFIRM </a></li>
				</ul>
			</li>

			<li class="<?php echo $this->uri->segment(1) == "deploy" || $this->uri->segment(1) == "deployjap" ? "active" : ""; ?> has-sub">
				<a href="javascript:;">
					<b class="caret"></b>
					<i class="fa fa-truck"></i> DEPLOY
				</a>
				<ul class="sub-menu">
					<li class="<?php echo $this->uri->segment(1) == "deploy" && $this->uri->segment(2) == "" ? "active" : ""; ?>"><a href="<?php echo base_url('index.php/deploy'); ?>">DEPLOY</a></li>
					<li class="<?php echo $this->uri->segment(1) == "deployjap" && $this->uri->segment(2) == "" ? "active" : ""; ?>"><a href="<?php echo base_url('index.php/deployjap'); ?>">DEPLOY BY JAPANG</a></li>
				</ul>
			</li>

			<li class="nav-header">Japang</li>
			<li class="<?php echo $this->uri->segment(1) == "japang"  ? "active" : ""; ?> has-sub">
				<a href="javascript:;">
					<b class="caret"></b>
					<i class="fa fa-building"></i> JAPANG LEADS
				</a>
				<ul class="sub-menu">
					<li class="<?php echo $this->uri->segment(1) == "japang" && $this->uri->segment(2) == "eligible" ? "active" : ""; ?>"><a href="<?php echo base_url('index.php/japang/eligible'); ?>">ELIGIBLE JAPANG</a></li>
					<li class="<?php echo $this->uri->segment(1) == "japang" && $this->uri->segment(2) == "deploy" ? "active" : ""; ?>"><a href="<?php echo base_url('index.php/japang/deploy'); ?>">DEPLOY JAPANG</a></li>
				</ul>
			</li>

			<li class="nav-header">Extra</li>
			<li class="<?php echo $this->uri->segment(1) == "eligible"  ? "active" : ""; ?> has-sub">
				<a href="javascript:;">
					<b class="caret"></b>
					<i class="fa fa-building"></i> JAPANG
				</a>
				<ul class="sub-menu">
					<li class="<?php echo $this->uri->segment(1) == "eligible" && $this->uri->segment(2) == "" ? "active" : ""; ?>"><a href="<?php echo base_url('index.php/eligible'); ?>">ELIGIBLE ASSESMENT</a></li>
					<li class="<?php echo $this->uri->segment(2) == "rekap" ? "active" : ""; ?>"><a href="<?php echo base_url('index.php/eligible/rekap'); ?>">REKAP ELIGIBLE</a></li>
				</ul>
			</li>

			<li class="<?php echo $this->uri->segment(1) == "score" ? "active" : ""; ?> has-sub">
				<a href="javascript:;">
					<b class="caret"></b>
					<i class="fa fa-star"></i> SCORE JAWARA
				</a>
				<ul class="sub-menu">
					<li class="<?php echo $this->uri->segment(1) == "score" ? "active" : ""; ?>"><a href="<?php echo base_url('index.php/score'); ?>">SELF ASSESSMENT</a></li>
				</ul>
			</li>

			<li class="<?php echo $this->uri->segment(1) == "delivery" ? "active" : ""; ?> has-sub">
				<a href="javascript:;">
					<b class="caret"></b>
					<i class="fa fa-sticky-note"></i> SURAT JALAN
				</a>
				<ul class="sub-menu">
					<li class="<?php echo $this->uri->segment(1) == "delivery" ? "active" : ""; ?>"><a href="<?php echo base_url('index.php/delivery'); ?>">SURAT JALAN</a></li>
				</ul>
			</li>

			<li class="<?php echo $this->uri->segment(2) == "price" ? "active" : ""; ?> has-sub">
				<a href="javascript:;">
					<b class="caret"></b>
					<i class="fa fa-money"></i> PRICE
				</a>
				<ul class="sub-menu">
					<li class="<?php echo $this->uri->segment(2) == "price" ? "active" : ""; ?>"><a href="<?php echo base_url('index.php/config/price'); ?>">PRICE</a></li>
				</ul>
			</li>
		</ul>

		<!-- <ul class="nav">
			<li class="nav-header">Jawara</li>
			<li class="<?php echo $this->uri->segment(1) == "register" ? "active" : ""; ?> has-sub">
				<a href="<?php echo base_url('index.php/register/jawara'); ?>">
					<i class="fa fa-registered"></i>
					<span>REGISTER JAWARA</span>
				</a>
			</li>
		</ul> -->

		<!-- <ul class="nav">
			<li class="nav-header">Uploads</li>
			<li class="<?php echo $this->uri->segment(1) == "uploads" && $this->uri->segment(2) == "" ? "active" : ""; ?> has-sub">
				<a href="javascript:;">
					<b class="caret"></b>
					<i class="fa fa-upload"></i> UPLOADS
				</a>
				<ul class="sub-menu">
					<li class="<?php echo $this->uri->segment(2) == "v_leads" ? "active" : ""; ?>"><a href="<?php echo base_url('index.php/uploads/v_leads'); ?>">BULK UPLOAD</a></li>
					<li class="<?php echo $this->uri->segment(2) == "leads" ? "active" : ""; ?>"><a href="<?php echo base_url('index.php/uploads/leads'); ?>">NEW JAWARA</a></li>
					<li class="<?php echo $this->uri->segment(2) == "assessment" ? "active" : ""; ?>"><a href="<?php echo base_url('index.php/uploads/assessment'); ?>">NEW ASSESSMENT</a></li>
				</ul>
			</li>
		</ul> -->
		
		<?php } ?>

		<?php if($this->session->userdata('is_p2p') == TRUE){?>
		<ul class="nav">
			<li class="nav-header">Utama</li>
			<li class="<?php echo $this->uri->segment(1) == "p2p" ? "active" : ""; ?> has-sub">
				<a href="<?php echo base_url('index.php/p2p'); ?>">
					<i class="fa fa-th-large"></i>
					<span>FINANCIAL INSTITUTION</span>
				</a>
			</li>
		</ul>
		<?php } ?>

		<?php if($this->session->userdata('is_surveyor') == TRUE){?>
		<ul class="nav">
			<li class="nav-header">Utama</li>
			<li class="<?php echo $this->uri->segment(1) == "surveyor" ? "active" : ""; ?> has-sub">
				<a href="<?php echo base_url('index.php/surveyor'); ?>">
					<i class="fa fa-th-large"></i>
					<span>SCORE CARD/ON GROUND</span>
				</a>
			</li>
		</ul>
		<?php } ?>

		<?php if($this->session->userdata('is_sales') == TRUE){?>
		<ul class="nav">
			<li class="nav-header">Utama</li>
			<li class="<?php echo $this->uri->segment(1) == "sales" ? "active" : ""; ?> has-sub">
				<a href="<?php echo base_url('index.php/sales'); ?>">
					<i class="fa fa-th-large"></i>
					<span>DISBURSEMENT</span>
				</a>
			</li>
		</ul>
		<?php } ?>
	</div>
</div>
<div class="sidebar-bg"></div>
<!-- end #sidebar -->
