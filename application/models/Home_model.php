<?php
class Home_model extends CI_Model{

    public function totalLeads(){
        return $this->db->query("SELECT COUNT(*) as totalLeads FROM jawara_leads")->result();
    }

    public function leadsHariIni(){
        $date = date('Y-m-d');
        return $this->db->query("SELECT COUNT(*) as leadsHariIni FROM jawara_leads WHERE created_at LIKE '%$date%'")->result();
    }

    //KYC
    public function doneKYC(){
        return $this->db->query("SELECT COUNT(*) as doneKYC FROM jawara_leads WHERE status_kyc = 1")->result();
    }

    public function doneKYCHari(){
        $date = date('Y-m-d');
        return $this->db->query("SELECT COUNT(*) as doneKYCHari FROM jawara_leads WHERE status_kyc = 1 AND updated_at LIKE '%$date%'")->result();
    }

    public function notyetKYC(){
        return $this->db->query("SELECT COUNT(*) as notyetKYC FROM jawara_leads WHERE created_at >= '2022-03-01' AND status_kyc IS NULL AND kode_referral != 'LOCA'")->result()[0]->notyetKYC;
    }

    public function belumDi(){
        return $this->db->query("SELECT COUNT(*) as belumDi
        FROM jawara_leads as a WHERE a.status_digital_inspection_kyc IS NULL AND (a.status_jawara_kyc = 1 OR a.status_jawara_kyc = 2)")->result()[0]->belumDi;
    }

    public function doneDi(){
        return $this->db->query("SELECT COUNT(*) as doneDi
        FROM jawara_leads as a WHERE a.status_digital_inspection_kyc = '1' AND (a.status_jawara_kyc = 1 OR a.status_jawara_kyc = 2)")->result()[0]->doneDi;
    }

    public function rejectKyc(){
        return $this->db->query("SELECT COUNT(*) as rejectKyc
        FROM jawara_leads as a WHERE a.status_jawara_kyc = 0")->result()[0]->rejectKyc;
    }
    //END KYC
    
    //SCORE
    public function lolosSurvey(){
        $inspection = $this->db->select('count(*) as inspect')->from('jawara_leads')->where('status_jawara_di',1)->where('status_ground_di', NULL)->get()->result()[0]->inspect;
        $score = $this->db->select('count(*) as score')->from('jawara_leads')->where('status_score_sc',1)->get()->result()[0]->score;

        return $inspection+$score;
    }

    public function lolosSurveyHari(){
        $date = date('Y-m-d');
        $inspection = $this->db->select('count(*) as inspect')->from('jawara_leads')
        ->where('status_jawara_di',1)->where('status_ground_di', NULL)
        ->like('created_at_di',$date)
        ->get()->result()[0]->inspect;

        $score = $this->db->select('count(*) as score')->from('jawara_leads')
        ->where('status_score_sc',1)
        ->like('created_at_sc',$date)
        ->get()->result()[0]->score;

        return $inspection+$score;
    }

    public function eligible(){
        return $this->db->query("SELECT
            count(*) as eligible
            FROM jawara_leads
            WHERE (status_jawara_di = 1  OR status_score_sc = 1) AND status_eligible IS NULL")
        ->result()[0]->eligible;
    }

    public function pendingFI(){
        return $this->db->select('COUNT(*) as pendingFI')->from('jawara_leads')->where('status_eligible', 1)->where('status_lend', NULL)->get()->result()[0]->pendingFI;
    }

    public function approveFI(){
        return $this->db->select('COUNT(*) as approveFI')->from('jawara_leads')->where('status_eligible', 1)->where('status_lend', 1)->get()->result()[0]->approveFI;
    }

    public function rejectFI(){
        return $this->db->select('COUNT(*) as rejectFI')->from('jawara_leads')->where('status_eligible', 1)->where('status_lend', 2)->get()->result()[0]->rejectFI;
    }
    //END SCORE

    //DISBURSEMENT
    public function totalDisbursement(){
        return $this->db->select('count(*) as totalDisbursement')->from('jawara_leads')->where('status_lend', 1)->where('status_disbursement IS NOT NULL')->get()->result()[0]->totalDisbursement;
    }

    public function totalDisbursementHari(){
        $date = date('Y-m-d');
        return $this->db->select('count(*) as totalDisbursement')->from('jawara_leads')->where('status_lend', 1)
        ->like('created_at',$date)
        ->get()->result()[0]->totalDisbursement;
    }

    public function getDisbursement(){
        return $this->db->select('count(*) as getDisbursement')->from('jawara_leads')->where('status_lend', 1)->where('status_disbursement', NULL)->get()->result()[0]->getDisbursement;
    }

    public function getDisbursementDone(){
        return $this->db->select('count(*) as getDisbursementDone')->from('jawara_leads')->where('created_at_dis IS NOT NULL')->get()->result()[0]->getDisbursementDone;
    }

    public function getConfirmPending(){
        return $this->db->select('count(*) as getConfirmPending')->from('jawara_leads')->where('created_at_dis IS NOT NULL')->where('updated_confirm_dis', NULL)->get()->result()[0]->getConfirmPending;
    }

    public function getConfirmAccept(){
        return $this->db->select('count(*) as getConfirmAccept')->from('jawara_leads')->where('status_confirm_dis', 1)->get()->result()[0]->getConfirmAccept;
    }
    //END DISBURSEMENT

    //DEPLOY
    public function totalDeploy(){
        return $this->db->select('count(*) as totalDeploy')->from('jawara_leads')->where('created_at_dep IS NOT NULL')->get()->result()[0]->totalDeploy;
    }

    public function totalDeployHari(){
        $date = date('Y-m-d');
        return $this->db->select('count(*) as totalDeploy')->from('jawara_leads')->where('created_at_dep IS NOT NULL')
        ->like('created_at_dep',$date)
        ->get()->result()[0]->totalDeploy;
    }

    public function getOpenDeploy(){
        return $this->db->select('count(*) as getOpenDeploy')->from('jawara_leads')->where('status_confirm_dis IS NOT NULL')->where('status_deploy_dis', NULL)->get()->result()[0]->getOpenDeploy;
    }

    public function totalDeployJapang(){
        return $this->db->select('count(*) as totalDeployJapang')->from('jawara_score_eligible')->where('created_at_dep IS NOT NULL')->get()->result()[0]->totalDeployJapang;
    }
    //end deploy

    public function totalperProvinsi(){
        return $this->db->select('upper(provinsi) as provinsi, count(*) as total')->from('jawara_leads')
        ->where('provinsi', 'DKI JAKARTA')
        ->or_where('provinsi', 'JAWA BARAT')
        ->or_where('provinsi', 'JAWA TIMUR')
        ->group_by('provinsi')->get()->result();
    }

    public function getTotal(){
        return $this->db->query("SELECT total, tanggal
            FROM (SELECT count(*) as total, DATE_FORMAT((`created_at`), '%Y-%m-%d') AS tanggal
            FROM jawara_leads
            GROUP BY DATE_FORMAT((`created_at`), '%Y-%m-%d')
            ORDER BY DATE_FORMAT((`created_at`), '%Y-%m-%d') DESC
            LIMIT 7) a
            ORDER BY tanggal ASC");

        // $this->db->select("count(*) as total, DATE_FORMAT((`created_at`), '%d-%m-%Y') AS tanggal ");
		// $this->db->from('jawara_leads');
		// $this->db->group_by("DATE_FORMAT((`created_at`), '%d-%m-%Y')");
        // $this->db->order_by('created_at', 'DESC');
        // $this->db->limit(7);
		// $result = $this->db->get();

		// return $result;
    }

    public function maps(){
        return $this->db->query("SELECT gmaps_di FROM jawara_leads WHERE gmaps_di IS NOT NULL AND gmaps_di != 1 AND gmaps_di != 0")->result();
    }

    public function active(){
        return $this->db->query("SELECT gmaps as gmaps_di FROM jawara_active WHERE is_active = '1'")->result();
    }

    public function mapsJakarta(){
        return $this->db->query("SELECT gmaps_di FROM jawara_leads WHERE provinsi = 'DKI JAKARTA' AND gmaps_di IS NOT NULL AND gmaps_di != 0")->result();
    }

    public function mapsJawaBarat(){
        return $this->db->query("SELECT gmaps_di FROM jawara_leads WHERE provinsi = 'JAWA BARAT' AND gmaps_di IS NOT NULL AND gmaps_di != 0")->result();
    }

    public function mapsJawaTimur(){
        return $this->db->query("SELECT gmaps_di FROM jawara_leads WHERE provinsi = 'JAWA TIMUR' AND gmaps_di IS NOT NULL AND gmaps_di != 0")->result();
    }

    public function delivery(){
        $date = date('Y-m-d');
        return $this->db->query("SELECT total, total_hari, total_beras, total_ayam, total_telur
            FROM
            (SELECT round(sum(a.jumlah * b.tonase),1) as total_beras
            FROM delivery_detail as a 
            JOIN jawara_sku as b ON a.sku = b.sku
            JOIN delivery as c ON a.delivery_kode = c.delivery_kode
            WHERE (a.sku LIKE '%BP%' OR a.sku LIKE '%BM%') AND c.status_reversal IS NULL) a,
            (SELECT round(sum(a.jumlah * b.tonase),1) as total_ayam
            FROM delivery_detail as a 
            JOIN jawara_sku as b ON a.sku = b.sku 
            JOIN delivery as c ON a.delivery_kode = c.delivery_kode
            WHERE a.sku LIKE '%JP%' AND c.status_reversal IS NULL) b,
            (SELECT round(sum(a.jumlah * b.tonase),1) as total_telur
            FROM delivery_detail as a 
            JOIN jawara_sku as b ON a.sku = b.sku 
            JOIN delivery as c ON a.delivery_kode = c.delivery_kode
            WHERE a.sku LIKE '%TJ%' AND c.status_reversal IS NULL) c,
            (SELECT COUNT(*) as total
            FROM delivery as a 
            WHERE a.status_reversal IS NULL) d,
            (SELECT COUNT(*) as total_hari
            FROM delivery as a 
            WHERE a.status_reversal IS NULL AND a.created_at LIKE '%$date%') e")
        ->result()[0];
    }

}
?>