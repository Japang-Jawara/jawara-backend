<div id="content" class="content">
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="<?php echo base_url('home');?>">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="<?php echo base_url('kepegawaian/thl');?>">DATA JAWARA LEADS</a></li>
    </ol>
    <h1 class="page-header"><b>DATA JAWARA LEADS</b></h1>
    <div class="note note-info note-with-right-icon">
        <div class="note-icon"><i class="fa fa-lightbulb"></i></div>
        <div class="note-content text-right">
            <h2><b>Jawara Leads</b></h2>
            <p>Kumpulan calon - calon yang mengikuti program <b>JAWARA</b>.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <!-- <a href="<?= base_url('index.php/jawara/all_data') ?>" class="btn btn-xl btn-primary" >Download All Data</a> -->
                    </h4>
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                </div>
        
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="tbl-all-jawara" class="table table-striped table-bordered table-td-valign-middle" width="100%">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>TIMESTAMP</th>
                                    <th>JAWARA ID</th>
                                    <th>EMAIL</th>
                                    <th>PHONE</th>
                                    <th>NAME</th>
                                    <th>TANGGAL LAHIR</th>
                                    <th>NIK KTP</th>
                                    <th>NIK KK</th>
                                    <th>ALAMAT RUMAH</th>
                                    <th>ALAMAT STORE</th>
                                    <th>KECAMATAN</th>
                                    <th>KABUPATEN/KOTA</th>
                                    <th>PROVINSI</th>
                                    <th>AREA JAWARA</th>
                                    <th>KODE POS</th>
                                    <th>KODE REFERRAL</th>
                                    <th>FILE KTP</th>
                                    <th>FILE KK</th>
                                    <th>FILE STORE</th>
                                    <th>SALES MAINTAIN</th>
                                    <th>ACTION</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!-- end panel-body -->
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-10 -->
    </div>

    <!-- Modal -->
    <div class="modal fade" id="addsales" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">ADD SALES MAINTAIN</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="jawara/addSales" enctype="multipart/form-data" method="POST">
                        
                        <div class="row">
                            <input class="form-control" type="hidden" name="id" id="idx" />

                            <div class="form-group col-xl-12">
                            <label class="col-form-label">Sales Maintain <span class="text-danger">*</span></label>
                                <select name="sales_maintain" id="sales_maintain" class="form-control" required>
                                    <option value="">Pilih Sales Maintain</option>
                                    <?php
                                        foreach ($getSalesMaintain as $row) {
                                            echo '<option value="' . $row->name . '">' . $row->name . '</option>';
                                        }
                                    ?>
                                </select>
                            </div>                 
                        </div>
                        
                        </br>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="reset" name="reset" class="btn btn-info"><i class="fa fa-cut"></i> Reset</button>
                            <button type="submit" class="btn btn-primary" value="Cek"><i class="fa fa-save"></i> Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Manage -->
    <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/app-manage.js');?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>

    <script>
        // autocomplete functionality
        if (jQuery('input#bank_code').length > 0) {
            jQuery('input#bank_code').typeahead({
                displayText: function(item) {
                    return item.name
                },
                afterSelect: function(item) {
                    this.$element[0].value = item.name;
                    jQuery("input#field-autocomplete").val(item.name);
                },
                source: function (query, process) {
                    jQuery.ajax({
                        url: "jawara/get_bank",
                        data: {query:query.toUpperCase()},
                        dataType: "json",
                        type: "POST",
                        success: function (data) {
                                process(data)
                        }
                    })
                }   
            });
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function() {

            $('#select-all').click(function(event) {
                if (this.checked) {
                    // Iterate each checkbox
                    $(':checkbox').each(function() {
                        this.checked = true;
                    });
                } else {
                    $(':checkbox').each(function() {
                        this.checked = false;
                    });
                }
            });

            $('.approve_checkbox').click(function() {
                if ($(this).is(':checked')) {
                    $(this).closest('tr').addClass('removeRow');
                } else {
                    $(this).closest('tr').removeClass('removeRow');
                }
            });

            $('#priority_all').click(function() {
                var checkbox = $('.approve_checkbox:checked');
                // console.log(checkbox);
                if (checkbox.length > 0) {
                    var checkbox_value = [];
                    $(checkbox).each(function() {
                        checkbox_value.push($(this).val());
                    });
                    var yakin = confirm("Apakah kamu yakin akan Mengapprove Pengajuan?");

                    if (yakin) {
                        $.ajax({
                            url: "jawara/priority_all",
                            method: "POST",
                            data: {
                                checkbox_value: checkbox_value
                            },
                            success: function() {
                                setInterval('location.reload()', 2000);
                            }
                        })
                        Swal.fire({
                            title: 'Informasi!',
                            text: 'Data Berhasil di Priority',
                            // 	// imageUrl: '<?= base_url('assets/vendors/images/svg/files-sent-animate.svg') ?>',
                            // 	// imageWidth: 400,
                            // 	// imageHeight: 250,
                            // 	// imageAlt: 'Custom image',
                        });
                    } else {
                        Swal.fire({
                            title: 'Informasi!',
                            text: 'Data Gagal di Priority',
                            // imageUrl: '<?= base_url('assets/vendors/images/svg/files-sent-animate.svg') ?>',
                            // imageWidth: 400,
                            // imageHeight: 250,
                            // imageAlt: 'Custom image',
                        });
                    }
                } else {
                    alert('Tidak ada data yang ingin di Priority, silahkan pilih data terlebih dahulu.');
                }
            });
        });
    </script>
