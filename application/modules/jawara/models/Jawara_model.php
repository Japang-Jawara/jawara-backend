<?php
class Jawara_model extends CI_Model{

    function priority_all($jawara_id, $data)
    {
        $this->db->where('id', $jawara_id);
        $result = $this->db->update('jawara_leads', $data);
    }

    public function setUserName($userName) 
	{
        return $this->_userName = $userName;
	}

    public function getPotensial(){
        return $this->db->query("SELECT a.*,
        CASE 
            WHEN b.id IS NULL THEN 'Belum Daftar'
            WHEN b.id IS NOT NULL THEN 'Sudah Daftar'
        END as status
        FROM jawara_leads_awal as a 
        LEFT JOIN jawara_leads as b ON a.phone = b.phone")->result();
    }

    public function getPotensialDistinct(){
        return $this->db->query("SELECT a.*,
        CASE 
            WHEN b.id IS NULL THEN 'Belum Daftar'
            WHEN b.id IS NOT NULL THEN 'Sudah Daftar'
        END as status
        FROM jawara_leads_awal as a 
        LEFT JOIN jawara_leads as b ON a.phone = b.phone GROUP BY a.phone")->result();
    }

	public function getAllBank()
	{
		$this->db->select(array('c.*'));
		$this->db->from('bank as c');
        $this->db->like('UPPER (c.name)', $this->_userName, 'both');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getJawaraLeds(){
        return $this->db->select('*')->from('jawara_leads')->where('status_kyc', NULL)->where('priority', NULL)->order_by('id', 'DESC')->get()->result();
    }

    public function getJawaraPriority(){
        return $this->db->select('*')->from('jawara_leads')->where('status_kyc', NULL)->where('priority', 1)->order_by('id', 'DESC')->get()->result();
    }

    public function doneKyc(){
        return $this->db->select('*')->from('jawara_leads')->where('status_kyc', '1')->order_by('id', 'DESC')->get()->result();
    }

    public function allJawara(){
        return $this->db->select('*')->from('jawara_leads')->get()->result();
    }

    public function getDSC(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'DSC')->get()->result();
    }

    public function getSD(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'SD')->get()->result();
    }

    public function getSalesMaintain(){
        return $this->db->select('*')->from('sales_maintain')->where('is_active', '1')->get()->result();
    }

    public function createKYC($data, $data2, $id){

        $result = $this->db->where('id', $id)->update('jawara_leads', $data);

        if($result){
            $this->db->where('id', $id)->update('jawara_leads', $data2);
            $this->session->set_flashdata('success','Jawara Registration KYC Telah Berhasil Diinput Ke Sistem');
            return TRUE;
        }else{
            $this->session->set_flashdata('success','Jawara Registration KYC Gagal Diinput Ke Sistem, Silahkan Coba Lagi');
            return FALSE;
        }
    }

    public function addSales($data, $id){

        $result = $this->db->where('id', $id)->update('jawara_leads', $data);

        if($result){
            $this->session->set_flashdata('success','Sales Maintain Telah Berhasil Diinput Ke Sistem');
            return TRUE;
        }else{
            $this->session->set_flashdata('success','Sales Maintain Gagal Diinput Ke Sistem, Silahkan Coba Lagi');
            return FALSE;
        }
    }

}
?>