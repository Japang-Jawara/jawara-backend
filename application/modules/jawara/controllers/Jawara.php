<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jawara extends CI_Controller{
     function __construct(){
          parent::__construct();

          $this->load->model('Jawara_model', 'modelJawara');
     }

     function uploadKTP()
	{
		foreach ($_FILES as $name => $fileInfo) {
			$filename = $_FILES['file_ktp']['name'];
			$tmpname = $_FILES['file_ktp']['tmp_name'];
			$exp = explode('.', $filename);
			$ext = end($exp);
			$newname =  'KTP'.date('Ymdhis').".". $ext;
			$config['upload_path'] = './assets/berkas/ktp/';
			$config['upload_url'] =  base_url() . 'assets/berkas/ktp/';
			$config['allowed_types'] = "png|jpg|jpeg";
			$config['max_size'] = '2000';
			$config['file_name'] = $newname;
			$this->load->library('upload', $config);
			move_uploaded_file($tmpname, "assets/berkas/ktp/" . $newname);
			return $newname;
		}
	}

     function uploadKTPPasangan()
	{
		foreach ($_FILES as $name => $fileInfo) {
			$filename = $_FILES['file_ktp_pasangan']['name'];
			$tmpname = $_FILES['file_ktp_pasangan']['tmp_name'];
			$exp = explode('.', $filename);
			$ext = end($exp);
			$newname =  'KTP_PASANGAN'.date('Ymdhis').".". $ext;
			$config['upload_path'] = './assets/berkas/ktppasangan/';
			$config['upload_url'] =  base_url() . 'assets/berkas/ktppasangan/';
			$config['allowed_types'] = "png|jpg|jpeg";
			$config['max_size'] = '2000';
			$config['file_name'] = $newname;
			$this->load->library('upload', $config);
			move_uploaded_file($tmpname, "assets/berkas/ktppasangan/" . $newname);
			return $newname;
		}
	}

     function uploadKK()
	{
		foreach ($_FILES as $name => $fileInfo) {
			$filename = $_FILES['file_kk']['name'];
			$tmpname = $_FILES['file_kk']['tmp_name'];
			$exp = explode('.', $filename);
			$ext = end($exp);
			$newname =  'KK'.date('Ymdhis')."." . $ext;
			$config['upload_path'] = './assets/berkas/kk/';
			$config['upload_url'] =  base_url() . 'assets/berkas/kk/';
			$config['allowed_types'] = "png|jpg|jpeg";
			$config['max_size'] = '2000';
			$config['file_name'] = $newname;
			$this->load->library('upload', $config);
			move_uploaded_file($tmpname, "assets/berkas/kk/" . $newname);
			return $newname;
		}
	}

     function uploadStore()
	{
		foreach ($_FILES as $name => $fileInfo) {
			$filename = $_FILES['file_store']['name'];
			$tmpname = $_FILES['file_store']['tmp_name'];
			$exp = explode('.', $filename);
			$ext = end($exp);
			$newname =  'STORE'.date('Ymdhis')."." . $ext;
			$config['upload_path'] = './assets/berkas/store/';
			$config['upload_url'] =  base_url() . 'assets/berkas/store/';
			$config['allowed_types'] = "png|jpg|jpeg";
			$config['max_size'] = '2000';
			$config['file_name'] = $newname;
			$this->load->library('upload', $config);
			move_uploaded_file($tmpname, "assets/berkas/store/" . $newname);
			return $newname;
		}
	}

     function index(){
          if($this->session->userdata('is_login') == TRUE){

               $x['getDSC'] = $this->modelJawara->getDSC();
               $x['getSD'] = $this->modelJawara->getSD();

               $this->load->view("include/head");
               $this->load->view("include/top-header");
               $this->load->view('jawara', $x);
               $this->load->view("include/sidebar");
               $this->load->view("include/panel");
               $this->load->view("include/footer");
               $this->load->view("include/alert");
          }else{
               $this->session->set_flashdata('success', 'Upsss!!!, Login dulu ya.');
               redirect('index.php/login');
          }
     }

     function allJawara(){
          if($this->session->userdata('is_login') == TRUE){

               $x['getDSC'] = $this->modelJawara->getDSC();
               $x['getSD'] = $this->modelJawara->getSD();
               $x['getSalesMaintain'] = $this->modelJawara->getSalesMaintain();

               $this->load->view("include/head");
               $this->load->view("include/top-header");
               $this->load->view('allJawara', $x);
               $this->load->view("include/sidebar");
               $this->load->view("include/panel");
               $this->load->view("include/footer");
               $this->load->view("include/alert");
          }else{
               $this->session->set_flashdata('success', 'Upsss!!!, Login dulu ya.');
               redirect('index.php/login');
          }
     }

     function potensialLeads(){
          if($this->session->userdata('is_login') == TRUE){

               $this->load->view("include/head");
               $this->load->view("include/top-header");
               $this->load->view('index');
               $this->load->view("include/sidebar");
               $this->load->view("include/panel");
               $this->load->view("include/footer");
               $this->load->view("include/alert");
          }else{
               $this->session->set_flashdata('success', 'Upsss!!!, Login dulu ya.');
               redirect('index.php/login');
          }
     }

     function getPotensial() {
          $data = $this->modelJawara->getPotensial();

          $no = 1;
          $jawara = array();

          foreach($data as $r) {

               $created_at = $r->created_at;
               $phone = $r->phone == NULL ? "<font>Handphone Tidak Ada</font>" : $r->phone;
               $name = $r->name == NULL ? "<font>Name Tidak Ada</font>" : $r->name;
               $status = $r->status;

               $jawara[] = array(
                    $no++,
                    $created_at,
                    $name,
                    $phone,
                    $status
               );
               
          }
          
          echo json_encode($jawara);
     }

     function getPotensialDistinct() {
          $data = $this->modelJawara->getPotensialDistinct();

          $no = 1;
          $jawara = array();

          foreach($data as $r) {

               $created_at = $r->created_at;
               $phone = $r->phone == NULL ? "<font>Handphone Tidak Ada</font>" : $r->phone;
               $name = $r->name == NULL ? "<font>Name Tidak Ada</font>" : $r->name;
               $status = $r->status;

               $jawara[] = array(
                    $no++,
                    $created_at,
                    $name,
                    $phone,
                    $status
               );
               
          }
          
          echo json_encode($jawara);
     }

     function tableAllJawara() {
          $data = $this->modelJawara->allJawara();

          $no = 1;
          $jawara = array();

          foreach($data as $r) {

               $email = "<a href='".base_url('index.php/profile?id='.$r->id)."'>".$r->email."</a>";
               $jawara_id = $r->id;
               $phone = $r->phone == NULL ? "<font>Handphone Tidak Ada</font>" : $r->phone;
               $name = $r->name == NULL ? "<font>Name Tidak Ada</font>" : $r->name;
               $nik_ktp = $r->nik_ktp == NULL ? "<font>NIK KTP Tidak Ada</font>" : $r->nik_ktp;
               $nik_kk = $r->nik_kk == NULL ? "<font>NIK KK Tidak Ada</font>" : $r->nik_kk;
               $alamat_rumah = $r->alamat_rumah == NULL ? "<font>Alamat Rumah Tidak Ada</font>" : $r->alamat_rumah;
               $alamat_usaha = $r->alamat_usaha == NULL ? "<font>Alamat Store Tidak Ada</font>" : $r->alamat_usaha;
               $kecamatan = $r->kecamatan == NULL ? "<font>Kecamatan Tidak Ada</font>" : $r->kecamatan;
               $kab_kota = $r->kab_kota == NULL ? "<font>Kabupaten/Kota Tidak Ada</font>" : $r->kab_kota;
               $provinsi = $r->provinsi == NULL ? "<font>Provinsi Tidak Ada</font>" : $r->provinsi;
               $kodepos = $r->kodepos == NULL ? "<font>Kodepos Tidak Ada</font>" : $r->kodepos;

               $kode_referral = $r->kode_referral == NULL ? "<font>Kode Referral Tidak Ada</font>" : $r->kode_referral;
               $area_jawara = $r->area_jawara == NULL ? "<font>Area Jawara Tidak Ada</font>" : $r->area_jawara;
               $created_at = $r->created_at;

               if($r->file_ktp != NULL){
                    $file_ktp = '<a href="'.$r->file_ktp.'" target="_blank" class="btn btn-sm btn-primary">'.$r->file_ktp.'</a>';
               }else{
                    $file_ktp = 'Tidak ada File KTP';
               }

               if($r->file_kk != NULL){
                    $file_kk = '<a href="'.$r->file_kk.'" target="_blank" class="btn btn-sm btn-success">'.$r->file_kk.'</a>';
               }else{
                    $file_kk = 'Tidak ada File KK';
               }

               if($r->file_store != NULL){
                    $file_store = '<a href="'.$r->file_store.'" target="_blank" class="btn btn-sm btn-warning">'.$r->file_store.'</a>';
               }else{
                    $file_store = 'Tidak ada File STORE';
               }

               $sales_maintain = $r->sales_maintain == NULL ? "<font>Sales Maintain Tidak Ada</font>" : $r->sales_maintain;

               $tanggal_lahir = $r->tanggal_lahir == NULL ? "<font>Tanggal Lahir Tidak Ada</font>" : $r->tanggal_lahir;

               $action = "<a href='javascript:;' 
               data-id='$r->id'
               data-toggle='modal' data-target='#addsales'
               class='btn btn-sm btn-success'><i class='fa fas fa-edit'></i> + SALES</a>";

               $jawara[] = array(
                    $no++,
                    $created_at,
                    $jawara_id,
                    $email,
                    $phone,
                    $name,
                    $tanggal_lahir,
                    $nik_ktp,
                    $nik_kk,
				$alamat_rumah,
                    $alamat_usaha,
                    $kecamatan,
                    $kab_kota,
                    $provinsi,
                    $area_jawara,
                    $kodepos,
                    $kode_referral,
                    $file_ktp,
                    $file_kk,
                    $file_store,
                    $sales_maintain,
                    $action
               );
               
          }
          
          echo json_encode($jawara);
     }

     function tableJawaraLeds() {
          $data = $this->modelJawara->getJawaraLeds();

          $no = 1;
          $jawara = array();

          foreach($data as $r) {

               $input = "<input type='checkbox' class='approve_checkbox' value='$r->id'>";
               $email = "<a href='".base_url('index.php/profile?id='.$r->id)."'>".$r->email."</a>";
               $jawara_id = $r->id;
               $phone = $r->phone == NULL ? "<font>Handphone Tidak Ada</font>" : $r->phone;
               $name = $r->name == NULL ? "<font>Name Tidak Ada</font>" : $r->name;
               $nik_ktp = $r->nik_ktp == NULL ? "<font>NIK KTP Tidak Ada</font>" : $r->nik_ktp;
               $nik_kk = $r->nik_kk == NULL ? "<font>NIK KK Tidak Ada</font>" : $r->nik_kk;
               $alamat_rumah = $r->alamat_rumah == NULL ? "<font>Alamat Rumah Tidak Ada</font>" : $r->alamat_rumah;
               $alamat_usaha = $r->alamat_usaha == NULL ? "<font>Alamat Store Tidak Ada</font>" : $r->alamat_usaha;
               $kecamatan = $r->kecamatan == NULL ? "<font>Kecamatan Tidak Ada</font>" : $r->kecamatan;
               $kab_kota = $r->kab_kota == NULL ? "<font>Kabupaten/Kota Tidak Ada</font>" : $r->kab_kota;
               $provinsi = $r->provinsi == NULL ? "<font>Provinsi Tidak Ada</font>" : $r->provinsi;
               $kodepos = $r->kodepos == NULL ? "<font>Kodepos Tidak Ada</font>" : $r->kodepos;

               $kode_referral = $r->kode_referral == NULL ? "<font>Kode Referral Tidak Ada</font>" : $r->kode_referral;
               $created_at = $r->created_at;

               if($r->file_ktp != NULL){
                    $file_ktp = '<a href="'.$r->file_ktp.'" target="_blank" class="btn btn-sm btn-primary">'.$r->file_ktp.'</a>';
               }else{
                    $file_ktp = 'Tidak ada File KTP';
               }

               if($r->file_kk != NULL){
                    $file_kk = '<a href="'.$r->file_kk.'" target="_blank" class="btn btn-sm btn-success">'.$r->file_kk.'</a>';
               }else{
                    $file_kk = 'Tidak ada File KK';
               }

               if($r->file_store != NULL){
                    $file_store = '<a href="'.$r->file_store.'" target="_blank" class="btn btn-sm btn-warning">'.$r->file_store.'</a>';
               }else{
                    $file_store = 'Tidak ada File STORE';
               }

               $action = "<a href='javascript:;' 
               data-id='$r->id'
               data-name='$r->name'
               data-phone='$r->phone'
               data-nik_ktp='$r->nik_ktp'
               data-nik_kk='$r->nik_kk'
               data-file_ktp='$r->file_ktp'
               data-file_kk='$r->file_kk'
               data-file_store='$r->file_store'
               data-alamat_rumah='$r->alamat_rumah'
               data-toggle='modal' data-target='#editkyc'
               class='btn btn-sm btn-success'><i class='fa fas fa-edit'></i> KYC</a>";


               $jawara[] = array(
                    $no++,
                    $input,
                    $created_at,
                    $jawara_id,
                    $email,
                    $phone,
                    $name,
                    $nik_ktp,
                    $nik_kk,
				$alamat_rumah,
                    $alamat_usaha,
                    $kecamatan,
                    $kab_kota,
                    $provinsi,
                    $kodepos,
                    $kode_referral,
                    $file_ktp,
                    $file_kk,
                    $file_store,
                    $action
               );
               
          }
          
          echo json_encode($jawara);
     }

     function tableJawaraPriority() {
          $data = $this->modelJawara->getJawaraPriority();

          $no = 1;
          $jawara = array();

          foreach($data as $r) {

               $email = "<a href='".base_url('index.php/profile?id='.$r->id)."'>".$r->email."</a>";
               $jawara_id = $r->id;
               $phone = $r->phone == NULL ? "<font>Handphone Tidak Ada</font>" : $r->phone;
               $name = $r->name == NULL ? "<font>Name Tidak Ada</font>" : $r->name;
               $nik_ktp = $r->nik_ktp == NULL ? "<font>NIK KTP Tidak Ada</font>" : $r->nik_ktp;
               $nik_kk = $r->nik_kk == NULL ? "<font>NIK KK Tidak Ada</font>" : $r->nik_kk;
               $alamat_rumah = $r->alamat_rumah == NULL ? "<font>Alamat Rumah Tidak Ada</font>" : $r->alamat_rumah;
               $alamat_usaha = $r->alamat_usaha == NULL ? "<font>Alamat Store Tidak Ada</font>" : $r->alamat_usaha;
               $kecamatan = $r->kecamatan == NULL ? "<font>Kecamatan Tidak Ada</font>" : $r->kecamatan;
               $kab_kota = $r->kab_kota == NULL ? "<font>Kabupaten/Kota Tidak Ada</font>" : $r->kab_kota;
               $provinsi = $r->provinsi == NULL ? "<font>Provinsi Tidak Ada</font>" : $r->provinsi;
               $kodepos = $r->kodepos == NULL ? "<font>Kodepos Tidak Ada</font>" : $r->kodepos;

               $kode_referral = $r->kode_referral == NULL ? "<font>Kode Referral Tidak Ada</font>" : $r->kode_referral;
               $created_at = $r->created_at;

               if($r->file_ktp != NULL){
                    $file_ktp = '<a href="'.$r->file_ktp.'" target="_blank" class="btn btn-sm btn-primary">'.$r->file_ktp.'</a>';
               }else{
                    $file_ktp = 'Tidak ada File KTP';
               }

               if($r->file_kk != NULL){
                    $file_kk = '<a href="'.$r->file_kk.'" target="_blank" class="btn btn-sm btn-success">'.$r->file_kk.'</a>';
               }else{
                    $file_kk = 'Tidak ada File KK';
               }

               if($r->file_store != NULL){
                    $file_store = '<a href="'.$r->file_store.'" target="_blank" class="btn btn-sm btn-warning">'.$r->file_store.'</a>';
               }else{
                    $file_store = 'Tidak ada File STORE';
               }

               $action = "<a href='javascript:;' 
               data-id='$r->id'
               data-name='$r->name'
               data-phone='$r->phone'
               data-nik_ktp='$r->nik_ktp'
               data-nik_kk='$r->nik_kk'
               data-file_ktp='$r->file_ktp'
               data-file_kk='$r->file_kk'
               data-file_store='$r->file_store'
               data-alamat_rumah='$r->alamat_rumah'
               data-toggle='modal' data-target='#editkyc'
               class='btn btn-sm btn-success'><i class='fa fas fa-edit'></i> KYC</a>";


               $jawara[] = array(
                    $no++,
                    $created_at,
                    $jawara_id,
                    $email,
                    $phone,
                    $name,
                    $nik_ktp,
                    $nik_kk,
				$alamat_rumah,
                    $alamat_usaha,
                    $kecamatan,
                    $kab_kota,
                    $provinsi,
                    $kodepos,
                    $kode_referral,
                    $file_ktp,
                    $file_kk,
                    $file_store,
                    $action
               );
               
          }
          
          echo json_encode($jawara);
     }

     function doneKYC() {
          $data = $this->modelJawara->doneKyc();

          $no = 1;
          $jawara = array();

          foreach($data as $r) {

               $email = "<a href='".base_url('index.php/profile?id='.$r->id)."'>".$r->email."</a>";
               $jawara_id = $r->id;
               $phone = $r->phone == NULL ? "<font>Handphone Tidak Ada</font>" : $r->phone;
               $name = $r->name == NULL ? "<font>Name Tidak Ada</font>" : $r->name;
               $nik_ktp = $r->nik_ktp == NULL ? "<font>NIK KTP Tidak Ada</font>" : $r->nik_ktp;
               $nik_kk = $r->nik_kk == NULL ? "<font>NIK KK Tidak Ada</font>" : $r->nik_kk;
               $alamat_rumah = $r->alamat_rumah == NULL ? "<font>Alamat Rumah Tidak Ada</font>" : $r->alamat_rumah;
               $alamat_usaha = $r->alamat_usaha == NULL ? "<font>Alamat Store Tidak Ada</font>" : $r->alamat_usaha;
               $kecamatan = $r->kecamatan == NULL ? "<font>Kecamatan Tidak Ada</font>" : $r->kecamatan;
               $kab_kota = $r->kab_kota == NULL ? "<font>Kabupaten/Kota Tidak Ada</font>" : $r->kab_kota;
               $provinsi = $r->provinsi == NULL ? "<font>Provinsi Tidak Ada</font>" : $r->provinsi;
               $kodepos = $r->kodepos == NULL ? "<font>Kodepos Tidak Ada</font>" : $r->kodepos;

               $kode_referral = $r->kode_referral == NULL ? "<font>Kode Referral Tidak Ada</font>" : $r->kode_referral;
               $created_at = $r->created_at;
               $updated_at = $r->updated_at;

               if($r->file_ktp != NULL){
                    $file_ktp = '<a href="'.$r->file_ktp.'" target="_blank" class="btn btn-sm btn-primary">'.$r->file_ktp.'</a>';
               }else{
                    $file_ktp = 'Tidak ada File KTP';
               }

               if($r->file_kk != NULL){
                    $file_kk = '<a href="'.$r->file_kk.'" target="_blank" class="btn btn-sm btn-success">'.$r->file_kk.'</a>';
               }else{
                    $file_kk = 'Tidak ada File KK';
               }

               if($r->file_store != NULL){
                    $file_store = '<a href="'.$r->file_store.'" target="_blank" class="btn btn-sm btn-warning">'.$r->file_store.'</a>';
               }else{
                    $file_store = 'Tidak ada File STORE';
               }

               $jawara[] = array(
                    $no++,
                    $created_at,
                    $jawara_id,
                    $email,
                    $phone,
                    $name,
                    $nik_ktp,
                    $nik_kk,
				$alamat_rumah,
                    $alamat_usaha,
                    $kecamatan,
                    $kab_kota,
                    $provinsi,
                    $kodepos,
                    $kode_referral,
                    $updated_at,
                    $file_ktp,
                    $file_kk,
                    $file_store
               );
               
          }
          
          echo json_encode($jawara);
     }

     function createKYC(){

          $id = $this->input->post('id');

          $ktp = '';
          $ktp_pasangan = '';
          $kk = '';
          $store = '';

          if (!empty($_FILES['file_ktp']['name'])) {
               $newname = $this->uploadKTP();
               $data['file_ktp'] = $newname;
               $ktp = $newname;

               $x['file_ktp_kyc'] = base_url('assets/berkas/ktp/').$ktp;
          }else{
               $x['file_ktp_kyc'] = $this->input->post('file_ktpx');
          }

          if (!empty($_FILES['file_ktp_pasangan']['name'])) {
               $newname = $this->uploadKTPPasangan();
               $data['file_ktp_pasangan'] = $newname;
               $ktp_pasangan = $newname;

               $x['file_ktp_pasangan_kyc'] = base_url('assets/berkas/ktppasangan/').$ktp_pasangan ;
          }

          if (!empty($_FILES['file_kk']['name'])) {
               $newname = $this->uploadKK();
               $data['file_kK'] = $newname;
               $kk = $newname;

               $x['file_kk_kyc'] = base_url('assets/berkas/kk/').$kk;
          }else{
               $x['file_kk_kyc'] = $this->input->post('file_kkx');
          }

          if (!empty($_FILES['file_store']['name'])) {
               $newname = $this->uploadStore();
               $data['file_store'] = $newname;
               $store = $newname;

               $x['file_store_kyc'] = base_url('assets/berkas/store/').$store;
          }else{
               $x['file_store_kyc'] = $this->input->post('file_storex');
          }          

          //INSERT DATA FROM TABLE JAWARA KYC
          $x['email_verifikasi_kyc'] = $this->input->post('email');
          $x['delivered_email_kyc'] = $this->input->post('delivered_email');
          $x['active_phone_kyc'] = $this->input->post('active_phone');
          $x['active_whatsapp_kyc'] = $this->input->post('active_whatsapp');
          $x['correct_ktp_kyc'] = $this->input->post('correct_ktp');
          $x['correct_kk_kyc'] = $this->input->post('correct_kk');
          $x['nama_jawara_kyc'] = $this->input->post('nama_jawara');
          $x['ktp_jelas_kyc'] = $this->input->post('ktp_jelas');
          $x['kk_jelas_kyc'] = $this->input->post('kk_jelas');
          $x['store_jelas_kyc'] = $this->input->post('store_jelas');
          $x['status_jawara_kyc'] = $this->input->post('status');
          $x['notes_kyc'] = $this->input->post('notes');
          $x['created_at_kyc'] = date('Y-m-d H:i:s');
          
          $x['phone_kyc'] = $this->input->post('phone');
          $x['name_kyc'] = $this->input->post('name');
          $x['nik_ktp_kyc'] = $this->input->post('nik_ktp');
          $x['nik_kk_kyc'] = $this->input->post('nik_kk');
          $x['bank_code_kyc'] = $this->input->post('bank_code');
          $x['nomor_rekening_kyc'] = $this->input->post('nomor_rekening');
          $x['same_alamat_kyc'] = $this->input->post('same_alamat_kyc');
          
          //UPDATE DATA FROM TABLE JAWARA LEDS
          $u['status_kyc'] = '1';
          $u['updated_at'] = date('Y-m-d H:i:s');

          $this->modelJawara->createKYC($x, $u, $id);

          redirect('index.php/jawara');
     }

     public function get_bank() 
	{
        $json = array();
        $userName =  $this->input->post('query');
        $this->modelJawara->setUserName($userName);
        $getBank = $this->modelJawara->getAllBank();
        foreach ($getBank as $element) {
            $json[] = array(
               'name' => $element['name'], 
               'code' => $element['code'],
            );
        }
		echo json_encode($json);
	}

     public function priority_all()
     {
          if ($this->input->post('checkbox_value')) {
               $jawara_id = $this->input->post('checkbox_value');

               $x['priority'] = '1';
               $x['updated_at'] = date('Y-m-d H:i:s');

               for ($count = 0; $count < count($jawara_id); $count++) {
                    $x['id'] = $jawara_id[$count];
                    $this->modelJawara->priority_all($jawara_id[$count], $x);
               }
          }
     }

     function all_data()
     {
          $this->load->dbutil();
          $this->load->helper('file');
          $this->load->helper('download');
          $delimiter = ",";
          $newline = "\r\n";
          $filename = "bulk_upload_update.csv";
          $query = "SELECT * FROM jawara_leads"; //USE HERE YOUR QUERY
          $result = $this->db->query($query);
          $data = $this->dbutil->csv_from_result($result, $delimiter, $newline);
          force_download($filename, $data);
  
     }

     function addSales(){

          $id = $this->input->post('id');       

          //INSERT DATA FROM TABLE SALES MAINTAIN
          $x['sales_maintain'] = $this->input->post('sales_maintain');
          
          $this->modelJawara->addSales($x, $id);

          redirect('index.php/jawara/allJawara');
     }

}