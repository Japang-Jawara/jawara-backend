<?php
class Inspection_model extends CI_Model{

    public function getDSC(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'DSC')->get()->result();
    }

    public function getSD(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'SD')->get()->result();
    }

    public function getBA(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'BA')->get()->result();
    }

    public function getInspection(){
        return $this->db->select('*')->from('jawara_leads')->where('status_jawara_di', '1')->where('status_ground_di', NULL)->order_by('id', 'DESC')->get()->result();
        // return $this->db->query("SELECT * FROM jawara_leads as a WHERE a.status_jawara = '1'")->result();
    }

    public function getInspectionrendah(){
        return $this->db->select('*')->from('jawara_leads')->where('status_jawara_di', '0')->where('status_ground_di', NULL)->order_by('id', 'DESC')->get()->result();
    }

    public function getDoneground(){
        return $this->db->select('*')->from('jawara_leads')->where('status_ground_di', '1')->order_by('id', 'DESC')->get()->result();
    }

    public function createGround($data, $data2, $id){

        $result = $this->db->where('id', $id)->update('jawara_leads', $data);

        if($result){
            $this->db->where('id', $id)->update('jawara_leads', $data2);
            $this->session->set_flashdata('success','Jawara Score Card/On Ground Telah Berhasil Diinput Ke Sistem');
            return TRUE;
        }else{
            $this->session->set_flashdata('success','Jawara Score Card/On Ground Gagal Diinput Ke Sistem, Silahkan Coba Lagi');
            return FALSE;
        }
    }
}
?>