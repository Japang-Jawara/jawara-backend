<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inspection extends CI_Controller{

     function __construct(){
          parent::__construct();

          $this->load->model('Inspection_model', 'modelInspection');
     }

     function uploadSelfie()
	{
		foreach ($_FILES as $name => $fileInfo) {
			$filename = $_FILES['file_selfie']['name'];
			$tmpname = $_FILES['file_selfie']['tmp_name'];
			$exp = explode('.', $filename);
			$ext = end($exp);
			$newname =  'SELFIE'.date('Ymdhis')."." . $ext;
			$config['upload_path'] = './assets/berkas/selfie/';
			$config['upload_url'] =  base_url() . 'assets/berkas/selfie/';
			$config['allowed_types'] = "png|jpg|jpeg";
			$config['max_size'] = '2000';
			$config['file_name'] = $newname;
			$this->load->library('upload', $config);
			move_uploaded_file($tmpname, "assets/berkas/selfie/" . $newname);
			return $newname;
		}
	}

     function index(){
          if($this->session->userdata('is_login') == TRUE){

               // $x['getDSC'] = $this->modelInspection->getDSC();
               // $x['getSD'] = $this->modelInspection->getSD();
               $x['getBA'] = $this->modelInspection->getBA();

               $this->load->view("include/head");
               $this->load->view("include/top-header");
               $this->load->view('inspection', $x);
               $this->load->view("include/sidebar");
               $this->load->view("include/panel");
               $this->load->view("include/footer");
               $this->load->view("include/alert");
          }else{
               $this->session->set_flashdata('success', 'Upsss!!!, Login dulu ya.');
               redirect('index.php/login');
          }
     }

     function tableInspection() {
          
          $data = $this->modelInspection->getInspection();

          $no = 1;
          $jawara = array();

          foreach($data as $r) {

               $email_verifikasi = $r->email_verifikasi_di;
               $jawara_id = $r->id;
               $nik_ktp = $r->nik_ktp_di;
               $name = $r->name_di;
               $phone = $r->phone_di;
               $cek_npwp = $r->cek_npwp_di;
               $cek_getcontact = $r->cek_getcontact_di;
               $cek_gearth = $r->cek_gearth_di;
               $cek_phone = $r->cek_phone_di;

               $score_getcontact = $r->score_getcontact_di;
               $score_gearth = $r->score_gearth_di;

               $npwp = $r->npwp_di;

               $gf_competitor = $r->gf_competitor_di;
               $gf_kelayakan = $r->gf_kelayakan_di;
               $gf_lokasi = $r->gf_lokasi_di;

               if($r->file_gearth_di != NULL){
                    $file_gearth = '<a href="'.$r->file_gearth_di.'" target="_blank" class="btn btn-sm btn-primary">'.$r->file_gearth_di.'</a>';
               }else{
                    $file_gearth = 'Tidak ada File KTP';
               }

               $gcf_tags = $r->gcf_tags_di;
               $gcf_penilaian = $r->gcf_penilaian_di;
               $gcf_aktif = $r->gcf_aktif_di;
               
               if($r->file_gcf_di != NULL){
                    $file_gcf = '<a href="'.$r->file_gcf_di.'" target="_blank" class="btn btn-sm btn-secondary">'.$r->file_gcf_di.'</a>';
               }else{
                    $file_gcf = 'Tidak ada File KTP Pasangan';
               }

               $notes = $r->notes_di == NULL ? "<font>Catatan Tidak Ada</font>" : $r->notes_di;

               $gmaps = $r->gmaps_di;
               $kyc = $r->kyc_di;
               $score = $r->score_di;
               $gearth = $r->gearth_di;
               $get_contact = $r->get_contact_di;
               $percent_kyc = $r->percent_kyc_di;
               $percent_score = $r->percent_score_di;
               $percent_gearth = $r->percent_gearth_di;
               $percent_get_contact = $r->percent_get_contact_di;
               $total_score = $r->total_score_di;
               $user_grading = $r->user_grading_di;

               $created_at = $r->created_at_di;

               $jawara[] = array(
                    $no++,
                    $email_verifikasi,
                    $jawara_id,
                    $nik_ktp,
                    $name,
                    $phone,
                    $cek_npwp,
                    $cek_getcontact,
                    $cek_gearth,
                    $cek_phone,

                    $score_getcontact,
                    $score_gearth,

                    $npwp,

                    $gf_competitor,
                    $gf_kelayakan,
                    $gf_lokasi,

                    $file_gearth,

                    $gcf_tags,
                    $gcf_penilaian,
                    $gcf_aktif,

                    $file_gcf,

                    $notes,

                    $gmaps,
                    $kyc,
                    $score,
                    $gearth,
                    $get_contact,
                    $percent_kyc,
                    $percent_score,
                    $percent_gearth,
                    $percent_get_contact,
                    $total_score,
                    $user_grading,
                    $created_at
               );
               
          }
          
          echo json_encode($jawara);
     }

     function tableInspectionrendah() {
          
          $data = $this->modelInspection->getInspectionrendah();

          $no = 1;
          $jawara = array();

          foreach($data as $r) {

               $email_verifikasi = $r->email_verifikasi_di;
               $jawara_id = $r->id;
               $nik_ktp = $r->nik_ktp_di;
               $name = $r->name_di;
               $phone = $r->phone_di;
               $cek_npwp = $r->cek_npwp_di;
               $cek_getcontact = $r->cek_getcontact_di;
               $cek_gearth = $r->cek_gearth_di;
               $cek_phone = $r->cek_phone_di;

               $score_getcontact = $r->score_getcontact_di;
               $score_gearth = $r->score_gearth_di;

               $npwp = $r->npwp_di;

               $gf_competitor = $r->gf_competitor_di;
               $gf_kelayakan = $r->gf_kelayakan_di;
               $gf_lokasi = $r->gf_lokasi_di;

               if($r->file_gearth_di != NULL){
                    $file_gearth = '<a href="'.$r->file_gearth_di.'" target="_blank" class="btn btn-sm btn-primary">'.$r->file_gearth_di.'</a>';
               }else{
                    $file_gearth = 'Tidak ada File KTP';
               }

               $gcf_tags = $r->gcf_tags_di;
               $gcf_penilaian = $r->gcf_penilaian_di;
               $gcf_aktif = $r->gcf_aktif_di;
               
               if($r->file_gcf_di != NULL){
                    $file_gcf = '<a href="'.$r->file_gcf_di.'" target="_blank" class="btn btn-sm btn-secondary">'.$r->file_gcf_di.'</a>';
               }else{
                    $file_gcf = 'Tidak ada File KTP Pasangan';
               }

               $notes = $r->notes_di == NULL ? "<font>Catatan Tidak Ada</font>" : $r->notes_di;

               $gmaps = $r->gmaps_di;
               $kyc = $r->kyc_di;
               $score = $r->score_di;
               $gearth = $r->gearth_di;
               $get_contact = $r->get_contact_di;
               $percent_kyc = $r->percent_kyc_di;
               $percent_score = $r->percent_score_di;
               $percent_gearth = $r->percent_gearth_di;
               $percent_get_contact = $r->percent_get_contact_di;
               $total_score = $r->total_score_di;
               $user_grading = $r->user_grading_di;

               $created_at = $r->created_at_di;

               $action = "<a href='javascript:;' 
                    data-id='$r->id'
                    data-name='$r->name_di'
                    data-phone='$r->phone_di'
                    data-nik_ktp='$r->nik_ktp_di'
                    data-toggle='modal' data-target='#onground'
                    class='btn btn-sm btn-success'><i class='fa fas fa-edit'></i> Score Card/On Ground</a>
               ";

               $jawara[] = array(
                    $no++,
                    $action,
                    $email_verifikasi,
                    $jawara_id,
                    $nik_ktp,
                    $name,
                    $phone,
                    $cek_npwp,
                    $cek_getcontact,
                    $cek_gearth,
                    $cek_phone,

                    $score_getcontact,
                    $score_gearth,

                    $npwp,

                    $gf_competitor,
                    $gf_kelayakan,
                    $gf_lokasi,

                    $file_gearth,

                    $gcf_tags,
                    $gcf_penilaian,
                    $gcf_aktif,

                    $file_gcf,

                    $notes,

                    $gmaps,
                    $kyc,
                    $score,
                    $gearth,
                    $get_contact,
                    $percent_kyc,
                    $percent_score,
                    $percent_gearth,
                    $percent_get_contact,
                    $total_score,
                    $user_grading,
                    $created_at
               );
               
          }
          
          echo json_encode($jawara);
     }

     function tableDoneground() {
          
          $data = $this->modelInspection->getDoneground();

          $no = 1;
          $jawara = array();

          foreach($data as $r) {

               $email_verifikasi = $r->email_verifikasi_di;
               $jawara_id = $r->id;
               $nik_ktp = $r->nik_ktp_di;
               $name = $r->name_di;
               $phone = $r->phone_di;
               $cek_npwp = $r->cek_npwp_di;
               $cek_getcontact = $r->cek_getcontact_di;
               $cek_gearth = $r->cek_gearth_di;
               $cek_phone = $r->cek_phone_di;

               $score_getcontact = $r->score_getcontact_di;
               $score_gearth = $r->score_gearth_di;

               $npwp = $r->npwp_di;

               $gf_competitor = $r->gf_competitor_di;
               $gf_kelayakan = $r->gf_kelayakan_di;
               $gf_lokasi = $r->gf_lokasi_di;

               if($r->file_gearth_di != NULL){
                    $file_gearth = '<a href="'.$r->file_gearth_di.'" target="_blank" class="btn btn-sm btn-primary">'.$r->file_gearth_di.'</a>';
               }else{
                    $file_gearth = 'Tidak ada File KTP';
               }

               $gcf_tags = $r->gcf_tags_di;
               $gcf_penilaian = $r->gcf_penilaian_di;
               $gcf_aktif = $r->gcf_aktif_di;
               
               if($r->file_gcf_di != NULL){
                    $file_gcf = '<a href="'.$r->file_gcf_di.'" target="_blank" class="btn btn-sm btn-secondary">'.$r->file_gcf_di.'</a>';
               }else{
                    $file_gcf = 'Tidak ada File KTP Pasangan';
               }

               $notes = $r->notes_di == NULL ? "<font>Catatan Tidak Ada</font>" : $r->notes_di;

               $gmaps = $r->gmaps_di;
               $kyc = $r->kyc_di;
               $score = $r->score_di;
               $gearth = $r->gearth_di;
               $get_contact = $r->get_contact_di;
               $percent_kyc = $r->percent_kyc_di;
               $percent_score = $r->percent_score_di;
               $percent_gearth = $r->percent_gearth_di;
               $percent_get_contact = $r->percent_get_contact_di;
               $total_score = $r->total_score_di;
               $user_grading = $r->user_grading_di;

               $created_at = $r->created_at_di;
               $updated_ground = $r->updated_ground_di;

               $jawara[] = array(
                    $no++,
                    $email_verifikasi,
                    $jawara_id,
                    $nik_ktp,
                    $name,
                    $phone,
                    $cek_npwp,
                    $cek_getcontact,
                    $cek_gearth,
                    $cek_phone,

                    $score_getcontact,
                    $score_gearth,

                    $npwp,

                    $gf_competitor,
                    $gf_kelayakan,
                    $gf_lokasi,

                    $file_gearth,

                    $gcf_tags,
                    $gcf_penilaian,
                    $gcf_aktif,

                    $file_gcf,

                    $notes,

                    $gmaps,
                    $kyc,
                    $score,
                    $gearth,
                    $get_contact,
                    $percent_kyc,
                    $percent_score,
                    $percent_gearth,
                    $percent_get_contact,
                    $total_score,
                    $user_grading,
                    $created_at,
                    $updated_ground
               );
               
          }
          
          echo json_encode($jawara);
     }

     public function createGround(){

          $jawara_id = $this->input->post('id');

          $file_selfie = '';

          $x['email_verifikasi_sc'] = $this->input->post('email');
          $x['nik_ktp_sc'] = $this->input->post('nik_ktp');
          $x['name_sc'] = $this->input->post('name');
          $x['phone_sc'] = $this->input->post('phone');
          $x['shop_name_sc'] = $this->input->post('shop_name');
          $x['shop_location_sc'] = $this->input->post('shop_location');

          $x['sa_pengalaman_sc'] = $this->input->post('sa_pengalaman');
          $x['sa_karyawan_sc'] = $this->input->post('sa_karyawan');
          $x['sa_keluarga_sc'] = $this->input->post('sa_keluarga');
          $x['sa_aktif_sc'] = $this->input->post('sa_aktif');

          $x['ba_jusaha_sc'] = $this->input->post('ba_jusaha');
          $x['ba_musaha_sc'] = $this->input->post('ba_musaha');

          $x['ca_location_sc'] = $this->input->post('ca_location');
          $x['ca_kompetitor_sc'] = $this->input->post('ca_kompetitor');

          $x['frma_kendaraan_sc'] = $this->input->post('frma_kendaraan');
          $x['frma_aset_sc'] = $this->input->post('frma_aset');
          $x['frma_pendapatan_sc'] = $this->input->post('frma_pendapatan');

          $x['notes_sc'] = $this->input->post('notes');

          if (!empty($_FILES['file_selfie']['name'])) {
               $newname = $this->uploadSelfie();
               $data['file_selfie'] = $newname;
               $file_gearth = $newname;

               $x['file_selfie_sc'] = base_url('assets/berkas/selfie/').$file_selfie;
          }

          $ms = '2';

          $x['ts_sa_sc'] = ($x['sa_pengalaman_sc']*$ms) + ($x['sa_karyawan_sc']*$ms) + ($x['sa_keluarga_sc']*$ms) + ($x['sa_aktif_sc']*$ms);
          $x['ts_ba_sc'] = ($x['ba_jusaha_sc']*$ms) + ($x['ba_musaha_sc']*$ms);
          $x['ts_ca_sc'] = ($x['ca_location_sc']*$ms) + ($x['ca_kompetitor_sc']*$ms);
          $x['ts_frma_sc'] = ($x['frma_kendaraan_sc']*$ms) + ($x['frma_aset_sc']*$ms) + ($x['frma_pendapatan_sc']*$ms);

          // RUMUS
          $x['percent_sa_sc'] = round(($x['ts_sa_sc'] / 21 * 100));
          $x['percent_ba_sc'] = round(($x['ts_ba_sc'] / 9 * 100));
          $x['percent_ca_sc'] = round(($x['ts_ca_sc'] / 12 * 100));
          $x['percent_frma_sc'] = round(($x['ts_frma_sc'] / 18 * 100));

          $percent_sa = round(($x['ts_sa_sc'] / 21 * 100))/100;
          $percent_ba = round(($x['ts_ba_sc'] / 9 * 100))/100;
          $percent_ca = round(($x['ts_ca_sc'] / 12 * 100))/100;
          $percent_frma = round(($x['ts_frma_sc'] / 18 * 100))/100;

          
          $sa = '0.2';
          $ba = '0.2';
          $ca = '0.2';
          $frma = '0.4';

          $total = ($percent_sa*$sa) + ($percent_ba*$ba) + ($percent_ca*$ca) + ($percent_frma*$frma);
          $x['total_score_sc'] = round(($total * 100));

          $total_score = round(($total * 100));

          if($total_score >= 86){
               $x['user_grading_sc'] = 'A+';
               $x['status_score_sc'] = '1';
          }else if($total_score >= 71){
               $x['user_grading_sc'] = 'A';
               $x['status_score_sc'] = '1';
          }else if($total_score >= 61){
               $x['user_grading_sc'] = 'B+';
               $x['status_score_sc'] = '1';
          }else if($total_score >= 51){
               $x['user_grading_sc'] = 'B';
               $x['status_score_sc'] = '1';
          }else if($total_score >= 41){
               $x['user_grading_sc'] = 'C';
               $x['status_score_sc'] = '1';
          }else if($total_score >= 31){
               $x['user_grading_sc'] = 'C-';
               $x['status_score_sc'] = '0';
          }else if($total_score >= 16){
               $x['user_grading_sc'] = 'D';
               $x['status_score_sc'] = '0';
          }else{
               $x['user_grading_sc'] = 'E';
               $x['status_score_sc'] = '0';
          }
          
          $x['created_at_sc'] = date('Y-m-d H:i:s');

          //UPDATE DATA FROM TABLE JAWARA LEDS
          $u['status_ground_di'] = '1';
          $u['updated_ground_di'] = date('Y-m-d H:i:s');

          $this->modelInspection->createGround($x, $u, $jawara_id);

          redirect('index.php/inspection');

     }


}