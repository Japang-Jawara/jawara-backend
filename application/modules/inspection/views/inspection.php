<div id="content" class="content">
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="<?php echo base_url('home');?>">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="<?php echo base_url('kepegawaian/thl');?>">DATA DIGITAL INSPECTION</a></li>
    </ol>
    <h1 class="page-header"><b>DATA DIGITAL INSPECTION</b></h1>
    <div class="note note-info note-with-right-icon">
        <div class="note-icon"><i class="fa fa-lightbulb"></i></div>
        <div class="note-content text-right">
            <h2><b>Digital Inspection</b></h2>
            <p>Mengecek kelayakan dan menilai calon jawara dari google earth (lokasi lengkap dan melihat competitor pada daerah jawara) dan get contact (nilai contact dan berapa yang save contact).</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <?php if($this->session->userdata('role') == 'Admin' || $this->session->userdata('role') == 'SuperAdmin' || $this->session->userdata('role') == 'Kepegawaian'){?>
                        <a href="" class="btn btn-icon btn-sm btn-inverse" data-toggle="modal" data-target="#addthl"><i class="fa fa-plus-square"></i></a>
                        <?php } ?>
                    </h4>
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                </div>

                <div class="panel-body">
                    <div class ="table-responsive">
                        <div class="col-xl-12">
                            <ul class="nav nav-tabs nav-tabs-inverse nav-justified nav-justified-mobile" data-sortable-id="index-2">
                                <li class="nav-item">
                                    <a href="#bm" data-toggle="tab" class="nav-link active">
                                        <span><i class="fa fa-check-circle fa-lg m-r-5"></i> NILAI A+ S/D C</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#dt" data-toggle="tab" class="nav-link">
                                        <span><i class="fa fa-list fa-lg m-r-5"></i> NILAI C- S/D E</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#og" data-toggle="tab" class="nav-link">
                                        <span><i class="fa fa-stop fa-lg m-r-5"></i> SCORE CARD/ON GROUND</span>
                                    </a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane fade active show" id="bm">
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table id="tbl-inspection" class="table table-striped table-bordered table-td-valign-middle" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>NO</th>
                                                        <th>EMAIL VERIFIKASI</th>
                                                        <th>JAWARA ID</th>
                                                        <th>NIK KTP</th>
                                                        <th>NAME</th>
                                                        <th>PHONE</th>
                                                        <th>KYC (NPWP STATUS)</th>
                                                        <th>KYC (GET CONTACT STATUS)</th>
                                                        <th>KYC (GEARTH STATUS)</th>
                                                        <th>KYC (PHONE STATUS)</th>
                                                        <th>SCORE (GET CONTACT STATUS)</th>
                                                        <th>SCORE (GEARTH STATUS)</th>
                                                        <th>NO NPWP</th>
                                                        <th>GFINDINGS (COMPETITOR)</th>
                                                        <th>GFINDINGS (KELAYAKAN)</th>
                                                        <th>GFINDINGS (LOKASI)</th>
                                                        <th>FILE GEARTH STORE</th>
                                                        <th>GCF (JUMLAH)</th>
                                                        <th>GCF (PENILAIAN)</th>
                                                        <th>GCF (AKTIF ORGANISASI)</th>
                                                        <th>FILE GET CONTACT FINDING</th>
                                                        <th>NOTES</th>
                                                        <th>GMAPS LONG & LAT</th>
                                                        <th>KYC</th>
                                                        <th>SCORE</th>
                                                        <th>GOOGLE EARTH</th>
                                                        <th>GET CONTACT</th>
                                                        <th>PERCENT KYC</th>
                                                        <th>PERCENT SCORE</th>
                                                        <th>PERCENT GOOGLE EARTH</th>
                                                        <th>PERCENT GET CONTACT</th>
                                                        <th>TOTAL SCORE</th>
                                                        <th>USER GRADING KYC</th>
                                                        <th>TIMESTAMP</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="dt">
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table id="tbl-inspection-rendah" class="table table-striped table-bordered table-td-valign-middle" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>NO</th>
                                                        <th>ACTION</th>
                                                        <th>EMAIL VERIFIKASI</th>
                                                        <th>JAWARA ID</th>
                                                        <th>NIK KTP</th>
                                                        <th>NAME</th>
                                                        <th>PHONE</th>
                                                        <th>KYC (NPWP STATUS)</th>
                                                        <th>KYC (GET CONTACT STATUS)</th>
                                                        <th>KYC (GEARTH STATUS)</th>
                                                        <th>KYC (PHONE STATUS)</th>
                                                        <th>SCORE (GET CONTACT STATUS)</th>
                                                        <th>SCORE (GEARTH STATUS)</th>
                                                        <th>NO NPWP</th>
                                                        <th>GFINDINGS (COMPETITOR)</th>
                                                        <th>GFINDINGS (KELAYAKAN)</th>
                                                        <th>GFINDINGS (LOKASI)</th>
                                                        <th>FILE GEARTH STORE</th>
                                                        <th>GCF (JUMLAH)</th>
                                                        <th>GCF (PENILAIAN)</th>
                                                        <th>GCF (AKTIF ORGANISASI)</th>
                                                        <th>FILE GET CONTACT FINDING</th>
                                                        <th>NOTES</th>
                                                        <th>GMAPS LONG & LAT</th>
                                                        <th>KYC</th>
                                                        <th>SCORE</th>
                                                        <th>GOOGLE EARTH</th>
                                                        <th>GET CONTACT</th>
                                                        <th>PERCENT KYC</th>
                                                        <th>PERCENT SCORE</th>
                                                        <th>PERCENT GOOGLE EARTH</th>
                                                        <th>PERCENT GET CONTACT</th>
                                                        <th>TOTAL SCORE</th>
                                                        <th>USER GRADING</th>
                                                        <th>TIMESTAMP</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="og">
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table id="tbl-done-ground" class="table table-striped table-bordered table-td-valign-middle" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>NO</th>
                                                        <th>EMAIL VERIFIKASI</th>
                                                        <th>JAWARA ID</th>
                                                        <th>NIK KTP</th>
                                                        <th>NAME</th>
                                                        <th>PHONE</th>
                                                        <th>KYC (NPWP STATUS)</th>
                                                        <th>KYC (GET CONTACT STATUS)</th>
                                                        <th>KYC (GEARTH STATUS)</th>
                                                        <th>KYC (PHONE STATUS)</th>
                                                        <th>SCORE (GET CONTACT STATUS)</th>
                                                        <th>SCORE (GEARTH STATUS)</th>
                                                        <th>NO NPWP</th>
                                                        <th>GFINDINGS (COMPETITOR)</th>
                                                        <th>GFINDINGS (KELAYAKAN)</th>
                                                        <th>GFINDINGS (LOKASI)</th>
                                                        <th>FILE GEARTH STORE</th>
                                                        <th>GCF (JUMLAH)</th>
                                                        <th>GCF (PENILAIAN)</th>
                                                        <th>GCF (AKTIF ORGANISASI)</th>
                                                        <th>FILE GET CONTACT FINDING</th>
                                                        <th>NOTES</th>
                                                        <th>GMAPS LONG & LAT</th>
                                                        <th>KYC</th>
                                                        <th>SCORE</th>
                                                        <th>GOOGLE EARTH</th>
                                                        <th>GET CONTACT</th>
                                                        <th>PERCENT KYC</th>
                                                        <th>PERCENT SCORE</th>
                                                        <th>PERCENT GOOGLE EARTH</th>
                                                        <th>PERCENT GET CONTACT</th>
                                                        <th>TOTAL SCORE</th>
                                                        <th>USER GRADING</th>
                                                        <th>TIMESTAMP</th>
                                                        <th>DONE SCORE CARD/ON GROUND</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end panel-body -->
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-10 -->
    </div>

    <div class="modal fade" id="onground" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">SCORE CARD/ON GROUND</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="inspection/createGround" enctype="multipart/form-data" method="POST">
                        
                        <div class="row">
                            <input type="hidden" name="autocomplete" id="field-autocomplete">
                            <input class="form-control" type="hidden" name="id" id="idx" />

                            <div class="form-group col-xl-12">
                                <label class="col-form-label">Email Verifikator <span class="text-danger">*</span></label>
                                <input type="email" class="form-control" name="email" id="email" placeholder="Email Verifikator" autocomplete="off" value="<?= $this->session->userdata('email_address'); ?>" required readonly>
                            </div>

                            <div class="form-group col-xl-12">
                                <label class="col-form-label">No KTP Pemilik <span class="text-danger">*</span></label>
                                <input type="text" pattern="^[0-9]{16,}$" class="form-control" name="nik_ktp" id="nik_ktp" placeholder="No KTP Pemilik" autocomplete="off" required>
                            </div>

                            <div class="form-group col-xl-6">
                                <label class="col-form-label">Nama Pemilik <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="Nama jawara" autocomplete="off" required>
                            </div>

                            <div class="form-group col-xl-6">
                                <label class="col-form-label">No Telp Pemilik <span class="text-danger">*</span></label>
                                <input type="tel" pattern="^628[0-9]{8,}$" class="form-control" name="phone" id="phone" placeholder="628xxxxxxxxx" autocomplete="off" required>
                                <span style="font-size: 12px;">Note: 628xxxxxxxxx (Terdiri dari panjang minimal 10 dan selalu dimulai dengan 628)</span>
                            </div>

                            <div class="form-group col-xl-12">
                                <label class="col-form-label">Nama Toko <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="shop_name" id="shop_name" placeholder="Nama Toko" autocomplete="off" required>
                            </div>

                            <div class="form-group col-xl-12">
                                <label class="col-form-label">Lokasi (Alamat Toko):</label>
                                <textarea class="form-control" name="shop_location" id="shop_location"></textarea>
                            </div>

                            <div class="form-group col-xl-12">
                                <h2>Social Aspects</h2>
                                <br>
                                <h5>Administration Score</h5>
                                <br>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Pengalaman Berdagang <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getBA as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="sa_pengalaman" id="sa_pengalaman" value="<?= $r->status; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                            </div>

                            <div class="form-group col-xl-12">
                                <h5>Behavior and Personality Score</h5>
                                <br>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Jumlah Karyawan <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getBA as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="sa_karyawan" id="sa_karyawan" value="<?= $r->status; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Jumlah Keluarga yang menjadi tanggungan <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getBA as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="sa_keluarga" id="sa_keluarga" value="<?= $r->status; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Aktif dalam kegiatan lingkungan RT/RW/Tempat Ibadah dll <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getBA as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="sa_aktif" id="sa_aktif" value="<?= $r->status; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                            </div>

                            <div class="form-group col-xl-12">
                                <h2>Business Aspects</h2>
                                <p>review of personal capabilities based on business experience</p>
                                <br>
                                <h5>Business Experience</h5>
                                <br>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Jenis Usaha <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getBA as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="ba_jusaha" id="ba_jusaha" value="<?= $r->status; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Modal Usaha diluar JAWARA <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getBA as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="ba_musaha" id="ba_musaha" value="<?= $r->status; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                            </div>

                            <div class="form-group col-xl-12">
                                <h2>Commercial Aspects</h2>
                                <p>Business location and environment that supports business openings</p>
                                <br>
                                <h5>Market Scoring</h5>
                                <br>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Posisi Lokasi <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getBA as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="ca_location" id="ca_location" value="<?= $r->status; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Kompetitor sejenis di area <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getBA as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="ca_kompetitor" id="ca_kompetitor" value="<?= $r->status; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                            </div>

                            <div class="form-group col-xl-12">
                                <h2>Financial and Risk Management Aspects</h2>
                                <br>
                                <h5>Personal Financial Scoring</h5>
                                <br>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Memiliki Kendaraan Pribadi <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getBA as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="frma_kendaraan" id="frma_kendaraan" value="<?= $r->status; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Memiliki Tanah dan Bangunan Pribadi <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getBA as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="frma_aset" id="frma_aset" value="<?= $r->status; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Memiliki Pendapatan lain diluar berdagang (Gaji/investasi) <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getBA as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="frma_pendapatan" id="frma_pendapatan" value="<?= $r->status; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                            </div>

                            <div class="form-group col-xl-12">
                                <!-- begin form-group -->
                                <h5>Selfie Surveyor Japang & Jawara PIC</h5>
                                <br>
                                <div class="form-group row m-b-10">
                                    <label class="col-lg-3 text-lg-right col-form-label">Selfie Surveyor Japang & Jawara PIC</label>
                                    <div class="col-lg-9 col-xl-6">
                                        <input type="file" name="file_selfie" accept="image/png, image/jpg, image/jpeg" class="form-control"/>
                                        <span style="font-size: 12px;">Note : Ukuran Foto max. 2000kb.</span>
                                    </div>
                                </div>
                                <!-- end form-group -->
                            </div>
                            

                            <div class="form-group col-xl-12">
                                <label class="col-form-label">Additional Notes to highlight:</label>
                                <textarea class="form-control" name="notes" id="notes"></textarea>
                            </div>                    
                        </div>
                        
                        </br>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="reset" name="reset" class="btn btn-info"><i class="fa fa-cut"></i> Reset</button>
                            <button type="submit" class="btn btn-primary" value="Cek"><i class="fa fa-save"></i> Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Manage -->
    <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/app-manage.js');?>"></script>

    
