<?php

class Delivery_model extends CI_Model{

    public function getDetail($nomor){
        return $this->db->select('*')->from('delivery_detail')->where('delivery_kode', $nomor)->get()->result();
    }

    public function getData($nomor){
        return $this->db->select('*')->from('delivery')->where('delivery_kode', $nomor)->get()->result()[0];
    }

    public function getDelivery(){
        return $this->db->select('*')->from('delivery')->get()->result();
    }

    public function sqlDelivery($query){
        // var_dump($query);exit;
        return $this->db->query("SELECT *
        FROM delivery
        $query
        ")->result();
    }

    public function getDeliveryDetail(){
        return $this->db->select('*')->from('delivery_detail')->get()->result();
    }

    public function sqlDeliveryDetail($queryde){
        return $this->db->query("SELECT *
        FROM delivery_detail
        $queryde
        ")->result();
    }

}
?>