<?php
defined('BASEPATH') or exit('No direct script access allowed ');

require('./application/libraries/pdf/TCPDF.php');

require('./application/third_party/phpoffice/vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class DeliveryPDF extends TCPDF
{

	public function Header()
	{
		$image_file = "<img src=\"assets/img/logo/logojapang.png\" width=\"60\" height=\"15\"/>";
		$this->SetY(10);
		$isi_header = "
		<table align=\"right\">
			<tr>
				<td>" . $image_file . "</td>
			</tr>
		</table>
		<table align=\"right\">
			<tr>
				<td align=\"center\"><h4>SURAT JALAN</h4></td>
			</tr>
			<tr>
				<td align=\"center\"><p style=\"font-size:10px\">PT. Jaring Pangan Indonesia, Sovereign Plaza 12th Floor, Unit E</p></td>
			</tr>
			<tr>
				<td align=\"center\"><p style=\"font-size:10px\">Jakarta Selatan - 12430</p></td>
			</tr>
		</table>";
		$this->writeHTML($isi_header, true, false, false, false, '');
	}

	public function Footer()
	{
		// $image_file = "<img src=\"assets/pdf/laporanpengaduan.png\" width=\"180\" height=\"70\"/>";
		// $this->SetY(-40);
		// $this->writeHTML($image_file, true, false, false, false, '');
		// $this->SetY(-15);
		// $this->writeHTML("<hr>", true, false, false, false, '');
		// $this->SetFont('helvetica', '', 12);
		// $this->Cell(0, 10, ''.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
	}
}

class Delivery extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		//LOAD MODELS
        $this->load->model('Delivery_model', 'modelDelivery');
	}

    public function index(){

        if($this->session->userdata('is_login') == TRUE){
			$this->load->view("include/head");
			$this->load->view("include/top-header");
			$this->load->view('delivery');
			$this->load->view("include/sidebar");
			$this->load->view("include/panel");
			$this->load->view("include/footer");
			$this->load->view("include/alert");
	   }else{
			$this->session->set_flashdata('success', 'Upsss!!!, Login dulu ya.');
			redirect('index.php/login');
	   }

    }

	public function getDelivery(){
		$data = $this->modelDelivery->getDelivery();

		$no = 1;
		$delivery = array();

		foreach($data as $r) {

			$delivery_kode = $r->delivery_kode;
			$jawara_id = $r->jawara_id;
			$delivery_email = $r->delivery_email;
			$delivery_tanggal = $r->delivery_tanggal;
			$delivery_customer = $r->delivery_customer;
			$delivery_address = $r->delivery_address;
			$delivery_no = $r->delivery_no;
			$total_price = $r->total_price;
			$status = $r->status;

			// $action = '<a href="delivery/surat?nomor='.$r->delivery_kode.'" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-print"></i></a>';

			$delivery[] = array(
				$no++,
				$delivery_kode,
				$jawara_id,
				$delivery_email,
				$delivery_tanggal,
				$delivery_customer,
				$delivery_address,
				$delivery_no,
				$total_price,
				$status
			);
			
		}
          
		echo json_encode($delivery);
	}

	public function surat(){

		$nomor = $this->input->get('nomor');

		$pdf = new DeliveryPDF('P', PDF_UNIT, 'A4', true, 'UTF-8', false);

		$pdf->SetTitle('SURAT JALAN');
		$pdf->SetSubject('SURAT JALAN');

		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$pdf->AddPage();

		$getData = $this->modelDelivery->getData($nomor);

		$html2 = "
			<table>
				<tr>
					<td align=\"right\"><h5>NO : $getData->delivery_kode</h5></td>
				</tr>
				<tr>
					<td align=\"right\"><h5>Tanggal : $getData->delivery_tanggal</h5></td>
				</tr>
			</table>

            <table>	
				<tr>
					<td align=\"left\"><h5>KLIEN</h5></td>
				</tr>
				<tr>
                    <td width=\"25%\"><p style=\"font-size:10px\">Nama</p></td>
					<td width=\"5%\">:</td>
                    <td><p style=\"font-size:10px\">$getData->delivery_customer</p></td>
                </tr>
                <tr>
					<td width=\"25%\"><p style=\"font-size:10px\">Alamat</p></td>
					<td width=\"5%\">:</td>
					<td><p style=\"font-size:10px\">$getData->delivery_address</p></td>
                </tr>
				<tr>
					<td width=\"25%\"><p style=\"font-size:10px\">Telepon</p></td>
					<td width=\"5%\">:</td>
					<td></td>
                </tr>
			</table>
			<br><br>
			<table>
				<tr>
					<td align=\"left\"><h5>DETAIL</h5></td>
				</tr>
				<tr>
                    <td width=\"25%\"><p style=\"font-size:10px\">No. Surat Jalan</p></td>
					<td width=\"5%\">:</td>
                    <td width=\"15%\"><p style=\"font-size:10px\">$getData->delivery_no</p></td>

					<td width=\"12%\"><p style=\"font-size:10px\">No. Polisi</p></td>
					<td width=\"5%\">:</td>
					<td><p style=\"font-size:10px\">$getData->delivery_mobil</p></td>
                </tr>
			</table>
		";

		$html2 .= "
			<p style=\"font-size:10px\">Dikirmkan barang - barang sebagai berikut: </p>
			<table class=\"table\">
				<thead>
					<tr>
						<th width=\"10%\" style=\"font-size:10px\"><b>No</b></th>
						<th style=\"font-size:10px\"><b>Nama Barang</b></th>
						<th width=\"15%\" style=\"font-size:10px\"><b>Qty</b></th>
						<th width=\"100%\" style=\"font-size:10px\"><b>Keterangan</b></th>
					</tr>
				</thead>
				<tbody>
					
				
		";

		$getDetail = $this->modelDelivery->getDetail($nomor);

		$no = 0;
	
		foreach ($getDetail as $row) {
			$no++;
			$harga = "Rp " . number_format($row->price,2,',','.');
			$html2 .= "				
				<tr>
					<th width=\"10%\" style=\"font-size:10px\">$no.</th>
					<th style=\"font-size:10px\">$row->sku</th>
					<th width=\"15%\" style=\"font-size:10px\">$row->jumlah</th>
					<th width=\"100%\" style=\"font-size:10px\">Harga : $harga</th>
				</tr>
			";
		}

		$html2 .= "
			</tbody>
		</table>
		";

		$html2 .= "
			<br><br>
			<table>
				<tr>
					<td align=\"left\"><h5>Pengirim</h5></td>
					<td align=\"left\"><h5>Supir</h5></td>
					<td align=\"left\"><h5>Diterima Oleh</h5></td>
				</tr>
			</table>
		";

		$pdf->writeHTML($html2, true, false, false, false, '');

		$namaPDF = 'LAPORANPERWALIAN.pdf';
		$pdf->Output($namaPDF, 'I');
	}

	function export(){

		$query = '';

		$jumlah = count($this->input->post('field'));

		if($jumlah == '1'){
			$value = $this->input->post('value')[0];
			$query = "WHERE ".$this->input->post('field')[0]." LIKE "."'%$value%'";
		}else{
			for($i=0;$i<$jumlah;$i++){
				$value = $this->input->post('value')[$i];

				if($i == 0){
					$query = "WHERE ".$this->input->post('field')[$i]." LIKE "."'%$value%'";
				}else{
					$query .= " AND ".$this->input->post('field')[$i]." LIKE "."'%$value%'";
				}
			}
		}
		
		$spreadsheet = new Spreadsheet;

		//MASTER SURAT JALAN
		// $surat_jalan = $this->modelDelivery->getDelivery();
		$surat_jalan = $this->modelDelivery->sqlDelivery($query);

		$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A1','no')
			->setCellValue('B1','delivery_kode')
			->setCellValue('C1','jawara_id')
			->setCellValue('D1','delivery_email')
			->setCellValue('E1','delivery_tanggal')
			->setCellValue('F1','delivery_customer')
			->setCellValue('G1','delivery_address')
			->setCellValue('H1','delivery_no')
			->setCellValue('I1','total_price')
			->setCellValue('J1','status');

		$kolom = 2;
		$nomor = 1;

		foreach($surat_jalan as $delivery) {

			$spreadsheet ->setActiveSheetIndex(0)
			->setCellValue('A'. $kolom, $nomor)
			->setCellValue('B'. $kolom, $delivery->delivery_kode)
			->setCellValue('C'. $kolom, $delivery->jawara_id)
			->setCellValue('D'. $kolom, $delivery->delivery_email)
			->setCellValue('E'. $kolom, $delivery->delivery_tanggal)
			->setCellValue('F'. $kolom, $delivery->delivery_customer)
			->setCellValue('G'. $kolom, $delivery->delivery_address)
			->setCellValue('H'. $kolom, $delivery->delivery_no)
			->setCellValue('I'. $kolom, $delivery->total_price)
			->setCellValue('J'. $kolom, $delivery->status);

			$kolom++;
			$nomor++;

		}
		$spreadsheet->getActiveSheet()->setTitle('SJMASTER');

		
		$queryde = '';
		$jumlahde = count($this->input->post('fieldde'));

		if($jumlahde == '1'){
			$valuede = $this->input->post('valuede')[0];
			$queryde = "WHERE ".$this->input->post('fieldde')[0]." LIKE "."'%$valuede%'";
		}else{
			for($i=0;$i<$jumlahde;$i++){
				$valuede = $this->input->post('valuede')[$i];

				if($i == 0){
					$queryde = "WHERE ".$this->input->post('fieldde')[$i]." LIKE "."'%$valuede%'";
				}else{
					$queryde .= " AND ".$this->input->post('fieldde')[$i]." LIKE "."'%$valuede%'";
				}
			}
		}

		//DETAIL SURAT JALAN
		// $detail = $this->modelDelivery->getDeliveryDetail();
		$detail = $this->modelDelivery->sqlDeliveryDetail($queryde);

		$spreadsheet->createSheet();
		$spreadsheet->setActiveSheetIndex(1)
			->setCellValue('A1','no')
			->setCellValue('B1','delivery_kode')
			->setCellValue('C1','sku')
			->setCellValue('D1','jumlah')
			->setCellValue('E1','price')
			->setCellValue('F1','total');

		$kolom2 = 2;
		$nomor2 = 1;

		foreach($detail as $details) {

			$spreadsheet ->setActiveSheetIndex(1)
			->setCellValue('A'. $kolom2, $nomor2)
			->setCellValue('B'. $kolom2, $details->delivery_kode)
			->setCellValue('C'. $kolom2, $details->sku)
			->setCellValue('D'. $kolom2, $details->jumlah)
			->setCellValue('E'. $kolom2, $details->price)
			->setCellValue('F'. $kolom2, $details->total);

			$kolom2++;
			$nomor2++;

		}
		$spreadsheet->getActiveSheet()->setTitle('SJPRODUCT');

		$spreadsheet->setActiveSheetIndex(0);

		$writer = new Xlsx($spreadsheet);

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="DataSuratJalan.xlsx"');
		header('Cache-Control: max-age=0');

		$writer->save('php://output');
	}

	function export_all(){
		
		$spreadsheet = new Spreadsheet;

		//MASTER SURAT JALAN
		$surat_jalan = $this->modelDelivery->getDelivery();

		$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A1','no')
			->setCellValue('B1','delivery_kode')
			->setCellValue('C1','jawara_id')
			->setCellValue('D1','delivery_email')
			->setCellValue('E1','delivery_tanggal')
			->setCellValue('F1','delivery_customer')
			->setCellValue('G1','delivery_address')
			->setCellValue('H1','delivery_no')
			->setCellValue('I1','total_price')
			->setCellValue('J1','status');

		$kolom = 2;
		$nomor = 1;

		foreach($surat_jalan as $delivery) {

			$spreadsheet ->setActiveSheetIndex(0)
			->setCellValue('A'. $kolom, $nomor)
			->setCellValue('B'. $kolom, $delivery->delivery_kode)
			->setCellValue('C'. $kolom, $delivery->jawara_id)
			->setCellValue('D'. $kolom, $delivery->delivery_email)
			->setCellValue('E'. $kolom, $delivery->delivery_tanggal)
			->setCellValue('F'. $kolom, $delivery->delivery_customer)
			->setCellValue('G'. $kolom, $delivery->delivery_address)
			->setCellValue('H'. $kolom, $delivery->delivery_no)
			->setCellValue('I'. $kolom, $delivery->total_price)
			->setCellValue('J'. $kolom, $delivery->status);

			$kolom++;
			$nomor++;

		}
		$spreadsheet->getActiveSheet()->setTitle('SJMASTER');

		//DETAIL SURAT JALAN
		$detail = $this->modelDelivery->getDeliveryDetail();

		$spreadsheet->createSheet();
		$spreadsheet->setActiveSheetIndex(1)
			->setCellValue('A1','no')
			->setCellValue('B1','delivery_kode')
			->setCellValue('C1','sku')
			->setCellValue('D1','jumlah')
			->setCellValue('E1','price')
			->setCellValue('F1','total');

		$kolom2 = 2;
		$nomor2 = 1;

		foreach($detail as $details) {

			$spreadsheet ->setActiveSheetIndex(1)
			->setCellValue('A'. $kolom2, $nomor2)
			->setCellValue('B'. $kolom2, $details->delivery_kode)
			->setCellValue('C'. $kolom2, $details->sku)
			->setCellValue('D'. $kolom2, $details->jumlah)
			->setCellValue('E'. $kolom2, $details->price)
			->setCellValue('F'. $kolom2, $details->total);

			$kolom2++;
			$nomor2++;

		}
		$spreadsheet->getActiveSheet()->setTitle('SJPRODUCT');

		$spreadsheet->setActiveSheetIndex(0);

		$writer = new Xlsx($spreadsheet);

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="DataSuratJalan.xlsx"');
		header('Cache-Control: max-age=0');

		$writer->save('php://output');
	}
}
