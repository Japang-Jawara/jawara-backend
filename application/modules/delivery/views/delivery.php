<div id="content" class="content">
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="<?php echo base_url('home');?>">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="<?php echo base_url('kepegawaian/thl');?>">DATA SURAT JALAN</a></li>
    </ol>
    <h1 class="page-header"><b>DATA SURAT JALAN</b></h1>
    <div class="note note-info note-with-right-icon">
        <div class="note-icon"><i class="fa fa-lightbulb"></i></div>
        <div class="note-content text-right">
            <h2><b>Surat Jalan</b></h2>
            <p>Kumpulan surat jalan.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <div class="row">
                            <div class="col-sm-3">
                                <a href="<?= base_url('index.php/delivery/export_all') ?>" class="btn btn-sm btn-primary">EXPORT ALL DATA</a>
                            </div>
                        </div>
                    </h4>
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                </div>
        
                <div class="panel-body">
                    <!-- <div class="col-sm-6 mb-5">
                        <form action="delivery/export" enctype="multipart/form-data" method="POST">
                           
                            <div class="form-group">
                                <label>QUERY</label>
                                <textarea class="form-control" id="query" name="query" rows="3">SELECT de.*, dede.* 
FROM delivery as de
JOIN delivery_detail as dede ON de.delivery_kode = dede.delivery_kode
                                </textarea>
                            </div>
                                
                            <button type="submit" class="btn btn-primary">EXPORT DATA</button>
                        </form>
                    </div> -->
                    <center>
                    <form action="delivery/export" enctype="multipart/form-data" method="POST">
                        <h1 class="mb-4"><b>FILTER EXCEL SURAT JALAN</b></h1>
                        <div class="col-sm-6">
                            <h5 style="text-align: left">FILTER MASTER</h5>
							<div class="form-group row m-b-10">
								<label class="col-lg-3 col-form-label">FILTER MASTER 1<span class="text-danger"> *</span></label>
								<div class="col-lg-3 col-xl-3">
									<select class="form-control" name="field[]" id="field1" aria-label="Default select example" required>
										<option value="" selected>PILIH FIELD</option>
                                        <option value="delivery_kode">delivery_kode</option>
                                        <option value="status">status</option>
                                        <option value="total_price">total_price</option>
                                        <option value="status_reversal">status_reversal</option>
									</select>
								</div>
								<div class="col-lg-3 col-xl-3">
									<span>
										<input type="text" name="value[]" id="value1" placeholder="VALUE" data-parsley-group="step-1" data-parsley-required="true" class="form-control" autocomplete="off" required/>
									</span>
								</div>
							</div>
							
							<div class="ln_solid"></div>
							<div id="nextkolom" name="nextkolom"></div>
							<button type="button" id="jumlahkolom" value="1" style="display:none"></button>

							<div class="form-group">
								<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="button" class="btn btn-info tambah-form">Tambah Filter Master</button>
								</div>
							</div>
                        </div>

                        <div class="col-sm-6">
                            <h5 style="text-align: left">FILTER PRODUCT</h5>
							<div class="form-group row m-b-10">
								<label class="col-lg-3 col-form-label">FILTER PRODUCT 1<span class="text-danger"> *</span></label>
								<div class="col-lg-3 col-xl-3">
									<select class="form-control" name="fieldde[]" id="fieldde1" aria-label="Default select example" required>
										<option value="" selected>PILIH FIELD</option>
                                        <option value="delivery_kode">delivery_kode</option>
                                        <option value="sku">sku</option>
                                        <option value="jumlah">jumlah</option>
                                        <option value="price">price</option>
									</select>
								</div>
								<div class="col-lg-3 col-xl-3">
									<span>
										<input type="text" name="valuede[]" id="valuede1" placeholder="VALUE" data-parsley-group="step-1" data-parsley-required="true" class="form-control" autocomplete="off" required/>
									</span>
								</div>
							</div>
							
							<div class="ln_solid"></div>
							<div id="nextkolomde" name="nextkolomde"></div>
							<button type="button" id="jumlahkolomde" value="1" style="display:none"></button>

							<div class="form-group">
								<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="button" class="btn btn-info tambah-formde">Tambah Filter Product</button>
								</div>
							</div>
                        </div>


                        <button type="submit" class="btn btn-success mb-4">DOWNLOAD RESULT FILTER</button>
                            
                    </form>
                    </center>

                    <div class="table-responsive">
                        <table id="tbl-delivery" class="table table-striped table-bordered table-td-valign-middle" width="100%">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>KODE DELIVERY</th>
                                    <th>JAWARA ID</th>
                                    <th>EMAIL VERIFIKATOR</th>
                                    <th>TANGGAL DELIVERY</th>
                                    <th>NAMA KLIEN</th>
                                    <th>ALAMAT KLIEN</th>
                                    <th>NOMOR SURAT</th>
                                    <th>TOTAL HARGA</th>
                                    <th>STATUS</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!-- end panel-body -->
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-10 -->
    </div>

    <!-- Manage -->
    <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/app-manage.js');?>"></script>

    <script>
        $(document).ready(function() {
            var i=2;
            $(".tambah-form").on('click', function(){        
                row =
                    '<div class="rec-element">'+
                        '<div class="form-group row m-b-10">'+
                            '<label class="col-lg-3 col-form-label">FILTER MASTER'+i+'<span class="text-danger"> *</span></label>'+
                            '<div class="col-lg-3 col-xl-3">'+
                                '<select class="form-control" name="field[]" id="field'+i+'" aria-label="Default select example" required>'+
                                    '<option value="" selected>PILIH FIELD</option>'+
                                    '<option value="delivery_kode">delivery_kode</option>'+
                                    '<option value="status">status</option>'+
                                    '<option value="total_price">total_price</option>'+
                                    '<option value="status_reversal">status_reversal</option>'+
                                '</select>'+					
                            '</div>'+
                            '<div class="col-lg-3 col-xl-3">'+
                                '<span>'+
                                    '<input type="text" name="value[]" id="value'+i+'" placeholder="VALUE" data-parsley-group="step-1" data-parsley-required="true" class="form-control" autocomplete="off" required/>'+
                                '</span>'+
                            '</div>'+
                            '<div class="col-lg-3 col-xl-3">'+
                                '<span class="input-group-btn">'+
                                    '<button type="button" class="btn btn-warning del-element"><i class="fa fa-minus-square"></i> Hapus</button>'+
                                '</span>'+
                            '</div>'+
                        '</div>'+
                        '<div class="ln_solid"></div>'+
                    
                    '</div>'
                    ;
                $(row).insertBefore("#nextkolom");

                $('#jumlahkolom').val(i+1);

                i++;        
            });

            $(document).on('click','.del-element',function (e) {        
                e.preventDefault()
                i--;
                //$(this).parents('.rec-element').fadeOut(400);
                $(this).parents('.rec-element').remove();
                $('#jumlahkolom').val(i-1);
            });      
        });
        

        // PRODUCT
        $(document).ready(function() {
            var i=2;
            $(".tambah-formde").on('click', function(){        
                row =
                    '<div class="rec-elementde">'+
                        '<div class="form-group row m-b-10">'+
                            '<label class="col-lg-3 col-form-label">FILTER PRODUCT'+i+'<span class="text-danger"> *</span></label>'+
                            '<div class="col-lg-3 col-xl-3">'+
                                '<select class="form-control" name="fieldde[]" id="fieldde'+i+'" aria-label="Default select example" required>'+
                                    '<option value="" selected>PILIH FIELD</option>'+
                                    '<option value="delivery_kode">delivery_kode</option>'+
                                    '<option value="sku">sku</option>'+
                                    '<option value="jumlah">jumlah</option>'+
                                    '<option value="price">price</option>'+
                                '</select>'+					
                            '</div>'+
                            '<div class="col-lg-3 col-xl-3">'+
                                '<span>'+
                                    '<input type="text" name="valuede[]" id="valuede'+i+'" placeholder="VALUE" data-parsley-group="step-1" data-parsley-required="true" class="form-control" autocomplete="off" required/>'+
                                '</span>'+
                            '</div>'+
                            '<div class="col-lg-3 col-xl-3">'+
                                '<span class="input-group-btn">'+
                                    '<button type="button" class="btn btn-warning del-elementde"><i class="fa fa-minus-square"></i> Hapus</button>'+
                                '</span>'+
                            '</div>'+
                        '</div>'+
                        '<div class="ln_solid"></div>'+
                    
                    '</div>'
                    ;
                $(row).insertBefore("#nextkolomde");

                $('#jumlahkolomde').val(i+1);

                i++;        
            });

            $(document).on('click','.del-elementde',function (e) {        
                e.preventDefault()
                i--;
                //$(this).parents('.rec-element').fadeOut(400);
                $(this).parents('.rec-elementde').remove();
                $('#jumlahkolomde').val(i-1);
            });      
        });
    </script>