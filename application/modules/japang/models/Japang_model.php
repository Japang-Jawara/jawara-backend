<?php
class Japang_model extends CI_Model{

    public function leads_japang(){
        return $this->db->query("SELECT * FROM jawara_leads 
            WHERE same_alamat_kyc = 'YES' 
            AND status_lend = 2 
            AND tunggakan_lend <= 5000000
            AND status_eligible_jap IS NULL
        ")->result();
    }

    public function leads_japang_approve(){
        return $this->db->query(" SELECT * FROM jawara_leads 
            WHERE status_eligible_jap = 1
        ")->result();
    }

    public function leads_japang_reject(){
        return $this->db->query(" SELECT * FROM jawara_leads 
            WHERE status_eligible_jap = 2
        ")->result();
    }

    public function leads_japang_reject_fi(){
        return $this->db->query(" SELECT * FROM jawara_leads 
            WHERE status_lend = 2 AND status_eligible_jap IS NULL AND (tunggakan_lend <= 5000000 || tunggakan_lend IS NULL)
        ")->result();
    }

    public function get_limit(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'LIMITJAP')->get()->result()[0]->name;
    }

    function approve_all($jawara_id){
        $limit = $this->get_limit();

        $data = array(
            'status_eligible_jap' => 1,
            'limit_eligible_jap' => $limit,
            'timestamp_eligible_jap'  => date('Y-m-d H:i:s')
        );
        
        $this->db->where('id', $jawara_id);
        return $this->db->update('jawara_leads', $data);
        
	}

    function tolak_all($jawara_id){
		$input_data['status_eligible_jap'] = 2;
        $input_data['limit_eligible_jap'] = 0;
        $input_data['timestamp_eligible_jap'] = date('Y-m-d H:i:s');

		$this->db->where('id', $jawara_id);
		$this->db->update('jawara_leads', $input_data);
	}

    public function getDeploy(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'SDEPLOY')->get()->result();
    }

    public function getOpenDeploy(){
        return $this->db->select('*')
        ->from('jawara_leads')
        ->where('status_eligible_jap', 1)
        ->where('status_depjap', NULL)
        ->get()->result();
    }

    public function getDoneDeploy(){
        return $this->db->select('*')
        ->from('jawara_leads')
        ->where('timestamp_depjap IS NOT NULL')->get()->result();
    }

    public function createDeploy($data, $id){

        $result = $this->db->where('id', $id)->update('jawara_leads', $data);

        if($result){
            $this->session->set_flashdata('success','Deploy Telah Berhasil Diinput Ke Sistem');
            return TRUE;
        }else{
            $this->session->set_flashdata('success','Deploy Gagal Diinput Ke Sistem, Silahkan Coba Lagi');
            return FALSE;
        }
    }

    public function updateDeploy($data, $id){

        $result = $this->db->where('id', $id)->update('jawara_leads', $data);

        if($result){
            $this->session->set_flashdata('success','Update Data Deploy Telah Berhasil Diinput Ke Sistem');
            return TRUE;
        }else{
            $this->session->set_flashdata('success','Uodate Data Deploy Gagal Diinput Ke Sistem, Silahkan Coba Lagi');
            return FALSE;
        }
    }

    public function createActive($id){
        $data = $this->db->select('*')->from('jawara_leads')->where('id', $id)->get()->result()[0];

        $x['jawara_id'] = $id;
        $x['name'] = $data->name;
        $x['shop_name'] = $data->shop_name_dis;
        $x['gmaps'] = $data->gmaps_di;
        $x['is_active'] = 1;

        $this->db->insert('jawara_active', $x);
    }

    public function createActiveUpdate($id){
        $cek = $this->db->select('*')->from('jawara_active')->where('jawara_id', $id)->get()->result();

        if($cek){
            // Kosong
        }else{
            $data = $this->db->select('*')->from('jawara_leads')->where('id', $id)->get()->result()[0];

            $x['jawara_id'] = $id;
            $x['name'] = $data->name;
            $x['shop_name'] = $data->shop_name_dis;
            $x['gmaps'] = $data->gmaps_di;
            $x['is_active'] = 1;

            $this->db->insert('jawara_active', $x);
        }
    }

}
?>