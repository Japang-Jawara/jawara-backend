<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Japang extends CI_Controller{

     function __construct(){
          parent::__construct();

          $this->load->model('Japang_model', 'modelJapang');
     }

     function eligible(){
          if($this->session->userdata('is_login') == TRUE){
               $this->load->view("include/head");
               $this->load->view("include/top-header");
               $this->load->view('index');
               $this->load->view("include/sidebar");
               $this->load->view("include/panel");
               $this->load->view("include/footer");
               $this->load->view("include/alert");
          }else{
               $this->session->set_flashdata('success', 'Upsss!!!, Login dulu ya');
               redirect('index.php/login');
          }
     }

     function deploy(){
          if($this->session->userdata('is_login') == TRUE){
               $x['getDeploy'] = $this->modelJapang->getDeploy();

               $this->load->view("include/head");
               $this->load->view("include/top-header");
               $this->load->view('deploy', $x);
               $this->load->view("include/sidebar");
               $this->load->view("include/panel");
               $this->load->view("include/footer");
               $this->load->view("include/alert");
          }else{
               $this->session->set_flashdata('success', 'Upsss!!!, Login dulu ya');
               redirect('index.php/login');
          }
     }

     function leads_japang(){

          $data = $this->modelJapang->leads_japang();

          $no = 1;
          $jawara = array();

          foreach($data as $r) {

               $input = "<input type='checkbox' class='approve_checkbox' value='$r->id'>";
               $updated_lend = $r->updated_lend;
               $same_alamat_kyc = $r->same_alamat_kyc == NULL ? "<font>Alamat = KTP Tidak Ada</font>" : $r->same_alamat_kyc;
               $name_lend = $r->name_lend;
               $name = $r->name == NULL ? "<font>NAMA Tidak Ada</font>" : $r->name;
               $provinsi = $r->provinsi == NULL ? "<font>PROVINSI Tidak Ada</font>" : $r->provinsi;
               $kab_kota = $r->kab_kota == NULL ? "<font>KAB/KOTA Tidak Ada</font>" : $r->kab_kota;
               $kecamatan = $r->kecamatan == NULL ? "<font>KECAMATAN Tidak Ada</font>" : $r->kecamatan;
               $user_grading_sc = $r->user_grading_sc == NULL ? "<font>User Grading Tidak Ada</font>" : $r->user_grading_sc;
               $status_kol_lend = $r->status_kol_lend == NULL ? "<font>Status KOL Tidak Ada</font>" : $r->status_kol_lend;
               $tunggakan_lend = $r->tunggakan_lend == NULL ? "<font>Tunggakan Tidak Ada</font>" : $r->tunggakan_lend;
               $notes_sc = $r->notes_sc == NULL ? "<font>Notes Tidak Ada</font>" : $r->notes_sc;

               $jawara[] = array(
                    $no++,
                    $input,
                    $updated_lend,
                    $same_alamat_kyc,
                    $name_lend,
                    $name,
                    $provinsi,
                    $kab_kota,
                    $kecamatan,
                    $user_grading_sc,
                    $status_kol_lend,
                    $tunggakan_lend,
                    $notes_sc
               );
               
          }
          
          echo json_encode($jawara);
     }

     function leads_japang_approve(){

          $data = $this->modelJapang->leads_japang_approve();

          $no = 1;
          $jawara = array();

          foreach($data as $r) {

               $timestamp_eligible_jap = $r->timestamp_eligible_jap;
               $same_alamat_kyc = $r->same_alamat_kyc == NULL ? "<font>Alamat = KTP Tidak Ada</font>" : $r->same_alamat_kyc;
               $nik_ktp = $r->nik_ktp == NULL ? "<font>NIK KTP Tidak Ada</font>" : $r->nik_ktp;
               $name = $r->name == NULL ? "<font>NAMA Tidak Ada</font>" : $r->name;
               $limit_eligible_jap = $r->limit_eligible_jap == NULL ? "<font>Limit Belum Ada</font>" : $r->limit_eligible_jap;
               $provinsi = $r->provinsi == NULL ? "<font>PROVINSI Tidak Ada</font>" : $r->provinsi;
               $kab_kota = $r->kab_kota == NULL ? "<font>KAB/KOTA Tidak Ada</font>" : $r->kab_kota;
               $kecamatan = $r->kecamatan == NULL ? "<font>KECAMATAN Tidak Ada</font>" : $r->kecamatan;
               $user_grading_sc = $r->user_grading_sc == NULL ? "<font>User Grading Tidak Ada</font>" : $r->user_grading_sc;
               $status_kol_lend = $r->status_kol_lend == NULL ? "<font>Status KOL Tidak Ada</font>" : $r->status_kol_lend;
               $tunggakan_lend = $r->tunggakan_lend == NULL ? "<font>Tunggakan Tidak Ada</font>" : $r->tunggakan_lend;
               $notes_sc = $r->notes_sc == NULL ? "<font>Notes Tidak Ada</font>" : $r->notes_sc;

               $jawara[] = array(
                    $no++,
                    $timestamp_eligible_jap,
                    $same_alamat_kyc,
                    $nik_ktp,
                    $name,
                    $limit_eligible_jap,
                    $provinsi,
                    $kab_kota,
                    $kecamatan,
                    $user_grading_sc,
                    $status_kol_lend,
                    $tunggakan_lend,
                    $notes_sc
               );
               
          }
          
          echo json_encode($jawara);
     }

     function leads_japang_reject(){

          $data = $this->modelJapang->leads_japang_reject();

          $no = 1;
          $jawara = array();

          foreach($data as $r) {

               $timestamp_eligible_jap = $r->timestamp_eligible_jap;
               $same_alamat_kyc = $r->same_alamat_kyc == NULL ? "<font>Alamat = KTP Tidak Ada</font>" : $r->same_alamat_kyc;
               $nik_ktp = $r->nik_ktp == NULL ? "<font>NIK KTP Tidak Ada</font>" : $r->nik_ktp;
               $name = $r->name == NULL ? "<font>NAMA Tidak Ada</font>" : $r->name;
               $provinsi = $r->provinsi == NULL ? "<font>PROVINSI Tidak Ada</font>" : $r->provinsi;
               $kab_kota = $r->kab_kota == NULL ? "<font>KAB/KOTA Tidak Ada</font>" : $r->kab_kota;
               $kecamatan = $r->kecamatan == NULL ? "<font>KECAMATAN Tidak Ada</font>" : $r->kecamatan;
               $user_grading_sc = $r->user_grading_sc == NULL ? "<font>User Grading Tidak Ada</font>" : $r->user_grading_sc;
               $status_kol_lend = $r->status_kol_lend == NULL ? "<font>Status KOL Tidak Ada</font>" : $r->status_kol_lend;
               $tunggakan_lend = $r->tunggakan_lend == NULL ? "<font>Tunggakan Tidak Ada</font>" : $r->tunggakan_lend;
               $notes_sc = $r->notes_sc == NULL ? "<font>Notes Tidak Ada</font>" : $r->notes_sc;

               $jawara[] = array(
                    $no++,
                    $timestamp_eligible_jap,
                    $same_alamat_kyc,
                    $nik_ktp,
                    $name,
                    $provinsi,
                    $kab_kota,
                    $kecamatan,
                    $user_grading_sc,
                    $status_kol_lend,
                    $tunggakan_lend,
                    $notes_sc
               );
               
          }
          
          echo json_encode($jawara);
     }

     function leads_japang_reject_fi(){

          $data = $this->modelJapang->leads_japang_reject_fi();

          $no = 1;
          $jawara = array();

          foreach($data as $r) {

               $updated_lend = $r->updated_lend;
               $same_alamat_kyc = $r->same_alamat_kyc == NULL ? "<font>Alamat = KTP Tidak Ada</font>" : $r->same_alamat_kyc;
               $name_lend = $r->name_lend;
               $nik_ktp = $r->nik_ktp == NULL ? "<font>NIK KTP Tidak Ada</font>" : $r->nik_ktp;
               $name = $r->name == NULL ? "<font>NAMA Tidak Ada</font>" : $r->name;
               $provinsi = $r->provinsi == NULL ? "<font>PROVINSI Tidak Ada</font>" : $r->provinsi;
               $kab_kota = $r->kab_kota == NULL ? "<font>KAB/KOTA Tidak Ada</font>" : $r->kab_kota;
               $kecamatan = $r->kecamatan == NULL ? "<font>KECAMATAN Tidak Ada</font>" : $r->kecamatan;
               $user_grading_sc = $r->user_grading_sc == NULL ? "<font>User Grading Tidak Ada</font>" : $r->user_grading_sc;
               $status_kol_lend = $r->status_kol_lend == NULL ? "<font>Status KOL Tidak Ada</font>" : $r->status_kol_lend;
               $tunggakan_lend = $r->tunggakan_lend == NULL ? "<font>Tunggakan Tidak Ada</font>" : $r->tunggakan_lend;
               $notes_sc = $r->notes_sc == NULL ? "<font>Notes Tidak Ada</font>" : $r->notes_sc;

               $jawara[] = array(
                    $no++,
                    $updated_lend,
                    $same_alamat_kyc,
                    $name_lend,
                    $nik_ktp,
                    $name,
                    $provinsi,
                    $kab_kota,
                    $kecamatan,
                    $user_grading_sc,
                    $status_kol_lend,
                    $tunggakan_lend,
                    $notes_sc
               );
               
          }
          
          echo json_encode($jawara);
     }

     public function approve_all()
	{
          if ($this->input->post('checkbox_value')) {
               $jawara_id = $this->input->post('checkbox_value');
               
               for ($count = 0; $count < count($jawara_id); $count++) {
                    $this->modelJapang->approve_all($jawara_id[$count]);
               }exit;
          }
	}

	public function tolak_all()
	{
		if ($this->input->post('checkbox_value')) {
			$id = $this->input->post('checkbox_value');
			for ($count = 0; $count < count($id); $count++) {
				$this->modelJapang->tolak_all($id[$count]);
			}
		}
	}

     // OPEN DEPLOY
     function getOpenDeploy() {
          
          $data = $this->modelJapang->getOpenDeploy();

          $no = 1;
          $jawara = array();

          foreach($data as $r) {
               
               $timestamp_eligible_jap = $r->timestamp_eligible_jap;
               $limit_eligible_jap = $r->limit_eligible_jap == NULL ? "<font>Limit Belum Ada</font>" : $r->limit_eligible_jap;
               $same_alamat_kyc = $r->same_alamat_kyc == NULL ? "<font>Alamat = KTP Tidak Ada</font>" : $r->same_alamat_kyc;
               $nik_ktp = $r->nik_ktp == NULL ? "<font>NIK KTP Tidak Ada</font>" : $r->nik_ktp;
               $name = $r->name == NULL ? "<font>NAMA Tidak Ada</font>" : $r->name;
               $phone = $r->phone == NULL ? "<font>PHONE Tidak Ada</font>" : $r->phone;
               $alamat_rumah = $r->alamat_rumah;
               $shop_name_sc = $r->shop_name_sc;
               $shop_location_sc = $r->shop_location_sc;
               $provinsi = $r->provinsi == NULL ? "<font>PROVINSI Tidak Ada</font>" : $r->provinsi;
               $kab_kota = $r->kab_kota == NULL ? "<font>KAB/KOTA Tidak Ada</font>" : $r->kab_kota;
               $kecamatan = $r->kecamatan == NULL ? "<font>KECAMATAN Tidak Ada</font>" : $r->kecamatan;
               $kodepos = $r->kodepos == NULL ? "<font>KODEPOS Tidak Ada</font>" : $r->kodepos;
               
               $action = "<a href='javascript:;' 
                    data-id='$r->id'
                    data-toggle='modal' data-target='#deploy-japang'
                    class='btn btn-sm btn-success'><i class='fa fas fa-edit'></i> Deploy</a>
               ";


               $jawara[] = array(
                    $no++,
                    $action,
                    $timestamp_eligible_jap,
                    $limit_eligible_jap,
                    $same_alamat_kyc,
                    $nik_ktp,
                    $name,
                    $phone,
                    $alamat_rumah,
                    $shop_name_sc,
                    $shop_location_sc,
                    $provinsi,
                    $kab_kota,
                    $kecamatan,
                    $kodepos
               );
               
          }
          
          echo json_encode($jawara);
     }

     // DONE DEPLOY
     function getDoneDeploy() {
          
          $data = $this->modelJapang->getDoneDeploy();

          $no = 1;
          $jawara = array();

          foreach($data as $r) {
               
               $timestamp = $r->timestamp_depjap;
               $limit_eligible_jap = $r->limit_eligible_jap == NULL ? "<font>Limit Belum Ada</font>" : $r->limit_eligible_jap;
               $jawara_id = $r->id;
               $email_verifikasi = $r->email_verifikasi_depjap;
               $date_delivery = $r->date_delivery_depjap;

               if($r->link_surat_depjap != NULL){
                    $link_surat = '<a href="'.$r->link_surat_depjap.'" target="_blank" class="btn btn-sm btn-secondary">'.$r->link_surat_depjap.'</a>';
               }else{
                    $link_surat = 'Tidak ada Surat jalan';
               }

               $status = $r->status_depjap;
               $notes = $r->notes_depjap;

               $nik_ktp = $r->nik_ktp_kyc;
               $name = $r->name_kyc;
               $spanduk = $r->spanduk_depjap == NULL ? "Belum Ada Data" : $r->spanduk_depjap;
               $freezer = $r->freezer_depjap == NULL ? "Belum Ada Data" : $r->freezer_depjap;
               
               $date = date("Y-m-d", strtotime($r->date_delivery_depjap));
               $time = date("H:i", strtotime($r->date_delivery_depjap));

               $action = "<a href='javascript:;' 
                    data-id='$r->id'
                    data-date_delivery_depjap='$date'
                    data-time_delivery_depjap='$time'
                    data-link_surat_depjap='$r->link_surat_depjap'
                    data-status_depjap='$r->status_depjap'
                    data-spanduk_depjap='$r->spanduk_depjap'
                    data-freezer_depjap='$r->freezer_depjap'
                    data-notes_depjap='$r->notes_depjap'
                    data-toggle='modal' data-target='#edit-deploy-japang'
                    class='btn btn-sm btn-success'><i class='fa fas fa-edit'></i> Edit</a>
               ";

               $jawara[] = array(
                    $no++,
                    $action,
                    $timestamp,
                    $limit_eligible_jap,
                    $jawara_id,
                    $email_verifikasi,
                    $nik_ktp,
                    $name,
                    $date_delivery,
                    $link_surat,
                    $status,
                    $spanduk,
                    $freezer,
                    $notes
               );
               
          }
          
          echo json_encode($jawara);
     }

     public function createDeploy(){

          $jawara_id = $this->input->post('id');

          $x['email_verifikasi_depjap'] = $this->input->post('email_verifikasi_depjap');
          $x['date_delivery_depjap'] = $this->input->post('date_delivery_depjap').' '.$this->input->post('time_delivery_depjap');
          $x['link_surat_depjap'] = $this->input->post('link_surat_depjap');
          $x['status_depjap'] = $this->input->post('status_depjap');
          $x['notes_depjap'] = $this->input->post('notes_depjap');
          $x['timestamp_depjap'] = date('Y-m-d H:i:s');

          if($x['status_depjap'] == 'Delivery Success'){
               $this->modelJapang->createActive($jawara_id);
          }

          $x['spanduk_depjap'] = $this->input->post('spanduk_depjap');
          $x['freezer_depjap'] = $this->input->post('freezer_depjap');

          $this->modelJapang->createDeploy($x, $jawara_id);

          redirect('index.php/japang/deploy');

     }

     public function updateDeploy(){

          $jawara_id = $this->input->post('id');

          $x['status_depjap'] = $this->input->post('status_depjap');

          if($x['status_depjap'] == 'Delivery Success'){
               $this->modelJapang->createActiveUpdate($jawara_id);
          }

          $x['date_delivery_depjap'] = $this->input->post('date_delivery_depjap').' '.$this->input->post('time_delivery_depjap');
          $x['link_surat_depjap'] = $this->input->post('link_surat_depjap');

          $x['notes_depjap'] = $this->input->post('notes_depjap');

          $x['spanduk_depjap'] = $this->input->post('spanduk_depjap');
          $x['freezer_depjap'] = $this->input->post('freezer_depjap');

          $this->modelJapang->updateDeploy($x, $jawara_id);

          redirect('index.php/japang/deploy');

     }



}