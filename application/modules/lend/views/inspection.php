<div id="content" class="content">
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="<?php echo base_url('home');?>">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="<?php echo base_url('kepegawaian/thl');?>">FINANCIAL INSTITUTION</a></li>
    </ol>
    <h1 class="page-header"><b>FINANCIAL INSTITUTION CHECK ELIGIBLE</b></h1>
    <div class="note note-info note-with-right-icon">
        <div class="note-icon"><i class="fa fa-lightbulb"></i></div>
        <div class="note-content text-right">
            <h2><b>FINANCIAL INSTITUTION</b></h2>
            <p>Lembaga keuangan yang akan memberikan dukungan modal.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">
                    </h4>
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                </div>

                <div class="panel-body">
                    <div class ="table-responsive">
                        <br>
                        <?php foreach ($lend as $r){ ?>
                            <button name="<?= $r->name ?>" id="<?= $r->name ?>" value="<?= $r->code ?>.<?= $r->ket ?>" class="btn btn-success btn-sm"><?= $r->name ?></button>
                        <?php } ?>
                        <br><br>
                        <table id="tbl-lendinspection" class="table table-striped table-bordered table-td-valign-middle" width="100%">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>
                                        <input type="checkbox" name="select-all" id="select-all" />
                                    </th>
                                    <th>JAWARA ID</th>
                                    <th>ALAMAT=KTP</th>
                                    <th>NAME</th>
                                    <th>PROVINSI</th>
                                    <th>KABUPATEN/KOTA</th>
                                    <th>USER GRADING</th>
                                    <th>NOTES</th>
                                    <th>NIK KTP</th>
                                    <th>NIK KK</th>
                                    <th>PHONE</th>
                                    <th>ALAMAT LENGKAP RUMAH</th>
                                    <th>FILE KTP</th>
                                    <th>FILE KTP PASANGAN</th>
                                    <th>FILE KK</th>
                                    <th>ALAMAT USAHA</th>
                                    <th>KODEPOS</th>
                                    <th>KECAMATAN</th>
                                    <th>FILE USAHA</th>
                                    <th>KODE REFERRAL</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!-- end panel-body -->
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-10 -->
    </div>

    <!-- Manage -->
    <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/app-manage.js');?>"></script>

    <script type="text/javascript">
        $(document).ready(function() {

            $('#select-all').click(function(event) {
                if (this.checked) {
                    // Iterate each checkbox
                    $(':checkbox').each(function() {
                        this.checked = true;
                    });
                } else {
                    $(':checkbox').each(function() {
                        this.checked = false;
                    });
                }
            });

            $('.approve_checkbox').click(function() {
                if($(this).is(':checked')){
                    $('.approve_checkbox').prop('checked', true);
                }else{
                    $('.approve_checkbox').prop('checked', false);
                }
            });

            <?php foreach ($lend as $a){ ?>

            $('#<?php echo $a->name; ?>').click(function() {

                var checkbox_value = [];
                var oTable = $('#tbl-lendinspection').dataTable();
                var rowcollection =  oTable.$(".approve_checkbox:checked", {"page": "all"});
                rowcollection.each(function(index,elem){
                    checkbox_value.push($(elem).val());

                });

                var fired_button = $(this).val();
                var tabel = 'inspection';

                if (checkbox_value.length > 0) {
                    
                    var yakin = confirm("Apakah kamu yakin akan Mengajukan ke P2P ? "+fired_button);

                    if (yakin) {
                        $.ajax({
                            url: "lend/eligible",
                            method: "POST",
                            data: {
                                checkbox_value: checkbox_value,
                                lend_stats: fired_button,
                                tabel: tabel
                            },
                            success: function() {
                                setInterval('location.reload()', 2000);
                            }
                        })
                        Swal.fire({
                            title: 'Informasi!',
                            text: 'Data Berhasil di Teruskan ke P2P '+fired_button,
                        });
                    } else {
                        Swal.fire({
                            title: 'Informasi!',
                            text: 'Data Gagal di Teruskan ke P2P '+fired_button,
                        });
                    }
                } else {
                    alert('Tidak ada data yang ingin di ELIGIBLE, silahkan pilih data terlebih dahulu.');
                }
            });
            
            <?php } ?>
            
        });
    </script>

    
