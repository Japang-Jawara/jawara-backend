<div id="content" class="content">
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="<?php echo base_url('home');?>">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="<?php echo base_url('kepegawaian/thl');?>">FINANCIAL INSTITUTION</a></li>
    </ol>
    <h1 class="page-header"><b>REKAP FINANCIAL INSTITUTION</b></h1>
    <div class="note note-info note-with-right-icon">
        <div class="note-icon"><i class="fa fa-lightbulb"></i></div>
        <div class="note-content text-right">
            <h2><b>FINANCIAL INSTITUTION</b></h2>
            <p>Lembaga keuangan yang akan memberikan dukungan modal.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">
                    </h4>
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                </div>

                <div class="panel-body">
                    <div class ="table-responsive">
                        <table id="tbl-p2p" class="table table-striped table-bordered table-td-valign-middle" width="100%">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>JAWARA ID</th>
                                    <th>NIK KTP</th>
                                    <th>NIK KK</th>
                                    <th>PHONE</th>
                                    <th>NAME</th>
                                    <th>TANGGAL LAHIR</th>
                                    <th>UMUR</th>
                                    <th>ALAMAT LENGKAP RUMAH</th>
                                    <th>FILE KTP</th>
                                    <th>FILE KTP PASANGAN</th>
                                    <th>FILE KK</th>
                                    <th>ALAMAT USAHA</th>
                                    <th>KODEPOS</th>
                                    <th>KECAMATAN</th>
                                    <th>KABUPATEN/KOTA</th>
                                    <th>PROVINSI</th>
                                    <th>FILE USAHA</th>
                                    <th>KODE REFERRAL</th>
                                    <th>P2P LEND</th>
                                    <th>STATUS JAWARA</th>
                                    <th>STATUS KOL</th>
                                    <th>JUMLAH TUNGGAKAN</th>
                                    <th>NOTES APPROVE/REJECT</th>
                                    <th>NOTES P2P</th>
                                    <th>TIMESTAMP</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!-- end panel-body -->
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-10 -->
    </div>

    <!-- Manage -->
    <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/app-manage.js');?>"></script>

    
