<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lend extends CI_Controller{

     function __construct(){
          parent::__construct();

          $this->load->model('Lend_model', 'modelLend');
     }

     function index(){
          if($this->session->userdata('is_login') == TRUE){

               $x['lend'] = $this->modelLend->getLend();

               $this->load->view("include/head");
               $this->load->view("include/top-header");
               $this->load->view('inspection', $x);
               $this->load->view("include/sidebar");
               $this->load->view("include/panel");
               $this->load->view("include/footer");
               $this->load->view("include/alert");
          }else{
               $this->session->set_flashdata('success', 'Upsss!!!, Login dulu ya');
               redirect('index.php/login');
          }
     }

     function p2p(){
          if($this->session->userdata('is_login') == TRUE){
               $this->load->view("include/head");
               $this->load->view("include/top-header");
               $this->load->view('p2p');
               $this->load->view("include/sidebar");
               $this->load->view("include/panel");
               $this->load->view("include/footer");
               $this->load->view("include/alert");
          }else{
               $this->session->set_flashdata('success', 'Upsss!!!, Login dulu ya');
               redirect('index.php/login');
          }
     }

     function getAlldata(){
          $data = $this->modelLend->getAlldata();

          $no = 1;
          $jawara = array();

          foreach($data as $r) {

               $input = "<input type='checkbox' class='approve_checkbox' value='$r->id'>";
               $jawara_id = $r->id;
               $same_alamat = $r->same_alamat_kyc == NULL ? "<font>Belum dicek</font>" : $r->same_alamat_kyc;;
               $nik_ktp = $r->nik_ktp_kyc;
               $nik_kk = $r->nik_kk_kyc == NULL ? "<font>Nik KK Tidak Ada</font>" : $r->nik_kk_kyc;
               $phone = $r->phone_kyc;
               $name = $r->name_kyc;

               $alamat_rumah = $r->alamat_rumah == NULL ? "<font>Alamat Rumah Tidak Ada</font>" : $r->alamat_rumah;

               if($r->file_ktp_kyc != NULL){
                    $file_ktp = '<a href="'.$r->file_ktp_kyc.'" target="_blank" class="btn btn-sm btn-primary">'.$r->file_ktp_kyc.'</a>';
               }else{
                    $file_ktp = 'Tidak ada File KTP';
               }
               
               if($r->file_ktp_pasangan_kyc != NULL){
                    $file_ktp_pasangan = '<a href="'.$r->file_ktp_pasangan_kyc.'" target="_blank" class="btn btn-sm btn-secondary">'.$r->file_ktp_pasangan_kyc.'</a>';
               }else{
                    $file_ktp_pasangan = 'Tidak ada File KTP Pasangan';
               }

               if($r->file_kk_kyc != NULL){
                    $file_kk = '<a href="'.$r->file_kk_kyc.'" target="_blank" class="btn btn-sm btn-success">'.$r->file_kk_kyc.'</a>';
               }else{
                    $file_kk = 'Tidak ada File KK';
               }

               $alamat_usaha = $r->alamat_usaha;
               $kodepos = $r->kodepos == NULL ? "<font>Kode Pos Tidak Ada</font>" : $r->kodepos;
               $kecamatan = $r->kecamatan == NULL ? "<font>Kecamatan Tidak Ada</font>" : $r->kecamatan;
               $kab_kota = $r->kab_kota == NULL ? "<font>Kab/Kota Tidak Ada</font>" : $r->kab_kota;
               $provinsi = $r->provinsi == NULL ? "<font>Provinsi Tidak Ada</font>" : $r->provinsi;

               if($r->file_store_kyc != NULL){
                    $file_store = '<a href="'.$r->file_store_kyc.'" target="_blank" class="btn btn-sm btn-warning">'.$r->file_store_kyc.'</a>';
               }else{
                    $file_store = 'Tidak ada File STORE';
               }
               
               $kode_referral = $r->kode_referral == NULL ? "<font>Kode Referral Tidak Ada</font>" : $r->kode_referral;

               if($r->user_grading_sc == NULL){
                    $user_grading = $r->user_grading_di;
                    $notes = $r->notes_di == NULL ? "<font>Notes Tidak Ada</font>" : $r->notes_di;
               }else{
                    $user_grading = $r->user_grading_sc;
                    $notes = $r->notes_di == NULL ? "<font>Notes Tidak Ada</font>" : $r->notes_sc;
               }
              
               $jawara[] = array(
                    $no++,
                    $input,
                    $jawara_id,
                    $same_alamat,
                    $name,
                    $provinsi,
                    $kab_kota,
                    $user_grading,
                    $notes,
                    $nik_ktp,
                    $nik_kk,
                    $phone,
                    $alamat_rumah,
                    $file_ktp,
                    $file_ktp_pasangan,
                    $file_kk,
                    $alamat_usaha,
                    $kodepos,
                    $kecamatan,
                    $file_store,
                    $kode_referral
               );
               
          }
          
          echo json_encode($jawara);
     }

     public function eligible()
     {
          if ($this->input->post('checkbox_value')) {
               $jawara_id = $this->input->post('checkbox_value');
               $lend_stats = $this->input->post('lend_stats');

               $code_lend = explode('.', $lend_stats)[0];
               $name_lend = explode('.', $lend_stats)[1];
               
               for ($count = 0; $count < count($jawara_id); $count++) {

                    if($code_lend == '1116'){

                         $getData = $this->modelLend->getData($jawara_id[$count]);
                         $cekData = $this->modelLend->cekData($jawara_id[$count]);
                         
                         $insert['jawara_id'] = $jawara_id[$count];
                         $insert['nik_ktp'] = $getData[0]->nik_ktp;
                         $insert['name'] = $getData[0]->name;
                         $insert['alamat_rumah'] = $getData[0]->alamat_rumah;
                         $insert['alamat_usaha'] = $getData[0]->alamat_usaha;

                         if($getData[0]->user_grading_sc == NULL){ 
                              $insert['user_grading'] = $getData[0]->user_grading_di;
                         }else{
                              $insert['user_grading'] = $getData[0]->user_grading_sc;
                         }

                         $insert['area_jawara'] = $getData[0]->area_jawara;
                         $insert['file_ktp'] = $getData[0]->file_ktp;
                         $insert['created_at'] = date('Y-m-d H:i:s');

                         if($cekData == NULL){
                              $this->modelLend->insert($insert);
                         }

                    }else{
                         $add['code_lend'] = $code_lend;
                         $add['name_lend'] = $name_lend;
                         $add['created_at_lend'] = date('Y-m-d H:i:s');
                         $add['status_eligible'] = '1';

                         $this->modelLend->eligible($jawara_id[$count], $add);
                    }
               }
          }
     }

     function getP2p() {
          
          $data = $this->modelLend->getP2p();

          $no = 1;
          $jawara = array();

          foreach($data as $r) {

               $jawara_id = $r->id;
               $nik_ktp = $r->nik_ktp_kyc;
               $nik_kk = $r->nik_kk_kyc == NULL ? "<font>Nik KK Tidak Ada</font>" : $r->nik_kk_kyc;
               $phone = $r->phone_kyc;
               $name = $r->name_kyc;

               if($r->tanggal_lahir == NULL){
                    // Cek Tanggal Lahir
                    $tanggal_lahir = substr($r->nik_ktp_kyc, 6, 2);
                    if (intval($tanggal_lahir) > 40) { 
                         $tanggal_lahir = intval($tanggal_lahir) - 40;
                    } else { 
                         $tanggal_lahir = intval($tanggal_lahir); 
                    }

                    // Cek Bulan Lahir
                    $bulan_lahir = substr($r->nik_ktp_kyc, 8, 2); 

                    // Cek Tahun Lahir
                    $tahun_lahir = substr($r->nik_ktp_kyc, 10, 2);
                    $cek_tahun1 = substr($tahun_lahir, 0, 1);
                    $cek_tahun2 = substr($tahun_lahir, 1, 2);

                    if($cek_tahun1 == 0){
                         if (intval($cek_tahun2) <= 40) { 
                              $tahun_lahir = '20'.$tahun_lahir;
                         } else { 
                              $tahun_lahir = '19'.$tahun_lahir;
                         }
                    }else{
                         if (intval($tahun_lahir) <= 40) { 
                              $tahun_lahir = '20'.$tahun_lahir;
                         } else { 
                              $tahun_lahir = '19'.$tahun_lahir;
                         }
                    }

                    $tanggal_kelahiran = $tahun_lahir.'-'.$bulan_lahir.'-'.$tanggal_lahir;
               }else{
                    $tanggal_kelahiran = $r->tanggal_lahir;
               }

               $umur = date('Y')-$tahun_lahir;

               $alamat_rumah = $r->alamat_rumah == NULL ? "<font>Alamat Rumah Tidak Ada</font>" : $r->alamat_rumah;

               if($r->file_ktp_kyc != NULL){
                    $file_ktp = '<a href="'.$r->file_ktp_kyc.'" target="_blank" class="btn btn-sm btn-primary">'.$r->file_ktp_kyc.'</a>';
               }else{
                    $file_ktp = 'Tidak ada File KTP';
               }
               
               if($r->file_ktp_pasangan_kyc != NULL){
                    $file_ktp_pasangan = '<a href="'.$r->file_ktp_pasangan_kyc.'" target="_blank" class="btn btn-sm btn-secondary">'.$r->file_ktp_pasangan_kyc.'</a>';
               }else{
                    $file_ktp_pasangan = 'Tidak ada File KTP Pasangan';
               }

               if($r->file_kk_kyc != NULL){
                    $file_kk = '<a href="'.$r->file_kk_kyc.'" target="_blank" class="btn btn-sm btn-success">'.$r->file_kk_kyc.'</a>';
               }else{
                    $file_kk = 'Tidak ada File KK';
               }

               $shop_location = $r->alamat_usaha;
               $kodepos = $r->kodepos == NULL ? "<font>Kode Pos Tidak Ada</font>" : $r->kodepos;
               $kecamatan = $r->kecamatan == NULL ? "<font>Kecamatan Tidak Ada</font>" : $r->kecamatan;
               $kab_kota = $r->kab_kota == NULL ? "<font>Kab/Kota Tidak Ada</font>" : $r->kab_kota;
               $provinsi = $r->provinsi == NULL ? "<font>Provinsi Tidak Ada</font>" : $r->provinsi;

               if($r->file_store_kyc != NULL){
                    $file_store = '<a href="'.$r->file_store_kyc.'" target="_blank" class="btn btn-sm btn-warning">'.$r->file_store_kyc.'</a>';
               }else{
                    $file_store = 'Tidak ada File STORE';
               }
               
               $kode_referral = $r->kode_referral == NULL ? "<font>Kode Referral Tidak Ada</font>" : $r->kode_referral;
               
               $name_lend = $r->name_lend;

               if($r->status_lend == NULL){
                    $status_lend = 'Belum Ada Tanggapan dari P2P';
               }else if($r->status_lend == 1){
                    $status_lend = 'Approve';
               }else if($r->status_lend == 2){
                    $status_lend = 'Reject';
               }

               $status_kol = $r->status_kol_lend == NULL ? "<font>Status Kol Belum Ada</font>" : $r->status_kol_lend;
               $tunggakan_lend = $r->tunggakan_lend == NULL ? "<font>Jumlah Tunggakan Belum Ada</font>" : "Rp " . number_format($r->tunggakan_lend,2,',','.');
               $notes_lend = $r->notes_lend == NULL ? "<font>Notes Approve/Reject Belum Ada</font>" : $r->notes_lend;
               $comments_lend = $r->comments_lend == NULL ? "<font>Notes P2P Belum Ada</font>" : $r->comments_lend;
               $updated_lend = $r->updated_lend == NULL ? "<font>Belum Ada Aksi Oleh P2P</font>" : $r->updated_lend;

               $jawara[] = array(
                    $no++,
                    $jawara_id,
                    $nik_ktp,
                    $nik_kk,
                    $phone,
                    $name,
                    $tanggal_kelahiran,
                    $umur,
                    $alamat_rumah,
                    $file_ktp,
                    $file_ktp_pasangan,
                    $file_kk,
                    $shop_location,
                    $kodepos,
                    $kecamatan,
                    $kab_kota,
                    $provinsi,
                    $file_store,
                    $kode_referral,
                    $name_lend,
                    $status_lend,
                    $status_kol,
                    $tunggakan_lend,
                    $notes_lend,
                    $comments_lend,
                    $updated_lend
               );
               
          }
          
          echo json_encode($jawara);
     }

}