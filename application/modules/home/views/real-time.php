<script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>
<script>
$(document).ready(function(){
	setInterval(function(){
		$("#content").load('../welcome')
	}, 10000);
});
</script>
<div id="content" class="content">
	<ol class="breadcrumb float-xl-right">
		<li class="breadcrumb-item"><a href="javascript:;">Dashboard</a></li>
		<li class="breadcrumb-item active">Jawara</li>
	</ol>
	<!-- end breadcrumb -->
	<!-- begin page-header -->
	<h1 class="page-header">JAWARA Acquisition Dashboard</h1>
	<div class="d-sm-flex align-items-center mb-3">
		<a href="#" class="btn btn-inverse mr-2 text-truncate" id="daterange-filter">
			<i class="fa fa-calendar fa-fw text-white-transparent-5 ml-n1"></i> 
			<span><?php echo date("d/m/Y"); ?></span>
		</a>
		<a href="<?= base_url('index.php/home/jawaraTV') ?>" class="btn btn-danger mr-2 text-truncate" id="daterange-filter">
			<i class="fa fa-television  fa-fw text-white-transparent-5 ml-n1"></i> 
			<span>JAWARA SMART TV</span>
		</a>
	</div>

	<div class="row">
		<!-- begin col-8 -->
		<div class="col-xl-6">
			<!-- begin panel -->
			<div class="widget widget-stats bg-teal">
				<div class="stats-icon stats-icon-lg"><i class="fa fa-globe fa-fw"></i></div><br>
				<div class="stats-content">
					<div class="stats-title">Total Jawara Leads</div>
					<div class="stats-number"><?= $totalLeads[0]->totalLeads; ?></div>
					<div class="stats-progress progress">
						<div class="progress-bar" style="width: 100%;"></div>
					</div>
					<div class="stats-desc">+ <?= $leadsHariIni[0]->leadsHariIni; ?> Hari Ini</div><br>
					<div class="d-flex align-items-center">
						<i class="fa fa-circle text-warning f-s-8 mr-2"></i>
						Pending KYC (<?= $notyetKYC ?>)
					</div><br>
				</div>

				<div class="stats-link">
					<a href="<?= base_url('index.php/jawara'); ?>">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
				</div>
			</div>
			<div class="panel panel-inverse" data-sortable-id="index-1">
				<a href="<?= base_url('index.php/home/active') ?>"><div id="maps-dashboardd" class="bg-dark-darker" style="height: 230px;"></div></a>
			</div>
		</div>

		<!-- end col-8 -->
		<div class="col-xl-3">
			<div class="card border-0 bg-dark text-white text-truncate mb-3">
				<!-- begin card-body -->
				<div class="card-body">
					<!-- begin title -->
					<div class="mb-3 text-grey">
						<b class="mb-3">KYC</b> 
						
					</div>
					<!-- end title -->
					<!-- begin conversion-rate -->
					<div class="d-flex align-items-center mb-1">
						<h2 class="text-white mb-0"><span data-animation="number" data-value="<?= $doneKYC[0]->doneKYC ?>"><?= $doneKYC[0]->doneKYC ?></span></h2>
						<div class="ml-auto">
							<div id="conversion-rate-sparkline"></div>
						</div>
					</div>
					<!-- end conversion-rate -->
					<!-- begin percentage -->
					<div class="mb-4 text-grey">
						<i class="fa fa-plus"></i> <span data-animation="number" data-value="<?= $doneKYCHari[0]->doneKYCHari ?>"><?= $doneKYCHari[0]->doneKYCHari ?></span> Hari Ini
					</div>
					<!-- end percentage -->
					<!-- begin info-row -->
					<div class="d-flex mb-2">
						<div class="d-flex align-items-center">
							<i class="fa fa-circle text-red f-s-8 mr-2"></i>
							Pending DI
						</div>
						<div class="d-flex align-items-center ml-auto">
							<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number" data-value="<?= $belumDi ?>"><?= $belumDi ?></span></div>
						</div>
					</div>
					<!-- end info-row -->
					<!-- begin info-row -->
					<div class="d-flex mb-2">
						<div class="d-flex align-items-center">
							<i class="fa fa-circle text-warning f-s-8 mr-2"></i>
							Done DI
						</div>
						<div class="d-flex align-items-center ml-auto">
							<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number" data-value="<?= $doneDi ?>"><?= $doneDi ?></span></div>
						</div>
					</div>
					<!-- end info-row -->
					<!-- begin info-row -->
					<div class="d-flex mb-2">
						<div class="d-flex align-items-center">
							<i class="fa fa-circle text-lime f-s-8 mr-2"></i>
							Reject/Cancel Join
						</div>
						<div class="d-flex align-items-center ml-auto">
							<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number" data-value="<?= $rejectKyc ?>"><?= $rejectKyc ?></span></div>
						</div>
					</div>
					<!-- end info-row -->
					<div class="d-flex">
						<div class="d-flex align-items-center ml-auto">
							<a href="<?= base_url('index.php/kyc'); ?>" class="text-white">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
						</div>
					</div>
				</div>
				<!-- end card-body -->
			</div>

			<div class="card border-0 bg-dark text-white text-truncate mb-3">
				<!-- begin card-body -->
				<div class="card-body">
					<!-- begin title -->
					<div class="mb-3 text-grey">
						<b class="mb-3">SCORE A+ s/d C</b> 
						
					</div>
					<!-- end title -->
					<!-- begin conversion-rate -->
					<div class="d-flex align-items-center mb-1">
						<h2 class="text-white mb-0"><span data-animation="number" data-value="<?= $lolosSurvey ?>"><?= $lolosSurvey ?></span></h2>
						<div class="ml-auto">
							<div id="conversion-rate-sparkline"></div>
						</div>
					</div>
					<!-- end conversion-rate -->
					<!-- begin percentage -->
					<div class="mb-4 text-grey">
						<i class="fa fa-plus"></i> <span data-animation="number" data-value="<?= $lolosSurveyHari ?>"><?= $lolosSurveyHari ?></span> Hari Ini
					</div>
					<!-- end percentage -->
					<!-- begin info-row -->
					<div class="d-flex mb-2">
						<div class="d-flex align-items-center">
							<i class="fa fa-circle text-red f-s-8 mr-2"></i>
							Pending Eligible
						</div>
						<div class="d-flex align-items-center ml-auto">
							<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number" data-value="<?= $eligible ?>"><?= $eligible ?></span></div>
						</div>
					</div>
					<!-- end info-row -->
					<!-- begin info-row -->
					<div class="d-flex mb-2">
						<div class="d-flex align-items-center">
							<i class="fa fa-circle text-blue f-s-8 mr-2"></i>
							Pending FI
						</div>
						<div class="d-flex align-items-center ml-auto">
							<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number" data-value="<?= $pendingFI ?>"><?= $pendingFI ?></span></div>
						</div>
					</div>
					<!-- end info-row -->
					<!-- begin info-row -->
					<div class="d-flex mb-2">
						<div class="d-flex align-items-center">
							<i class="fa fa-circle text-warning f-s-8 mr-2"></i>
							Approve FI
						</div>
						<div class="d-flex align-items-center ml-auto">
							<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number" data-value="<?= $approveFI ?>"><?= $approveFI ?></span></div>
						</div>
					</div>
					<!-- end info-row -->
					<!-- begin info-row -->
					<div class="d-flex mb-2">
						<div class="d-flex align-items-center">
							<i class="fa fa-circle text-lime f-s-8 mr-2"></i>
							Reject FI
						</div>
						<div class="d-flex align-items-center ml-auto">
							<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number" data-value="<?= $rejectFI ?>"><?= $rejectFI ?></span></div>
						</div>
					</div>
					<div class="d-flex">
						<div class="d-flex align-items-center ml-auto">
							<a href="<?= base_url('index.php/lend'); ?>" class="text-white">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
						</div>
					</div>
					<!-- end info-row -->
				</div>
				<!-- end card-body -->
			</div>
		</div>

		<div class="col-xl-3">
			<div class="card border-0 bg-dark text-white text-truncate mb-3">
				<!-- begin card-body -->
				<div class="card-body">
					<!-- begin title -->
					<div class="mb-3 text-grey">
						<b class="mb-3">DISBURSEMENT</b> 
						
					</div>
					<!-- end title -->
					<!-- begin conversion-rate -->
					<div class="d-flex align-items-center mb-1">
						<h2 class="text-white mb-0"><span data-animation="number" data-value="<?= $totalDisbursement?>"><?= $totalDisbursement ?></span></h2>
						<div class="ml-auto">
							<div id="conversion-rate-sparkline"></div>
						</div>
					</div>
					<!-- end conversion-rate -->
					<!-- begin percentage -->
					<div class="mb-4 text-grey">
						<i class="fa fa-plus"></i> <span data-animation="number" data-value="<?= $totalDisbursementHari ?>"><?= $totalDisbursementHari ?></span> Hari Ini
					</div>
					<!-- end percentage -->
					<!-- begin info-row -->
					<div class="d-flex mb-2">
						<div class="d-flex align-items-center">
							<i class="fa fa-circle text-red f-s-8 mr-2"></i>
							Pending Product Req
						</div>
						<div class="d-flex align-items-center ml-auto">
							<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number" data-value="<?= $getDisbursement ?>"><?= $getDisbursement ?></span></div>
						</div>
					</div>
					<!-- end info-row -->
					<!-- begin info-row -->
					<div class="d-flex mb-2">
						<div class="d-flex align-items-center">
							<i class="fa fa-circle text-warning f-s-8 mr-2"></i>
							Done Product Req
						</div>
						<div class="d-flex align-items-center ml-auto">
							<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number" data-value="<?= $getDisbursementDone ?>"><?= $getDisbursementDone ?></span></div>
						</div>
					</div>
					<!-- end info-row -->
					<!-- begin info-row -->
					<div class="d-flex mb-2">
						<div class="d-flex align-items-center">
							<i class="fa fa-circle text-blue f-s-8 mr-2"></i>
							Pending Disburse
						</div>
						<div class="d-flex align-items-center ml-auto">
							<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number" data-value="<?= $getConfirmPending ?>"><?= $getConfirmPending ?></span></div>
						</div>
					</div>
					<!-- end info-row -->
					<!-- begin info-row -->
					<div class="d-flex mb-2">
						<div class="d-flex align-items-center">
							<i class="fa fa-circle text-lime f-s-8 mr-2"></i>
							Done Disburse
						</div>
						<div class="d-flex align-items-center ml-auto">
							<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number" data-value="<?= $getConfirmAccept ?>"><?= $getConfirmAccept ?></span></div>
						</div>
					</div>
					<!-- end info-row -->
					<div class="d-flex">
						<div class="d-flex align-items-center ml-auto">
							<a href="<?= base_url('index.php/disbursement'); ?>" class="text-white">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
						</div>
					</div>
				</div>
				<!-- end card-body -->
			</div>
			<div class="card border-0 bg-dark text-white text-truncate mb-3">
				<!-- begin card-body -->
				<div class="card-body">
					<!-- begin title -->
					<div class="mb-3 text-grey">
						<b class="mb-3">DEPLOY</b> 
						
					</div>
					<!-- end title -->
					<!-- begin conversion-rate -->
					<div class="d-flex align-items-center mb-1">
						<h2 class="text-white mb-0"><span data-animation="number" data-value="<?= $sumDeploy ?>"><?= $sumDeploy ?></span></h2>
						<div class="ml-auto">
							<div id="conversion-rate-sparkline"></div>
						</div>
					</div>
					<!-- end conversion-rate -->
					<!-- begin percentage -->
					<div class="mb-4 text-grey">
						<i class="fa fa-plus"></i> <span data-animation="number" data-value="<?= $totalDeployHari ?>"><?= $totalDeployHari ?></span> Hari Ini
					</div>
					<!-- end percentage -->
					<!-- begin info-row -->
					<div class="d-flex mb-2">
						<div class="d-flex align-items-center">
							<i class="fa fa-circle text-red f-s-8 mr-2"></i>
							Pending Deploy
						</div>
						<div class="d-flex align-items-center ml-auto">
							<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number" data-value="<?= $getOpenDeploy ?>"><?= $getOpenDeploy ?></span></div>
						</div>
					</div>
					<!-- end info-row -->
					<!-- begin info-row -->
					<div class="d-flex mb-2">
						<div class="d-flex align-items-center">
							<i class="fa fa-circle text-warning f-s-8 mr-2"></i>
							Done Deploy
						</div>
						<div class="d-flex align-items-center ml-auto">
							<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number" data-value="<?= $totalDeploy ?>"><?= $totalDeploy ?></span></div>
						</div>
					</div>
					<!-- end info-row -->
					<!-- begin info-row -->
					<div class="d-flex mb-2">
						<div class="d-flex align-items-center">
							<i class="fa fa-circle text-lime f-s-8 mr-2"></i>
							Done Deploy Japang
						</div>
						<div class="d-flex align-items-center ml-auto">
							<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number" data-value="<?= $totalDeployJapang ?>"><?= $totalDeployJapang ?></span></div>
						</div>
					</div>
					<!-- end info-row -->
					<div class="d-flex">
						<div class="d-flex align-items-center ml-auto">
							<a href="<?= base_url('index.php/deploy'); ?>" class="text-white">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
						</div>
					</div>
				</div>
				<!-- end card-body -->
			</div>
		</div>
		<!-- end col-4 -->
	</div>

	<div class="row">
		<!-- begin col-8 -->
		<div class="col-xl-6">
			<div class="widget-chart with-sidebar inverse-mode">
				<div class="widget-chart-content bg-dark">
					<h4 class="chart-title">
						Leads Analytics
						<small>Pendaftar Calon Jawara</small>
					</h4>
					<canvas id="myCharts1" class="d-sm-none"></canvas>
					<canvas id="myChart1" class="d-sm-block d-none"></canvas>
					<!-- <div id="visitors-line-chart" class="widget-chart-full-width nvd3-inverse-mode" style="height: 260px;"></div> -->
				</div>
			</div>
		</div>
		<!-- end col-8 -->
		<!-- begin col-4 -->
		<div class="col-xl-3">
			<div class="card border-0 bg-dark text-white text-truncate mb-3">
				<!-- begin card-body -->
				<div class="card-body">
					<!-- begin title -->
					<div class="mb-3 text-grey">
						<b class="mb-3">DISTRIBUTION</b> 
						
					</div>
					<!-- end title -->

					<!-- begin conversion-rate -->
					<div class="d-flex align-items-center mb-1">
						<h2 class="text-white mb-0"><span data-animation="number" data-value="<?= $delivery->total ?>"><?= $delivery->total ?></span></h2>
						<div class="ml-auto">
							<div id="conversion-rate-sparkline"></div>
						</div>
					</div>
					<!-- end conversion-rate -->

					<!-- begin percentage -->
					<div class="mb-4 text-grey">
						<i class="fa fa-plus"></i> <span data-animation="number" data-value="<?= $delivery->total_hari ?>"><?= $delivery->total_hari ?></span> Hari Ini
					</div>
					<!-- end percentage -->

					<!-- begin info-row -->
					<div class="d-flex mb-2">
						<div class="d-flex align-items-center">
							<i class="fa fa-circle text-red f-s-8 mr-2"></i>
							Beras
						</div>
						<div class="d-flex align-items-center ml-auto">
							<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number" data-value="<?= $delivery->total_beras ?>"><?= $delivery->total_beras ?></span></div>
						</div>
					</div>
					<!-- end info-row -->
					<!-- begin info-row -->
					<div class="d-flex mb-2">
						<div class="d-flex align-items-center">
							<i class="fa fa-circle text-warning f-s-8 mr-2"></i>
							Ayam
						</div>
						<div class="d-flex align-items-center ml-auto">
							<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number" data-value="<?= $delivery->total_ayam ?>"><?= $delivery->total_ayam ?></span></div>
						</div>
					</div>

					<!-- begin info-row -->
					<div class="d-flex mb-2">
						<div class="d-flex align-items-center">
							<i class="fa fa-circle text-red f-s-8 mr-2"></i>
							Telur
						</div>
						<div class="d-flex align-items-center ml-auto">
							<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number" data-value="<?= $delivery->total_telur ?>"><?= $delivery->total_telur ?></span></div>
						</div>
					</div>
					<!-- end info-row -->
					
					<div class="d-flex">
						<div class="d-flex align-items-center ml-auto">
							<a href="<?= base_url('index.php/delivery'); ?>" class="text-white">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
						</div>
					</div>
				</div>
				<!-- end card-body -->
			</div>
		</div>
		<!-- end col-4 -->

		<div class="col-xl-3">
			<div class="card border-0 bg-dark text-white text-truncate mb-3">
				<!-- begin card-body -->
				<div class="card-body">
					<!-- begin title -->
					<div class="mb-3 text-grey">
						<b class="mb-3">JAPANG</b> 
						
					</div>
					<!-- end title -->

					<h4 class="mt-4">Eligible</h4>

					<!-- begin conversion-rate -->
					<div class="d-flex align-items-center mb-1">
						<h2 class="text-white mb-0"><span data-animation="number" data-value="<?= $dataJapang->total_eligible ?>"><?= $dataJapang->total_eligible ?></span></h2>
						<div class="ml-auto">
							<div id="conversion-rate-sparkline"></div>
						</div>
					</div>
					<!-- end conversion-rate -->

					<!-- begin percentage -->
					<div class="mb-4 text-grey">
						<i class="fa fa-plus"></i> <span data-animation="number" data-value="<?= $dataJapang->total_eligible_hari ?>"><?= $dataJapang->total_eligible_hari ?></span> Hari Ini
					</div>
					<!-- end percentage -->

					<!-- begin info-row -->
					<div class="d-flex mb-2">
						<div class="d-flex align-items-center">
							<i class="fa fa-circle text-success f-s-8 mr-2"></i>
							Pending Eligible
						</div>
						<div class="d-flex align-items-center ml-auto">
							<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number" data-value="<?= $dataJapang->pending_eligible ?>"><?= $dataJapang->pending_eligible ?></span></div>
						</div>
					</div>
					<!-- end info-row -->

					<!-- begin info-row -->
					<div class="d-flex mb-2">
						<div class="d-flex align-items-center">
							<i class="fa fa-circle text-success f-s-8 mr-2"></i>
							Approve
						</div>
						<div class="d-flex align-items-center ml-auto">
							<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number" data-value="<?= $dataJapang->approve_eligible ?>"><?= $dataJapang->approve_eligible ?></span></div>
						</div>
					</div>
					<!-- end info-row -->

					<!-- begin info-row -->
					<div class="d-flex mb-2">
						<div class="d-flex align-items-center">
							<i class="fa fa-circle text-success f-s-8 mr-2"></i>
							Reject
						</div>
						<div class="d-flex align-items-center ml-auto">
							<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number" data-value="<?= $dataJapang->reject_eligible ?>"><?= $dataJapang->reject_eligible ?></span></div>
						</div>
					</div>
					<!-- end info-row -->

					<h4 class="mt-4">Deploy</h4>

					<!-- begin conversion-rate -->
					<div class="d-flex align-items-center mb-1">
						<h2 class="text-white mb-0"><span data-animation="number" data-value="<?= $dataJapang->total_deploy ?>"><?= $dataJapang->total_deploy ?></span></h2>
						<div class="ml-auto">
							<div id="conversion-rate-sparkline"></div>
						</div>
					</div>
					<!-- end conversion-rate -->

					<!-- begin percentage -->
					<div class="mb-4 text-grey">
						<i class="fa fa-plus"></i> <span data-animation="number" data-value="<?= $dataJapang->total_deploy_hari ?>"><?= $dataJapang->total_deploy_hari ?></span> Hari Ini
					</div>
					<!-- end percentage -->

					<!-- begin info-row -->
					<div class="d-flex mb-2">
						<div class="d-flex align-items-center">
							<i class="fa fa-circle text-success f-s-8 mr-2"></i>
							Pending Deploy
						</div>
						<div class="d-flex align-items-center ml-auto">
							<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number" data-value="<?= $dataJapang->pending_deploy ?>"><?= $dataJapang->pending_deploy ?></span></div>
						</div>
					</div>
					<!-- end info-row -->

					<!-- begin info-row -->
					<div class="d-flex mb-2">
						<div class="d-flex align-items-center">
							<i class="fa fa-circle text-success f-s-8 mr-2"></i>
							Delivery Success
						</div>
						<div class="d-flex align-items-center ml-auto">
							<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number" data-value="<?= $dataJapang->delivery_success ?>"><?= $dataJapang->delivery_success ?></span></div>
						</div>
					</div>
					<!-- end info-row -->

					<!-- begin info-row -->
					<div class="d-flex mb-2">
						<div class="d-flex align-items-center">
							<i class="fa fa-circle text-success f-s-8 mr-2"></i>
							Cancel Delivery
						</div>
						<div class="d-flex align-items-center ml-auto">
							<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number" data-value="<?= $dataJapang->cancel_delivery ?>"><?= $dataJapang->cancel_delivery ?></span></div>
						</div>
					</div>
					<!-- end info-row -->

					<!-- begin info-row -->
					<div class="d-flex mb-2">
						<div class="d-flex align-items-center">
							<i class="fa fa-circle text-success f-s-8 mr-2"></i>
							Pending
						</div>
						<div class="d-flex align-items-center ml-auto">
							<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number" data-value="<?= $dataJapang->pending ?>"><?= $dataJapang->pending ?></span></div>
						</div>
					</div>
					<!-- end info-row -->

					<!-- begin info-row -->
					<div class="d-flex mb-2">
						<div class="d-flex align-items-center">
							<i class="fa fa-circle text-success f-s-8 mr-2"></i>
							Partial
						</div>
						<div class="d-flex align-items-center ml-auto">
							<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number" data-value="<?= $dataJapang->partial ?>"><?= $dataJapang->partial ?></span></div>
						</div>
					</div>
					<!-- end info-row -->

					<!-- begin info-row -->
					<div class="d-flex mb-2">
						<div class="d-flex align-items-center">
							<i class="fa fa-circle text-success f-s-8 mr-2"></i>
							Return Product
						</div>
						<div class="d-flex align-items-center ml-auto">
							<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number" data-value="<?= $dataJapang->return_product ?>"><?= $dataJapang->return_product ?></span></div>
						</div>
					</div>
					<!-- end info-row -->

					<!-- begin info-row -->
					<div class="d-flex mb-2">
						<div class="d-flex align-items-center">
							<i class="fa fa-circle text-success f-s-8 mr-2"></i>
							On Hold
						</div>
						<div class="d-flex align-items-center ml-auto">
							<div class="width-50 text-right pl-2 f-w-600"><span data-animation="number" data-value="<?= $dataJapang->on_hold ?>"><?= $dataJapang->on_hold ?></span></div>
						</div>
					</div>
					<!-- end info-row -->

					<div class="d-flex">
						<div class="d-flex align-items-center ml-auto">
							<a href="<?= base_url('index.php/deploy'); ?>" class="text-white">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
						</div>
					</div>
				</div>
				<!-- end card-body -->
			</div>
		</div>
	</div>	
</div>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_Y2r-MY7gUT5gLOurnweiCsinrcz1vNQ&callback=initMap&v=weekly" async></script>
<script>

	let map;

	function initMap() {
		mapp = new google.maps.Map(document.getElementById("maps-dashboardd"), {
            zoom: 5,
            center: { lat:  -7.285695813685049, lng: 109.54936137079416 },
        });

        setMarkerss(mapp);
    }

	const actives = [
        <?php 
        foreach($active as $r){
            $data = explode(',',$r->gmaps_di); 
        ?>
            ["Jawara", <?= $data[0] ?>, <?= $data[1] ?>],
        <?php 
        } 
        ?>
    ];

	function setMarkerss(map) {
        // Adds markers to the map.
        // Marker sizes are expressed as a Size of X,Y where the origin of the image
        // (0,0) is located in the top left of the image.
        // Origins, anchor positions and coordinates of the marker increase in the X
        // direction to the right and in the Y direction down.
        const image = {
            // url: "https://developers.google.com/maps/documentation/javascript/examples/full/images/pin.png",
            // This marker is 20 pixels wide by 32 pixels high.
            size: new google.maps.Size(20, 32),
            // The origin for this image is (0, 0).
            origin: new google.maps.Point(0, 0),
            // The anchor for this image is the base of the flagpole at (0, 32).
            anchor: new google.maps.Point(0, 32),
        };
        // Shapes define the clickable region of the icon. The type defines an HTML
        // <area> element 'poly' which traces out a polygon as a series of X,Y points.
        // The final coordinate closes the poly by connecting to the first coordinate.
        const shape = {
            coords: [1, 1, 1, 20, 18, 20, 18, 1],
            type: "poly",
        };

        for (let i = 0; i < actives.length; i++) {
            const active = actives[i];

            new google.maps.Marker({
                position: { lat: active[1], lng: active[2] },
                map,
                icon: image,
                shape: shape,
                title: active[0],
                zIndex: active[3],
            });
        }
    }
</script>

<script src="<?php echo base_url().'assets/js/jquery.min.js'?>"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.0/chart.min.js"></script>

<script src="https://www.chartjs.org/dist/2.9.4/Chart.min.js"></script>

<script>
	var back = ["153, 102, 255", "102, 255, 153", "204, 255, 102", "255, 102, 204"];
	var rand = "";

	<?php $cc = 1; ?>
	<?php foreach ($chart as $x): ?>

	$(document).ready(function() {

		<?php $ch = json_encode($x) ?>

		var labels<?php echo $cc; ?> = <?php echo $ch;?>.map(function(e) {
			return e.tanggal;
		});
		var data1_<?php echo $cc;?> = <?php echo $ch;?>.map(function(e) {
			return e.total;
		});

		var ctxs<?php echo $cc; ?> = document.getElementById("myCharts<?php echo $cc; ?>").getContext('2d');
		var ctx<?php echo $cc; ?> = document.getElementById("myChart<?php echo $cc; ?>").getContext('2d');

		rand = back[Math.floor(Math.random() * back.length)];
		var gradientFills = ctxs<?php echo $cc; ?>.createLinearGradient(0, 0, 0, 290);
		var gradientFill = ctx<?php echo $cc; ?>.createLinearGradient(0, 0, 0, 290);
		gradientFills.addColorStop(0, "rgba(" + rand + ", 1)");
		gradientFills.addColorStop(1, "rgba(" + rand + ", 0.1)");
		gradientFill.addColorStop(0, "rgba(" + rand + ", 1)");
		gradientFill.addColorStop(1, "rgba(" + rand + ", 0.1)");

		var config = {
			type: 'line',
			data: {
				labels: labels<?php echo $cc;?>,
				datasets: [{
					label: 'Total Pendaftar Calon Jawara',
					data: data1_<?php echo $cc;?>,
					pointBackgroundColor: 'white',
					pointBorderWidth: 1,
					backgroundColor: gradientFill,
					borderColor: 'rgba(' + rand + ', 1)',
					lineTension: 0
				}]
			},
			options: {
				scales: {
					yAxes: [{
						ticks: {
							fontColor: "#aaaaaa",
							beginAtZero: true,
							userCallback: function(value, index, values) {
								// Convert the number to a string and splite the string every 3 charaters from the end
								value = value.toString();
								value = value.split(/(?=(?:...)*$)/);
								// Convert the array to a string and format the output
								value = value.join('.');
								return value;
							}
						},
						gridLines: { color: "#444444" }
					}],
					xAxes: [{
						ticks: {
							fontColor: "#aaaaaa",
						},
						gridLines: { color: "#444444" }
					}]
				},
				legend: {
					display: true,
					labels: {
					fontColor: "#aaaaaa"
					}
				},
				responsive: true,
				chartArea: {
					backgroundColor: 'rgba(251, 85, 85, 0.4)'
				}
			}
		};

		var charts<?php echo $cc; ?> = new Chart(ctxs<?php echo $cc; ?>, config);
		var chart<?php echo $cc; ?> = new Chart(ctx<?php echo $cc++; ?>, config);

	});

	<?php endforeach; ?>
</script>

<!-- Score Card -->
<script type="text/javascript">

	var handleVisitorsDonutChart = function() {
		var visitorDonutChartData = [

			<?php foreach ($pendaftar as $x): ?>
				{ 'label': '<?= $x->tanggal ?>', 'value' : <?= $x->total ?>, 'color': COLOR_GREEN },

			<?php endforeach; ?>
		];
		var arcRadius = [
			{ inner: 0.65, outer: 0.93 },
			{ inner: 0.65, outer: 0.93 },
			{ inner: 0.65, outer: 0.93 },
			{ inner: 0.65, outer: 0.93 },
			{ inner: 0.65, outer: 0.93 },
			{ inner: 0.65, outer: 0.93 },
			{ inner: 0.65, outer: 0.93 },
			{ inner: 0.65, outer: 0.93 }
		];

		nv.addGraph(function() {
		var donutChart = nv.models.pieChart()
			.x(function(d) { return d.label })
			.y(function(d) { return d.value })
			.margin({'left': 10,'right':  10,'top': 10,'bottom': 10})
			.showLegend(false)
			.donut(true) 
			.growOnHover(false)
			.arcsRadius(arcRadius)
			.donutRatio(0.5);
			
			donutChart.labelFormat(d3.format(',.0f'));
			
			d3.select('#visitors-donut-chart').append('svg')
				.datum(visitorDonutChartData)
				.transition().duration(3000)
				.call(donutChart);
			
			return donutChart;
		});
	};

	var handleVisitorsVectorMap = function() {
		if ($('#visitorss-map').length !== 0) {
			$('#visitorss-map').vectorMap({
				map: 'world_mill',
				scaleColors: [COLOR_DARK_LIGHTER, COLOR_DARK],
				container: $('#visitorss-map'),
				normalizeFunction: 'linear',
				hoverOpacity: 0.5,
				hoverColor: false,
				zoomOnScroll: false,
				markerStyle: {
					initial: {
						fill: COLOR_DARK,
						stroke: 'transparent',
						r: 3
					}
				},
				regions: [{
					attribute: 'fill'
				}],
				regionStyle: {
					initial: {
						fill: COLOR_DARK_LIGHTER,
						"fill-opacity": 1,
						stroke: 'none',
						"stroke-width": 0.4,
						"stroke-opacity": 1
					},
					hover: {
						"fill-opacity": 0.8
					},
					selected: {
						fill: 'yellow'
					}
				},
				series: {
					regions: [{
						values: {
							ID: COLOR_BLUE
						}
					}]
				},
				// focusOn: {
				// 	// latLng: [47.774099, -52.793427],
				// 	latLng: [-6.200000, 106.816666],
                //     scale: 5
				// },
				latLngToPoint: {
					lat: -6.200000,
					lng: 106.816666
				},
				backgroundColor: 'transparent'
			});
		}
	};

	var DashboardV2 = function () {
		"use strict";
		return {
			//main function
			init: function () {
				handleVisitorsDonutChart();
				handleVisitorsVectorMap();
			}
		};
	}();

	$(document).ready(function() {
		DashboardV2.init();
	});

	setTimeout(function() {
		location.reload();
	}, 1200000);
</script>