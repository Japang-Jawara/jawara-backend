<div id="content" class="content content-full-width content-inverse-mode">

    <div class="map">
        <div id="google-map-japang" class="height-full width-full"></div>
    </div>
</div>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_Y2r-MY7gUT5gLOurnweiCsinrcz1vNQ&callback=initMap&v=weekly" async></script>

<script>
    function initMap() {
        const map = new google.maps.Map(document.getElementById("google-map-japang"), {
            zoom: 10,
            center: { lat:  -6.315953, lng: 106.863018 },
        });

        setMarkers(map);
    } 

    // Data for the markers consisting of a name, a LatLng and a zIndex for the
    // order in which these markers should display on top of each other.
    const beaches = [
        <?php 
        foreach($maps as $r){
            $data = explode(',',$r->gmaps_di); 
        ?>
            ["<?= $r->name ?>", <?= $data[0] ?>, <?= $data[1] ?>],
        <?php 
        } 
        ?>
    ];

    function setMarkers(map) {
        // Adds markers to the map.
        // Marker sizes are expressed as a Size of X,Y where the origin of the image
        // (0,0) is located in the top left of the image.
        // Origins, anchor positions and coordinates of the marker increase in the X
        // direction to the right and in the Y direction down.
        const image = {
            // url: "https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png",
            // This marker is 20 pixels wide by 32 pixels high.
            size: new google.maps.Size(20, 32),
            // The origin for this image is (0, 0).
            origin: new google.maps.Point(0, 0),
            // The anchor for this image is the base of the flagpole at (0, 32).
            anchor: new google.maps.Point(0, 32),
        };
        // Shapes define the clickable region of the icon. The type defines an HTML
        // <area> element 'poly' which traces out a polygon as a series of X,Y points.
        // The final coordinate closes the poly by connecting to the first coordinate.
        const shape = {
            coords: [1, 1, 1, 20, 18, 20, 18, 1],
            type: "poly",
        };

        for (let i = 0; i < beaches.length; i++) {
            const beach = beaches[i];

            new google.maps.Marker({
                position: { lat: beach[1], lng: beach[2] },
                map,
                icon: image,
                shape: shape,
                title: beach[0],
                zIndex: beach[3],
            });
        }
    }
</script>

<!-- <style>
    #map {
        /* height: 100%; */
    }

    html,
    body {
        height: 100%;
        margin: 0;
        padding: 0;
    }
</style> -->


