<?php defined("BASEPATH") OR exit("No direct script access allowed");

class Home extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Home_model', 'modelHome');
    }

    public function index()
    {
        if($this->session->userdata('is_login') == TRUE || $this->session->userdata('is_p2p') == TRUE){

            $x['totalLeads'] = $this->modelHome->totalLeads();
            $x['leadsHariIni'] = $this->modelHome->leadsHariIni();

            //KYC
            $x['doneKYC'] = $this->modelHome->doneKYC();
            $x['doneKYCHari'] = $this->modelHome->doneKYCHari();
            $x['notyetKYC'] = $this->modelHome->notyetKYC();
            $x['belumDi'] = $this->modelHome->belumDi();
            $x['doneDi'] = $this->modelHome->doneDi();
            $x['rejectKyc'] = $this->modelHome->rejectKyc();
            //END KYC

            //SCORE
            $x['lolosSurvey'] = $this->modelHome->lolosSurvey();
            $x['lolosSurveyHari'] = $this->modelHome->lolosSurveyHari();
            $x['eligible'] = $this->modelHome->eligible();
            $x['pendingFI'] = $this->modelHome->pendingFI();
            $x['approveFI'] = $this->modelHome->approveFI();
            $x['rejectFI'] = $this->modelHome->rejectFI();
            //END SCORE

            //DISBURSEMENT
            $x['totalDisbursement'] = $this->modelHome->totalDisbursement()+$this->modelHome->getConfirmAccept();
            $x['totalDisbursementHari'] = $this->modelHome->totalDisbursementHari();
            $x['getDisbursement'] = $this->modelHome->getDisbursement();
            $x['getDisbursementDone'] = $this->modelHome->getDisbursementDone();
            $x['getConfirmPending'] = $this->modelHome->getConfirmPending();
            $x['getConfirmAccept'] = $this->modelHome->getConfirmAccept();
            //END DISBURSEMENT

            //DEPLOY
            $x['totalDeploy'] = $this->modelHome->totalDeploy();
            $x['totalDeployHari'] = $this->modelHome->totalDeployHari();
            $x['getOpenDeploy'] = $this->modelHome->getOpenDeploy();

            $x['totalDeployJapang'] = $this->modelHome->totalDeployJapang();
            $x['sumDeploy'] = $this->modelHome->totalDeploy()+$this->modelHome->totalDeployJapang();
            //END DEPLOY

            //JAPANG
            $x['dataJapang'] = $this->modelHome->dataJapang()[0];
            //END JAPANG

            //DELIVERY
            $x['delivery'] = $this->modelHome->delivery();

            $x['totalperProvinsi'] = $this->modelHome->totalperProvinsi();

            $chart = $this->modelHome->getTotal()->result();

            $chart = json_encode($chart);

            $kat = "";
            $arr = $tmp = array();
            foreach (json_decode($chart, true) as $z):
                array_push($tmp, array(
                    'total'			=>  $z['total'],
                    'tanggal'		=>	$z['tanggal']
                ));

            endforeach;
            array_push($arr, $tmp);

            $x['chart'] = $arr;

            $pendaftar = $this->modelHome->getTotal()->result();
            $x['pendaftar'] = $pendaftar;

            $x['maps'] = $this->modelHome->maps();
            $x['active'] = $this->modelHome->active();

            $this->load->view("include/head");
            $this->load->view("include/top-header");
            $this->load->view('home-new', $x);
            $this->load->view("include/sidebar");
            $this->load->view("include/panel");
            $this->load->view("include/footer");
            $this->load->view("include/alert");
        }else{
            $this->session->set_flashdata('success', 'Upsss!!!, Login dulu ya.');
            redirect('index.php/login');
        }
    }

    public function maps(){
        if($this->session->userdata('is_login') == TRUE || $this->session->userdata('is_p2p') == TRUE){

            $x['maps'] = $this->modelHome->maps();

            $this->load->view("include/head");
            $this->load->view("include/top-header");
            $this->load->view('maps', $x);
            $this->load->view("include/sidebar");
            $this->load->view("include/panel");
            $this->load->view("include/footer");
            $this->load->view("include/alert");
        }else{
            $this->session->set_flashdata('success', 'Upsss!!!, Login dulu ya.');
            redirect('index.php/login');
        }
    }

    public function active(){
        if($this->session->userdata('is_login') == TRUE || $this->session->userdata('is_p2p') == TRUE){

            $x['maps'] = $this->modelHome->active();

            $this->load->view("include/head");
            $this->load->view("include/top-header");
            $this->load->view('active', $x);
            $this->load->view("include/sidebar");
            $this->load->view("include/panel");
            $this->load->view("include/footer");
            $this->load->view("include/alert");
        }else{
            $this->session->set_flashdata('success', 'Upsss!!!, Login dulu ya.');
            redirect('index.php/login');
        }
    }

    public function jawaraTV()
    {
        if($this->session->userdata('is_login') == TRUE || $this->session->userdata('is_p2p') == TRUE){

            $x['totalLeads'] = $this->modelHome->totalLeads();
            $x['leadsHariIni'] = $this->modelHome->leadsHariIni();

            //KYC
            $x['doneKYC'] = $this->modelHome->doneKYC();
            $x['doneKYCHari'] = $this->modelHome->doneKYCHari();
            $x['notyetKYC'] = $this->modelHome->notyetKYC();
            $x['belumDi'] = $this->modelHome->belumDi();
            $x['doneDi'] = $this->modelHome->doneDi();
            $x['rejectKyc'] = $this->modelHome->rejectKyc();
            //END KYC

            //SCORE
            $x['lolosSurvey'] = $this->modelHome->lolosSurvey();
            $x['lolosSurveyHari'] = $this->modelHome->lolosSurveyHari();
            $x['eligible'] = $this->modelHome->eligible();
            $x['pendingFI'] = $this->modelHome->pendingFI();
            $x['approveFI'] = $this->modelHome->approveFI();
            $x['rejectFI'] = $this->modelHome->rejectFI();
            //END SCORE

            //DISBURSEMENT
            $x['totalDisbursement'] = $this->modelHome->totalDisbursement()+$this->modelHome->getConfirmAccept();
            $x['totalDisbursementHari'] = $this->modelHome->totalDisbursementHari();
            $x['getDisbursement'] = $this->modelHome->getDisbursement();
            $x['getDisbursementDone'] = $this->modelHome->getDisbursementDone();
            $x['getConfirmPending'] = $this->modelHome->getConfirmPending();
            $x['getConfirmAccept'] = $this->modelHome->getConfirmAccept();
            //END DISBURSEMENT

            //DEPLOY
            $x['totalDeploy'] = $this->modelHome->totalDeploy();
            $x['totalDeployHari'] = $this->modelHome->totalDeployHari();
            $x['getOpenDeploy'] = $this->modelHome->getOpenDeploy();

            $x['totalDeployJapang'] = $this->modelHome->totalDeployJapang();
            $x['sumDeploy'] = $this->modelHome->totalDeploy()+$this->modelHome->totalDeployJapang();
            //END DEPLOY

            //JAPANG
            $x['dataJapang'] = $this->modelHome->dataJapang()[0];
            //END JAPANG

            //DELIVERY
            $x['delivery'] = $this->modelHome->delivery();

            $x['totalperProvinsi'] = $this->modelHome->totalperProvinsi();

            $chart = $this->modelHome->getTotal()->result();

            $chart = json_encode($chart);

            $kat = "";
            $arr = $tmp = array();
            foreach (json_decode($chart, true) as $z):
                array_push($tmp, array(
                    'total'			=>  $z['total'],
                    'tanggal'		=>	$z['tanggal']
                ));

            endforeach;
            array_push($arr, $tmp);

            $x['chart'] = $arr;

            $pendaftar = $this->modelHome->getTotal()->result();
            $x['pendaftar'] = $pendaftar;

            $x['maps'] = $this->modelHome->maps();
            $x['active'] = $this->modelHome->active();

            $this->load->view("include/head");
            $this->load->view("include/top-header");
            $this->load->view('real-time', $x);
            $this->load->view("include/sidebar");
            $this->load->view("include/panel");
            $this->load->view("include/footer");
            $this->load->view("include/alert");
        }else{
            $this->session->set_flashdata('success', 'Upsss!!!, Login dulu ya.');
            redirect('index.php/login');
        }
    }

    public function getData() {
        $url = 'application\views\home.php';
        $err = file_get_contents("$url");
        // $err = file_get_contents("$url");
        echo preg_replace(array('/(^|\R)ERROR\s*-\s*/', '/(^|\R)(.?)\s-->\s*/'), array('$1', '$1$2 '), $err);
        echo "<br>";
        echo "Content akan load selama 5 detik";

        for ($i = 0; $i <=10; $i++) {
            echo $i. "<br/>";
        }
    }
}
?>
