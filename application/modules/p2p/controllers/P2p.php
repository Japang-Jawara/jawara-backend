<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class P2p extends CI_Controller{

     function __construct(){
          parent::__construct();

          $this->load->model('P2p_model', 'modelP2p');
     }

     function index(){
          if($this->session->userdata('is_p2p') == TRUE){
               $this->load->view("include/head");
               $this->load->view("include/top-header");
               $this->load->view('p2p');
               $this->load->view("include/sidebar");
               $this->load->view("include/panel");
               $this->load->view("include/footer");
               $this->load->view("include/alert");
          }else{
               $this->session->set_flashdata('success', 'Upsss!!!, Anda Tidak Punya Akses Ke P2P');
               redirect('index.php/home');
          }
     }

     function getP2p() {
          
          $code_lend = $this->session->userdata('role');

          $data = $this->modelP2p->getP2p($code_lend);

          $no = 1;
          $jawara = array();

          foreach($data as $r) {

               // $input = "<input type='checkbox' class='approve_checkbox' value='$r->id'>";
               $nik_ktp = $r->nik_ktp_kyc;
               $nik_kk = $r->nik_kk_kyc == NULL ? "<font>Nik KK Tidak Ada</font>" : $r->nik_kk_kyc;
               $phone = $r->phone_kyc;
               $name = $r->name_kyc;

               $alamat_rumah = $r->alamat_rumah == NULL ? "<font>Alamat Rumah Tidak Ada</font>" : $r->alamat_rumah;

               if($r->file_ktp_kyc != NULL){
                    $file_ktp = '<a href="'.$r->file_ktp_kyc.'" target="_blank" class="btn btn-sm btn-primary">'.$r->file_ktp_kyc.'</a>';
               }else{
                    $file_ktp = 'Tidak ada File KTP';
               }
               
               if($r->file_ktp_pasangan_kyc != NULL){
                    $file_ktp_pasangan = '<a href="'.$r->file_ktp_pasangan_kyc.'" target="_blank" class="btn btn-sm btn-secondary">'.$r->file_ktp_pasangan_kyc.'</a>';
               }else{
                    $file_ktp_pasangan = 'Tidak ada File KTP Pasangan';
               }

               if($r->file_kk_kyc != NULL){
                    $file_kk = '<a href="'.$r->file_kk_kyc.'" target="_blank" class="btn btn-sm btn-success">'.$r->file_kk_kyc.'</a>';
               }else{
                    $file_kk = 'Tidak ada File KK';
               }

               $alamat_usaha = $r->alamat_usaha;
               $kodepos = $r->kodepos == NULL ? "<font>Kode Pos Tidak Ada</font>" : $r->kodepos;
               $kecamatan = $r->kecamatan == NULL ? "<font>Kecamatan Tidak Ada</font>" : $r->kecamatan;
               $kab_kota = $r->kab_kota == NULL ? "<font>Kab/Kota Tidak Ada</font>" : $r->kab_kota;
               $provinsi = $r->provinsi == NULL ? "<font>Provinsi Tidak Ada</font>" : $r->provinsi;

               if($r->file_store_kyc != NULL){
                    $file_store = '<a href="'.$r->file_store_kyc.'" target="_blank" class="btn btn-sm btn-warning">'.$r->file_store_kyc.'</a>';
               }else{
                    $file_store = 'Tidak ada File STORE';
               }
               
               $kode_referral = $r->kode_referral == NULL ? "<font>Kode Referral Tidak Ada</font>" : $r->kode_referral;

               if($r->user_grading_sc == NULL){
                    $user_grading = $r->user_grading_di;
               }else{
                    $user_grading = $r->user_grading_sc;
               }
               $name_lend = $r->name_lend;

               if($r->tanggal_lahir == NULL){
                    // Cek Tanggal Lahir
                    $tanggal_lahir = substr($r->nik_ktp_kyc, 6, 2);
                    if (intval($tanggal_lahir) > 40) { 
                         $tanggal_lahir = intval($tanggal_lahir) - 40;
                    } else { 
                         $tanggal_lahir = intval($tanggal_lahir); 
                    }

                    // Cek Bulan Lahir
                    $bulan_lahir = substr($r->nik_ktp_kyc, 8, 2); 

                    // Cek Tahun Lahir
                    $tahun_lahir = substr($r->nik_ktp_kyc, 10, 2);
                    $cek_tahun1 = substr($tahun_lahir, 0, 1);
                    $cek_tahun2 = substr($tahun_lahir, 1, 2);

                    if($cek_tahun1 == 0){
                         if (intval($cek_tahun2) <= 40) { 
                              $tahun_lahir = '20'.$tahun_lahir;
                         } else { 
                              $tahun_lahir = '19'.$tahun_lahir;
                         }
                    }else{
                         if (intval($tahun_lahir) <= 40) { 
                              $tahun_lahir = '20'.$tahun_lahir;
                         } else { 
                              $tahun_lahir = '19'.$tahun_lahir;
                         }
                    }

                    $tanggal_kelahiran = $tahun_lahir.'-'.$bulan_lahir.'-'.$tanggal_lahir;
               }else{
                    $tanggal_kelahiran = $r->tanggal_lahir;
               }

               $action = "<a href='javascript:;' 
               data-id='$r->id'
               data-toggle='modal' data-target='#ubahstatus'
               class='btn btn-sm btn-success'><i class='fa fas fa-edit'></i> Approve/Reject</a>
               
               <a href='javascript:;' 
               data-id='$r->id'
               data-toggle='modal' data-target='#comments'
               class='btn btn-sm btn-primary'><i class='fa fas fa-comment'></i> Comment</a>";

               $jawara[] = array(
                    $no++,
                    $action,
                    // $input,
                    $nik_ktp,
                    $nik_kk,
                    $phone,
                    $name,
                    $tanggal_kelahiran,
                    $alamat_rumah,
                    $file_ktp,
                    $file_ktp_pasangan,
                    $file_kk,
                    $alamat_usaha,
                    $kodepos,
                    $kecamatan,
                    $kab_kota,
                    $provinsi,
                    $file_store,
                    $kode_referral,
                    $user_grading,
                    $name_lend
               );
               
          }
          
          echo json_encode($jawara);
     }

     function getP2pApprove() {
          
          $code_lend = $this->session->userdata('role');

          $data = $this->modelP2p->getP2pApprove($code_lend);

          $no = 1;
          $jawara = array();

          foreach($data as $r) {

               $nik_ktp = $r->nik_ktp_kyc;
               $nik_kk = $r->nik_kk_kyc == NULL ? "<font>Nik KK Tidak Ada</font>" : $r->nik_kk_kyc;
               $phone = $r->phone_kyc;
               $name = $r->name_kyc;

               $alamat_rumah = $r->alamat_rumah == NULL ? "<font>Alamat Rumah Tidak Ada</font>" : $r->alamat_rumah;

               if($r->file_ktp_kyc != NULL){
                    $file_ktp = '<a href="'.$r->file_ktp_kyc.'" target="_blank" class="btn btn-sm btn-primary">'.$r->file_ktp_kyc.'</a>';
               }else{
                    $file_ktp = 'Tidak ada File KTP';
               }
               
               if($r->file_ktp_pasangan_kyc != NULL){
                    $file_ktp_pasangan = '<a href="'.$r->file_ktp_pasangan_kyc.'" target="_blank" class="btn btn-sm btn-secondary">'.$r->file_ktp_pasangan_kyc.'</a>';
               }else{
                    $file_ktp_pasangan = 'Tidak ada File KTP Pasangan';
               }

               if($r->file_kk_kyc != NULL){
                    $file_kk = '<a href="'.$r->file_kk_kyc.'" target="_blank" class="btn btn-sm btn-success">'.$r->file_kk_kyc.'</a>';
               }else{
                    $file_kk = 'Tidak ada File KK';
               }

               $alamat_usaha = $r->alamat_usaha;
               $kodepos = $r->kodepos == NULL ? "<font>Kode Pos Tidak Ada</font>" : $r->kodepos;
               $kecamatan = $r->kecamatan == NULL ? "<font>Kecamatan Tidak Ada</font>" : $r->kecamatan;
               $kab_kota = $r->kab_kota == NULL ? "<font>Kab/Kota Tidak Ada</font>" : $r->kab_kota;
               $provinsi = $r->provinsi == NULL ? "<font>Provinsi Tidak Ada</font>" : $r->provinsi;

               if($r->file_store_kyc != NULL){
                    $file_store = '<a href="'.$r->file_store_kyc.'" target="_blank" class="btn btn-sm btn-warning">'.$r->file_store_kyc.'</a>';
               }else{
                    $file_store = 'Tidak ada File STORE';
               }
               
               $kode_referral = $r->kode_referral == NULL ? "<font>Kode Referral Tidak Ada</font>" : $r->kode_referral;

               if($r->user_grading_sc == NULL){
                    $user_grading = $r->user_grading_di;
               }else{
                    $user_grading = $r->user_grading_sc;
               }
               $name_lend = $r->name_lend;

               if($r->sign_pk_dis != NULL){
                    $sign_pk = '<a href="'.$r->sign_pk_dis.'" target="_blank" class="btn btn-sm btn-success">'.$r->sign_pk_dis.'</a>';
               }else{
                    $sign_pk = 'Tidak ada File sign PK';
               }
     

               $jawara[] = array(
                    $no++,
                    $nik_ktp,
                    $nik_kk,
                    $phone,
                    $name,
                    $alamat_rumah,
                    $file_ktp,
                    $file_ktp_pasangan,
                    $file_kk,
                    $alamat_usaha,
                    $kodepos,
                    $kecamatan,
                    $kab_kota,
                    $provinsi,
                    $file_store,
                    $kode_referral,
                    $user_grading,
                    $name_lend,
                    $sign_pk
               );
               
          }
          
          echo json_encode($jawara);
     }

     function getP2pReject() {
          
          $code_lend = $this->session->userdata('role');

          $data = $this->modelP2p->getP2pReject($code_lend);

          $no = 1;
          $jawara = array();

          foreach($data as $r) {

               $nik_ktp = $r->nik_ktp_kyc;
               $nik_kk = $r->nik_kk_kyc == NULL ? "<font>Nik KK Tidak Ada</font>" : $r->nik_kk_kyc;
               $phone = $r->phone_kyc;
               $name = $r->name_kyc;

               $alamat_rumah = $r->alamat_rumah == NULL ? "<font>Alamat Rumah Tidak Ada</font>" : $r->alamat_rumah;

               if($r->file_ktp_kyc != NULL){
                    $file_ktp = '<a href="'.$r->file_ktp_kyc.'" target="_blank" class="btn btn-sm btn-primary">'.$r->file_ktp_kyc.'</a>';
               }else{
                    $file_ktp = 'Tidak ada File KTP';
               }
               
               if($r->file_ktp_pasangan_kyc != NULL){
                    $file_ktp_pasangan = '<a href="'.$r->file_ktp_pasangan_kyc.'" target="_blank" class="btn btn-sm btn-secondary">'.$r->file_ktp_pasangan_kyc.'</a>';
               }else{
                    $file_ktp_pasangan = 'Tidak ada File KTP Pasangan';
               }

               if($r->file_kk_kyc != NULL){
                    $file_kk = '<a href="'.$r->file_kk_kyc.'" target="_blank" class="btn btn-sm btn-success">'.$r->file_kk_kyc.'</a>';
               }else{
                    $file_kk = 'Tidak ada File KK';
               }

               $alamat_usaha = $r->alamat_usaha;
               $kodepos = $r->kodepos == NULL ? "<font>Kode Pos Tidak Ada</font>" : $r->kodepos;
               $kecamatan = $r->kecamatan == NULL ? "<font>Kecamatan Tidak Ada</font>" : $r->kecamatan;
               $kab_kota = $r->kab_kota == NULL ? "<font>Kab/Kota Tidak Ada</font>" : $r->kab_kota;
               $provinsi = $r->provinsi == NULL ? "<font>Provinsi Tidak Ada</font>" : $r->provinsi;

               if($r->file_store_kyc != NULL){
                    $file_store = '<a href="'.$r->file_store_kyc.'" target="_blank" class="btn btn-sm btn-warning">'.$r->file_store_kyc.'</a>';
               }else{
                    $file_store = 'Tidak ada File STORE';
               }
               
               $kode_referral = $r->kode_referral == NULL ? "<font>Kode Referral Tidak Ada</font>" : $r->kode_referral;

               if($r->user_grading_sc == NULL){
                    $user_grading = $r->user_grading_di;
               }else{
                    $user_grading = $r->user_grading_sc;
               }
               $name_lend = $r->name_lend;

               $jawara[] = array(
                    $no++,
                    $nik_ktp,
                    $nik_kk,
                    $phone,
                    $name,
                    $alamat_rumah,
                    $file_ktp,
                    $file_ktp_pasangan,
                    $file_kk,
                    $alamat_usaha,
                    $kodepos,
                    $kecamatan,
                    $kab_kota,
                    $provinsi,
                    $file_store,
                    $kode_referral,
                    $user_grading,
                    $name_lend
               );
               
          }
          
          echo json_encode($jawara);
     }

     public function approve_all()
	{
		if ($this->input->post('checkbox_value')) {
			$id = $this->input->post('checkbox_value');
			for ($count = 0; $count < count($id); $count++) {
				$this->modelP2p->approve_all($id[$count]);
			}
		}
	}

	public function tolak_all()
	{
		if ($this->input->post('checkbox_value')) {
			$id = $this->input->post('checkbox_value');
			for ($count = 0; $count < count($id); $count++) {
				$this->modelP2p->tolak_all($id[$count]);
			}
		}
	}

     public function ubahStatus(){
          $id = $this->input->post('id');

          $input['status_lend'] = $this->input->post('status_lend');
          $input['status_kol_lend'] = $this->input->post('status_kol_lend');
          $input['tunggakan_lend'] = str_replace('.', '', $this->input->post('tunggakan_lend'));
          $input['notes_lend'] = $this->input->post('notes_lend');
          $input['updated_lend'] = date('Y-m-d H:i:s');

          $this->modelP2p->ubahStatus($input, $id);

          redirect('index.php/p2p');
     } 

     public function comments(){
          $id = $this->input->post('id');

          $input['comments_lend'] = $this->input->post('comments');

          $this->modelP2p->comments($input, $id);

          redirect('index.php/p2p');
     }
     


}