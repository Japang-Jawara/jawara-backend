<?php
class P2p_model extends CI_Model{

    public function getP2p($code_lend){
        return $this->db->select('*')->from('jawara_leads')->where('code_lend', $code_lend)->where('status_lend', NULL)->order_by('id', 'DESC')->get()->result();
    }

    public function getP2pApprove($code_lend){
        return $this->db->select('*')->from('jawara_leads')->where('code_lend', $code_lend)->where('status_lend', 1)->get()->result();
    }

    public function getP2pReject($code_lend){
        return $this->db->select('*')->from('jawara_leads')->where('code_lend', $code_lend)->where('status_lend', 2)->get()->result();
    }

    function approve_all($jawara_id){
		$input_data['status_lend'] = '1';
        $input_data['updated_lend'] = date('Y-m-d H:i:s');
		$this->db->where('id', $jawara_id);
		$this->db->update('jawara_leads', $input_data);
	}

    function tolak_all($jawara_id){
		$input_data['status_lend'] = '2';
        $input_data['updated_lend'] = date('Y-m-d H:i:s');
		$this->db->where('id', $jawara_id);
		$this->db->update('jawara_leads', $input_data);
	}

    function ubahStatus($input, $id){
        
        $this->db->where('id', $id);
        $result = $this->db->update('jawara_leads', $input);

        if($result){
            $this->session->set_flashdata('success','Data Berhasil Diinput Ke Sistem');
            return TRUE;
        }else{
            $this->session->set_flashdata('success','Data Gagal Diinput Ke Sistem, Silahkan Coba Lagi');
            return FALSE;
        }
    }

    function comments($input, $id){
        
        $this->db->where('id', $id);
        $result = $this->db->update('jawara_leads', $input);

        if($result){
            $this->session->set_flashdata('success','Notes Berhasil Diinput Ke Sistem');
            return TRUE;
        }else{
            $this->session->set_flashdata('success','Notes Gagal Diinput Ke Sistem, Silahkan Coba Lagi');
            return FALSE;
        }
    }

}
?>