<div id="content" class="content">
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="<?php echo base_url('home');?>">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="<?php echo base_url('kepegawaian/thl');?>">FINANCIAL INSTITUTION</a></li>
    </ol>
    <h1 class="page-header"><B>FINANCIAL INSTITUTION</b></h1>
    <div class="row">
        <div class="col-xl-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">
                    </h4>
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                </div>

                <div class="panel-body">
                <div class ="table-responsive">
                        <div class="col-xl-12">
                            <ul class="nav nav-tabs nav-tabs-inverse nav-justified nav-justified-mobile" data-sortable-id="index-2">
                                <li class="nav-item">
                                    <a href="#bm" data-toggle="tab" class="nav-link active">
                                        <span><i class="fa fa-list fa-lg m-r-5"></i> Approve/Reject</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#dt" data-toggle="tab" class="nav-link">
                                        <span><i class="fa fa-check-circle fa-lg m-r-5"></i> Approve</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#og" data-toggle="tab" class="nav-link">
                                        <span><i class="fa fa-stop fa-lg m-r-5"></i> Reject</span>
                                    </a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane fade active show" id="bm">
                                    <div class="panel-body">
                                        <div class ="table-responsive">
                                            <!-- <p>Action Approve or Reject Jawara</p>
                                            <button type="button" name="approve_all" id="approve_all" class="btn btn-success btn-xl">Approve</button>
                                            <button typr="button" name="tolak_all" id="tolak_all" class="btn btn-warning btn-xl">Reject</button>
                                            <br><br> -->
                                            <table id="tbl-lend" class="table table-striped table-bordered table-td-valign-middle" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>NO</th>
                                                        <th>ACTION</th>
                                                        <!-- <th>
                                                            <input type="checkbox" name="select-all" id="select-all" />
                                                        </th> -->
                                                        <th>NIK KTP</th>
                                                        <th>NIK KK</th>
                                                        <th>PHONE</th>
                                                        <th>NAME</th>
                                                        <th>TANGGAL LAHIR</th>
                                                        <th>ALAMAT LENGKAP RUMAH</th>
                                                        <th>FILE KTP</th>
                                                        <th>FILE KTP PASANGAN</th>
                                                        <th>FILE KK</th>
                                                        <th>ALAMAT USAHA</th>
                                                        <th>KODEPOS</th>
                                                        <th>KECAMATAN</th>
                                                        <th>KABUPATEN/KOTA</th>
                                                        <th>PROVINSI</th>
                                                        <th>FILE USAHA</th>
                                                        <th>KODE REFERRAL</th>
                                                        <th>USER GRADING</th>
                                                        <th>P2P LEND</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="dt">
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                        <table id="tbl-lend-approve" class="table table-striped table-bordered table-td-valign-middle" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>NO</th>
                                                        <th>NIK KTP</th>
                                                        <th>NIK KK</th>
                                                        <th>PHONE</th>
                                                        <th>NAME</th>
                                                        <th>ALAMAT LENGKAP RUMAH</th>
                                                        <th>FILE KTP</th>
                                                        <th>FILE KTP PASANGAN</th>
                                                        <th>FILE KK</th>
                                                        <th>ALAMAT USAHA</th>
                                                        <th>KODEPOS</th>
                                                        <th>KECAMATAN</th>
                                                        <th>KABUPATEN/KOTA</th>
                                                        <th>PROVINSI</th>
                                                        <th>FILE USAHA</th>
                                                        <th>KODE REFERRAL</th>
                                                        <th>USER GRADING</th>
                                                        <th>P2P LEND</th>
                                                        <th>SIGN PK</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="og">
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                        <table id="tbl-lend-reject" class="table table-striped table-bordered table-td-valign-middle" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>NO</th>
                                                        <th>NIK KTP</th>
                                                        <th>NIK KK</th>
                                                        <th>PHONE</th>
                                                        <th>NAME</th>
                                                        <th>ALAMAT LENGKAP RUMAH</th>
                                                        <th>FILE KTP</th>
                                                        <th>FILE KTP PASANGAN</th>
                                                        <th>FILE KK</th>
                                                        <th>ALAMAT USAHA</th>
                                                        <th>KODEPOS</th>
                                                        <th>KECAMATAN</th>
                                                        <th>KABUPATEN/KOTA</th>
                                                        <th>PROVINSI</th>
                                                        <th>FILE USAHA</th>
                                                        <th>KODE REFERRAL</th>
                                                        <th>USER GRADING</th>
                                                        <th>P2P LEND</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end panel-body -->
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-10 -->
    </div>

    <!-- Modal -->
    <div class="modal fade" id="ubahstatus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Approve or Reject Jawara</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="p2p/ubahStatus" enctype="multipart/form-data" method="POST">
                        
                        <div class="row">
                            <input class="form-control" type="hidden" name="id" id="id" />
                            <div class="form-group col-xl-12">
                                <label class="col-form-label">Status Jawara Approve or Rejcet<span class="text-danger">*</span></label>
                                <select class="form-control" name="status_lend" searchable="Search here.." data-parsley-group="step-1" data-parsley-required="true" required>
                                    <option value="" disabled selected>Choose status jawara</option>
                                    <option value="1">Approve</option>
                                    <option value="2">Reject</option>
                                </select>
                            </div>
                            <div class="form-group col-xl-6">
                                <label class="col-form-label">Status KOL<span class="text-danger">*</span></label>
                                <select class="form-control" name="status_kol_lend" searchable="Search here.." data-parsley-group="step-1" data-parsley-required="true" required>
                                    <option value="" disabled selected>Choose status KOL</option>
                                    <option value="KOL 1">KOL 1</option>
                                    <option value="KOL 2">KOL 2</option>
                                    <option value="KOL 3">KOL 3</option>
                                    <option value="KOL 4">KOL 4</option>
                                    <option value="KOL 5">KOL 5</option>
                                    <option value="TIDAK ADA">TIDAK ADA</option>
                                </select>
                            </div>
                            <div class="form-group col-xl-6">
                                <label class="col-form-label">Jumlah Tunggakan<span class="text-danger">*</span></label>
                                <!-- <input type="number" class="form-control" name="tunggakan_lend" required/> -->
                                <!-- <input type="text" class="form-control" data-affixes-stay="true" data-prefix="Rp " data-thousands="." data-decimal="," data-precision="0" /> -->
                                <input id="tanpa-rupiah" class="form-control"  name="tunggakan_lend" required/>
                            </div>
                            <div class="form-group col-xl-6">
                                <label class="col-form-label">Notes<span class="text-danger">*</span></label>
                                <textarea class="form-control"  name="notes_lend" required></textarea>
                            </div>                   
                        </div>
                        
                        </br>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="reset" name="reset" class="btn btn-info"><i class="fa fa-cut"></i> Reset</button>
                            <button type="submit" class="btn btn-primary" value="Cek"><i class="fa fa-save"></i> Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="comments" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Jawara</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="p2p/comments" enctype="multipart/form-data" method="POST">
                        
                        <div class="row">
                            <input class="form-control" type="hidden" name="id" id="id" />
                            <div class="form-group col-xl-6">
                                <label class="col-form-label">Notes<span class="text-danger">*</span></label>
                                <textarea class="form-control"  name="comments" required></textarea>
                            </div>                   
                        </div>
                        
                        </br>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="reset" name="reset" class="btn btn-info"><i class="fa fa-cut"></i> Reset</button>
                            <button type="submit" class="btn btn-primary" value="Cek"><i class="fa fa-save"></i> Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <style>
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type=number] {
        -moz-appearance: textfield;
    }

    #tanpa-rupiah {
        text-align: right;
    }
    </style>

    <!-- Manage -->
    <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/app-manage.js');?>"></script>

    <script type="text/javascript">
        
        $(document).ready(function() {

            /* Tanpa Rupiah */
            var tanpa_rupiah = document.getElementById('tanpa-rupiah');
            tanpa_rupiah.addEventListener('keyup', function(e)
            {
                tanpa_rupiah.value = formatRupiah(this.value);
            });

            /* Fungsi */
            function formatRupiah(angka, prefix)
            {
                var number_string = angka.replace(/[^,\d]/g, '').toString(),
                    split    = number_string.split(','),
                    sisa     = split[0].length % 3,
                    rupiah     = split[0].substr(0, sisa),
                    ribuan     = split[0].substr(sisa).match(/\d{3}/gi);
                    
                if (ribuan) {
                    separator = sisa ? '.' : '';
                    rupiah += separator + ribuan.join('.');
                }
                
                rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
            }

            $('#select-all').click(function(event) {
                if (this.checked) {
                    // Iterate each checkbox
                    $(':checkbox').each(function() {
                        this.checked = true;
                    });
                } else {
                    $(':checkbox').each(function() {
                        this.checked = false;
                    });
                }
            });

            $('.approve_checkbox').click(function() {
                if($(this).is(':checked')){
                    $('.approve_checkbox').prop('checked', true);
                }else{
                    $('.approve_checkbox').prop('checked', false);
                }
            });

            $('#approve_all').click(function() {
                var checkbox_value = [];
                var oTable = $('#tbl-lend').dataTable();
                var rowcollection =  oTable.$(".approve_checkbox:checked", {"page": "all"});
                rowcollection.each(function(index,elem){
                    checkbox_value.push($(elem).val());

                });
                // console.log(checkbox);
                if (checkbox_value.length > 0) {
                 
                    var yakin = confirm("Apakah kamu yakin akan Mengapprove Pengajuan?");

                    if (yakin) {
                        $.ajax({
                            url: "p2p/approve_all",
                            method: "POST",
                            data: {
                                checkbox_value: checkbox_value
                            },
                            success: function() {
                                setInterval('location.reload()', 2000);
                            }
                        })
                        Swal.fire({
                            title: 'Informasi!',
                            text: 'Data Berhasil di Approve',
                            imageUrl: '<?= base_url('assets/svg/completed-animate.svg') ?>',
                            imageWidth: 400,
                            imageHeight: 250,
                            imageAlt: 'Custom image',
                        });
                    } else {
                        Swal.fire({
                            title: 'Informasi!',
                            text: 'Data Gagal di Approve, Silahkan Coba Lagi',
                            imageUrl: '<?= base_url('assets/svg/questions-animate.svg') ?>',
                            imageWidth: 400,
                            imageHeight: 250,
                            imageAlt: 'Custom image',
                        });
                    }
                } else {
                    alert('Tidak ada data yang ingin di Approve, silahkan pilih data terlebih dahulu.');
                }
            });

            $('#tolak_all').click(function() {
                var checkbox_value = [];
                var oTable = $('#tbl-lend').dataTable();
                var rowcollection =  oTable.$(".approve_checkbox:checked", {"page": "all"});
                rowcollection.each(function(index,elem){
                    checkbox_value.push($(elem).val());

                });
                // console.log(checkbox);
                if (checkbox_value.length > 0) {

                    var yakin = confirm("Apakah kamu yakin akan Reject Jawara?");

                    if (yakin) {
                        $.ajax({
                            url: "p2p/tolak_all",
                            method: "POST",
                            data: {
                                checkbox_value: checkbox_value
                            },
                            success: function() {
                                setInterval('location.reload()', 2000);
                            }
                        })
                        Swal.fire({
                            title: 'Informasi!',
                            text: 'Data Berhasil di Reject',
                            imageUrl: '<?= base_url('assets/svg/empty-animate.svg') ?>',
                            imageWidth: 400,
                            imageHeight: 250,
                            imageAlt: 'Custom image',
                        });
                    } else {
                        Swal.fire({
                            title: 'Informasi!',
                            text: 'Data Gagal di Reject, Silahkan Coba Lagi',
                            imageUrl: '<?= base_url('assets/svg/questions-animate.svg') ?>',
                            imageWidth: 400,
                            imageHeight: 250,
                            imageAlt: 'Custom image',
                        });
                    }
                } else {
                    alert('Tidak ada data yang ingin di Reject, silahkan pilih data terlebih dahulu.');
                }
            });
        });
    </script>

    
