<?php
class Sales_model extends CI_Model{

    public function getSKT(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'SKT')->get()->result();
    }

    public function getKKL(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'KKL')->get()->result();
    }

    public function getPR(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'PR')->get()->result();
    }

    public function getJB(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'JB')->get()->result();
    }

    public function getJA(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'JA')->get()->result();
    }

    public function getJT(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'JT')->get()->result();
    }

    public function getJCT(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'JCT')->get()->result();
    }

    public function getSSIGN(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'SSIGN')->get()->result();
    }

    function getUomTelur(){
        return $this->db->select('*')->from('unit_of_measure')->where('category', 'telur')->where('is_active', '1')->get()->result();
    }

    function get_provinsi()
    {
        $this->db->order_by('name', 'ASC');
        $query = $this->db->get('provinces');
        return $query->result();
    }

    function get_kabupaten($provinsi_id)
    {
        //ambil data kabupaten berdasarkan id provinsi yang dipilih
        $this->db->where('province_id', $provinsi_id);
        $this->db->order_by('name', 'ASC');
        $query = $this->db->get('regencies');

        $output = '<option value="">-- Pilih Kabupaten --</option>';

        //looping data
        foreach ($query->result() as $row) {
            $output .= '<option value="' . $row->id . '">' . $row->name . '</option>';
        }
        //return data kabupaten
        return $output;
    }

    function get_kecamatan($kabupaten_id)
    {
        //ambil data kecamatan berdasarkan id kabupaten yang dipilih
        $this->db->where('regency_id', $kabupaten_id);
        $this->db->order_by('name', 'ASC');
        $query = $this->db->get('districts');

        $output = '<option value="">-- Pilih Kecamatan --</option>';

        //looping data
        foreach ($query->result() as $row) {
            $output .= '<option value="' . $row->name . '">' . $row->name . '</option>';
        }
        //return data kecamatan
        return $output;
    }

    function cekProvinsi($prov){
        return $this->db->where('id', $prov)->get('provinces')->result()[0]->name;
    }

    function cekKabupaten($kab){
        return $this->db->where('id', $kab)->get('regencies')->result()[0]->name;
    }

    public function getSales(){
        return $this->db->select('*')->from('jawara_leads')->where('status_lend', 1)->where('status_disbursement', NULL)->get()->result();
    }

    public function createSales($data, $data2, $id){

        $result = $this->db->where('id', $id)->update('jawara_leads', $data);

        if($result){
            $this->db->where('id', $id)->update('jawara_leads', $data2);
            $this->session->set_flashdata('success','Jawara Product Disbursement Telah Berhasil Diinput Ke Sistem');
            return TRUE;
        }else{
            $this->session->set_flashdata('success','Jawara Product Disbursement Gagal Diinput Ke Sistem, Silahkan Coba Lagi');
            return FALSE;
        }
    }
}
?>