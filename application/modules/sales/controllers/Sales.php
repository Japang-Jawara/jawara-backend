<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales extends CI_Controller{

     function __construct(){
          parent::__construct();

          $this->load->model('Sales_model', 'modelSales');
     }

     function uploadTerms($jawara_id)
	{
		foreach ($_FILES as $name => $fileInfo) {
			$filename = $_FILES['file_terms']['name'];
			$tmpname = $_FILES['file_terms']['tmp_name'];
			$exp = explode('.', $filename);
			$ext = end($exp);
			$newname =  'TERMS_'.$jawara_id.'_'.date('Ymdhis')."." . $ext;
			$config['upload_path'] = './assets/berkas/terms/';
			$config['upload_url'] =  base_url() . 'assets/berkas/terms/';
			$config['allowed_types'] = "png|jpg|jpeg";
			$config['max_size'] = '2000';
			$config['file_name'] = $newname;
			$this->load->library('upload', $config);
			move_uploaded_file($tmpname, "assets/berkas/terms/" . $newname);
			return $newname;
		}
	}

     function index(){
          if($this->session->userdata('is_sales') == TRUE){

               $x['getSKT'] = $this->modelSales->getSKT();
               $x['getKKL'] = $this->modelSales->getKKL();
               $x['getPR'] = $this->modelSales->getPR();
               $x['getJB'] = $this->modelSales->getJB();
               $x['getJA'] = $this->modelSales->getJA();
               $x['getJT'] = $this->modelSales->getJT();
               $x['getJCT'] = $this->modelSales->getJCT();

               $x['getSSIGN'] = $this->modelSales->getSSIGN();

               $x['getUomTelur'] = $this->modelSales->getUomTelur();

               $x['provinsi'] = $this->modelSales->get_provinsi();

               $this->load->view("include/head");
               $this->load->view("include/top-header");
               $this->load->view('index',$x);
               $this->load->view("include/sidebar");
               $this->load->view("include/panel");
               $this->load->view("include/footer");
               $this->load->view("include/alert");
          }else{
               $this->session->set_flashdata('success', 'Upsss!!!, Anda Tidak Punya Akses Ke Sales');
               redirect('index.php/home');
          }
     }

     function getSales() {
          
          $data = $this->modelSales->getSales();

          $no = 1;
          $jawara = array();

          foreach($data as $r) {

               $nik_ktp = $r->nik_ktp_kyc;
               $phone = $r->phone_kyc;
               $name = $r->name_kyc;

               $alamat_rumah = $r->alamat_rumah == NULL ? "<font>Alamat Rumah Tidak Ada</font>" : $r->alamat_rumah;

               $shop_location = $r->alamat_usaha;
               $kodepos = $r->kodepos == NULL ? "<font>Kode Pos Tidak Ada</font>" : $r->kodepos;
               $kecamatan = $r->kecamatan == NULL ? "<font>Kecamatan Tidak Ada</font>" : $r->kecamatan;
               $kab_kota = $r->kab_kota == NULL ? "<font>Kab/Kota Tidak Ada</font>" : $r->kab_kota;
               $provinsi = $r->provinsi == NULL ? "<font>Provinsi Tidak Ada</font>" : $r->provinsi;
               
               $name_lend = $r->name_lend;

               if($r->status_lend == NULL){
                    $status_lend = 'Belum Ada Tanggapan dari P2P';
               }else if($r->status_lend == 1){
                    $status_lend = 'Approve';
               }else if($r->status_lend == 2){
                    $status_lend = 'Reject';
               }

               $action = "<a href='javascript:;' 
                    data-id='$r->id'
                    data-nik_ktp='$r->nik_ktp_kyc'
                    data-name='$r->name_kyc'
                    data-phone='$r->phone_kyc'
                    data-alamat_rumah='$r->alamat_rumah'
                    data-shop_location='$r->alamat_usaha'
                    data-kodepos='$r->kodepos'
                    data-kecamatan='$r->kecamatan'
                    data-kab_kota='$r->kab_kota'
                    data-provinsi='$r->provinsi'
                    data-founders='$r->name_lend'
                    data-code_lend='$r->code_lend'
                    data-status_lend='$r->status_lend'
                    data-status_lends='$status_lend'
                    data-toggle='modal' data-target='#disbursement'
                    class='btn btn-sm btn-success'><i class='fa fas fa-edit'></i> Disbursement</a>
               ";

               $jawara[] = array(
                    $no++,
                    $action,
                    $nik_ktp,
                    $phone,
                    $name,
                    $alamat_rumah,
                    $shop_location,
                    $kodepos,
                    $kecamatan,
                    $kab_kota,
                    $provinsi,
                    $name_lend,
                    $status_lend
               );
               
          }
          
          echo json_encode($jawara);
     }

     public function createSales(){

          $jawara_id = $this->input->post('id');

          $file_terms = '';

          $x['email_verifikasi_dis'] = $this->input->post('email');
          $x['nik_ktp_dis'] = $this->input->post('nik_ktp');
          $x['ibu_kandung_dis'] = $this->input->post('ibu_kandung');
          $x['name_dis'] = $this->input->post('name');
          $x['phone_dis'] = $this->input->post('phone');
          $x['phone_wa_dis'] = $this->input->post('phone_wa');
          $x['alamat_rumah_dis'] = $this->input->post('alamat_rumah');

          $x['shop_name_dis'] = $this->input->post('shop_name');
          $x['shop_location_dis'] = $this->input->post('shop_location');
          $x['status_kepemilikan_dis'] = $this->input->post('status_kepemilikan');

          $x['kodepos_dis'] = $this->input->post('kodepos');
          $x['kelurahan_dis'] = $this->input->post('kelurahan');
          $x['kecamatan_dis'] = $this->input->post('kecamatan');
          $x['kab_kota_dis'] = $this->modelSales->cekKabupaten($this->input->post('kab_kota'));
          $x['provinsi_dis'] = $this->modelSales->cekProvinsi($this->input->post('provinsi'));

          $x['kapasitas_kwh_dis'] = $this->input->post('kapasitas_kwh');

          $x['produk_beras1_dis'] = $this->input->post('produk_beras1');
          $x['produk_beras2_dis'] = $this->input->post('produk_beras2');
          $x['produk_ayam_dis'] = $this->input->post('produk_ayam');
          $x['produk_telur_dis'] = $this->input->post('produk_telur');

          $x['jumlah_beras_dis'] = $this->input->post('jumlah_beras');
          $x['jumlah_beras2_dis'] = $this->input->post('jumlah_beras2');

          $x['jumlah_ayam_dis'] = $this->input->post('jumlah_ayam');

          $x['jumlah_telur_dis'] = $this->input->post('jumlah_telur');
          $x['uom_telur_dis'] = $this->input->post('uom_telur');

          if (!empty($_FILES['file_terms']['name'])) {
               $newname = $this->uploadTerms($jawara_id);
               $data['file_terms'] = $newname;
               $file_terms = $newname;

               $x['file_terms_dis'] = base_url('assets/berkas/terms/').$file_terms;
          }

          $x['ssign_dis'] = $this->input->post('ssign');
          $x['notes_dis'] = $this->input->post('notes');
          $x['join_telegram_dis'] = $this->input->post('join_telegram');
          $x['created_at_dis'] = date('Y-m-d H:i:s');

          //UPDATE DATA FROM TABLE JAWARA ELIGIBLE
          $u['status_disbursement'] = '1';
          $u['updated_disbursement'] = date('Y-m-d H:i:s');
          $u['kecamatan'] = $this->input->post('kecamatan');
          $u['kab_kota'] = $this->modelSales->cekKabupaten($this->input->post('kab_kota'));
          $u['provinsi'] = $this->modelSales->cekProvinsi($this->input->post('provinsi'));

          $this->modelSales->createSales($x, $u, $jawara_id);

          redirect('index.php/sales');

     }

     // request data kecamatan berdasarkan id PROVINSI yang dipilih
     function get_kabupaten()
     {
          if ($this->input->post('provinsi_id')) {
               echo $this->modelSales->get_kabupaten($this->input->post('provinsi_id'));
          }
     }

     //request data kecamatan berdasarkan id kabupaten yang dipilih
     function get_kecamatan()
     {
          if ($this->input->post('kabupaten_id')) {
               echo $this->modelSales->get_kecamatan($this->input->post('kabupaten_id'));
          }
     }
}