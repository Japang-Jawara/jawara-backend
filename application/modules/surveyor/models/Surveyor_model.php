<?php
class Surveyor_model extends CI_Model{

    public function getData(){
        return $this->db->select('*')->from('jawara_leads')->where('status_jawara_di', '0')->where('status_ground_di', NULL)->get()->result();
    }

    public function getBA(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'BA')->get()->result();
    }

    public function createSurveyor($data, $data2, $id){

        $result = $this->db->where('id', $id)->update('jawara_leads', $data);

        if($result){
            $this->db->where('id', $id)->update('jawara_leads', $data2);
            $this->session->set_flashdata('success','Jawara Score Card/On Ground Telah Berhasil Diinput Ke Sistem');
            return TRUE;
        }else{
            $this->session->set_flashdata('success','Jawara Score Card/On Ground Gagal Diinput Ke Sistem, Silahkan Coba Lagi');
            return FALSE;
        }
    }
}
?>