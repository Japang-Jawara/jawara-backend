<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Deployjap extends CI_Controller{

     function __construct(){
          parent::__construct();

          $this->load->model('Deployjap_model', 'modelDeployjap');
     }

     function index(){
          if($this->session->userdata('is_login') == TRUE){

               $x['getDeploy'] = $this->modelDeployjap->getConfig();

               $this->load->view("include/head");
               $this->load->view("include/top-header");
               $this->load->view('index', $x);
               $this->load->view("include/sidebar");
               $this->load->view("include/panel");
               $this->load->view("include/footer");
               $this->load->view("include/alert");
          }else{
               $this->session->set_flashdata('success', 'Upsss!!!, Login dulu ya.');
               redirect('index.php/login');
          }
     }

     // OPEN DEPLOY JAPANG
     function getOpenDeploy() {
          
          $data = $this->modelDeployjap->getOpenDeploy();

          $no = 1;
          $jawara = array();

          foreach($data as $r) {
               
               $timestamp = $r->created_at;
               $jawara_id = $r->jawara_id;
               $nik_ktp = $r->nik_ktp == NULL ? "<font>Nik KTP Tidak Ada</font>" : $r->nik_ktp;
               $name = $r->name;
               $alamat_rumah = $r->alamat_rumah;
               $alamat_usaha = $r->alamat_usaha;

               $area_jawara = $r->area_jawara == NULL ? "<font>Area Jawara Tidak Ada</font>" : $r->area_jawara;

               $action = "<a href='javascript:;' 
                    data-id='$r->id'
                    data-toggle='modal' data-target='#deployjap'
                    class='btn btn-sm btn-success'><i class='fa fas fa-edit'></i> Deploy</a>
               ";


               $jawara[] = array(
                    $no++,
                    $action,
                    $timestamp,
                    $jawara_id,
                    $nik_ktp,
                    $name,
                    $alamat_rumah,
                    $alamat_usaha,
                    $area_jawara
               );
               
          }
          
          echo json_encode($jawara);
     }

     // DONE Deploy Japang
     function getDoneDeploy() {
          
          $data = $this->modelDeployjap->getDoneDeploy();

          $no = 1;
          $jawara = array();

          foreach($data as $r) {
               
               $timestamp = $r->created_at_dep;
               $jawara_id = $r->jawara_id;
               $email_verifikasi = $r->email_verifikasi_dep;
               $date_delivery = $r->date_delivery_dep;

               if($r->link_surat_dep != NULL){
                    $link_surat = '<a href="'.$r->link_surat_dep.'" target="_blank" class="btn btn-sm btn-secondary">'.$r->link_surat_dep.'</a>';
               }else{
                    $link_surat = 'Tidak ada Surat jalan';
               }

               $status = $r->status_dep;
               $notes = $r->notes_dep;

               $nik_ktp = $r->nik_ktp;
               $name = $r->name;

               $jawara[] = array(
                    $no++,
                    $timestamp,
                    $jawara_id,
                    $email_verifikasi,
                    $nik_ktp,
                    $name,
                    $date_delivery,
                    $link_surat,
                    $status,
                    $notes
               );
               
          }
          
          echo json_encode($jawara);
     }

     public function createDeploy(){

          $jawara_id = $this->input->post('id');

          $x['email_verifikasi_dep'] = $this->input->post('email');
          $x['date_delivery_dep'] = $this->input->post('date_delivery').' '.$this->input->post('time_delivery');
          $x['link_surat_dep'] = $this->input->post('link_surat');
          $x['status_dep'] = $this->input->post('status_dep');
          $x['notes_dep'] = $this->input->post('notes');
          $x['created_at_dep'] = date('Y-m-d H:i:s');

          //UPDATE DATA FROM TABLE JAWARA ELIGIBLE
          $x['status_deploy_dep'] = '1';

          $this->modelDeployjap->createDeploy($x, $jawara_id);

          redirect('index.php/deployjap');

     }

}