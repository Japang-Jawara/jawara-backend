<div id="content" class="content">
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="<?php echo base_url('home');?>">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="<?php echo base_url('kepegawaian/thl');?>">DEPLOY</a></li>
    </ol>
    <h1 class="page-header"><b>DEPLOY</b></h1>
    <div class="note note-info note-with-right-icon">
        <div class="note-icon"><i class="fa fa-lightbulb"></i></div>
        <div class="note-content text-right">
            <h2><b>DEPLOY</b></h2>
            <p>Delivery product ke jawara.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <?php if($this->session->userdata('role') == 'Admin' || $this->session->userdata('role') == 'SuperAdmin' || $this->session->userdata('role') == 'Kepegawaian'){?>
                        <a href="" class="btn btn-icon btn-sm btn-inverse" data-toggle="modal" data-target="#addthl"><i class="fa fa-plus-square"></i></a>
                        <?php } ?>
                    </h4>
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                </div>

                <div class="panel-body">
                    <div class ="table-responsive">
                        <div class="col-xl-12">
                            <ul class="nav nav-tabs nav-tabs-inverse nav-justified nav-justified-mobile" data-sortable-id="index-2">
                                <li class="nav-item">
                                    <a href="#bm" data-toggle="tab" class="nav-link active">
                                        <span><i class="fa fa-spinner fa-lg m-r-5"></i> PENDING</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#dt" data-toggle="tab" class="nav-link">
                                        <span><i class="fa fa-check-circle fa-lg m-r-5"></i> DONE</span>
                                    </a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane fade active show" id="bm">
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table id="tbl-deploy-jap" class="table table-striped table-bordered table-td-valign-middle" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>NO</th>
                                                        <th>ACTION</th>
                                                        <th>TIMESTAMP</th>
                                                        <th>JAWARA ID</th>
                                                        <th>NIK KTP</th>
                                                        <th>NAMA</th>
                                                        <th>ALAMAT RUMAH</th>
                                                        <th>ALAMAT USAHA</th>
                                                        <th>AREA JAWARA</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="dt">
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table id="tbl-deploy-jap-done" class="table table-striped table-bordered table-td-valign-middle" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>NO</th>
                                                        <th>TIMESTAMP</th>
                                                        <th>JAWARA ID</th>
                                                        <th>EMAIL VERIFIKASI</th>
                                                        <th>NIK KTP</th>
                                                        <th>NAMA</th>
                                                        <th>TANGGAL DELIVERY</th>
                                                        <th>SURAT JALAN</th>
                                                        <th>STATUS</th>
                                                        <th>KETERANGAN</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end panel-body -->
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-10 -->
    </div>

    <div class="modal fade" id="deployjap" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">DEPLOY</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="deployjap/createDeploy" enctype="multipart/form-data" method="POST">
                        
                        <div class="row">
                            <input type="hidden" name="autocomplete" id="field-autocomplete">
                            <input class="form-control" type="hidden" name="id" id="idx" />

                            <div class="form-group col-xl-12">
                                <label class="col-form-label">Email Verifikator <span class="text-danger">*</span></label>
                                <input type="email" class="form-control" name="email" id="email" placeholder="Email Verifikator" autocomplete="off" value="<?= $this->session->userdata('email_address'); ?>" required readonly>
                            </div>

                            <div class="form-group col-xl-6">
                                <label class="col-form-label">Delivery Date <span class="text-danger">*</span></label>
                                <input type="date" class="form-control" name="date_delivery" id="date_delivery" autocomplete="off" required />
                            </div>
                            <div class="form-group col-xl-6">
                                <label class="col-form-label">Delivery Time <span class="text-danger">*</span></label>
                                <input type="time" class="form-control" name="time_delivery" id="time_delivery" autocomplete="off" required />
                            </div>

                            <div class="form-group col-xl-12">
                                <label class="col-form-label">Link Surat Jalan <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="link_surat" id="link_surat" placeholder="Link Surat Jalan" autocomplete="off" required>
                            </div>

                            <div class="form-group col-xl-12">
                                <label class="col-form-label">Status Deploy <span class="text-danger">*</span></label>
                                <select name="status_dep" id="status_dep" class="form-control" required>
                                    <option value="">Pilih Status</option>
                                    <?php
                                        foreach ($getDeploy as $row) {
                                            echo '<option value="' . $row->name . '">' . $row->name . '</option>';
                                        }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group col-xl-12">
                                <label class="col-form-label">Keterangan <span class="text-danger">*</span></label>
                                <textarea class="form-control" name="notes" id="notes" required></textarea>
                            </div>
                                         
                        </div>
                        
                        </br>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="reset" name="reset" class="btn btn-info"><i class="fa fa-cut"></i> Reset</button>
                            <button type="submit" class="btn btn-primary" value="Cek"><i class="fa fa-save"></i> Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Manage -->
    <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/app-manage.js');?>"></script>

    
