<?php
class Deployjap_model extends CI_Model{


    public function getOpenDeploy(){
        return $this->db->select('*')->from('jawara_score_eligible')->where('created_at_dep IS NULL')->get()->result();
    }

    public function getDoneDeploy(){
        return $this->db->select('*')->from('jawara_score_eligible')->where('created_at_dep IS NOT NULL')->get()->result();
    }

    public function getConfig(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'SDeploy')->get()->result();
    }

    public function createDeploy($data, $id){

        $result = $this->db->where('id', $id)->update('jawara_score_eligible', $data);

        if($result){
            $this->session->set_flashdata('success','Deploy By Japang Telah Berhasil Diinput Ke Sistem');
            return TRUE;
        }else{
            $this->session->set_flashdata('success','Deploy By Japang Gagal Diinput Ke Sistem, Silahkan Coba Lagi');
            return FALSE;
        }
    }

}
?>