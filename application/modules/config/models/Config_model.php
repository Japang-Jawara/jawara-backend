<?php

class Config_model extends CI_Model{

    public function getPrice(){
        return $this->db->select('tbl_price.*, jawara_sku.*')
        ->from('tbl_price')
        ->join('jawara_sku', 'tbl_price.sku = jawara_sku.sku')
        ->order_by('date', 'DESC')
        ->get()->result();
    }

    public function getSku(){
        return $this->db->select('*')->from('jawara_sku')->order_by('sku', 'ASC')->get()->result();
    }

    public function tambahPrice($data){
        $result = $this->db->insert('tbl_price', $data);

        if($result){
            $this->session->set_flashdata('success','PRICE BERHASIL DISIMPAN');
            return TRUE;
        }else{
            $this->session->set_flashdata('success','PRICE GAGAL DISIMPAN');
            return FALSE;
        }
    }

}
?>