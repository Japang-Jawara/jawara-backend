<?php
defined('BASEPATH') or exit('No direct script access allowed ');

class Config extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		//LOAD MODELS
        $this->load->model('Config_model', 'modelConfig');
	}

    public function price(){

        if($this->session->userdata('is_login') == TRUE){

			$x['sku'] = $this->modelConfig->getSku();

			$this->load->view("include/head");
			$this->load->view("include/top-header");
			$this->load->view('price', $x);
			$this->load->view("include/sidebar");
			$this->load->view("include/panel");
			$this->load->view("include/footer");
			$this->load->view("include/alert");
	   }else{
			$this->session->set_flashdata('success', 'Upsss!!!, Login dulu ya.');
			redirect('index.php/login');
	   }

    }

	public function getPrice(){
		$data = $this->modelConfig->getPrice();

		$no = 1;
		$getPrice = array();

		foreach($data as $r) {

			$sku = $r->sku;
			$name = ' ('.$r->product.' '.$r->type.' / '.$r->size.$r->uom.') ';
			$date = $r->date;
			$price = "Rp " . number_format($r->price, 0, "", ".");

			$getPrice[] = array(
				$no++,
				$sku,
				$name,
				$price,
				$date
			);
			
		}
          
		echo json_encode($getPrice);
	}

	public function tambahPrice(){

		$x['sku'] = $this->input->post('sku');
		$x['date'] = $this->input->post('date');
		$x['price'] = $this->input->post('price');

		$this->modelConfig->tambahPrice($x);

		redirect('index.php/config/price');
   	}

}
