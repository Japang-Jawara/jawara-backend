<div id="content" class="content">
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="<?php echo base_url('home');?>">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="<?php echo base_url('kepegawaian/thl');?>">DAFTAR HARGA BARANG</a></li>
    </ol>
    <h1 class="page-header"><b>DAFTAR HARGA BARANG</b></h1>
    <div class="note note-info note-with-right-icon">
        <div class="note-icon"><i class="fa fa-lightbulb"></i></div>
        <div class="note-content text-right">
            <h2><b>Harga Barang</b></h2>
            <p>Kumpulan harga product japang.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <?php if($this->session->userdata('role') == 997 && ($this->session->userdata('email_address') == 'donald.harefa@jaringpangan.com' || $this->session->userdata('email_address') == 'deploy@jaringpangan.com')) {?>
                            <span><a href="" class="btn btn-sm btn-success" data-toggle="modal" data-target="#add">TAMBAH</a></span>
                        <?php } ?>
                    </h4>
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                </div>
        
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="tbl-price" class="table table-striped table-bordered table-td-valign-middle" width="100%">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>SKU</th>
                                    <th>NAME</th>
                                    <th>HARGA</th>
                                    <th>DATE</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!-- end panel-body -->
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-10 -->
    </div>

    <!-- Modal ADD -->
    <div class="modal fade bd-example-modal-sm" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">PRICE</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="config/tambahPrice" enctype="multipart/form-data" method="POST">
                        
                        <div class="row">

                            <div class="form-group col-xl-12">
                                <label class="col-form-label">SKUU:</label>
                                <select name="sku" class="form-control" required>
                                    <?php foreach($sku as $r) { ?>
                                        <option value="<?= $r->sku ?>"><?= $r->sku. ' ('.$r->product.' '.$r->type.' / '.$r->size.$r->uom.') '?></option>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="form-group col-xl-12">
                                <label class="col-form-label">Date:</label>
                                <input type="date" class="form-control" id="date" name="date" required>
                            </div>

                            <div class="form-group col-xl-12">
                                <label class="col-form-label">Price:</label>
                                <input type="number" class="form-control" id="price" name="price" required>
                            </div>

                        </div>
                        
                        </br>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="reset" name="reset" class="btn btn-info"><i class="fa fa-cut"></i> Reset</button>
                            <button type="submit" class="btn btn-primary" value="Cek"><i class="fa fa-save"></i> Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Manage -->
    <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/app-manage.js');?>"></script>