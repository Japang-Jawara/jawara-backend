<?php

class Deploy_model extends CI_Model{

    public function getOpenDeploy(){
        return $this->db->select('*')->from('jawara_leads')->where('status_confirm_dis IS NOT NULL')->where('status_deploy_dis', NULL)->get()->result();
    }

    public function getDoneDeploy(){
        return $this->db->select('*')->from('jawara_leads')->where('created_at_dep IS NOT NULL')->get()->result();
    }

    public function getDeploy(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'SDEPLOY')->get()->result();
    }

    public function createDeploy($data, $data2, $id){

        $result = $this->db->where('id', $id)->update('jawara_leads', $data);

        if($result){
            $this->db->where('id', $id)->update('jawara_leads', $data2);
            $this->session->set_flashdata('success','Deploy Telah Berhasil Diinput Ke Sistem');
            return TRUE;
        }else{
            $this->session->set_flashdata('success','Deploy Gagal Diinput Ke Sistem, Silahkan Coba Lagi');
            return FALSE;
        }
    }

    public function updateDeploy($data, $data2, $id){

        $result = $this->db->where('id', $id)->update('jawara_leads', $data);

        if($result){
            $this->db->where('id', $id)->update('jawara_leads', $data2);
            $this->session->set_flashdata('success','Update Data Deploy Telah Berhasil Diinput Ke Sistem');
            return TRUE;
        }else{
            $this->session->set_flashdata('success','Uodate Data Deploy Gagal Diinput Ke Sistem, Silahkan Coba Lagi');
            return FALSE;
        }
    }

    public function createActive($id){
        $data = $this->db->select('*')->from('jawara_leads')->where('id', $id)->get()->result()[0];

        $x['jawara_id'] = $id;
        $x['name'] = $data->name;
        $x['shop_name'] = $data->shop_name_dis;
        $x['gmaps'] = $data->gmaps_di;
        $x['is_active'] = 1;

        $this->db->insert('jawara_active', $x);
    }

    public function createActiveUpdate($id){
        $cek = $this->db->select('*')->from('jawara_active')->where('jawara_id', $id)->get()->result();

        if($cek){
            // Kosong
        }else{
            $data = $this->db->select('*')->from('jawara_leads')->where('id', $id)->get()->result()[0];

            $x['jawara_id'] = $id;
            $x['name'] = $data->name;
            $x['shop_name'] = $data->shop_name_dis;
            $x['gmaps'] = $data->gmaps_di;
            $x['is_active'] = 1;

            $this->db->insert('jawara_active', $x);
        }
    }

}
?>