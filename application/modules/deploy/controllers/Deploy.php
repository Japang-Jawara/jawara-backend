<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Deploy extends CI_Controller{

     function __construct(){
          parent::__construct();

          $this->load->model('Deploy_model', 'modelDeploy');
     }

     function index(){
          if($this->session->userdata('is_login') == TRUE){

               $x['getDeploy'] = $this->modelDeploy->getDeploy();

               $this->load->view("include/head");
               $this->load->view("include/top-header");
               $this->load->view('deploy', $x);
               $this->load->view("include/sidebar");
               $this->load->view("include/panel");
               $this->load->view("include/footer");
               $this->load->view("include/alert");
          }else{
               $this->session->set_flashdata('success', 'Upsss!!!, Login dulu ya.');
               redirect('index.php/login');
          }
     }

     // OPEN DEPLOY
     function getOpenDeploy() {
          
          $data = $this->modelDeploy->getOpenDeploy();

          $no = 1;
          $jawara = array();

          foreach($data as $r) {
               
               $timestamp = $r->updated_confirm_dis;
               $jawara_id = $r->id;
               $email_verifikasi = $r->email_verifikasi_dis;
               $founders = $r->name_lend == NULL ? "<font>Funders Tidak Ada</font>" : $r->name_lend;
               $nik_ktp = $r->nik_ktp_kyc == NULL ? "<font>Nik KTP Tidak Ada</font>" : $r->nik_ktp_kyc;
               $name = $r->name_kyc;
               $ibu_kandung = $r->ibu_kandung_dis;
               $phone = $r->phone_kyc;
               $phone_wa = $r->phone_wa_dis;
               $alamat_rumah = $r->alamat_rumah;
               $shop_name = $r->shop_name_dis;
               $shop_location = $r->shop_location_dis;
               $status_kepemilikan = $r->status_kepemilikan_dis;
               $kodepos = $r->kodepos_dis == NULL ? "<font>Kode Pos Tidak Ada</font>" : $r->kodepos_dis;
               $kelurahan = $r->kelurahan_dis == NULL ? "<font>Kelurahan Tidak Ada</font>" : $r->kelurahan_dis;
               $kecamatan = $r->kecamatan_dis == NULL ? "<font>Kecamatan Tidak Ada</font>" : $r->kecamatan_dis;
               $kab_kota = $r->kab_kota_dis == NULL ? "<font>Kab/Kota Tidak Ada</font>" : $r->kab_kota_dis;
               $provinsi = $r->provinsi_dis == NULL ? "<font>Provinsi Tidak Ada</font>" : $r->provinsi_dis;
               $kapasitas_kwh = $r->kapasitas_kwh_dis;
               $produk_beras1 = $r->produk_beras1_dis;
               $produk_beras2 = $r->produk_beras2_dis;
               $produk_ayam = $r->produk_ayam_dis;
               $produk_telur = $r->produk_telur_dis;
               $jumlah_beras = $r->jumlah_beras_dis == NULL ? "0 (PACK)" : $r->jumlah_beras_dis.' (PACK)';
               $jumlah_beras2 = $r->jumlah_beras2_dis == NULL ? "0 (PACK)" : $r->jumlah_beras2_dis.' (PACK)';
               $jumlah_ayam = $r->jumlah_ayam_dis.' (PCS)';
               $jumlah_telur = $r->uom_telur_dis == NULL ? $r->jumlah_telur_dis : $r->jumlah_telur_dis.' ('.$r->uom_telur_dis.')';
               
               if($r->file_terms_dis != NULL){
                    $file_terms = '<a href="'.$r->file_terms_dis.'" target="_blank" class="btn btn-sm btn-secondary">'.$r->file_terms_dis.'</a>';
               }else{
                    $file_terms = 'Tidak ada File Terms';
               }

               $join_telegram = $r->join_telegram_dis;

               $area_jawara = $r->area_jawara == NULL ? "<font>Area Jawara Tidak Ada</font>" : $r->area_jawara;

               $action = "<a href='javascript:;' 
                    data-id='$r->id'
                    data-toggle='modal' data-target='#deploy'
                    class='btn btn-sm btn-success'><i class='fa fas fa-edit'></i> Deploy</a>
               ";


               $jawara[] = array(
                    $no++,
                    $action,
                    $timestamp,
                    $jawara_id,
                    $email_verifikasi,
                    $founders,
                    $nik_ktp,
                    $name,
                    $ibu_kandung,
                    $phone,
                    $phone_wa,
                    $alamat_rumah,
                    $shop_name,
                    $shop_location,
                    $status_kepemilikan,
                    $kodepos,
                    $kelurahan,
                    $kecamatan,
                    $kab_kota,
                    $provinsi,
                    $kapasitas_kwh,
                    $produk_beras1,
                    $produk_beras2,
                    $produk_ayam,
                    $produk_telur,
                    $jumlah_beras,
                    $jumlah_beras2,
                    $jumlah_ayam,
                    $jumlah_telur,
                    $file_terms,
                    $join_telegram,
                    $area_jawara
               );
               
          }
          
          echo json_encode($jawara);
     }

     // DONE DEPLOY
     function getDoneDeploy() {
          
          $data = $this->modelDeploy->getDoneDeploy();

          $no = 1;
          $jawara = array();

          foreach($data as $r) {
               
               $timestamp = $r->created_at_dep;
               $jawara_id = $r->id;
               $email_verifikasi = $r->email_verifikasi_dep;
               $date_delivery = $r->date_delivery_dep;

               if($r->link_surat_dep != NULL){
                    $link_surat = '<a href="'.$r->link_surat_dep.'" target="_blank" class="btn btn-sm btn-secondary">'.$r->link_surat_dep.'</a>';
               }else{
                    $link_surat = 'Tidak ada Surat jalan';
               }

               $status = $r->status_dep;
               $notes = $r->notes_dep;

               $nik_ktp = $r->nik_ktp_kyc;
               $name = $r->name_kyc;
               $spanduk = $r->spanduk_dep == NULL ? "Belum Ada Data" : $r->spanduk_dep;
               $freezer = $r->freezer_dep == NULL ? "Belum Ada Data" : $r->freezer_dep;
               
               $date = date("Y-m-d", strtotime($r->date_delivery_dep));
               $time = date("H:i", strtotime($r->date_delivery_dep));

               $action = "<a href='javascript:;' 
                    data-id='$r->id'
                    data-date_delivery_dep='$date'
                    data-time_delivery_dep='$time'
                    data-link_surat_dep='$r->link_surat_dep'
                    data-status_dep='$r->status_dep'
                    data-spanduk_dep='$r->spanduk_dep'
                    data-freezer_dep='$r->freezer_dep'
                    data-notes_dep='$r->notes_dep'
                    data-toggle='modal' data-target='#edit-deploy'
                    class='btn btn-sm btn-success'><i class='fa fas fa-edit'></i> Edit</a>
               ";

               $jawara[] = array(
                    $no++,
                    $action,
                    $timestamp,
                    $jawara_id,
                    $email_verifikasi,
                    $nik_ktp,
                    $name,
                    $date_delivery,
                    $link_surat,
                    $status,
                    $spanduk,
                    $freezer,
                    $notes
               );
               
          }
          
          echo json_encode($jawara);
     }

     public function createDeploy(){

          $jawara_id = $this->input->post('id');

          $x['email_verifikasi_dep'] = $this->input->post('email');
          $x['date_delivery_dep'] = $this->input->post('date_delivery').' '.$this->input->post('time_delivery');
          $x['link_surat_dep'] = $this->input->post('link_surat');
          $x['status_dep'] = $this->input->post('status_dep');
          $x['notes_dep'] = $this->input->post('notes');
          $x['created_at_dep'] = date('Y-m-d H:i:s');

          if($x['status_dep'] == 'Delivery Success'){
               $this->modelDeploy->createActive($jawara_id);
          }

          $x['spanduk_dep'] = $this->input->post('spanduk_dep');
          $x['freezer_dep'] = $this->input->post('freezer_dep');

          //UPDATE DATA FROM TABLE JAWARA ELIGIBLE
          $u['status_deploy_dis'] = '1';
          $u['updated_deploy_dis'] = date('Y-m-d H:i:s');

          $this->modelDeploy->createDeploy($x, $u, $jawara_id);

          redirect('index.php/deploy');

     }

     public function updateDeploy(){

          $jawara_id = $this->input->post('id');

          $x['status_dep'] = $this->input->post('status_dep');

          if($x['status_dep'] == 'Delivery Success'){
               $this->modelDeploy->createActiveUpdate($jawara_id);
          }

          $x['date_delivery_dep'] = $this->input->post('date_delivery_dep').' '.$this->input->post('time_delivery_dep');
          $x['link_surat_dep'] = $this->input->post('link_surat_dep');

          $x['notes_dep'] = $this->input->post('notes_dep');

          $x['spanduk_dep'] = $this->input->post('spanduk_dep');
          $x['freezer_dep'] = $this->input->post('freezer_dep');

          //UPDATE DATA FROM TABLE JAWARA ELIGIBLE
          $u['updated_deploy_dis'] = date('Y-m-d H:i:s');

          $this->modelDeploy->updateDeploy($x, $u, $jawara_id);

          redirect('index.php/deploy');

     }

}