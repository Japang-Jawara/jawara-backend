<style>
    @media screen and (min-width: 450px) {
        .status {
            margin-top: -4rem;
        }
    }
</style>
<div class="shadow-lg p-3 mb-5 bg-white rounded">
    <div id="content" class="content content-full-width bg-white">
        <!-- begin profile -->
        <div class="profile">
            <div class="profile-header">
                <div class="profile-header-cover"></div>
                <div class="profile-header-content">
                    
                    <div class="profile-header-info">
                        <h2 class="mt-0 mb-1"><b><?= $nama_toko ?></b></h2>
                        <h5 class="mb-2"><?= $jawara->name ?></h5>
                        <a href="#" class="btn btn-xs btn-blue"><?= $founder ?></a>
                    </div>
                    <blockquote class="blockquote text-right status">
                        <a href="#" class="btn btn-xs btn-warning">Pembiayaan</a>
                        <h2><?= $pembiayaan ?></h2>
                    </blockquote>
                </div>
                <ul class="profile-header-tab nav nav-tabs bg-black">
                    <li class="nav-item"><a href="#profile-post" class="nav-link active text-white" data-toggle="tab">DETAIL JAWARA</a></li>
                </ul>
            </div>
        </div>
        <div class="profile-content bg-white">
            <div class="panel-body ">
                <div class="panel-group">
                    <h4 class="text-center">Status Jawara</h4>
                    <p class="text-center">Berikut ini detail dari JAWARA</p>
                    <div class="panel panel-default">
                        <div class="panel-heading bg-white">
                            <div class="table-responsive">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" href="#collapse1">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th scope="col">NIK KTP</th>
                                                    <th scope="col">NAME</th>
                                                    <th scope="col">PHONE</th>
                                                    <th scope="col">KABUPATEN/KOTA</th>
                                                    <th scope="col">PROVINSI</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><?=$dataJawara->nik_ktp ?></td>
                                                    <td><?=$dataJawara->name ?> </td>
                                                    <td><?=$dataJawara->phone ?> </td>
                                                    <td><?=$dataJawara->kab_kota ?> </td>
                                                    <td><?=$dataJawara->provinsi ?> </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </a>
                                </h4>
                            </div>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse">
                            <div class="row">
                                <div class="col">
                                    <div class="panel-heading bg-black">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" href="#collapse4" class="text-white"><b>DATA FINAL</b></a>
                                        </h4>
                                    </div>
                                    <div id="collapse4" class="panel-collapse collapse">
                                        <div class="table-responsive">
                                            <table id="tbl-profile" class="table table-striped table-bordered table-td-valign-middle" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>EMAIL VERIFIKASI</th>
                                                        <th>FUNDERS</th>
                                                        <th>NIK KTP</th>
                                                        <th>NAMA</th>
                                                        <th>NAMA IBU KANDUNG</th>
                                                        <th>PHONE</th>
                                                        <th>PHONE WHATSAPP</th>
                                                        <th>ALAMAT RUMAH</th>
                                                        <th>NAMA USAHA</th>
                                                        <th>LOKASI USAHA</th>
                                                        <th>STATUS KEPEMILIKAN</th>
                                                        <th>KODE POS</th>
                                                        <th>KELURAHAN</th>
                                                        <th>KECAMATAN</th>
                                                        <th>KAB/KOTA</th>
                                                        <th>PROVINSI</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="panel-heading bg-yellow">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" href="#collapse3"><b>HISTORY</b></a>
                                        </h4>
                                    </div>
                                    <div id="collapse3" class="panel-collapse collapse show">
                                        <ul class="timeline">
                                            <?= $history ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading bg-blue">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" href="#collapse2" class="text-white"><b>PAKET JAWARA</b></a>
                            </h4>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse">
                            <div class="table-responsive">
                                <table id="tbl-produk" class="table table-striped table-bordered table-td-valign-middle" width="100%">
                                    <thead>
                                        <tr>
                                            <th>PRODUK BERAS (5KG)</th>
                                            <th>PRODUK BERAS (10KG)</th>
                                            <th>PRODUK AYAM</th>
                                            <th>PRODUK TELUR</th>
                                            <th>JUMLAH BERAS</th>
                                            <th>JUMLAH AYAM</th>
                                            <th>JUMLAH TELUR</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
<script src="<?php echo base_url('assets/js/app-manage.js');?>"></script>