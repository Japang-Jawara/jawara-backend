<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller{
     function __construct(){
          parent::__construct();

          $this->load->model('Profile_model', 'modelProfile');
     }

     function index(){
          if($this->session->userdata('is_login') == TRUE){

               $id = $this->input->get('id');
               $x['id'] = $id;

               $x['jawara'] = $this->modelProfile->getJawaraId($id)[0];

               $kyc = $this->modelProfile->getJawaraKyc($id);
               $digital = $this->modelProfile->getJawaraDigital($id);
               $score = $this->modelProfile->getJawaraScore($id);
               $eligible = $this->modelProfile->getJawaraEligible($id);
               $disbursement = $this->modelProfile->getJawaraDisbursementId($id);
               $deploy = $this->modelProfile->getJawaraDeploy($id);

               // Pembiayaan, Nama Toko dan Data Lengkap Jawara
               if($disbursement[0]->shop_name_dis == NULL){
                    $pembiayaan = "Rp " . number_format(0,2,',','.');
                    $nama_toko = 'Belum Ada';
                    $data_jawara = $this->modelProfile->getJawaraId($id)[0];
                    $founder = 'Funder Not Yet';
               }else{
                    $pembiayaan = "Rp " . number_format($disbursement[0]->pembiayaan_dis,2,',','.');
                    $nama_toko = $disbursement[0]->shop_name_dis;
                    $data_jawara = $disbursement[0];
                    $founder = $eligible[0]->name_lend;
               }

               $x['pembiayaan'] = $pembiayaan;
               $x['nama_toko'] = $nama_toko;
               $x['dataJawara'] = $data_jawara;
               $x['founder'] = $founder;

               // End Pembiayaan, Nama Toko dan Data Lengkap Jawara

               $include1 = "
                    <li>
                         <div class='timeline-time'>
               ";
               $include2 = "
                         </div>
                         <div class='timeline-icon'>
                              <a href='javascript:;'>&nbsp;</a>
                         </div>
                         <div class='timeline-body'>
                              <div class='timeline-header'>
               ";
               $include3 = "
                              </div>
                         </div>
                    </li>
               ";

               // History
               if($kyc[0]->status_jawara_kyc != NULL){
                    if($kyc[0]->status_jawara_kyc == 1 || $kyc[0]->status_jawara_kyc == 2){
                         if($kyc[0]->status_digital_inspection_kyc == 1){
                              // DIGITAL INSPECTION
                              if($digital[0]->status_jawara_di == 1){
                                   if($digital[0]->status_eligible == 1){
                                        $history = "
                                             ".$include1."
                                                  <span class='date'>".date('d-m-Y', strtotime($kyc[0]->created_at_kyc))."</span>
                                                  <span class='time'>".date('H:i', strtotime($kyc[0]->created_at_kyc))."</span>
                                             ".$include2."
                                                  <span class='username'><a href='javascript:;'>DONE KYC</a> <small></small></span>
                                             ".$include3."
                                             
                                             ".$include1."
                                                  <span class='date'>".date('d-m-Y', strtotime($kyc[0]->updated_di_kyc))."</span>
                                                  <span class='time'>".date('H:i', strtotime($kyc[0]->updated_di_kyc))."</span>
                                             ".$include2."
                                                  <span class='username'><a href='javascript:;'>DONE DIGITAL INSPECTION (".$digital[0]->user_grading_di.")</a> <small></small></span>
                                             ".$include3." 

                                             ".$include1."
                                                  <span class='date'>".date('d-m-Y', strtotime($eligible[0]->created_at_lend))."</span>
                                                  <span class='time'>".date('H:i', strtotime($eligible[0]->created_at_lend))."</span>
                                             ".$include2."
                                                  <span class='username'><a href='javascript:;'>DONE ELIGIBLE</a> <small></small></span>
                                             ".$include3."
                                        ";
                                   }else{
                                        $history = "
                                             ".$include1."
                                                  <span class='date'>".date('d-m-Y', strtotime($kyc[0]->created_at_kyc))."</span>
                                                  <span class='time'>".date('H:i', strtotime($kyc[0]->created_at_kyc))."</span>
                                             ".$include2."
                                                  <span class='username'><a href='javascript:;'>DONE KYC</a> <small></small></span>
                                             ".$include3."
                                             
                                             ".$include1."
                                                  <span class='date'>".date('d-m-Y', strtotime($kyc[0]->updated_di_kyc))."</span>
                                                  <span class='time'>".date('H:i', strtotime($kyc[0]->updated_di_kyc))."</span>
                                             ".$include2."
                                                  <span class='username'><a href='javascript:;'>DONE DIGITAL INSPECTION (".$digital[0]->user_grading_di.")</a> <small></small></span>
                                             ".$include3." 

                                             ".$include1."
                                                  <span class='date'>".date('d-m-Y')."</span>
                                                  <span class='time'>".date('H:i')."</span>
                                             ".$include2."
                                                  <span class='username'><a href='javascript:;'>PENDING ELIGIBLE</a> <small></small></span>
                                             ".$include3." 
                                        ";
                                   }

                              // SCORE CARD
                              }else{
                                   if($digital[0]->status_ground_di == 1){
                                        if($score[0]->status_score_sc ==1){
                                             if($score[0]->status_eligible == 1){
                                                  $history = "
                                                       ".$include1."
                                                            <span class='date'>".date('d-m-Y', strtotime($kyc[0]->created_at_kyc))."</span>
                                                            <span class='time'>".date('H:i', strtotime($kyc[0]->created_at_kyc))."</span>
                                                       ".$include2."
                                                            <span class='username'><a href='javascript:;'>DONE KYC</a> <small></small></span>
                                                       ".$include3."
                                                       
                                                       ".$include1."
                                                            <span class='date'>".date('d-m-Y', strtotime($kyc[0]->updated_di_kyc))."</span>
                                                            <span class='time'>".date('H:i', strtotime($kyc[0]->updated_di_kyc))."</span>
                                                       ".$include2."
                                                            <span class='username'><a href='javascript:;'>DONE DIGITAL INSPECTION (".$digital[0]->user_grading_di.")</a> <small></small></span>
                                                       ".$include3." 

                                                       ".$include1."
                                                            <span class='date'>".date('d-m-Y', strtotime($score[0]->created_at_sc))."</span>
                                                            <span class='time'>".date('H:i', strtotime($score[0]->created_at_sc))."</span>
                                                       ".$include2."
                                                            <span class='username'><a href='javascript:;'>DONE SCORE CARD (".$digital[0]->user_grading_sc.")</a> <small></small></span>
                                                       ".$include3."

                                                       ".$include1."
                                                            <span class='date'>".date('d-m-Y', strtotime($eligible[0]->created_at_lend))."</span>
                                                            <span class='time'>".date('H:i', strtotime($eligible[0]->created_at_lend))."</span>
                                                       ".$include2."
                                                            <span class='username'><a href='javascript:;'>DONE ELIGIBLE</a> <small></small></span>
                                                       ".$include3."
                                                  ";
                                             }else{
                                                  $history = "
                                                       ".$include1."
                                                            <span class='date'>".date('d-m-Y', strtotime($kyc[0]->created_at_kyc))."</span>
                                                            <span class='time'>".date('H:i', strtotime($kyc[0]->created_at_kyc))."</span>
                                                       ".$include2."
                                                            <span class='username'><a href='javascript:;'>DONE KYC</a> <small></small></span>
                                                       ".$include3."
                                                       
                                                       ".$include1."
                                                            <span class='date'>".date('d-m-Y', strtotime($kyc[0]->updated_di_kyc))."</span>
                                                            <span class='time'>".date('H:i', strtotime($kyc[0]->updated_di_kyc))."</span>
                                                       ".$include2."
                                                            <span class='username'><a href='javascript:;'>DONE DIGITAL INSPECTION (".$digital[0]->user_grading_di.")</a> <small></small></span>
                                                       ".$include3." 

                                                       ".$include1."
                                                            <span class='date'>".date('d-m-Y', strtotime($score[0]->created_at_sc))."</span>
                                                            <span class='time'>".date('H:i', strtotime($score[0]->created_at_sc))."</span>
                                                       ".$include2."
                                                            <span class='username'><a href='javascript:;'>DONE SCORE CARD (".$digital[0]->user_grading_sc.")</a> <small></small></span>
                                                       ".$include3."

                                                       ".$include1."
                                                            <span class='date'>".date('d-m-Y')."</span>
                                                            <span class='time'>".date('H:i')."</span>
                                                       ".$include2."
                                                            <span class='username'><a href='javascript:;'>PENDING ELIGIBLE</a> <small></small></span>
                                                       ".$include3." 
                                                  ";
                                             }
                                        }else{
                                             $history = "
                                                  ".$include1."
                                                       <span class='date'>".date('d-m-Y', strtotime($kyc[0]->created_at_kyc))."</span>
                                                       <span class='time'>".date('H:i', strtotime($kyc[0]->created_at_kyc))."</span>
                                                  ".$include2."
                                                       <span class='username'><a href='javascript:;'>DONE KYC</a> <small></small></span>
                                                  ".$include3."
                                                  
                                                  ".$include1."
                                                       <span class='date'>".date('d-m-Y', strtotime($kyc[0]->updated_di_kyc))."</span>
                                                       <span class='time'>".date('H:i', strtotime($kyc[0]->updated_di_kyc))."</span>
                                                  ".$include2."
                                                       <span class='username'><a href='javascript:;'>DONE DIGITAL INSPECTION (".$digital[0]->user_grading_di.")</a> <small></small></span>
                                                  ".$include3."

                                                  ".$include1."
                                                       <span class='date'>".date('d-m-Y', strtotime($score[0]->created_at_sc))."</span>
                                                       <span class='time'>".date('H:i', strtotime($score[0]->created_at_sc))."</span>
                                                  ".$include2."
                                                       <span class='username'><a href='javascript:;'>DONE SCORE CARD DENGAN SCORE (".$digital[0]->user_grading_sc.") DAN TIDAK DAPAT DILANJUTKAN</a> <small></small></span>
                                                  ".$include3." 
                                             ";
                                        }
                                   }else{
                                        $history = "
                                             ".$include1."
                                                  <span class='date'>".date('d-m-Y', strtotime($kyc[0]->created_at_kyc))."</span>
                                                  <span class='time'>".date('H:i', strtotime($kyc[0]->created_at_kyc))."</span>
                                             ".$include2."
                                                  <span class='username'><a href='javascript:;'>DONE KYC</a> <small></small></span>
                                             ".$include3."
                                             
                                             ".$include1."
                                                  <span class='date'>".date('d-m-Y', strtotime($kyc[0]->updated_di_kyc))."</span>
                                                  <span class='time'>".date('H:i', strtotime($kyc[0]->updated_di_kyc))."</span>
                                             ".$include2."
                                                  <span class='username'><a href='javascript:;'>DONE DIGITAL INSPECTION (".$digital[0]->user_grading_di.")</a> <small></small></span>
                                             ".$include3."

                                             ".$include1."
                                                  <span class='date'>".date('d-m-Y')."</span>
                                                  <span class='time'>".date('H:i')."</span>
                                             ".$include2."
                                                  <span class='username'><a href='javascript:;'>NOT YET SCORE CARD</a> <small></small></span>
                                             ".$include3." 
                                        ";
                                   }
                              }
                         }else{
                              $history = "
                                   ".$include1."
                                        <span class='date'>".date('d-m-Y', strtotime($kyc[0]->created_at_kyc))."</span>
                                        <span class='time'>".date('H:i', strtotime($kyc[0]->created_at_kyc))."</span>
                                   ".$include2."
                                        <span class='username'><a href='javascript:;'>DONE KYC</a> <small></small></span>
                                   ".$include3."
                                   
                                   ".$include1."
                                        <span class='date'>".date('d-m-Y')."</span>
                                        <span class='time'>".date('H:i')."</span>
                                   ".$include2."
                                        <span class='username'><a href='javascript:;'>NOT YET DIGITAL INSPECTION</a> <small></small></span>
                                   ".$include3." 
                              ";
                         }
                    }else{
                         $history = "
                              ".$include1."
                                   <span class='date'>".date('d-m-Y')."</span>
                                   <span class='time'>".date('H:i')."</span>
                              ".$include2."
                                   <span class='username'><a href='javascript:;'>RESULT KYC REJECT</a> <small></small></span>
                              ".$include3." 
                         ";
                    }
               }else{
                    $history = "
                         ".$include1."
                              <span class='date'>".date('d-m-Y')."</span>
                              <span class='time'>".date('H:i')."</span>
                         ".$include2."
                              <span class='username'><a href='javascript:;'>NOT YET KYC</a> <small></small></span>
                         ".$include3." 
                    ";
               }

               if($eligible[0]->status_lend != NULL){
                    if($eligible[0]->status_lend != NULL){
                         if($eligible[0]->status_lend == 1){
                              if($eligible[0]->status_disbursement == 1){  
                                   if($disbursement[0]->status_confirm_dis == 1){
                                        if($disbursement[0]->status_deploy_dis == 1){
                                             $history2 = "
                                                  ".$include1."
                                                       <span class='date'>".date('d-m-Y', strtotime($eligible[0]->updated_lend))."</span>
                                                       <span class='time'>".date('H:i', strtotime($eligible[0]->updated_lend))."</span>
                                                  ".$include2."
                                                       <span class='username'><a href='javascript:;'>APPROVE FROM FUNDERS</a> <small></small></span>
                                                  ".$include3."
                                                  
                                                  ".$include1."
                                                       <span class='date'>".date('d-m-Y', strtotime($eligible[0]->created_at_dis))."</span>
                                                       <span class='time'>".date('H:i', strtotime($eligible[0]->created_at_dis))."</span>
                                                  ".$include2."
                                                       <span class='username'><a href='javascript:;'>DONE DISBURSEMENTS PROGRESS</a> <small></small></span>
                                                  ".$include3."

                                                  ".$include1."
                                                       <span class='date'>".date('d-m-Y', strtotime($disbursement[0]->updated_confirm_dis))."</span>
                                                       <span class='time'>".date('H:i', strtotime($disbursement[0]->updated_confirm_dis))."</span>
                                                  ".$include2."
                                                       <span class='username'><a href='javascript:;'>DONE DISBURSEMENTS CONFIRM</a> <small></small></span>
                                                  ".$include3."

                                                  ".$include1."
                                                       <span class='date'>".date('d-m-Y', strtotime($disbursement[0]->updated_deploy_dis))."</span>
                                                       <span class='time'>".date('H:i', strtotime($disbursement[0]->updated_deploy_dis))."</span>
                                                  ".$include2."
                                                       <span class='username'><a href='javascript:;'>FINISH DEPLOY</a> <small></small></span>
                                                  ".$include3."
                                             ";
                                        }else{
                                             $history2 = "
                                                  ".$include1."
                                                       <span class='date'>".date('d-m-Y', strtotime($eligible[0]->updated_lend))."</span>
                                                       <span class='time'>".date('H:i', strtotime($eligible[0]->updated_lend))."</span>
                                                  ".$include2."
                                                       <span class='username'><a href='javascript:;'>APPROVE FROM FUNDERS</a> <small></small></span>
                                                  ".$include3."
                                                  
                                                  ".$include1."
                                                       <span class='date'>".date('d-m-Y', strtotime($eligible[0]->created_at_dis))."</span>
                                                       <span class='time'>".date('H:i', strtotime($eligible[0]->created_at_dis))."</span>
                                                  ".$include2."
                                                       <span class='username'><a href='javascript:;'>DONE DISBURSEMENTS PROGRESS</a> <small></small></span>
                                                  ".$include3."

                                                  ".$include1."
                                                       <span class='date'>".date('d-m-Y', strtotime($disbursement[0]->updated_confirm_dis))."</span>
                                                       <span class='time'>".date('H:i', strtotime($disbursement[0]->updated_confirm_dis))."</span>
                                                  ".$include2."
                                                       <span class='username'><a href='javascript:;'>DONE DISBURSEMENTS CONFIRM</a> <small></small></span>
                                                  ".$include3."

                                                  ".$include1."
                                                       <span class='date'>".date('d-m-Y')."</span>
                                                       <span class='time'>".date('H:i')."</span>
                                                  ".$include2."
                                                       <span class='username'><a href='javascript:;'>NOT YET DEPLOY</a> <small></small></span>
                                                  ".$include3."
                                             ";
                                        }
                                   }else{
                                        $history2 = "
                                             ".$include1."
                                                  <span class='date'>".date('d-m-Y', strtotime($eligible[0]->updated_lend))."</span>
                                                  <span class='time'>".date('H:i', strtotime($eligible[0]->updated_lend))."</span>
                                             ".$include2."
                                                  <span class='username'><a href='javascript:;'>APPROVE FROM FUNDERS</a> <small></small></span>
                                             ".$include3."

                                             ".$include1."
                                                  <span class='date'>".date('d-m-Y', strtotime($eligible[0]->created_at_dis))."</span>
                                                  <span class='time'>".date('H:i', strtotime($eligible[0]->created_at_dis))."</span>
                                             ".$include2."
                                                  <span class='username'><a href='javascript:;'>DONE DISBURSEMENTS PROGRESS</a> <small></small></span>
                                             ".$include3."

                                             ".$include1."
                                                  <span class='date'>".date('d-m-Y')."</span>
                                                  <span class='time'>".date('H:i')."</span>
                                             ".$include2."
                                                  <span class='username'><a href='javascript:;'>NOT YET DISBURSEMENT CONFIRM</a> <small></small></span>
                                             ".$include3."
                                        ";
                                   }
                              }else{
                                   $history2 = "
                                        ".$include1."
                                             <span class='date'>".date('d-m-Y', strtotime($eligible[0]->updated_lend))."</span>
                                             <span class='time'>".date('H:i', strtotime($eligible[0]->updated_lend))."</span>
                                        ".$include2."
                                             <span class='username'><a href='javascript:;'>APPROVE FROM FUNDERS</a> <small></small></span>
                                        ".$include3."

                                        ".$include1."
                                             <span class='date'>".date('d-m-Y')."</span>
                                             <span class='time'>".date('H:i')."</span>
                                        ".$include2."
                                             <span class='username'><a href='javascript:;'>NOT YET DISBURSEMENT</a> <small></small></span>
                                        ".$include3." 
                                   ";
                              }
                         }else{
                              $history2 = "
                                   ".$include1."
                                        <span class='date'>".date('d-m-Y', strtotime($eligible[0]->updated_lend))."</span>
                                        <span class='time'>".date('H:i', strtotime($eligible[0]->updated_lend))."</span>
                                   ".$include2."
                                        <span class='username'><a href='javascript:;'>REJECT FROM FUNDERS</a> <small></small></span>
                                   ".$include3." 
                              ";
                         }
                    }else{
                         $history2 = "
                              ".$include1."
                                   <span class='date'>".date('d-m-Y')."</span>
                                   <span class='time'>".date('H:i')."</span>
                              ".$include2."
                                   <span class='username'><a href='javascript:;'>NO RESPONSE FROM FUNDERS</a> <small></small></span>
                              ".$include3." 
                         ";
                    }
               }else{
                    $history2 = '';
               }
               // End History

               $x['history'] = $history; 
               $x['history2'] = $history2; 

               $this->load->view("include/head");
               $this->load->view("include/top-header");
               $this->load->view('detail', $x);
               $this->load->view("include/sidebar");
               $this->load->view("include/footer");
               $this->load->view("include/alert");
          }else{
               $this->session->set_flashdata('success', 'Upsss!!!, Login dulu ya.');
               redirect('index.php/login');
          }
     }

     function getProduk() {

          $id = $this->input->get('id');
          
          $data = $this->modelProfile->getProfile($id);

          $no = 1;
          $jawara = array();

          foreach($data as $r) {
               
               $produk_beras1 = $r->produk_beras1_dis;
               $produk_beras2 = $r->produk_beras2_dis;
               $produk_ayam = $r->produk_ayam_dis;
               $produk_telur = $r->produk_telur_dis;
               $jumlah_beras = $r->jumlah_beras_dis;
               $jumlah_ayam = $r->jumlah_ayam_dis;
               $jumlah_telur = $r->jumlah_telur_dis;


               $jawara[] = array(
                    $produk_beras1,
                    $produk_beras2,
                    $produk_ayam,
                    $produk_telur,
                    $jumlah_beras,
                    $jumlah_ayam,
                    $jumlah_telur
               );
               
          }
          
          echo json_encode($jawara);
     }

     function getProfile() {
          
          $id = $this->input->get('id');
          $data = $this->modelProfile->getProfile($id);

          $no = 1;
          $jawara = array();

          foreach($data as $r) {
               
               $email_verifikasi = $r->email_verifikasi_dis;
               $founders = $r->name_lend == NULL ? "<font>Funders Tidak Ada</font>" : $r->name_lend;
               $nik_ktp = $r->nik_ktp_dis == NULL ? "<font>Nik KTP Tidak Ada</font>" : $r->nik_ktp_dis;
               $name = $r->name_dis;
               $ibu_kandung = $r->ibu_kandung_dis;
               $phone = $r->phone_dis;
               $phone_wa = $r->phone_wa_dis;
               $alamat_rumah = $r->alamat_rumah_dis;
               $shop_name = $r->shop_name_dis;
               $shop_location = $r->shop_location_dis;
               $status_kepemilikan = $r->status_kepemilikan_dis;
               $kodepos = $r->kodepos_dis == NULL ? "<font>Kode Pos Tidak Ada</font>" : $r->kodepos_dis;
               $kelurahan = $r->kelurahan_dis == NULL ? "<font>Kelurahan Tidak Ada</font>" : $r->kelurahan_dis;
               $kecamatan = $r->kecamatan_dis == NULL ? "<font>Kecamatan Tidak Ada</font>" : $r->kecamatan_dis;
               $kab_kota = $r->kab_kota_dis == NULL ? "<font>Kab/Kota Tidak Ada</font>" : $r->kab_kota_dis;
               $provinsi = $r->provinsi_dis == NULL ? "<font>Provinsi Tidak Ada</font>" : $r->provinsi_dis;


               $jawara[] = array(
                    $email_verifikasi,
                    $founders,
                    $nik_ktp,
                    $name,
                    $ibu_kandung,
                    $phone,
                    $phone_wa,
                    $alamat_rumah,
                    $shop_name,
                    $shop_location,
                    $status_kepemilikan,
                    $kodepos,
                    $kelurahan,
                    $kecamatan,
                    $kab_kota,
                    $provinsi
               );
               
          }
          
          echo json_encode($jawara);
     }

}