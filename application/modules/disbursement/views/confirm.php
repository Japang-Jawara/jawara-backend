<div id="content" class="content">
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="<?php echo base_url('home');?>">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="<?php echo base_url('kepegawaian/thl');?>">DISBURSEMENT CONFIRM</a></li>
    </ol>
    <h1 class="page-header"><b>DATA DISBURSEMENT CONFIRM</b></h1>
    <div class="note note-info note-with-right-icon">
        <div class="note-icon"><i class="fa fa-lightbulb"></i></div>
        <div class="note-content text-right">
            <h2><b>Disbursement Confirm</b></h2>
            <p>Konfirmasi tanggal pencairan dari funders.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <?php if($this->session->userdata('role') == 'Admin' || $this->session->userdata('role') == 'SuperAdmin' || $this->session->userdata('role') == 'Kepegawaian'){?>
                        <a href="" class="btn btn-icon btn-sm btn-inverse" data-toggle="modal" data-target="#addthl"><i class="fa fa-plus-square"></i></a>
                        <?php } ?>
                    </h4>
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                </div>

                <div class="panel-body">
                    <div class ="table-responsive">
                        <div class="col-xl-12">
                            <ul class="nav nav-tabs nav-tabs-inverse nav-justified nav-justified-mobile" data-sortable-id="index-2">
                                <li class="nav-item">
                                    <a href="#bm" data-toggle="tab" class="nav-link active">
                                        <span><i class="fa fa-spinner fa-lg m-r-5"></i> PENDING</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#dt" data-toggle="tab" class="nav-link">
                                        <span><i class="fa fa-check-circle fa-lg m-r-5"></i> ACCEPT</span>
                                    </a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane fade active show" id="bm">
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table id="tbl-confirm" class="table table-striped table-bordered table-td-valign-middle" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>NO</th>
                                                        <th>EMAIL VERIFIKASI</th>
                                                        <th>JAWARA ID</th>
                                                        <th>FUNDERS</th>
                                                        <th>PROVINSI</th>
                                                        <th>KAB/KOTA</th>
                                                        <th>KECAMATAN</th>
                                                        <th>KELURAHAN</th>
                                                        <th>KODE POS</th>
                                                        <th>LOKASI USAHA</th>
                                                        <th>NIK KTP</th>
                                                        <th>NAMA</th>
                                                        <th>NAMA IBU KANDUNG</th>
                                                        <th>PHONE</th>
                                                        <th>ACTION</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="dt">
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table id="tbl-confirm-accept" class="table table-striped table-bordered table-td-valign-middle" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>NO</th>
                                                        <th>EMAIL VERIFIKASI</th>
                                                        <th>JAWARA ID</th>
                                                        <th>TANGGAL PENCAIRAN</th>
                                                        <th>FUNDERS</th>
                                                        <th>PROVINSI</th>
                                                        <th>KAB/KOTA</th>
                                                        <th>KECAMATAN</th>
                                                        <th>KELURAHAN</th>
                                                        <th>KODE POS</th>
                                                        <th>LOKASI USAHA</th>
                                                        <th>NIK KTP</th>
                                                        <th>NAMA</th>
                                                        <th>NAMA IBU KANDUNG</th>
                                                        <th>PHONE</th>
                                                        <th>PEMBIAYAAN</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end panel-body -->
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-10 -->
    </div>

    <div class="modal fade" id="confirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">KONFIRMASI PENCAIRAN</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="disbursement/createConfirm" enctype="multipart/form-data" method="POST">
                        
                        <div class="row">
                            <input type="hidden" name="autocomplete" id="field-autocomplete">
                            <input class="form-control" type="hidden" name="id" id="idx" />

                            <div class="form-group col-xl-12">
                                <label class="col-form-label">Tanggal Konfirmasi Pencairan <span class="text-danger">*</span></label>
                                <input type="date" class="form-control" name="tanggal_pencairan" id="tanggal_pencairan" autocomplete="off" required />
                            </div>

                            <div class="form-group col-xl-12">
                                <label class="col-form-label">Pembiayaan <span class="text-danger">*</span></label>
                                <input type="text" pattern="^[0-9]{4,}$" class="form-control" name="pembiayaan" id="pembiayaan" autocomplete="off" required />
                            </div>

                        </div>
                        </br>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="reset" name="reset" class="btn btn-info"><i class="fa fa-cut"></i> Reset</button>
                            <button type="submit" class="btn btn-primary" value="Cek"><i class="fa fa-save"></i> Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Manage -->
    <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/app-manage.js');?>"></script>

    
