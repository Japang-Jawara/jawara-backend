<div id="content" class="content">
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="<?php echo base_url('home');?>">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="<?php echo base_url('kepegawaian/thl');?>">PRODUCT REQUEST</a></li>
    </ol>
    <h1 class="page-header"><b>PRODUCT REQUEST</b></h1>
    <div class="note note-info note-with-right-icon">
        <div class="note-icon"><i class="fa fa-lightbulb"></i></div>
        <div class="note-content text-right">
            <h2><b>PRODUCT REQUEST</b></h2>
            <p>Request product disetiap jawara.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">
                    </h4>
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                </div>

                <div class="panel-body">
                    <div class ="table-responsive">
                        <table id="tbl-disbursement" class="table table-striped table-bordered table-td-valign-middle" width="100%">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>JAWARA ID</th>
                                    <th>NIK KTP</th>
                                    <th>PHONE</th>
                                    <th>NAME</th>
                                    <th>ALAMAT LENGKAP RUMAH</th>
                                    <th>ALAMAT USAHA</th>
                                    <th>KODEPOS</th>
                                    <th>KECAMATAN</th>
                                    <th>KABUPATEN/KOTA</th>
                                    <th>PROVINSI</th>
                                    <th>FUNDERS</th>
                                    <th>STATUS</th>
                                    <th>ACTION</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!-- end panel-body -->
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-10 -->
    </div>

    <div class="modal fade" id="disbursement" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">DISBURSEMENT</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="disbursement/createDisbursement" enctype="multipart/form-data" method="POST">
                        
                        <div class="row">
                            <input type="hidden" name="autocomplete" id="field-autocomplete">
                            <input class="form-control" type="hidden" name="id" id="idx" />
                            <input class="form-control" type="hidden" name="code_lend" id="code_lend" />
                            <input class="form-control" type="hidden" name="status_lend" id="status_lend" />

                            <div class="form-group col-xl-12">
                                <label class="col-form-label">Email Verifikator <span class="text-danger">*</span></label>
                                <input type="email" class="form-control" name="email" id="email" placeholder="Email Verifikator" autocomplete="off" value="<?= $this->session->userdata('email_address'); ?>" required readonly>
                            </div>

                            <div class="form-group col-xl-6">
                                <label class="col-form-label">Funders <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="founders" id="founders" placeholder="Founders" autocomplete="off" required readonly>
                            </div>

                            <div class="form-group col-xl-6">
                                <label class="col-form-label">Sign Financial Institution Contract <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="status_lends" autocomplete="off" required readonly>
                            </div>

                            <div class="form-group col-xl-12">
                                <label class="col-form-label">No KTP Pemilik <span class="text-danger">*</span></label>
                                <input type="text" pattern="^[0-9]{16,}$" class="form-control" name="nik_ktp" id="nik_ktp" placeholder="No KTP Pemilik" autocomplete="off" required>
                            </div>
                             
                            <div class="form-group col-xl-6">
                                <label class="col-form-label">Nama Pemilik <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="Nama jawara" autocomplete="off" required>
                            </div>

                            <div class="form-group col-xl-6">
                                <label class="col-form-label">Nama Ibu kandung <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="ibu_kandung" id="ibu_kandung" placeholder="Nama Ibu Kandung" autocomplete="off" required>
                            </div>

                            <div class="form-group col-xl-6">
                                <label class="col-form-label">No Telp Aktif <span class="text-danger">*</span></label>
                                <input type="tel" pattern="^628[0-9]{8,}$" class="form-control" name="phone" id="phone" placeholder="628xxxxxxxxx" autocomplete="off" required>
                                <span style="font-size: 12px;">Note: 628xxxxxxxxx (Terdiri dari panjang minimal 10 dan selalu dimulai dengan 628)</span>
                            </div>

                            <div class="form-group col-xl-6">
                                <label class="col-form-label">No Telp WA <span class="text-danger">*</span></label>
                                <input type="tel" pattern="^628[0-9]{8,}$" class="form-control" name="phone_wa" id="phone_wa" placeholder="628xxxxxxxxx" autocomplete="off" required>
                                <span style="font-size: 12px;">Note: 628xxxxxxxxx (Terdiri dari panjang minimal 10 dan selalu dimulai dengan 628)</span>
                            </div>

                            <div class="form-group col-xl-12">
                                <label class="col-form-label">Alamat Lengkap Rumah <span class="text-danger">*</span></label>
                                <textarea class="form-control" name="alamat_rumah" id="alamat_rumah" required></textarea>
                            </div>

                            <div class="form-group col-xl-12">
                                <label class="col-form-label">Nama Toko <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="shop_name" id="shop_name" placeholder="Nama Toko" autocomplete="off" required>
                            </div>

                            <div class="form-group col-xl-12">
                                <label class="col-form-label">Lokasi (Alamat Toko) <span class="text-danger">*</span></label>
                                <textarea class="form-control" name="shop_location" id="shop_location" required></textarea>
                            </div>

                            <div class="form-group col-xl-12">
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Status Kepimilkan <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getSKT as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="status_kepemilikan" id="status_kepemilikan" value="<?= $r->name; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                            </div>
                            
                            <div class="form-group col-xl-6">
                                <label class="col-form-label">Kode Pos <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="kodepos" id="kodepos" placeholder="Kecamatan Toko" autocomplete="off" required>
                            </div>
                            
                            <div class="form-group col-xl-6">
                                <label class="col-form-label">Kelurahan Toko <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="kelurahan" id="kelurahan" placeholder="Kelurahan Toko" autocomplete="off" required>
                            </div>

                            <div class="form-group col-xl-6">
                                <label class="col-form-label">Kecamatan Toko <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="kecamatan" id="kecamatan" placeholder="Kecamatan Toko" autocomplete="off" required>
                            </div>

                            <div class="form-group col-xl-6">
                                <label class="col-form-label">Kab/Kota Toko <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="kab_kota" id="kab_kota" placeholder="Kecamatan Toko" autocomplete="off" required>
                            </div>

                            <div class="form-group col-xl-12">
                                <label class="col-form-label">Provinsi Toko <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="provinsi" id="provinsi" placeholder="Provinsi Toko" autocomplete="off" required>
                            </div>

                            <div class="form-group col-xl-12">
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Kapsistas KWH Listrik <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getKKL as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="kapasitas_kwh" id="kapasitas_kwh" value="<?= $r->name; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                            </div>

                            <div class="form-group col-xl-12">
                                <br>
                                <h5>Product Request</h5>
                                <p>untuk periode pertama</p>
                                <p><b>Produk Yang diinginkan</b></p>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Beras (5KG) <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getPR as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="produk_beras1" id="produk_beras1" value="<?= $r->name; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Beras (10KG) <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getPR as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="produk_beras2" id="produk_beras2" value="<?= $r->name; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Ayam <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getPR as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="produk_ayam" id="produk_ayam" value="<?= $r->name; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Telur <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getPR as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="produk_telur" id="produk_telur" value="<?= $r->name; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Jumlah Beras <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getJB as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="jumlah_beras" id="jumlah_beras" value="<?= $r->name; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Jumlah Ayam <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getJA as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="jumlah_ayam" id="jumlah_ayam" value="<?= $r->name; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Jumlah Telur <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getJT as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="jumlah_telur" id="jumlah_telur" value="<?= $r->name; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                            </div>

                            <div class="form-group col-xl-12">
                                <!-- begin form-group -->
                                <h5>Sign Trading Terms</h5>
                                <br>
                                <div class="form-group row m-b-10">
                                    <label class="col-lg-3 text-lg-right col-form-label">Sign Trading Terms</label>
                                    <div class="col-lg-9 col-xl-6">
                                        <input type="file" name="file_terms" accept="image/png, image/jpg, image/jpeg, application/pdf" class="form-control"/>
                                        <span style="font-size: 12px;">Note : Ukuran Foto max. 2000kb.</span>
                                    </div>
                                </div>
                                <!-- end form-group -->
                            </div>

                            <div class="form-group col-xl-12">
                                <br>
                                <h5>Join Telegram Channel</h5>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Join Telegram Channel <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getJCT as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="join_telegram" id="join_telegram" value="<?= $r->name; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                            </div>
                                         
                        </div>
                        
                        </br>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="reset" name="reset" class="btn btn-info"><i class="fa fa-cut"></i> Reset</button>
                            <button type="submit" class="btn btn-primary" value="Cek"><i class="fa fa-save"></i> Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Manage -->
    <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/app-manage.js');?>"></script>

    
