<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Disbursement extends CI_Controller{

     function __construct(){
          parent::__construct();

          $this->load->model('Disbursement_model', 'modelDisbursement');
     }

     function uploadSignpk($jawara_id)
	{
		foreach ($_FILES as $name => $fileInfo) {
			$filename = $_FILES['sign_pk']['name'];
			$tmpname = $_FILES['sign_pk']['tmp_name'];
			$exp = explode('.', $filename);
			$ext = end($exp);
			$newname =  'SIGNPK_'.$jawara_id.'_'.date('Ymdhis')."." . $ext;
			$config['upload_path'] = './assets/berkas/sign_pk/';
			$config['upload_url'] =  base_url() . 'assets/berkas/sign_pk/';
			$config['allowed_types'] = "png|jpg|jpeg";
			$config['max_size'] = '2000';
			$config['file_name'] = $newname;
			$this->load->library('upload', $config);
			move_uploaded_file($tmpname, "assets/berkas/sign_pk/" . $newname);
			return $newname;
		}
	}

     function uploadTerms($jawara_id)
	{
		foreach ($_FILES as $name => $fileInfo) {
			$filename = $_FILES['file_terms']['name'];
			$tmpname = $_FILES['file_terms']['tmp_name'];
			$exp = explode('.', $filename);
			$ext = end($exp);
			$newname =  'TERMS_'.$jawara_id.'_'.date('Ymdhis')."." . $ext;
			$config['upload_path'] = './assets/berkas/terms/';
			$config['upload_url'] =  base_url() . 'assets/berkas/terms/';
			$config['allowed_types'] = "png|jpg|jpeg";
			$config['max_size'] = '2000';
			$config['file_name'] = $newname;
			$this->load->library('upload', $config);
			move_uploaded_file($tmpname, "assets/berkas/terms/" . $newname);
			return $newname;
		}
	}

     function index(){
          if($this->session->userdata('is_login') == TRUE){

               $x['getSKT'] = $this->modelDisbursement->getSKT();
               $x['getKKL'] = $this->modelDisbursement->getKKL();
               $x['getPR'] = $this->modelDisbursement->getPR();
               $x['getJB'] = $this->modelDisbursement->getJB();
               $x['getJA'] = $this->modelDisbursement->getJA();
               $x['getJT'] = $this->modelDisbursement->getJT();
               $x['getJCT'] = $this->modelDisbursement->getJCT();

               $x['getSSIGN'] = $this->modelDisbursement->getSSIGN();

               $x['getUomTelur'] = $this->modelDisbursement->getUomTelur();

               $x['provinsi'] = $this->modelDisbursement->get_provinsi();

               $this->load->view("include/head");
               $this->load->view("include/top-header");
               $this->load->view('disbursement-new', $x);
               $this->load->view("include/sidebar");
               $this->load->view("include/panel");
               $this->load->view("include/footer");
               $this->load->view("include/alert");
          }else{
               $this->session->set_flashdata('success', 'Upsss!!!, Login dulu ya');
               redirect('index.php/login');
          }
     }

     function getDisbursement() {
          
          $data = $this->modelDisbursement->getDisbursement();

          $no = 1;
          $jawara = array();

          foreach($data as $r) {

               $jawara_id = $r->id;
               $nik_ktp = $r->nik_ktp_kyc;
               $phone = $r->phone_kyc;
               $name = $r->name_kyc;

               $alamat_rumah = $r->alamat_rumah == NULL ? "<font>Alamat Rumah Tidak Ada</font>" : $r->alamat_rumah;

               $shop_location = $r->alamat_usaha;
               $kodepos = $r->kodepos == NULL ? "<font>Kode Pos Tidak Ada</font>" : $r->kodepos;
               $kecamatan = $r->kecamatan == NULL ? "<font>Kecamatan Tidak Ada</font>" : $r->kecamatan;
               $kab_kota = $r->kab_kota == NULL ? "<font>Kab/Kota Tidak Ada</font>" : $r->kab_kota;
               $provinsi = $r->provinsi == NULL ? "<font>Provinsi Tidak Ada</font>" : $r->provinsi;
               
               $name_lend = $r->name_lend;

               if($r->status_lend == NULL){
                    $status_lend = 'Belum Ada Tanggapan dari P2P';
               }else if($r->status_lend == 1){
                    $status_lend = 'Approve';
               }else if($r->status_lend == 2){
                    $status_lend = 'Reject';
               }

               $action = "<a href='javascript:;' 
                    data-id='$r->id'
                    data-nik_ktp='$r->nik_ktp_kyc'
                    data-name='$r->name_kyc'
                    data-phone='$r->phone_kyc'
                    data-alamat_rumah='$r->alamat_rumah'
                    data-shop_name='$r->shop_name_sc'
                    data-shop_location='$r->alamat_usaha'
                    data-kodepos='$r->kodepos'
                    data-kecamatan='$r->kecamatan'
                    data-kab_kota='$r->kab_kota'
                    data-provinsi='$r->provinsi'
                    data-founders='$r->name_lend'
                    data-code_lend='$r->code_lend'
                    data-status_lend='$r->status_lend'
                    data-status_lends='$status_lend'
                    data-toggle='modal' data-target='#disbursement'
                    class='btn btn-sm btn-success'><i class='fa fas fa-edit'></i> Disbursement</a>

                    <a href='javascript:;' 
                    data-id='$r->id'
                    data-toggle='modal' data-target='#comment_cancel'
                    class='btn btn-sm btn-danger'><i class='fa fas fa-close'></i> Comment/Cancel</a>
               ";

               $jawara[] = array(
                    $no++,
                    $jawara_id,
                    $nik_ktp,
                    $phone,
                    $name,
                    $alamat_rumah,
                    $shop_location,
                    $kodepos,
                    $kecamatan,
                    $kab_kota,
                    $provinsi,
                    $name_lend,
                    $status_lend,
                    $action
               );
               
          }
          
          echo json_encode($jawara);
     }

     public function createDisbursement(){

          $jawara_id = $this->input->post('id');

          $file_terms = '';

          $x['email_verifikasi_dis'] = $this->input->post('email');
          $x['nik_ktp_dis'] = $this->input->post('nik_ktp');
          $x['ibu_kandung_dis'] = $this->input->post('ibu_kandung');
          $x['name_dis'] = $this->input->post('name');
          $x['phone_dis'] = $this->input->post('phone');
          $x['phone_wa_dis'] = $this->input->post('phone_wa');
          $x['alamat_rumah_dis'] = $this->input->post('alamat_rumah');

          $x['shop_name_dis'] = $this->input->post('shop_name');
          $x['shop_location_dis'] = $this->input->post('shop_location');
          $x['status_kepemilikan_dis'] = $this->input->post('status_kepemilikan');

          $x['kodepos_dis'] = $this->input->post('kodepos');
          $x['kelurahan_dis'] = $this->input->post('kelurahan');
          $x['kecamatan_dis'] = $this->input->post('kecamatan');
          $x['kab_kota_dis'] = $this->modelDisbursement->cekKabupaten($this->input->post('kab_kota'));
          $x['provinsi_dis'] = $this->modelDisbursement->cekProvinsi($this->input->post('provinsi'));

          $x['kapasitas_kwh_dis'] = $this->input->post('kapasitas_kwh');

          $x['produk_beras1_dis'] = $this->input->post('produk_beras1');
          $x['produk_beras2_dis'] = $this->input->post('produk_beras2');
          $x['produk_ayam_dis'] = $this->input->post('produk_ayam');
          $x['produk_telur_dis'] = $this->input->post('produk_telur');

          $x['jumlah_beras_dis'] = $this->input->post('jumlah_beras');
          $x['jumlah_beras2_dis'] = $this->input->post('jumlah_beras2');

          $x['jumlah_ayam_dis'] = $this->input->post('jumlah_ayam');

          $x['jumlah_telur_dis'] = $this->input->post('jumlah_telur');
          $x['uom_telur_dis'] = $this->input->post('uom_telur');

          if (!empty($_FILES['file_terms']['name'])) {
               $newname = $this->uploadTerms($jawara_id);
               $data['file_terms'] = $newname;
               $file_terms = $newname;

               $x['file_terms_dis'] = base_url('assets/berkas/terms/').$file_terms;
          }

          if (!empty($_FILES['sign_pk']['name'])) {
               $newname = $this->uploadSignpk($jawara_id);
               $data['sign_pk'] = $newname;
               $sign_pk = $newname;

               $x['sign_pk_dis'] = base_url('assets/berkas/sign_pk/').$sign_pk;
          }
          
          $x['ssign_dis'] = $this->input->post('ssign');
          $x['notes_dis'] = $this->input->post('notes');
          $x['join_telegram_dis'] = $this->input->post('join_telegram');
          $x['created_at_dis'] = date('Y-m-d H:i:s');

          //UPDATE DATA FROM TABLE JAWARA ELIGIBLE
          $u['status_disbursement'] = '1';
          $u['updated_disbursement'] = date('Y-m-d H:i:s');
          $u['kecamatan'] = $this->input->post('kecamatan');
          $u['kab_kota'] = $this->modelDisbursement->cekKabupaten($this->input->post('kab_kota'));
          $u['provinsi'] = $this->modelDisbursement->cekProvinsi($this->input->post('provinsi'));

          $this->modelDisbursement->createDisbursement($x, $u, $jawara_id);

          redirect('index.php/disbursement');

     }

     // DONE DISBURSEMENT
     function getDisbursementDone() {
          
          $data = $this->modelDisbursement->getDisbursementDone();

          $no = 1;
          $jawara = array();

          foreach($data as $r) {
               
               $timestamp = $r->created_at_dis;
               $jawara_id = $r->id;
               $email_verifikasi = $r->email_verifikasi_dis;
               $founders = $r->name_lend == NULL ? "<font>Funders Tidak Ada</font>" : $r->name_lend;
               $nik_ktp = $r->nik_ktp_dis == NULL ? "<font>Nik KTP Tidak Ada</font>" : $r->nik_ktp_dis;
               $name = $r->name_dis;
               $ibu_kandung = $r->ibu_kandung_dis;
               $phone = $r->phone_dis;
               $phone_wa = $r->phone_wa_dis;
               $alamat_rumah = $r->alamat_rumah_dis;
               $shop_name = $r->shop_name_dis;
               $shop_location = $r->shop_location_dis;
               $status_kepemilikan = $r->status_kepemilikan_dis;
               $kodepos = $r->kodepos_dis == NULL ? "<font>Kode Pos Tidak Ada</font>" : $r->kodepos_dis;
               $kelurahan = $r->kelurahan_dis == NULL ? "<font>Kelurahan Tidak Ada</font>" : $r->kelurahan_dis;
               $kecamatan = $r->kecamatan_dis == NULL ? "<font>Kecamatan Tidak Ada</font>" : $r->kecamatan_dis;
               $kab_kota = $r->kab_kota_dis == NULL ? "<font>Kab/Kota Tidak Ada</font>" : $r->kab_kota_dis;
               $provinsi = $r->provinsi_dis == NULL ? "<font>Provinsi Tidak Ada</font>" : $r->provinsi_dis;
               $kapasitas_kwh = $r->kapasitas_kwh_dis;
               $produk_beras1 = $r->produk_beras1_dis;
               $produk_beras2 = $r->produk_beras2_dis;
               $produk_ayam = $r->produk_ayam_dis;
               $produk_telur = $r->produk_telur_dis;
               $jumlah_beras = $r->jumlah_beras_dis == NULL ? "0 (PACK)" : $r->jumlah_beras_dis.' (PACK)';
               $jumlah_beras2 = $r->jumlah_beras2_dis == NULL ? "0 (PACK)" : $r->jumlah_beras2_dis.' (PACK)';
               $jumlah_ayam = $r->jumlah_ayam_dis.' (PCS)';
               $jumlah_telur = $r->uom_telur_dis == NULL ? $r->jumlah_telur_dis : $r->jumlah_telur_dis.' ('.$r->uom_telur_dis.')';
               
               if($r->file_terms_dis != NULL){
                    $file_terms = '<a href="'.$r->file_terms_dis.'" target="_blank" class="btn btn-sm btn-secondary">'.$r->file_terms_dis.'</a>';
               }else{
                    $file_terms = 'Tidak ada File Terms';
               }

               if($r->sign_pk_dis != NULL){
                    $sign_pk = '<a href="'.$r->sign_pk_dis.'" target="_blank" class="btn btn-sm btn-primary">'.$r->sign_pk_dis.'</a>';
               }else{
                    $sign_pk = 'Tidak ada File Sign PK';
               }

               $join_telegram = $r->join_telegram_dis;

               $jawara[] = array(
                    $no++,
                    $timestamp,
                    $jawara_id,
                    $email_verifikasi,
                    $founders,
                    $nik_ktp,
                    $name,
                    $ibu_kandung,
                    $phone,
                    $phone_wa,
                    $alamat_rumah,
                    $shop_name,
                    $shop_location,
                    $status_kepemilikan,
                    $kodepos,
                    $kelurahan,
                    $kecamatan,
                    $kab_kota,
                    $provinsi,
                    $kapasitas_kwh,
                    $produk_beras1,
                    $produk_beras2,
                    $produk_ayam,
                    $produk_telur,
                    $jumlah_beras,
                    $jumlah_beras2,
                    $jumlah_ayam,
                    $jumlah_telur,
                    $file_terms,
                    $sign_pk,
                    $join_telegram
               );
               
          }
          
          echo json_encode($jawara);
     }

     // CANCEL JOIN
     function getDisbursementCancel() {
          
          $data = $this->modelDisbursement->getDisbursementCancel();

          $no = 1;
          $jawara = array();

          foreach($data as $r) {
               
               $jawara_id = $r->id;
               $email_verifikasi = $r->email_verifikasi_cancel_sign;
               $founders = $r->name_lend == NULL ? "<font>Funders Tidak Ada</font>" : $r->name_lend;
               $nik_ktp = $r->nik_ktp_kyc == NULL ? "<font>Nik KTP Tidak Ada</font>" : $r->nik_ktp_kyc;
               $name = $r->name_kyc;
               
               $comment_cancel = $r->comment_cancel_sign;

               $jawara[] = array(
                    $no++,
                    $jawara_id,
                    $email_verifikasi,
                    $founders,
                    $nik_ktp,
                    $name,
                    $comment_cancel
               );
               
          }
          
          echo json_encode($jawara);
     }

     // KONFIRMASI PENCAIRAN
     function confirm(){
          if($this->session->userdata('is_login') == TRUE){

               $this->load->view("include/head");
               $this->load->view("include/top-header");
               $this->load->view('confirm');
               $this->load->view("include/sidebar");
               $this->load->view("include/panel");
               $this->load->view("include/footer");
               $this->load->view("include/alert");
          }else{
               $this->session->set_flashdata('success', 'Upsss!!!, Login dulu ya');
               redirect('index.php/login');
          }
     }

     function getConfirm() {
          
          $data = $this->modelDisbursement->getConfirm();

          $no = 1;
          $jawara = array();

          foreach($data as $r) {

               $email_verifikasi = $r->email_verifikasi_dis;
               $jawara_id = $r->id;
               $founders = $r->name_lend == NULL ? "<font>Funders Tidak Ada</font>" : $r->name_lend;
               $provinsi = $r->provinsi_dis == NULL ? "<font>Provinsi Tidak Ada</font>" : $r->provinsi_dis;
               $kab_kota = $r->kab_kota_dis == NULL ? "<font>Kab/Kota Tidak Ada</font>" : $r->kab_kota_dis;
               $kecamatan = $r->kecamatan_dis == NULL ? "<font>Kecamatan Tidak Ada</font>" : $r->kecamatan_dis;
               $kelurahan = $r->kelurahan_dis == NULL ? "<font>Kelurahan Tidak Ada</font>" : $r->kelurahan_dis;
               $shop_location = $r->shop_location_dis;
               $kodepos = $r->kodepos_dis == NULL ? "<font>Kode Pos Tidak Ada</font>" : $r->kodepos_dis;
               $nik_ktp = $r->nik_ktp_dis == NULL ? "<font>Nik KTP Tidak Ada</font>" : $r->nik_ktp_dis;
               $name = $r->name_dis;
               $ibu_kandung = $r->ibu_kandung_dis;
               $phone = $r->phone_dis;

               $action = "<a href='javascript:;' 
                    data-id='$r->id'
                    data-toggle='modal' data-target='#confirm'
                    class='btn btn-sm btn-success'><i class='fa fas fa-edit'></i> Confirm</a>
               ";

               $jawara[] = array(
                    $no++,
                    $email_verifikasi,
                    $jawara_id,
                    $founders,
                    $provinsi,
                    $kab_kota,
                    $kecamatan,
                    $kelurahan,
                    $kodepos,
                    $shop_location,
                    $nik_ktp,
                    $name,
                    $ibu_kandung,
                    $phone,
                    $action
               );
               
          }
          
          echo json_encode($jawara);
     }

     function getConfirmAccept() {
          
          $data = $this->modelDisbursement->getConfirmAccept();

          $no = 1;
          $jawara = array();

          foreach($data as $r) {

               $email_verifikasi = $r->email_verifikasi_dis;
               $jawara_id = $r->id;
               $tanggal_pencairan = $r->tanggal_pencairan_dis == NULL ? "<font>Tanggal Pencairan Tidak Ada</font>" : $r->tanggal_pencairan_dis;
               $founders = $r->name_lend == NULL ? "<font>Funders Tidak Ada</font>" : $r->name_lend;
               $provinsi = $r->provinsi_dis == NULL ? "<font>Provinsi Tidak Ada</font>" : $r->provinsi_dis;
               $kab_kota = $r->kab_kota_dis == NULL ? "<font>Kab/Kota Tidak Ada</font>" : $r->kab_kota_dis;
               $kecamatan = $r->kecamatan_dis == NULL ? "<font>Kecamatan Tidak Ada</font>" : $r->kecamatan_dis;
               $kelurahan = $r->kelurahan_dis == NULL ? "<font>Kelurahan Tidak Ada</font>" : $r->kelurahan_dis;
               $shop_location = $r->shop_location_dis;
               $kodepos = $r->kodepos_dis == NULL ? "<font>Kode Pos Tidak Ada</font>" : $r->kodepos_dis;
               $nik_ktp = $r->nik_ktp_dis == NULL ? "<font>Nik KTP Tidak Ada</font>" : $r->nik_ktp_dis;
               $name = $r->name_dis;
               $ibu_kandung = $r->ibu_kandung_dis;
               $phone = $r->phone_dis;
               $pembiayaan = "Rp " . number_format($r->pembiayaan_dis,2,',','.');

               $jawara[] = array(
                    $no++,
                    $email_verifikasi,
                    $jawara_id,
                    $tanggal_pencairan,
                    $founders,
                    $provinsi,
                    $kab_kota,
                    $kecamatan,
                    $kelurahan,
                    $kodepos,
                    $shop_location,
                    $nik_ktp,
                    $name,
                    $ibu_kandung,
                    $phone,
                    $pembiayaan
               );
               
          }
          
          echo json_encode($jawara);
     }

     public function createConfirm(){

          $jawara_id = $this->input->post('id');

          //UPDATE DATA FROM TABLE JAWARA ELIGIBLE
          $u['status_confirm_dis'] = '1';
          $u['tanggal_pencairan_dis'] = $this->input->post('tanggal_pencairan');
          $u['updated_confirm_dis'] = date('Y-m-d H:i:s');
          $u['pembiayaan_dis'] = $this->input->post('pembiayaan');

          $this->modelDisbursement->createConfirm($u, $jawara_id);

          redirect('index.php/disbursement/confirm');

     }

     function comment_cancel(){

          $id = $this->input->post('id');       

          //INSERT DATA
          $x['email_verifikasi_cancel_sign'] = $this->session->userdata('email_address');
          $x['comment_cancel_sign'] = $this->input->post('comment_cancel_sign');
          
          $this->modelDisbursement->comment_cancel($x, $id);

          redirect('index.php/disbursement');
     }

     // request data kecamatan berdasarkan id PROVINSI yang dipilih
     function get_kabupaten()
     {
          if ($this->input->post('provinsi_id')) {
               echo $this->modelDisbursement->get_kabupaten($this->input->post('provinsi_id'));
          }
     }

     //request data kecamatan berdasarkan id kabupaten yang dipilih
     function get_kecamatan()
     {
          if ($this->input->post('kabupaten_id')) {
               echo $this->modelDisbursement->get_kecamatan($this->input->post('kabupaten_id'));
          }
     }

}