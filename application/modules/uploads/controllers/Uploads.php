<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require('./application/third_party/phpoffice/vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class Uploads extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('uploads_model', 'modelUploads');
	}

	public function index()
	{
		if($this->session->userdata('is_login') == TRUE){
			redirect("index.php/home"); 
		}else{
			redirect("index.php/login");
		}
	}

	public function v_leads(){
		if($this->session->userdata('is_login') == TRUE){
			$this->load->view("include/head");
			$this->load->view("include/top-header");
			$this->load->view('v_leads');
			$this->load->view("include/sidebar");
			$this->load->view("include/panel");
			$this->load->view("include/footer"); 
		}else{
			redirect("index.php/login");
		}
	}

	public function leads(){
		if($this->session->userdata('is_login') == TRUE){
			$this->load->view("include/head");
			$this->load->view("include/top-header");
			$this->load->view('leads');
			$this->load->view("include/sidebar");
			$this->load->view("include/panel");
			$this->load->view("include/footer"); 
		}else{
			redirect("index.php/login");
		}
	}

	public function jawara(){
		if($this->session->userdata('is_login') == TRUE){
			$this->load->view("include/head");
			$this->load->view("include/top-header");
			$this->load->view('jawara');
			$this->load->view("include/sidebar");
			$this->load->view("include/panel");
			$this->load->view("include/footer"); 
		}else{
			redirect("index.php/login");
		}
	}

	public function assessment(){
		if($this->session->userdata('is_login') == TRUE){
			$this->load->view("include/head");
			$this->load->view("include/top-header");
			$this->load->view('assessment');
			$this->load->view("include/sidebar");
			$this->load->view("include/panel");
			$this->load->view("include/footer"); 
		}else{
			redirect("index.php/login");
		}
	}

	public function delivery(){
		if($this->session->userdata('is_login') == TRUE){
			$this->load->view("include/head");
			$this->load->view("include/top-header");
			$this->load->view('delivery');
			$this->load->view("include/sidebar");
			$this->load->view("include/panel");
			$this->load->view("include/footer"); 
			$this->load->view("include/alert");
		}else{
			redirect("index.php/login");
		}
	}

	public function detail_delivery(){
		if($this->session->userdata('is_login') == TRUE){
			$this->load->view("include/head");
			$this->load->view("include/top-header");
			$this->load->view('detail_delivery');
			$this->load->view("include/sidebar");
			$this->load->view("include/panel");
			$this->load->view("include/footer"); 
			$this->load->view("include/alert");
		}else{
			redirect("index.php/login");
		}
	}

	public function uploadLeads(){
		if($this->session->userdata('is_login') == TRUE){
            // Load plugin PHPExcel nya
            $file_mimes = array('application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            if(isset($_FILES['leads']['name']) && in_array($_FILES['leads']['type'], $file_mimes)) {

                $arr_file = explode('.', $_FILES['leads']['name']);
                $extension = end($arr_file);

                if($extension != 'xlsx') {
                    $this->session->set_flashdata('success', 'PROSES IMPORT DATA GAGAL! Format file yang anda masukkan salah!');

                    redirect('index.php/jawara'); 
                } else {
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                }

                $loadexcel  = $reader->load($_FILES['leads']['tmp_name']);
                $sheet  = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

                $list_sheet = $loadexcel->getSheetNames();

                $sheetData = $loadexcel->getSheetByName($list_sheet[0])->toArray(null, true, true ,true);

                $data = array();
                $numrow = 0;

                foreach($sheetData as $row){

					if($row['A'] != NULL){
						if($numrow >= 1){
							array_push($data, array(
								'email'  										=> $row['B'],
								'phone'  										=> $row['C'],
								'name'  										=> $row['D'],
								'nik_ktp'  										=> $row['E'],
								'nik_kk'  										=> $row['F'],
								'alamat_rumah'  								=> $row['G'],
								'rt_rumah'  									=> $row['H'],
								'rw_rumah'  									=> $row['I'],
								'nomor_rumah'  									=> $row['J'],
								'alamat_usaha'  								=> $row['K'],
								'rt_usaha'  									=> $row['L'],
								'rw_usaha'  									=> $row['M'],
								'nomor_usaha'  									=> $row['N'],
								'kecamatan'  									=> $row['O'],
								'kab_kota'  									=> $row['P'],
								'provinsi'  									=> $row['Q'],
								'kodepos'  										=> $row['R'],
								'file_ktp'  									=> $row['S'],
								'file_kk'  										=> $row['T'],
								'file_store'  									=> $row['U'],
								'kode_referral'  								=> $row['V'],
								'pinjaman'  									=> $row['W'],
								'status_kyc'  									=> $row['X'],
								'priority'  									=> $row['Y'],
								'term_condition'  								=> $row['Z'],
								'created_at'  									=> $row['AA']
							));
						}
						$numrow++;
					}
					
                }

                // INSERT TO DATABASE
				$this->modelUploads->uploadLeads($data);//redirect halaman
                redirect('index.php/jawara'); 
            }
        }else{
            redirect("index.php/login");
        }
	}

	public function uploadLeadsNew(){
		if($this->session->userdata('is_login') == TRUE){
            // Load plugin PHPExcel nya
            $file_mimes = array('application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            if(isset($_FILES['leads']['name']) && in_array($_FILES['leads']['type'], $file_mimes)) {

                $arr_file = explode('.', $_FILES['leads']['name']);
                $extension = end($arr_file);

                if($extension != 'xlsx') {
                    $this->session->set_flashdata('success', 'PROSES IMPORT DATA GAGAL! Format file yang anda masukkan salah!');

                    redirect('index.php/jawara'); 
                } else {
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                }

                $loadexcel  = $reader->load($_FILES['leads']['tmp_name']);
                $sheet  = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

                $list_sheet = $loadexcel->getSheetNames();

                $sheetData = $loadexcel->getSheetByName($list_sheet[0])->toArray(null, true, true ,true);

                $data = array();
                $numrow = 0;

                foreach($sheetData as $row){

					if($row['A'] != NULL){
						if($numrow >= 1){
							array_push($data, array(
								'email'  										=> $row['B'],
								'phone'  										=> $row['C'],
								'name'  										=> $row['D'],
								'nik_ktp'  										=> $row['E'],
								'nik_kk'  										=> $row['F'],
								'alamat_rumah'  								=> $row['G'],
								'rt_rumah'  									=> $row['H'],
								'rw_rumah'  									=> $row['I'],
								'nomor_rumah'  									=> $row['J'],
								'alamat_usaha'  								=> $row['K'],
								'rt_usaha'  									=> $row['L'],
								'rw_usaha'  									=> $row['M'],
								'nomor_usaha'  									=> $row['N'],
								'kecamatan'  									=> $row['O'],
								'kab_kota'  									=> $row['P'],
								'provinsi'  									=> $row['Q'],
								'kodepos'  										=> $row['R'],
								'file_ktp'  									=> $row['S'],
								'file_kk'  										=> $row['T'],
								'file_store'  									=> $row['U'],
								'kode_referral'  								=> $row['V'],
								'pinjaman'  									=> $row['W'],
								'status_kyc'  									=> $row['X'],
								'priority'  									=> $row['Y'],
								'term_condition'  								=> $row['Z'],
								'created_at'  									=> $row['AA'],
								'updated_at'  									=> $row['AB'],
								'email_verifikasi_kyc'  						=> $row['AC'],
								'phone_kyc'  									=> $row['AD'],
								'name_kyc'  									=> $row['AE'],
								'nik_ktp_kyc'  									=> $row['AF'],
								'nik_kk_kyc'  									=> $row['AG'],
								'bank_code_kyc'  								=> $row['AH'],
								'nomor_rekening_kyc'  							=> $row['AI'],
								'file_ktp_kyc'  								=> $row['AJ'],
								'file_ktp_pasangan_kyc'  						=> $row['AK'],
								'file_kk_kyc'  									=> $row['AL'],
								'file_store_kyc'  								=> $row['AM'],
								'delivered_email_kyc'  							=> $row['AN'],
								'active_phone_kyc'  							=> $row['AO'],
								'active_whatsapp_kyc'  							=> $row['AP'],
								'correct_ktp_kyc'  								=> $row['AQ'],
								'correct_kk_kyc'  								=> $row['AR'],
								'nama_jawara_kyc'  								=> $row['AS'],
								'ktp_jelas_kyc'  								=> $row['AT'],
								'kk_jelas_kyc'  								=> $row['AU'],
								'store_jelas_kyc'  								=> $row['AV'],
								'status_jawara_kyc'  							=> $row['AW'],
								'notes_kyc'  									=> $row['AX'],
								'status_digital_inspection_kyc'  				=> $row['AY'],
								'created_at_kyc'  								=> $row['AZ'],
								'updated_at_kyc'  								=> $row['BA'],
								'updated_di_kyc'  								=> $row['BB'],
								'email_verifikasi_di'  							=> $row['BC'],
								'nik_ktp_di'  									=> $row['BD'],
								'name_di'  										=> $row['BE'],
								'phone_di'  									=> $row['BF'],
								'cek_npwp_di'  									=> $row['BG'],
								'cek_getcontact_di'  							=> $row['BH'],
								'cek_gearth_di'  								=> $row['BI'],
								'cek_phone_di'  								=> $row['BJ'],
								'score_getcontact_di'  							=> $row['BK'],
								'score_gearth_di'  								=> $row['BL'],
								'npwp_di'  										=> $row['BM'],
								'gf_competitor_di'  							=> $row['BN'],
								'gf_kelayakan_di'  								=> $row['BO'],
								'gf_lokasi_di'  								=> $row['BP'],
								'file_gearth_di'  								=> $row['BQ'],
								'gcf_tags_di'  									=> $row['BR'],
								'gcf_penilaian_di'  							=> $row['BS'],
								'gcf_aktif_di'  								=> $row['BT'],
								'file_gcf_di'  									=> $row['BU'],
								'notes_di'  									=> $row['BV'],
								'gmaps_di'  									=> $row['BW'],
								'kyc_di'  										=> $row['BX'],
								'score_di'  									=> $row['BY'],
								'gearth_di'  									=> $row['BZ'],
								'get_contact_di'  								=> $row['CA'],
								'percent_kyc_di'  								=> $row['CB'],
								'percent_score_di'  							=> $row['CC'],
								'percent_gearth_di'  							=> $row['CD'],
								'percent_get_contact_di'  						=> $row['CE'],
								'total_score_di'  								=> $row['CF'],
								'user_grading_di'  								=> $row['CG'],
								'status_jawara_di'  							=> $row['CH'],
								'status_ground_di'  							=> $row['CI'],
								'created_at_di'  								=> $row['CJ'],
								'updated_at_di'  								=> $row['CK'],
								'updated_ground_di'  							=> $row['CL'],
								'email_verifikasi_sc'  							=> $row['CM'],
								'nik_ktp_sc'  									=> $row['CN'],
								'name_sc'  										=> $row['CO'],
								'phone_sc'  									=> $row['CP'],
								'shop_name_sc'  								=> $row['CQ'],
								'shop_location_sc'  							=> $row['CR'],
								'sa_pengalaman_sc'  							=> $row['CS'],
								'sa_karyawan_sc'  								=> $row['CT'],
								'sa_keluarga_sc'  								=> $row['CU'],
								'sa_aktif_sc'  									=> $row['CV'],
								'ba_jusaha_sc'  								=> $row['CW'],
								'ba_musaha_sc'  								=> $row['CX'],
								'ca_location_sc'  								=> $row['CY'],
								'ca_kompetitor_sc'  							=> $row['CZ'],
								'frma_kendaraan_sc'  							=> $row['DA'],
								'frma_aset_sc'  								=> $row['DB'],
								'frma_pendapatan_sc'  							=> $row['DC'],
								'notes_sc'  									=> $row['DD'],
								'file_selfie_sc'  								=> $row['DE'],
								'ts_sa_sc'  									=> $row['DF'],
								'ts_ba_sc'  									=> $row['DG'],
								'ts_ca_sc'  									=> $row['DH'],
								'ts_frma_sc'  									=> $row['DI'],
								'percent_sa_sc'  								=> $row['DJ'],
								'percent_ba_sc'  								=> $row['DK'],
								'percent_ca_sc'  								=> $row['DL'],
								'percent_frma_sc'  								=> $row['DM'],
								'total_score_sc'  								=> $row['DN'],
								'user_grading_sc'  								=> $row['DO'],
								'status_score_sc'  								=> $row['DP'],
								'created_at_sc'  								=> $row['DQ'],
								'updated_at_sc'  								=> $row['DR'],
								'status_eligible'  								=> $row['DS'],
								'code_lend'  									=> $row['DT'],
								'name_lend'  									=> $row['DU'],
								'created_at_lend'  								=> $row['DV'],
								'updated_at_lend'  								=> $row['DW'],
								'status_lend'  									=> $row['DX'],
								'status_kol_lend'  								=> $row['DY'],
								'tunggakan_lend'  								=> $row['DZ'],
								'updated_lend'  								=> $row['EA'],
								'status_disbursement'  							=> $row['EB'],
								'updated_disbursement'  						=> $row['EC'],
								'email_verifikasi_dis'  						=> $row['ED'],
								'nik_ktp_dis'  									=> $row['EE'],
								'name_dis'  									=> $row['EF'],
								'ibu_kandung_dis'  								=> $row['EG'],
								'phone_dis'  									=> $row['EH'],
								'phone_wa_dis'  								=> $row['EI'],
								'alamat_rumah_dis'  							=> $row['EJ'],
								'shop_name_dis'  								=> $row['EK'],
								'shop_location_dis'  							=> $row['EL'],
								'status_kepemilikan_dis'  						=> $row['EM'],
								'kodepos_dis'  									=> $row['EN'],
								'kelurahan_dis'  								=> $row['EO'],
								'kecamatan_dis'  								=> $row['EP'],
								'kab_kota_dis'  								=> $row['EQ'],
								'provinsi_dis'  								=> $row['ER'],
								'kapasitas_kwh_dis'  							=> $row['ES'],
								'produk_beras1_dis'  							=> $row['ET'],
								'produk_beras2_dis'  							=> $row['EU'],
								'produk_ayam_dis'  								=> $row['EV'],
								'produk_telur_dis'  							=> $row['EW'],
								'jumlah_beras_dis'  							=> $row['EX'],
								'jumlah_beras2_dis'  							=> $row['EY'],
								'jumlah_ayam_dis'  								=> $row['EZ'],
								'jumlah_telur_dis'  							=> $row['FA'],
								'uom_telur_dis'  								=> $row['FB'],
								'file_terms_dis'  								=> $row['FC'],
								'join_telegram_dis'  							=> $row['FD'],
								'ssign_dis'										=> $row['FE'],
								'notes_dis'										=> $row['FF'],
								'created_at_dis'  								=> $row['FG'],
								'updated_at_dis'  								=> $row['FH'],
								'status_confirm_dis'  							=> $row['FI'],
								'tanggal_pencairan_dis'  						=> $row['FJ'],
								'pembiayaan_dis'  								=> $row['FK'],
								'updated_confirm_dis'  							=> $row['FL'],
								'status_deploy_dis'  							=> $row['FM'],
								'updated_deploy_dis'  							=> $row['FN'],
								'email_verifikasi_dep'  						=> $row['FO'],
								'date_delivery_dep'  							=> $row['FP'],
								'link_surat_dep'  								=> $row['FQ'],
								'status_dep'  									=> $row['FR'],
								'notes_dep'  									=> $row['FS'],
								'created_at_dep'  								=> $row['FT'],
								'updated_at_dep'  								=> $row['FU'],
								'area_jawara'  									=> $row['FV']
							));
						}
						$numrow++;
					}
					
                }

                // INSERT TO DATABASE
				$this->db->truncate('jawara_leads');
				$this->modelUploads->uploadLeads($data);//redirect halaman
                redirect('index.php/jawara'); 
            }
        }else{
            redirect("index.php/login");
        }
	}

	public function bulkUpload(){
		if($this->session->userdata('is_login') == TRUE){
            // Load plugin PHPExcel nya
            $file_mimes = array('application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            if(isset($_FILES['leads']['name']) && in_array($_FILES['leads']['type'], $file_mimes)) {

                $arr_file = explode('.', $_FILES['leads']['name']);
                $extension = end($arr_file);

                if($extension != 'xlsx') {
                    $this->session->set_flashdata('success', 'PROSES IMPORT DATA GAGAL! Format file yang anda masukkan salah!');

                    redirect('index.php/jawara'); 
                } else {
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                }

                $loadexcel  = $reader->load($_FILES['leads']['tmp_name']);
                $sheet  = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

                $list_sheet = $loadexcel->getSheetNames();

                $sheetData = $loadexcel->getSheetByName($list_sheet[0])->toArray(null, true, true ,true);

                $data = array();
                $numrow = 0;

                foreach($sheetData as $row){

					if($row['A'] != NULL){
						if($numrow >= 1){
							array_push($data, array(
								'id' 	 										=> $row['A'],
								'email'  										=> $row['B'],
								'phone'  										=> $row['C'],
								'name'  										=> $row['D'],
								'nik_ktp'  										=> $row['E'],
								'nik_kk'  										=> $row['F'],
								'alamat_rumah'  								=> $row['G'],
								'rt_rumah'  									=> $row['H'],
								'rw_rumah'  									=> $row['I'],
								'nomor_rumah'  									=> $row['J'],
								'alamat_usaha'  								=> $row['K'],
								'rt_usaha'  									=> $row['L'],
								'rw_usaha'  									=> $row['M'],
								'nomor_usaha'  									=> $row['N'],
								'kecamatan'  									=> $row['O'],
								'kab_kota'  									=> $row['P'],
								'provinsi'  									=> $row['Q'],
								'kodepos'  										=> $row['R'],
								'file_ktp'  									=> $row['S'],
								'file_kk'  										=> $row['T'],
								'file_store'  									=> $row['U'],
								'kode_referral'  								=> $row['V'],
								'pinjaman'  									=> $row['W'],
								'status_kyc'  									=> $row['X'],
								'priority'  									=> $row['Y'],
								'term_condition'  								=> $row['Z'],
								'created_at'  									=> $row['AA'],
								'updated_at'  									=> $row['AB'],
								'email_verifikasi_kyc'  						=> $row['AC'],
								'phone_kyc'  									=> $row['AD'],
								'name_kyc'  									=> $row['AE'],
								'nik_ktp_kyc'  									=> $row['AF'],
								'nik_kk_kyc'  									=> $row['AG'],
								'bank_code_kyc'  								=> $row['AH'],
								'nomor_rekening_kyc'  							=> $row['AI'],
								'file_ktp_kyc'  								=> $row['AJ'],
								'file_ktp_pasangan_kyc'  						=> $row['AK'],
								'file_kk_kyc'  									=> $row['AL'],
								'file_store_kyc'  								=> $row['AM'],
								'delivered_email_kyc'  							=> $row['AN'],
								'active_phone_kyc'  							=> $row['AO'],
								'active_whatsapp_kyc'  							=> $row['AP'],
								'correct_ktp_kyc'  								=> $row['AQ'],
								'correct_kk_kyc'  								=> $row['AR'],
								'nama_jawara_kyc'  								=> $row['AS'],
								'ktp_jelas_kyc'  								=> $row['AT'],
								'kk_jelas_kyc'  								=> $row['AU'],
								'store_jelas_kyc'  								=> $row['AV'],
								'status_jawara_kyc'  							=> $row['AW'],
								'notes_kyc'  									=> $row['AX'],
								'status_digital_inspection_kyc'  				=> $row['AY'],
								'created_at_kyc'  								=> $row['AZ'],
								'updated_at_kyc'  								=> $row['BA'],
								'updated_di_kyc'  								=> $row['BB'],
								'email_verifikasi_di'  							=> $row['BC'],
								'nik_ktp_di'  									=> $row['BD'],
								'name_di'  										=> $row['BE'],
								'phone_di'  									=> $row['BF'],
								'cek_npwp_di'  									=> $row['BG'],
								'cek_getcontact_di'  							=> $row['BH'],
								'cek_gearth_di'  								=> $row['BI'],
								'cek_phone_di'  								=> $row['BJ'],
								'score_getcontact_di'  							=> $row['BK'],
								'score_gearth_di'  								=> $row['BL'],
								'npwp_di'  										=> $row['BM'],
								'gf_competitor_di'  							=> $row['BN'],
								'gf_kelayakan_di'  								=> $row['BO'],
								'gf_lokasi_di'  								=> $row['BP'],
								'file_gearth_di'  								=> $row['BQ'],
								'gcf_tags_di'  									=> $row['BR'],
								'gcf_penilaian_di'  							=> $row['BS'],
								'gcf_aktif_di'  								=> $row['BT'],
								'file_gcf_di'  									=> $row['BU'],
								'notes_di'  									=> $row['BV'],
								'gmaps_di'  									=> $row['BW'],
								'kyc_di'  										=> $row['BX'],
								'score_di'  									=> $row['BY'],
								'gearth_di'  									=> $row['BZ'],
								'get_contact_di'  								=> $row['CA'],
								'percent_kyc_di'  								=> $row['CB'],
								'percent_score_di'  							=> $row['CC'],
								'percent_gearth_di'  							=> $row['CD'],
								'percent_get_contact_di'  						=> $row['CE'],
								'total_score_di'  								=> $row['CF'],
								'user_grading_di'  								=> $row['CG'],
								'status_jawara_di'  							=> $row['CH'],
								'status_ground_di'  							=> $row['CI'],
								'created_at_di'  								=> $row['CJ'],
								'updated_at_di'  								=> $row['CK'],
								'updated_ground_di'  							=> $row['CL'],
								'email_verifikasi_sc'  							=> $row['CM'],
								'nik_ktp_sc'  									=> $row['CN'],
								'name_sc'  										=> $row['CO'],
								'phone_sc'  									=> $row['CP'],
								'shop_name_sc'  								=> $row['CQ'],
								'shop_location_sc'  							=> $row['CR'],
								'sa_pengalaman_sc'  							=> $row['CS'],
								'sa_karyawan_sc'  								=> $row['CT'],
								'sa_keluarga_sc'  								=> $row['CU'],
								'sa_aktif_sc'  									=> $row['CV'],
								'ba_jusaha_sc'  								=> $row['CW'],
								'ba_musaha_sc'  								=> $row['CX'],
								'ca_location_sc'  								=> $row['CY'],
								'ca_kompetitor_sc'  							=> $row['CZ'],
								'frma_kendaraan_sc'  							=> $row['DA'],
								'frma_aset_sc'  								=> $row['DB'],
								'frma_pendapatan_sc'  							=> $row['DC'],
								'notes_sc'  									=> $row['DD'],
								'file_selfie_sc'  								=> $row['DE'],
								'ts_sa_sc'  									=> $row['DF'],
								'ts_ba_sc'  									=> $row['DG'],
								'ts_ca_sc'  									=> $row['DH'],
								'ts_frma_sc'  									=> $row['DI'],
								'percent_sa_sc'  								=> $row['DJ'],
								'percent_ba_sc'  								=> $row['DK'],
								'percent_ca_sc'  								=> $row['DL'],
								'percent_frma_sc'  								=> $row['DM'],
								'total_score_sc'  								=> $row['DN'],
								'user_grading_sc'  								=> $row['DO'],
								'status_score_sc'  								=> $row['DP'],
								'created_at_sc'  								=> $row['DQ'],
								'updated_at_sc'  								=> $row['DR'],
								'status_eligible'  								=> $row['DS'],
								'code_lend'  									=> $row['DT'],
								'name_lend'  									=> $row['DU'],
								'created_at_lend'  								=> $row['DV'],
								'updated_at_lend'  								=> $row['DW'],
								'status_lend'  									=> $row['DX'],
								'status_kol_lend'  								=> $row['DY'],
								'tunggakan_lend'  								=> $row['DZ'],
								'updated_lend'  								=> $row['EA'],
								'status_disbursement'  							=> $row['EB'],
								'updated_disbursement'  						=> $row['EC'],
								'email_verifikasi_dis'  						=> $row['ED'],
								'nik_ktp_dis'  									=> $row['EE'],
								'name_dis'  									=> $row['EF'],
								'ibu_kandung_dis'  								=> $row['EG'],
								'phone_dis'  									=> $row['EH'],
								'phone_wa_dis'  								=> $row['EI'],
								'alamat_rumah_dis'  							=> $row['EJ'],
								'shop_name_dis'  								=> $row['EK'],
								'shop_location_dis'  							=> $row['EL'],
								'status_kepemilikan_dis'  						=> $row['EM'],
								'kodepos_dis'  									=> $row['EN'],
								'kelurahan_dis'  								=> $row['EO'],
								'kecamatan_dis'  								=> $row['EP'],
								'kab_kota_dis'  								=> $row['EQ'],
								'provinsi_dis'  								=> $row['ER'],
								'kapasitas_kwh_dis'  							=> $row['ES'],
								'produk_beras1_dis'  							=> $row['ET'],
								'produk_beras2_dis'  							=> $row['EU'],
								'produk_ayam_dis'  								=> $row['EV'],
								'produk_telur_dis'  							=> $row['EW'],
								'jumlah_beras_dis'  							=> $row['EX'],
								'jumlah_beras2_dis'  							=> $row['EY'],
								'jumlah_ayam_dis'  								=> $row['EZ'],
								'jumlah_telur_dis'  							=> $row['FA'],
								'uom_telur_dis'  								=> $row['FB'],
								'file_terms_dis'  								=> $row['FC'],
								'join_telegram_dis'  							=> $row['FD'],
								'ssign_dis'										=> $row['FE'],
								'notes_dis'										=> $row['FF'],
								'created_at_dis'  								=> $row['FG'],
								'updated_at_dis'  								=> $row['FH'],
								'status_confirm_dis'  							=> $row['FI'],
								'tanggal_pencairan_dis'  						=> $row['FJ'],
								'pembiayaan_dis'  								=> $row['FK'],
								'updated_confirm_dis'  							=> $row['FL'],
								'status_deploy_dis'  							=> $row['FM'],
								'updated_deploy_dis'  							=> $row['FN'],
								'email_verifikasi_dep'  						=> $row['FO'],
								'date_delivery_dep'  							=> $row['FP'],
								'link_surat_dep'  								=> $row['FQ'],
								'status_dep'  									=> $row['FR'],
								'notes_dep'  									=> $row['FS'],
								'created_at_dep'  								=> $row['FT'],
								'updated_at_dep'  								=> $row['FU'],
								'area_jawara'  									=> $row['FV']
							));
						}
						$numrow++;
					}
					
                }
				// UPDATE BATCH
				$result = $this->db->update_batch('jawara_leads',$data, 'id');

				if($result){
					$this->session->set_flashdata('success','Bulk Upload Data Jawara Leads Telah Berhasil Diupdate Ke Sistem');
					redirect('index.php/jawara'); 
				}else{
					$this->session->set_flashdata('success','Bulk Upload Data Jawara Leads Gagal Diupdate Ke Sistem, Silahkan Coba Lagi');	
					redirect('index.php/jawara'); 
				}

            }
        }else{
            redirect("index.php/login");
        }
	}

	public function bulkAsses(){
		if($this->session->userdata('is_login') == TRUE){
            // Load plugin PHPExcel nya
            $file_mimes = array('application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            if(isset($_FILES['leads']['name']) && in_array($_FILES['leads']['type'], $file_mimes)) {

                $arr_file = explode('.', $_FILES['leads']['name']);
                $extension = end($arr_file);

                if($extension != 'xlsx') {
                    $this->session->set_flashdata('success', 'PROSES IMPORT DATA GAGAL! Format file yang anda masukkan salah!');

                    redirect('index.php/jawara'); 
                } else {
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                }

                $loadexcel  = $reader->load($_FILES['leads']['tmp_name']);
                $sheet  = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

                $list_sheet = $loadexcel->getSheetNames();

                $sheetData = $loadexcel->getSheetByName($list_sheet[0])->toArray(null, true, true ,true);

                $data = array();
                $numrow = 0;

                foreach($sheetData as $row){

					if($row['A'] != NULL){
						if($numrow >= 1){
							array_push($data, array(
								'jawara_id' 	 								=> $row['B'],
								'email_jawara_sc'	  							=> $row['C'],
								'nik_ktp_sc'  									=> $row['D'],
								'name_sc'  										=> $row['E'],
								'phone_sc'  									=> $row['F'],
								'shop_name_sc'  								=> $row['G'],
								'shop_location_sc'  							=> $row['H'],
								'sa_pengalaman_sc'  							=> $row['I'],
								'sa_karyawan_sc'  								=> $row['J'],
								'sa_keluarga_sc'  								=> $row['K'],
								'sa_aktif_sc'  									=> $row['L'],
								'ba_jusaha_sc'  								=> $row['M'],
								'ba_musaha_sc'  								=> $row['N'],
								'ca_location_sc'  								=> $row['O'],
								'ca_kompetitor_sc'  							=> $row['P'],
								'frma_aset_sc'  								=> $row['Q'],
								'frma_pendapatan_sc'  							=> $row['R'],
								'notes_sc'  									=> $row['S'],
								'file_selfie_sc'  								=> $row['T'],
								'ts_sa_sc'  									=> $row['U'],
								'ts_ba_sc'  									=> $row['V'],
								'ts_ca_sc'  									=> $row['W'],
								'ts_frma_sc'  									=> $row['X'],
								'percent_sa_sc'  								=> $row['Y'],
								'percent_ba_sc'  								=> $row['Z'],
								'percent_ca_sc'  								=> $row['AA'],
								'percent_frma_sc'  								=> $row['AB'],
								'total_score_sc'  								=> $row['AC'],
								'user_grading_sc'  								=> $row['AD'],
								'status_score_sc'  								=> $row['AE'],
								'created_at_sc'  								=> $row['AF']
							));
						}
						$numrow++;
					}
					
                }
				// UPDATE BATCH
				$result = $this->db->insert_batch('jawara_score',$data);

				if($result){
					$this->session->set_flashdata('success','Bulk Upload Data Self Assestment Jawara Telah Berhasil Diupdate Ke Sistem');
					redirect('index.php/score'); 
				}else{
					$this->session->set_flashdata('success','Bulk Upload Data Self Assestment Jawara Gagal Diupdate Ke Sistem, Silahkan Coba Lagi');	
					redirect('index.php/score'); 
				}

            }
        }else{
            redirect("index.php/login");
        }
	}

	public function uploadDelivery(){
		if($this->session->userdata('is_login') == TRUE){
            // Load plugin PHPExcel nya
            $file_mimes = array('application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            if(isset($_FILES['leads']['name']) && in_array($_FILES['leads']['type'], $file_mimes)) {

                $arr_file = explode('.', $_FILES['leads']['name']);
                $extension = end($arr_file);

                if($extension != 'xlsx') {
                    $this->session->set_flashdata('success', 'PROSES IMPORT DATA GAGAL! Format file yang anda masukkan salah!');

                    redirect('index.php/uploads/delivery'); 
                } else {
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                }

                $loadexcel  = $reader->load($_FILES['leads']['tmp_name']);
                $sheet  = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

                $list_sheet = $loadexcel->getSheetNames();

                $sheetData = $loadexcel->getSheetByName($list_sheet[0])->toArray(null, true, true ,true);

                $data = array();
                $numrow = 0;

                foreach($sheetData as $row){

					if($row['A'] != NULL){
						if($numrow >= 1){
							array_push($data, array(
								'jawara_id' 								=> $row['B'],
								'delivery_kode'	  							=> $row['C'],
								'delivery_email'  							=> $row['D'],
								'delivery_tanggal'  						=> $row['E'],
								'delivery_customer'  						=> $row['F'],
								'delivery_phone'  							=> $row['G'],
								'delivery_address'  						=> $row['H'],
								'delivery_toko'  							=> $row['I'],
								'delivery_no'  								=> $row['J'],
								'delivery_mobil'  							=> $row['K'],
								'total_price'  								=> $row['L'],
								'status'  									=> $row['M'],
								'status_reversal'  							=> $row['N'],
								'created_at'  								=> $row['O']
							));
						}
						$numrow++;
					}
					
                }
				// UPDATE BATCH
				$result = $this->db->insert_batch('delivery',$data);

				if($result){
					$this->session->set_flashdata('success','Bulk Upload Data Surat Jalan Telah Berhasil Diupdate Ke Sistem');
					redirect('index.php/uploads/delivery'); 
				}else{
					$this->session->set_flashdata('success','Bulk Upload Data Surat Jalan Jawara Gagal Diupdate Ke Sistem, Silahkan Coba Lagi');	
					redirect('index.php/uploads/delivery'); 
				}

            }
        }else{
            redirect("index.php/login");
        }
	}

	public function uploadDetail(){
		if($this->session->userdata('is_login') == TRUE){
            // Load plugin PHPExcel nya
            $file_mimes = array('application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            if(isset($_FILES['leads']['name']) && in_array($_FILES['leads']['type'], $file_mimes)) {

                $arr_file = explode('.', $_FILES['leads']['name']);
                $extension = end($arr_file);

                if($extension != 'xlsx') {
                    $this->session->set_flashdata('success', 'PROSES IMPORT DATA GAGAL! Format file yang anda masukkan salah!');

                    redirect('index.php/uploads/detail_delivery'); 
                } else {
                    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
                }

                $loadexcel  = $reader->load($_FILES['leads']['tmp_name']);
                $sheet  = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

                $list_sheet = $loadexcel->getSheetNames();

                $sheetData = $loadexcel->getSheetByName($list_sheet[0])->toArray(null, true, true ,true);

                $data = array();
                $numrow = 0;

                foreach($sheetData as $row){

					if($row['A'] != NULL){
						if($numrow >= 1){
							array_push($data, array(
								'delivery_kode' 				=> $row['B'],
								'sku'	  						=> $row['C'],
								'jumlah'  						=> $row['D'],
								'price'  						=> $row['E'],
								'total'  						=> $row['F']
							));
						}
						$numrow++;
					}
					
                }
				// UPDATE BATCH
				$result = $this->db->insert_batch('delivery_detail',$data);

				if($result){
					$this->session->set_flashdata('success','Bulk Upload Data Detail Surat Jalan Telah Berhasil Diupdate Ke Sistem');
					redirect('index.php/uploads/detail_delivery'); 
				}else{
					$this->session->set_flashdata('success','Bulk Upload Data Detail Surat Jalan Jawara Gagal Diupdate Ke Sistem, Silahkan Coba Lagi');	
					redirect('index.php/uploads/detail_delivery'); 
				}

            }
        }else{
            redirect("index.php/login");
        }
	}

	
}

