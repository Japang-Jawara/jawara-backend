<?php
class uploads_model extends CI_Model {

	public function uploadLeads($data){

        $result = $this->db->insert_batch('jawara_leads', $data);

        if($result){
            $this->session->set_flashdata('success','Upload Data Jawara Leads Telah Berhasil Diinput Ke Sistem');
            return TRUE;
        }else{
            $this->session->set_flashdata('success','Upload Data Jawara Leads Gagal Diinput Ke Sistem, Silahkan Coba Lagi');
            return FALSE;
        }
   }

}
