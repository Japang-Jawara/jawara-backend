<style>
    blink {
        -webkit-animation: 2s linear infinite kedip; /* for Safari 4.0 - 8.0 */
        animation: 2s linear infinite kedip;
    }
    /* for Safari 4.0 - 8.0 */
    @-webkit-keyframes kedip { 
        0% {
            visibility: hidden;
        }
        50% {
            visibility: hidden;
        }
        100% {
            visibility: visible;
        }
        }
        @keyframes kedip {
        0% {
            visibility: hidden;
        }
        50% {
            visibility: hidden;
        }
        100% {
            visibility: visible;
        }
    }
</style>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb hidden-print float-xl-right">
        <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
        <li class="breadcrumb-item active">Upload</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header hidden-print">Bulk<small> upload detail delivery</small></h1>
    <!-- end page-header -->
    <!-- begin invoice -->
    <div class="invoice">
        <!-- begin invoice-company -->
        <div class="invoice-company text-center text-red">
            <h1><b><blink>PERHATIAN !!!</blink></b> </h1>
        </div>
        <h2 class="text-center text-white" style=" padding: 15px; background-color: #4189C9;">HANYA DIGUNAKAN KETIKA URGENSI SAJA</h2>
        <!-- end invoice-company -->

        <div class="invoice-content">
            <!-- begin invoice-price -->
            <form method="POST" action="uploads/uploadDetail" enctype="multipart/form-data">
                <div class="invoice-price">
                    <div class="invoice-price-left">
                        <div class="invoice-price-row">
                            <div class="sub-price">
                                <small>Action</small>
                                <span class="text-inverse">Upload</span>
                                <input type="file" name="leads" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="invoice-price-right">
                        <span class="f-w-600">
                            <button id="leads" type="submit" class="btn btn-success">UPLOAD DATA</button>
                        </span>
                    </div>
                </div>
            </form>
            <!-- end invoice-price -->
            <br><br>
            <p class="text-danger"><b>Note:</b></p>
            <p class="text-danger">* Upload data baru jawara, <b>disarankan</b> disaat ketika tidak ada lagi jawara yang melakukan pendaftaran.</p>
            <p class="text-danger">* Update data baru ini, diperuntukan untuk urgensi saja dan <b>tidak diperkenankan</b>    untuk dilakukan berkali-kali atau setiap saat.</p>
            <p class="text-danger">* Jika ada yang tidak <b>dimengerti</b>. Harap ditanyakan terlebih dahulu ke yang bersangkutan, untuk meminimalisir data yang akan di bulk upload.</p>
        </div>
        
        <div class="invoice-note">
            
        </div>

        <!-- begin invoice-footer -->
        <div class="invoice-footer">
            <p class="text-center m-b-5 f-w-600">
                THANK YOU FOR YOUR ATTENTION
            </p>
        </div>
        <!-- end invoice-footer -->
    </div>
    <!-- end invoice -->
</div>
<!-- end #content -->