<style>
    blink {
        -webkit-animation: 2s linear infinite kedip; /* for Safari 4.0 - 8.0 */
        animation: 2s linear infinite kedip;
    }
    /* for Safari 4.0 - 8.0 */
    @-webkit-keyframes kedip { 
        0% {
            visibility: hidden;
        }
        50% {
            visibility: hidden;
        }
        100% {
            visibility: visible;
        }
        }
        @keyframes kedip {
        0% {
            visibility: hidden;
        }
        50% {
            visibility: hidden;
        }
        100% {
            visibility: visible;
        }
    }
</style>
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb hidden-print float-xl-right">
        <li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
        <li class="breadcrumb-item active">Upload</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header hidden-print">Rules <small>Bulk upload data jawara</small></h1>
    <!-- end page-header -->
    <!-- begin invoice -->
    <div class="invoice">
        <!-- begin invoice-company -->
        <div class="invoice-company text-center text-red">
            <h1><b><blink>PERHATIAN !!!</blink></b> </h1>
        </div>
        <h2 class="text-center text-white" style=" padding: 15px; background-color: #4189C9;">Jangan lupa untuk diperhatikan dan dibaca rules untuk upload</h2>
        <!-- end invoice-company -->

        <div class="invoice-content">
            <!-- begin table-responsive -->
            <div class="table-responsive">
                <table class="table table-invoice">
                    <thead>
                        <tr>
                            <th>TAHAPAN</th>
                            <th class="text-center" width="30%"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <span class="text-inverse">Download Existing Data</span><br />
                                <small>Data yang di download berupa file excel.</small>
                            </td>
                            <td class="text-center"><a href="<?= base_url('index.php/jawara/all_data') ?>" class="btn btn-sm btn-primary"> Download </a></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="text-inverse">Download Template Upload</span><br />
                                <small>Template excel ini digunakan untuk melakukan bulk upload dengan data yang disesuaikan dengan ketentuan yang telah dibuat pada template.</small>
                            </td>
                            <td class="text-center"><a href="https://s.id/TemplateBulkUpdate" class="btn btn-sm btn-warning"> Download </a></td>
                        </tr>
                        <tr>
                            <td>
                                <span class="text-inverse">Upload Data</span><br />
                                <small>Upload ini merupakan proses dari bulk update data dengan format file .xlsx dan jangan merubah isian yang sudah ada.</small>
                            </td>
                            <td class="text-center"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- end table-responsive -->
            <!-- begin invoice-price -->
            <form method="POST" action="uploads/bulkUpload" enctype="multipart/form-data">
                <div class="invoice-price">
                    <div class="invoice-price-left">
                        <div class="invoice-price-row">
                            <div class="sub-price">
                                <small>Action</small>
                                <span class="text-inverse">Upload</span>
                                <input type="file" name="leads" class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="invoice-price-right">
                        <span class="f-w-600">
                            <button id="leads" type="submit" class="btn btn-success">UPLOAD DATA</button>
                        </span>
                    </div>
                </div>
            </form>
            <!-- end invoice-price -->
            <br><br>
            <p class="text-danger"><b>Note:</b></p>
            <p class="text-danger">* Bulk upload, <b>disarankan</b> disaat ketika tidak ada lagi jawara yang melakukan pendaftaran.</p>
            <p class="text-danger">* Bulk upload ini, diperuntukan untuk urgensi saja dan <b>tidak diperkenankan</b>    untuk dilakukan berkali-kali atau setiap saat.</p>
            <p class="text-danger">* Jika ada yang tidak <b>dimengerti</b>. Harap ditanyakan terlebih dahulu ke yang bersangkutan, untuk meminimalisir data yang akan di bulk upload.</p>
        </div>
        
        <div class="invoice-note">
            
        </div>

        <!-- begin invoice-footer -->
        <div class="invoice-footer">
            <p class="text-center m-b-5 f-w-600">
                THANK YOU FOR YOUR ATTENTION
            </p>
        </div>
        <!-- end invoice-footer -->
    </div>
    <!-- end invoice -->
</div>
<!-- end #content -->