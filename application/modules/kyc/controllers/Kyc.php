<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kyc extends CI_Controller{
     function __construct(){
          parent::__construct();

          $this->load->model('Kyc_model', 'modelKyc');
     }

     function uploadKTP()
	{
		foreach ($_FILES as $name => $fileInfo) {
			$filename = $_FILES['file_ktp']['name'];
			$tmpname = $_FILES['file_ktp']['tmp_name'];
			$exp = explode('.', $filename);
			$ext = end($exp);
			$newname =  'KTP'.date('Ymdhis').".". $ext;
			$config['upload_path'] = './assets/berkas/ktp/';
			$config['upload_url'] =  base_url() . 'assets/berkas/ktp/';
			$config['allowed_types'] = "png|jpg|jpeg";
			$config['max_size'] = '2000';
			$config['file_name'] = $newname;
			$this->load->library('upload', $config);
			move_uploaded_file($tmpname, "assets/berkas/ktp/" . $newname);
			return $newname;
		}
	}

     function uploadKTPPasangan()
	{
		foreach ($_FILES as $name => $fileInfo) {
			$filename = $_FILES['file_ktp_pasangan']['name'];
			$tmpname = $_FILES['file_ktp_pasangan']['tmp_name'];
			$exp = explode('.', $filename);
			$ext = end($exp);
			$newname =  'KTP_PASANGAN'.date('Ymdhis').".". $ext;
			$config['upload_path'] = './assets/berkas/ktppasangan/';
			$config['upload_url'] =  base_url() . 'assets/berkas/ktppasangan/';
			$config['allowed_types'] = "png|jpg|jpeg";
			$config['max_size'] = '2000';
			$config['file_name'] = $newname;
			$this->load->library('upload', $config);
			move_uploaded_file($tmpname, "assets/berkas/ktppasangan/" . $newname);
			return $newname;
		}
	}

     function uploadKK()
	{
		foreach ($_FILES as $name => $fileInfo) {
			$filename = $_FILES['file_kk']['name'];
			$tmpname = $_FILES['file_kk']['tmp_name'];
			$exp = explode('.', $filename);
			$ext = end($exp);
			$newname =  'KK'.date('Ymdhis')."." . $ext;
			$config['upload_path'] = './assets/berkas/kk/';
			$config['upload_url'] =  base_url() . 'assets/berkas/kk/';
			$config['allowed_types'] = "png|jpg|jpeg";
			$config['max_size'] = '2000';
			$config['file_name'] = $newname;
			$this->load->library('upload', $config);
			move_uploaded_file($tmpname, "assets/berkas/kk/" . $newname);
			return $newname;
		}
	}

     function uploadStore()
	{
		foreach ($_FILES as $name => $fileInfo) {
			$filename = $_FILES['file_store']['name'];
			$tmpname = $_FILES['file_store']['tmp_name'];
			$exp = explode('.', $filename);
			$ext = end($exp);
			$newname =  'STORE'.date('Ymdhis')."." . $ext;
			$config['upload_path'] = './assets/berkas/store/';
			$config['upload_url'] =  base_url() . 'assets/berkas/store/';
			$config['allowed_types'] = "png|jpg|jpeg";
			$config['max_size'] = '2000';
			$config['file_name'] = $newname;
			$this->load->library('upload', $config);
			move_uploaded_file($tmpname, "assets/berkas/store/" . $newname);
			return $newname;
		}
	}

     function uploadGearth()
	{
		foreach ($_FILES as $name => $fileInfo) {
			$filename = $_FILES['file_gearth']['name'];
			$tmpname = $_FILES['file_gearth']['tmp_name'];
			$exp = explode('.', $filename);
			$ext = end($exp);
			$newname =  'GEARTH'.date('Ymdhis')."." . $ext;
			$config['upload_path'] = './assets/berkas/gearth/';
			$config['upload_url'] =  base_url() . 'assets/berkas/gearth/';
			$config['allowed_types'] = "png|jpg|jpeg";
			$config['max_size'] = '2000';
			$config['file_name'] = $newname;
			$this->load->library('upload', $config);
			move_uploaded_file($tmpname, "assets/berkas/gearth/" . $newname);
			return $newname;
		}
	}

     function uploadGetContact()
	{
		foreach ($_FILES as $name => $fileInfo) {
			$filename = $_FILES['file_gcf']['name'];
			$tmpname = $_FILES['file_gcf']['tmp_name'];
			$exp = explode('.', $filename);
			$ext = end($exp);
			$newname =  'GCF'.date('Ymdhis')."." . $ext;
			$config['upload_path'] = './assets/berkas/get_contact/';
			$config['upload_url'] =  base_url() . 'assets/berkas/get_contact/';
			$config['allowed_types'] = "png|jpg|jpeg";
			$config['max_size'] = '2000';
			$config['file_name'] = $newname;
			$this->load->library('upload', $config);
			move_uploaded_file($tmpname, "assets/berkas/get_contact/" . $newname);
			return $newname;
		}
	}

     function index(){
          if($this->session->userdata('is_login') == TRUE){

               $x['getDSC'] = $this->modelKyc->getDSC();
               $x['getSD'] = $this->modelKyc->getSD();
               $x['getBA'] = $this->modelKyc->getBA();

               $this->load->view("include/head");
               $this->load->view("include/top-header");
               $this->load->view('kyc', $x);
               $this->load->view("include/sidebar");
               $this->load->view("include/panel");
               $this->load->view("include/footer");
               $this->load->view("include/alert");
          }else{
               $this->session->set_flashdata('success', 'Upsss!!!, Login dulu ya.');
               redirect('index.php/login');
          }
     }

     function tableKyc() {
          $data = $this->modelKyc->getKyc();

          $no = 1;
          $jawara = array();

          foreach($data as $r) {

               $input = "<input type='checkbox' class='approve_checkbox' value='$r->id'>";

               $email_verifikasi = $r->email_verifikasi_kyc;
               $jawara_id = $r->id;
               $nik_ktp = $r->nik_ktp_kyc;
               $name = $r->name_kyc;
               $phone = $r->phone_kyc;
               $bank_code = $r->bank_code_kyc;
               $nomor_rekening = $r->nomor_rekening_kyc;
               $delivered_email = $r->delivered_email_kyc;
               $active_phone = $r->active_phone_kyc;
               $active_whatsapp = $r->active_whatsapp_kyc;
               $correct_ktp = $r->correct_ktp_kyc;
               $correct_kk = $r->correct_kk_kyc;
               $nama_jawara = $r->nama_jawara_kyc;
               $ktp_jelas = $r->ktp_jelas_kyc;
               $kk_jelas = $r->kk_jelas_kyc;
               $store_jelas = $r->store_jelas_kyc;

               if($r->status_jawara_kyc == 0){
                    $status = 'Reject - Hold/Cancel Join';
               }else if($r->status_jawara_kyc == 1){
                    $status = 'Approve';
               }else if($r->status_jawara_kyc == 2){
                    $status = 'Approve - Pembaruan Data';
               }

               if($r->file_ktp_kyc != NULL){
                    $file_ktp = '<a href="'.$r->file_ktp_kyc.'" target="_blank" class="btn btn-sm btn-primary">'.$r->file_ktp_kyc.'</a>';
               }else{
                    $file_ktp = 'Tidak ada File KTP';
               }
               
               if($r->file_ktp_pasangan_kyc != NULL){
                    $file_ktp_pasangan = '<a href="'.$r->file_ktp_pasangan_kyc.'" target="_blank" class="btn btn-sm btn-secondary">'.$r->file_ktp_pasangan_kyc.'</a>';
               }else{
                    $file_ktp_pasangan = 'Tidak ada File KTP Pasangan';
               }

               if($r->file_kk_kyc != NULL){
                    $file_kk = '<a href="'.$r->file_kk_kyc.'" target="_blank" class="btn btn-sm btn-success">'.$r->file_kk_kyc.'</a>';
               }else{
                    $file_kk = 'Tidak ada File KK';
               }

               if($r->file_store_kyc != NULL){
                    $file_store = '<a href="'.$r->file_store_kyc.'" target="_blank" class="btn btn-sm btn-warning">'.$r->file_store_kyc.'</a>';
               }else{
                    $file_store = 'Tidak ada File STORE';
               }

               $nik_kk = $r->nik_kk_kyc;
               $notes = $r->notes_kyc == NULL ? "<font>Catatan Tidak Ada</font>" : $r->notes_kyc;
               $submit_check = $r->submit_check_kyc;
               $dokumen_pendukung = $r->dokumen_pendukung_kyc;
               $total_score = $r->total_score_kyc;

               $grading = $total_score >= 85 ? 'A+' : ($total_score >= 70 ? 'A' : ($total_score >= 60 ? 'B+' : ($total_score >= 50 ? 'B' : ($total_score >= 40 ? 'C' : ($total_score >= 30 ? 'C-' : ($total_score >= 15 ? 'D' : 'E'))))));

               $created_at = $r->created_at_kyc;

               $action = "<a href='javascript:;' 
               data-id='$r->id'
               data-name='$r->name_kyc'
               data-phone='$r->phone_kyc'
               data-nik_ktp='$r->nik_ktp_kyc'
               data-nik_kk='$r->nik_kk_kyc'
               data-bank_code='$r->bank_code_kyc'
               data-nomor_rekening='$r->nomor_rekening_kyc'

               data-file_ktp='$r->file_ktp_kyc'
               data-file_ktp_pasangan='$r->file_ktp_pasangan_kyc'
               data-file_kk='$r->file_kk_kyc'
               data-file_store='$r->file_store_kyc'
               
               data-nama_jawara='$r->nama_jawara_kyc'
               data-delivered_email='$r->delivered_email_kyc'
               data-active_phone='$r->active_phone_kyc'
               data-active_whatsapp='$r->active_whatsapp_kyc'
               data-correct_ktp='$r->correct_ktp_kyc'
               data-correct_kk='$r->correct_kk_kyc'

               data-ktp_jelas='$r->ktp_jelas_kyc'
               data-kk_jelas='$r->kk_jelas_kyc'
               data-store_jelas='$r->store_jelas_kyc'

               data-status='$r->status_jawara_kyc'

               data-notes='$r->notes_kyc'

               data-toggle='modal' data-target='#ubahkyc'
               class='btn btn-sm btn-warning'><i class='fa fas fa-edit'></i> EDIT</a>
               
               <a href='javascript:;' 
               data-id='$r->id'
               data-name='$r->name_kyc'
               data-phone='$r->phone_kyc'
               data-nik_ktp='$r->nik_ktp_kyc'
               data-toggle='modal' data-target='#digital_inspection'
               class='btn btn-sm btn-success'><i class='fa fas fa-edit'></i> Digital Inspection</a>
               ";

               $jawara[] = array(
                    $no++,
                    $input,
                    $email_verifikasi,
                    $jawara_id,
                    $nik_ktp,
                    $name,
                    $phone,
                    $bank_code,
                    $nomor_rekening,
                    $delivered_email,
                    $active_phone,
                    $active_whatsapp,
                    $correct_ktp,
                    $correct_kk,
                    $nama_jawara,
                    $ktp_jelas,
                    $kk_jelas,
                    $store_jelas,
                    $status,
                    $file_ktp,
                    $file_ktp_pasangan,
                    $file_kk,
                    $file_store,
                    $nik_kk,
                    $notes,
                    $submit_check,
                    $dokumen_pendukung,
                    $total_score,
                    $grading,
                    $created_at,
                    $action
               );
               
          }
          
          echo json_encode($jawara);
     }

     public function tableKycReject(){
          $data = $this->modelKyc->getKycReject();

          $no = 1;
          $jawara = array();

          foreach($data as $r) {

               $email_verifikasi = $r->email_verifikasi_kyc;
               $jawara_id = $r->id;
               $nik_ktp = $r->nik_ktp_kyc;
               $name = $r->name_kyc;
               $phone = $r->phone_kyc;
               $bank_code = $r->bank_code_kyc;
               $nomor_rekening = $r->nomor_rekening_kyc;
               $delivered_email = $r->delivered_email_kyc;
               $active_phone = $r->active_phone_kyc;
               $active_whatsapp = $r->active_whatsapp_kyc;
               $correct_ktp = $r->correct_ktp_kyc;
               $correct_kk = $r->correct_kk_kyc;
               $nama_jawara = $r->nama_jawara_kyc;
               $ktp_jelas = $r->ktp_jelas_kyc;
               $kk_jelas = $r->kk_jelas_kyc;
               $store_jelas = $r->store_jelas_kyc;

               if($r->status_jawara_kyc == 0){
                    $status = 'Reject - Hold/Cancel Join';
               }else if($r->status_jawara_kyc == 1){
                    $status = 'Approve';
               }else if($r->status_jawara_kyc == 2){
                    $status = 'Approve - Pembaruan Data';
               }

               if($r->file_ktp_kyc != NULL){
                    $file_ktp = '<a href="'.$r->file_ktp_kyc.'" target="_blank" class="btn btn-sm btn-primary">'.$r->file_ktp_kyc.'</a>';
               }else{
                    $file_ktp = 'Tidak ada File KTP';
               }
               
               if($r->file_ktp_pasangan_kyc != NULL){
                    $file_ktp_pasangan = '<a href="'.$r->file_ktp_pasangan_kyc.'" target="_blank" class="btn btn-sm btn-secondary">'.$r->file_ktp_pasangan_kyc.'</a>';
               }else{
                    $file_ktp_pasangan = 'Tidak ada File KTP Pasangan';
               }

               if($r->file_kk_kyc != NULL){
                    $file_kk = '<a href="'.$r->file_kk_kyc.'" target="_blank" class="btn btn-sm btn-success">'.$r->file_kk_kyc.'</a>';
               }else{
                    $file_kk = 'Tidak ada File KK';
               }

               if($r->file_store_kyc != NULL){
                    $file_store = '<a href="'.$r->file_store_kyc.'" target="_blank" class="btn btn-sm btn-warning">'.$r->file_store_kyc.'</a>';
               }else{
                    $file_store = 'Tidak ada File STORE';
               }

               $nik_kk = $r->nik_kk_kyc;
               $notes = $r->notes_kyc == NULL ? "<font>Catatan Tidak Ada</font>" : $r->notes_kyc;
               $submit_check = $r->submit_check_kyc;
               $dokumen_pendukung = $r->dokumen_pendukung_kyc;
               $total_score = $r->total_score_kyc;

               $grading = $total_score >= 85 ? 'A+' : ($total_score >= 70 ? 'A' : ($total_score >= 60 ? 'B+' : ($total_score >= 50 ? 'B' : ($total_score >= 40 ? 'C' : ($total_score >= 30 ? 'C-' : ($total_score >= 15 ? 'D' : 'E'))))));

               $created_at = $r->created_at_kyc;

               $action = "<a href='javascript:;' 
               data-id='$r->id'
               data-name='$r->name_kyc'
               data-phone='$r->phone_kyc'
               data-nik_ktp='$r->nik_ktp_kyc'
               data-nik_kk='$r->nik_kk_kyc'
               data-bank_code='$r->bank_code_kyc'
               data-nomor_rekening='$r->nomor_rekening_kyc'

               data-file_ktp='$r->file_ktp_kyc'
               data-file_ktp_pasangan='$r->file_ktp_pasangan_kyc'
               data-file_kk='$r->file_kk_kyc'
               data-file_store='$r->file_store_kyc'
               
               data-nama_jawara='$r->nama_jawara_kyc'
               data-delivered_email='$r->delivered_email_kyc'
               data-active_phone='$r->active_phone_kyc'
               data-active_whatsapp='$r->active_whatsapp_kyc'
               data-correct_ktp='$r->correct_ktp_kyc'
               data-correct_kk='$r->correct_kk_kyc'

               data-ktp_jelas='$r->ktp_jelas_kyc'
               data-kk_jelas='$r->kk_jelas_kyc'
               data-store_jelas='$r->store_jelas_kyc'

               data-status='$r->status_jawara_kyc'

               data-notes='$r->notes_kyc'

               data-toggle='modal' data-target='#ubahkyc'
               class='btn btn-sm btn-warning'><i class='fa fas fa-edit'></i> EDIT</a>
               
               ";

               $jawara[] = array(
                    $no++,
                    $email_verifikasi,
                    $jawara_id,
                    $nik_ktp,
                    $name,
                    $phone,
                    $bank_code,
                    $nomor_rekening,
                    $delivered_email,
                    $active_phone,
                    $active_whatsapp,
                    $correct_ktp,
                    $correct_kk,
                    $nama_jawara,
                    $ktp_jelas,
                    $kk_jelas,
                    $store_jelas,
                    $status,
                    $file_ktp,
                    $file_ktp_pasangan,
                    $file_kk,
                    $file_store,
                    $nik_kk,
                    $notes,
                    $submit_check,
                    $dokumen_pendukung,
                    $total_score,
                    $grading,
                    $created_at,
                    $action
               );
               
          }
          
          echo json_encode($jawara);
     }

     function doneDi() {
          $data = $this->modelKyc->doneDi();

          $no = 1;
          $jawara = array();

          foreach($data as $r) {

               $email_verifikasi = $r->email_verifikasi_kyc;
               $jawara_id = $r->id;
               $nik_ktp = $r->nik_ktp_kyc;
               $name = $r->name_kyc;
               $phone = $r->phone_kyc;
               $bank_code = $r->bank_code_kyc;
               $nomor_rekening = $r->nomor_rekening_kyc;
               $delivered_email = $r->delivered_email_kyc;
               $active_phone = $r->active_phone_kyc;
               $active_whatsapp = $r->active_whatsapp_kyc;
               $correct_ktp = $r->correct_ktp_kyc;
               $correct_kk = $r->correct_kk_kyc;
               $nama_jawara = $r->nama_jawara_kyc;
               $ktp_jelas = $r->ktp_jelas_kyc;
               $kk_jelas = $r->kk_jelas_kyc;
               $store_jelas = $r->store_jelas_kyc;

               if($r->status_jawara_kyc == 0){
                    $status = 'Reject - Hold/Cancel Join';
               }else if($r->status_jawara_kyc == 1){
                    $status = 'Approve';
               }else if($r->status_jawara_kyc == 2){
                    $status = 'Approve - Pembaruan Data';
               }

               if($r->file_ktp_kyc != NULL){
                    $file_ktp = '<a href="'.$r->file_ktp_kyc.'" target="_blank" class="btn btn-sm btn-primary">'.$r->file_ktp_kyc.'</a>';
               }else{
                    $file_ktp = 'Tidak ada File KTP';
               }
               
               if($r->file_ktp_pasangan_kyc != NULL){
                    $file_ktp_pasangan = '<a href="'.$r->file_ktp_pasangan_kyc.'" target="_blank" class="btn btn-sm btn-secondary">'.$r->file_ktp_pasangan_kyc.'</a>';
               }else{
                    $file_ktp_pasangan = 'Tidak ada File KTP Pasangan';
               }

               if($r->file_kk_kyc != NULL){
                    $file_kk = '<a href="'.$r->file_kk_kyc.'" target="_blank" class="btn btn-sm btn-success">'.$r->file_kk_kyc.'</a>';
               }else{
                    $file_kk = 'Tidak ada File KK';
               }

               if($r->file_store_kyc != NULL){
                    $file_store = '<a href="'.$r->file_store_kyc.'" target="_blank" class="btn btn-sm btn-warning">'.$r->file_store_kyc.'</a>';
               }else{
                    $file_store = 'Tidak ada File STORE';
               }

               $nik_kk = $r->nik_kk_kyc;
               $notes = $r->notes_kyc == NULL ? "<font>Catatan Tidak Ada</font>" : $r->notes_kyc;
               $submit_check = $r->submit_check_kyc;
               $dokumen_pendukung = $r->dokumen_pendukung_kyc;
               $total_score = $r->total_score_kyc;

               $grading = $total_score >= 85 ? 'A+' : ($total_score >= 70 ? 'A' : ($total_score >= 60 ? 'B+' : ($total_score >= 50 ? 'B' : ($total_score >= 40 ? 'C' : ($total_score >= 30 ? 'C-' : ($total_score >= 15 ? 'D' : 'E'))))));

               $created_at = $r->created_at_kyc;
               $updated_di = $r->updated_di_kyc;

               $action = "<a href='javascript:;' 
               data-id='$r->id'
               data-name='$r->name_kyc'
               data-phone='$r->phone_kyc'
               data-nik_ktp='$r->nik_ktp_kyc'
               data-nik_kk='$r->nik_kk_kyc'
               data-bank_code='$r->bank_code_kyc'
               data-nomor_rekening='$r->nomor_rekening_kyc'

               data-file_ktp='$r->file_ktp_kyc'
               data-file_ktp_pasangan='$r->file_ktp_pasangan_kyc'
               data-file_kk='$r->file_kk_kyc'
               data-file_store='$r->file_store_kyc'
               
               data-nama_jawara='$r->nama_jawara_kyc'
               data-delivered_email='$r->delivered_email_kyc'
               data-active_phone='$r->active_phone_kyc'
               data-active_whatsapp='$r->active_whatsapp_kyc'
               data-correct_ktp='$r->correct_ktp_kyc'
               data-correct_kk='$r->correct_kk_kyc'

               data-ktp_jelas='$r->ktp_jelas_kyc'
               data-kk_jelas='$r->kk_jelas_kyc'
               data-store_jelas='$r->store_jelas_kyc'

               data-status='$r->status_jawara_kyc'

               data-notes='$r->notes_kyc'

               data-toggle='modal' data-target='#ubahkyc'
               class='btn btn-sm btn-warning'><i class='fa fas fa-edit'></i> EDIT</a>";

               $jawara[] = array(
                    $no++,
                    $email_verifikasi,
                    $jawara_id,
                    $nik_ktp,
                    $name,
                    $phone,
                    $bank_code,
                    $nomor_rekening,
                    $delivered_email,
                    $active_phone,
                    $active_whatsapp,
                    $correct_ktp,
                    $correct_kk,
                    $nama_jawara,
                    $ktp_jelas,
                    $kk_jelas,
                    $store_jelas,
                    $status,
                    $file_ktp,
                    $file_ktp_pasangan,
                    $file_kk,
                    $file_store,
                    $nik_kk,
                    $notes,
                    $submit_check,
                    $dokumen_pendukung,
                    $total_score,
                    $grading,
                    $created_at,
                    $updated_di,
                    $action
               );
               
          }
          
          echo json_encode($jawara);
     }

     function ubahKYC(){

          $id = $this->input->post('id');

          $ktp = '';
          $ktp_pasangan = '';
          $kk = '';
          $store = '';

          if (!empty($_FILES['file_ktp']['name'])) {
               $newname = $this->uploadKTP();
               $data['file_ktp'] = $newname;
               $ktp = $newname;

               $x['file_ktp_kyc'] = base_url('assets/berkas/ktp/').$ktp;
          }else{
               $x['file_ktp_kyc'] = $this->input->post('file_ktpx');
          }

          if (!empty($_FILES['file_ktp_pasangan']['name'])) {
               $newname = $this->uploadKTPPasangan();
               $data['file_ktp_pasangan'] = $newname;
               $ktp_pasangan = $newname;

               $x['file_ktp_pasangan_kyc'] = base_url('assets/berkas/ktppasangan/').$ktp_pasangan ;
          }

          if (!empty($_FILES['file_kk']['name'])) {
               $newname = $this->uploadKK();
               $data['file_kK'] = $newname;
               $kk = $newname;

               $x['file_kk_kyc'] = base_url('assets/berkas/kk/').$kk;
          }else{
               $x['file_kk_kyc'] = $this->input->post('file_kkx');
          }

          if (!empty($_FILES['file_store']['name'])) {
               $newname = $this->uploadStore();
               $data['file_store'] = $newname;
               $store = $newname;

               $x['file_store_kyc'] = base_url('assets/berkas/store/').$store;
          }else{
               $x['file_store_kyc'] = $this->input->post('file_storex');
          }          

          //UBAH DATA FROM TABLE JAWARA KYC
          $x['email_verifikasi_kyc'] = $this->input->post('email');
          $x['delivered_email_kyc'] = $this->input->post('delivered_email');
          $x['active_phone_kyc'] = $this->input->post('active_phone');
          $x['active_whatsapp_kyc'] = $this->input->post('active_whatsapp');
          $x['correct_ktp_kyc'] = $this->input->post('correct_ktp');
          $x['correct_kk_kyc'] = $this->input->post('correct_kk');
          $x['nama_jawara_kyc'] = $this->input->post('nama_jawara');
          $x['ktp_jelas_kyc'] = $this->input->post('ktp_jelas');
          $x['kk_jelas_kyc'] = $this->input->post('kk_jelas');
          $x['store_jelas_kyc'] = $this->input->post('store_jelas');
          $x['status_jawara_kyc'] = $this->input->post('status');
          $x['updated_at_kyc'] = date('Y-m-d H:i:s');
          
          $x['phone_kyc'] = $this->input->post('phone');
          $x['name_kyc'] = $this->input->post('name');
          $x['nik_ktp_kyc'] = $this->input->post('nik_ktp');
          $x['nik_kk_kyc'] = $this->input->post('nik_kk');
          $x['bank_code_kyc'] = $this->input->post('bank_code');
          $x['nomor_rekening_kyc'] = $this->input->post('nomor_rekening');

          $x['notes_kyc'] = $this->input->post('notes');

          $this->modelKyc->ubahKYC($x, $id);

          redirect('index.php/kyc');
     }

     public function createDI(){

          $jawara_id = $this->input->post('id');

          $file_gearth = '';
          $file_gcf = '';

          if (!empty($_FILES['file_gearth']['name'])) {
               $newname = $this->uploadGearth();
               $data['file_gearth'] = $newname;
               $file_gearth = $newname;

               $x['file_gearth_di'] = base_url('assets/berkas/gearth/').$file_gearth;
          }

          if (!empty($_FILES['file_gcf']['name'])) {
               $newname = $this->uploadGetContact();
               $data['file_gcf'] = $newname;
               $file_gcf = $newname;

               $x['file_gcf_di'] = base_url('assets/berkas/get_contact/').$file_gcf;
          }

          $x['email_verifikasi_di'] = $this->input->post('email');
          $x['nik_ktp_di'] = $this->input->post('nik_ktp');
          $x['name_di'] = $this->input->post('name');
          $x['phone_di'] = $this->input->post('phone');

          $x['cek_npwp_di'] = $this->input->post('cek_npwp');
          $x['cek_getcontact_di'] = $this->input->post('cek_getcontact');
          $x['cek_gearth_di'] = $this->input->post('cek_gearth');
          $x['cek_phone_di'] = $this->input->post('cek_phone');

          $x['kyc_di'] = ($x['cek_npwp']*2) + ($x['cek_getcontact']*2) + ($x['cek_gearth']*2) + ($x['cek_phone']*2);

          $x['score_getcontact_di'] = $this->input->post('score_getcontact');
          $x['score_gearth_di'] = $this->input->post('score_gearth');
          
          $x['score_di'] = ($x['score_getcontact']*2) + ($x['score_gearth']*2);

          $x['npwp_di'] = $this->input->post('npwp');

          $x['gf_competitor_di'] = $this->input->post('gf_competitor');
          $x['gf_kelayakan_di'] = $this->input->post('gf_kelayakan');
          $x['gf_lokasi_di'] = $this->input->post('gf_lokasi');
          
          $x['gearth_di'] = ($x['gf_competitor_di']*2) + ($x['gf_kelayakan_di']*2) + ($x['gf_lokasi_di']*2);

          $x['gcf_tags_di'] = $this->input->post('gcf_tags');
          $x['gcf_penilaian_di'] = $this->input->post('gcf_penilaian');
          $x['gcf_aktif_di'] = $this->input->post('gcf_aktif');

          $x['get_contact_di'] = ($x['gcf_tags_di']*2) + ($x['gcf_penilaian_di']*2) + ($x['gcf_aktif_di']*2);

          $x['notes_di'] = $this->input->post('notes');
          $x['gmaps_di'] = $this->input->post('gmaps');

          // RUMUS
          $x['percent_kyc_di'] = round(($x['kyc_di'] / 8 * 100));
          $x['percent_score_di'] = round(($x['score_di'] / 12 * 100));
          $x['percent_gearth_di'] = round(($x['gearth_di'] / 18 * 100));
          $x['percent_get_contact_di'] = round(($x['get_contact_di'] / 18 * 100));

          $percent_kyc = round(($x['kyc_di'] / 8 * 100))/100;
          $percent_score = round(($x['score_di'] / 12 * 100))/100;
          $percent_gearth = round(($x['gearth_di'] / 18 * 100))/100;
          $percent_get_contact = round(($x['get_contact_di'] / 18 * 100))/100;
          
          $ws = '0.25';
          $total = ($percent_kyc*$ws) + ($percent_score*$ws) + ($percent_gearth*$ws) + ($percent_get_contact*$ws);
          $x['total_score_di'] = round(($total * 100));

          $total_score = round(($total * 100));
          
          if($total_score >= 85){
               $x['user_grading_di'] = 'A+';
               $x['status_jawara_di'] = '1';
          }else if($total_score >= 70){
               $x['user_grading_di'] = 'A';
               $x['status_jawara_di'] = '1';
          }else if($total_score >= 60){
               $x['user_grading_di'] = 'B+';
               $x['status_jawara_di'] = '1';
          }else if($total_score >= 50){
               $x['user_grading_di'] = 'B';
               $x['status_jawara_di'] = '1';
          }else if($total_score >= 40){
               $x['user_grading_di'] = 'C';
               $x['status_jawara_di'] = '1';
          }else if($total_score >= 30){
               $x['user_grading_di'] = 'C-';
               $x['status_jawara_di'] = '0';
          }else if($total_score >= 15){
               $x['user_grading_di'] = 'D';
               $x['status_jawara_di'] = '0';
          }else{
               $x['user_grading_di'] = 'E';
               $x['status_jawara_di'] = '0';
          }
          
          $x['created_at_di'] = date('Y-m-d H:i:s');

          //UPDATE DATA FROM TABLE JAWARA LEDS
          $u['status_digital_inspection_kyc'] = '1';
          $u['updated_di_kyc'] = date('Y-m-d H:i:s');

          $this->modelKyc->createDI($x, $u, $jawara_id);

          redirect('index.php/kyc');


     }

     public function approve_all()
	{
          if ($this->input->post('checkbox_value')) {
               $jawara_id = $this->input->post('checkbox_value');
               
               for ($count = 0; $count < count($jawara_id); $count++) {

                    $getJawara = $this->modelKyc->getJawara($jawara_id[$count]);

                    $x['file_gearth_di'] = 'https://dashboard.japangjawara.com/assets/berkas/gearth/GEARTH20220723070147.png';
                    $x['file_gcf_di'] = 'https://dashboard.japangjawara.com/assets/berkas/get_contact/GCF20220723070147.png';

                    $x['email_verifikasi_di'] = $this->session->userdata('email_address');

                    $x['nik_ktp_di'] = $getJawara->nik_ktp;
                    $x['name_di'] = $getJawara->name;
                    $x['phone_di'] = $getJawara->phone;

                    $x['cek_npwp_di'] = 0;
                    $x['cek_getcontact_di'] = 0;
                    $x['cek_gearth_di'] = 0;
                    $x['cek_phone_di'] = 0;

                    $x['kyc_di'] = 0;

                    $x['score_getcontact_di'] = 0;
                    $x['score_gearth_di'] = 0;
                    
                    $x['score_di'] = 0;

                    $x['npwp_di'] = 0;

                    $x['gf_competitor_di'] = 0;
                    $x['gf_kelayakan_di'] = 0;
                    $x['gf_lokasi_di'] = 0;
                    
                    $x['gearth_di'] = 0;

                    $x['gcf_tags_di'] = 0;
                    $x['gcf_penilaian_di'] = 0;
                    $x['gcf_aktif_di'] = 0;

                    $x['get_contact_di'] = 0;

                    $x['notes_di'] = '';
                    $x['gmaps_di'] = '0,0';

                    // RUMUS
                    $x['percent_kyc_di'] = 0;
                    $x['percent_score_di'] = 0;
                    $x['percent_gearth_di'] = 0;
                    $x['percent_get_contact_di'] = 0;

                    $x['total_score_di'] = 0;
                    $x['user_grading_di'] = 'E';
                    $x['status_jawara_di'] = '0';
                    $x['created_at_di'] = date('Y-m-d H:i:s');

                    //UPDATE DATA FROM TABLE JAWARA LEDS
                    $x['status_digital_inspection_kyc'] = '1';
                    $x['updated_di_kyc'] = date('Y-m-d H:i:s');

                    $this->modelKyc->approve_all($jawara_id[$count], $x);
               }
          }
	}

}