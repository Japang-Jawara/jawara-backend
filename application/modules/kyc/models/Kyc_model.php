<?php
class Kyc_model extends CI_Model{

    public function getJawara($jawara_id){
        return $this->db->select('*')->from('jawara_leads')->where('id', $jawara_id)->get()->result()[0];
    }

    public function getDSC(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'DSC')->get()->result();
    }

    public function getSD(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'SD')->get()->result();
    }

    public function getBA(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'BA')->get()->result();
    }

    public function getKyc(){
        return $this->db->query("SELECT a.*,
        @real_sc := a.delivered_email_kyc + a.active_phone_kyc + a.active_whatsapp_kyc + a.correct_ktp_kyc + a.correct_kk_kyc + a.nama_jawara_kyc AS real_sc,
        @submit_check_kyc := concat(round((@real_sc / 6 * 100)), '%') AS submit_check_kyc,
        
        @real_dp := a.ktp_jelas_kyc + a.kk_jelas_kyc + a.store_jelas_kyc AS real_dp,
        @dokumen_pendukung_kyc := concat(round((@real_dp / 3 * 100)), '%') AS dokumen_pendukung_kyc,
        
        @total_score_kyc := concat((@submit_check_kyc*0.5) + (@dokumen_pendukung_kyc*0.5), '%') AS total_score_kyc
        
        FROM jawara_leads as a WHERE a.status_digital_inspection_kyc IS NULL AND (a.status_jawara_kyc = 1 OR a.status_jawara_kyc = 2) ORDER BY a.id DESC")->result();
    }

    public function doneDi(){
        return $this->db->query("SELECT a.*,
        @real_sc := a.delivered_email_kyc + a.active_phone_kyc + a.active_whatsapp_kyc + a.correct_ktp_kyc + a.correct_kk_kyc + a.nama_jawara_kyc AS real_sc,
        @submit_check_kyc := concat(round((@real_sc / 6 * 100)), '%') AS submit_check_kyc,
        
        @real_dp := a.ktp_jelas_kyc + a.kk_jelas_kyc + a.store_jelas_kyc AS real_dp,
        @dokumen_pendukung_kyc := concat(round((@real_dp / 3 * 100)), '%') AS dokumen_pendukung_kyc,
        
        @total_score_kyc := concat((@submit_check_kyc*0.5) + (@dokumen_pendukung_kyc*0.5), '%') AS total_score_kyc
        
        FROM jawara_leads as a WHERE a.status_digital_inspection_kyc = '1' AND (a.status_jawara_kyc = 1 OR a.status_jawara_kyc = 2) ORDER BY a.id DESC")->result();
    }

    public function getKycReject(){
        return $this->db->query("SELECT a.*,
        @real_sc := a.delivered_email_kyc + a.active_phone_kyc + a.active_whatsapp_kyc + a.correct_ktp_kyc + a.correct_kk_kyc + a.nama_jawara_kyc AS real_sc,
        @submit_check_kyc := concat(round((@real_sc / 6 * 100)), '%') AS submit_check_kyc,
        
        @real_dp := a.ktp_jelas_kyc + a.kk_jelas_kyc + a.store_jelas_kyc AS real_dp,
        @dokumen_pendukung_kyc := concat(round((@real_dp / 3 * 100)), '%') AS dokumen_pendukung_kyc,
        
        @total_score_kyc := concat((@submit_check_kyc*0.5) + (@dokumen_pendukung_kyc*0.5), '%') AS total_score_kyc
        
        FROM jawara_leads as a WHERE a.status_jawara_kyc = 0 ORDER BY a.id DESC")->result();
    }

    public function ubahKYC($data, $id){

        $result = $this->db->where('id', $id)->update('jawara_leads', $data);

        if($result){
            $this->session->set_flashdata('success','Data Berhasil Diubah');
            return TRUE;
        }else{
            $this->session->set_flashdata('success','Data Gagal Diubah, Silahkan Coba Lagi');
            return FALSE;
        }
    }

    public function createDI($data, $data2, $id){

        $result = $this->db->where('id', $id)->update('jawara_leads', $data);

        if($result){
            $this->db->where('id', $id)->update('jawara_leads', $data2);
            $this->session->set_flashdata('success','Jawara Digital Inspection Telah Berhasil Diinput Ke Sistem');
            return TRUE;
        }else{
            $this->session->set_flashdata('success','Jawara Digital Inspection Gagal Diinput Ke Sistem, Silahkan Coba Lagi');
            return FALSE;
        }
    }

    function approve_all($jawara_id, $data){
        
        $this->db->where('id', $jawara_id);
        return $this->db->update('jawara_leads', $data);
        
	}
}
?>