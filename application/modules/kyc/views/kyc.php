<div id="content" class="content">
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="<?php echo base_url('home');?>">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="<?php echo base_url('kepegawaian/thl');?>">DATA KYC</a></li>
    </ol>
    <h1 class="page-header"><b>DATA KYC</b></h1>
    <div class="note note-info note-with-right-icon">
        <div class="note-icon"><i class="fa fa-lightbulb"></i></div>
        <div class="note-content text-right">
            <h2><b>Know Your Customer (KYC)</b></h2>
            <p>Proses untuk melengkapi data-data para jawara ataupun re-check data dari inputan jawara yang telah melakukan pendaftaran ke program <b>JAWARA</b>.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">
                    </h4>
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                </div>

                <div class="panel-body">
                    <div class ="table-responsive">
                        <div class="col-xl-12">
                            <ul class="nav nav-tabs nav-tabs-inverse nav-justified nav-justified-mobile" data-sortable-id="index-2">
                                <li class="nav-item">
                                    <a href="#bm" data-toggle="tab" class="nav-link active">
                                        <span><i class="fa fa-list fa-lg m-r-5"></i> BELUM DIGITAL INSPECTION</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#dt" data-toggle="tab" class="nav-link">
                                        <span><i class="fa fa-check-circle fa-lg m-r-5"></i> DONE DIGITAL INSPECTION</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#rh" data-toggle="tab" class="nav-link">
                                        <span><i class="fa fa-stop fa-lg m-r-5"></i> REJECT - HOLD/CANCEL JOIN</span>
                                    </a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane fade active show" id="bm">
                                    <div class="panel-body">
                                        <p>Action Approve</p>
                                        <button type="button" name="approve_all" id="approve_all" class="btn btn-success btn-xl">Approve</button>
                                        <br><br>
                                        <div class="table-responsive">
                                            <table id="tbl-kyc" class="table table-striped table-bordered table-td-valign-middle" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>NO</th>
                                                        <th>
                                                            <input type="checkbox" name="select-all" id="select-all" />
                                                        </th>
                                                        <th>EMAIL VERIFIKASI</th>
                                                        <th>JAWARA ID</th>
                                                        <th>NIK KTP</th>
                                                        <th>NAME</th>
                                                        <th>PHONE</th>
                                                        <th>NAMA BANK</th>
                                                        <th>NOMOR REKENING</th>
                                                        <th>DELIVERED EMAIL</th>
                                                        <th>ACTIVE PHONE NUMBER</th>
                                                        <th>AVAILABLE IN WHATSAPP</th>
                                                        <th>CORRECT KTP NO</th>
                                                        <th>CORRECT KK NO</th>
                                                        <th>NAMA JAWARA</th>
                                                        <th>KTP TAMPAK JELAS</th>
                                                        <th>KK TAMPAK JELAS</th>
                                                        <th>STORE TAMPAK JELAS</th>
                                                        <th>STATUS DATA</th>
                                                        <th>FILE KTP</th>
                                                        <th>FILE KTP PASANGAN</th>
                                                        <th>FILE KK</th>
                                                        <th>FILE STORE</th>
                                                        <th>NIK KK</th>
                                                        <th>NOTES</th>
                                                        <th>DATA SUBMIT CHECK</th>
                                                        <th>DOKUMEN PENDUKUNG </th>
                                                        <th>TOTAL SCORE</th>
                                                        <th>USER GRADING KYC</th>
                                                        <th>TIMESTAMP</th>
                                                        <th>ACTION</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="dt">
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table id="tbl-kyc-done" class="table table-striped table-bordered table-td-valign-middle" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>NO</th>
                                                        <th>EMAIL VERIFIKASI</th>
                                                        <th>JAWARA ID</th>
                                                        <th>NIK KTP</th>
                                                        <th>NAME</th>
                                                        <th>PHONE</th>
                                                        <th>NAMA BANK</th>
                                                        <th>NOMOR REKENING</th>
                                                        <th>DELIVERED EMAIL</th>
                                                        <th>ACTIVE PHONE NUMBER</th>
                                                        <th>AVAILABLE IN WHATSAPP</th>
                                                        <th>CORRECT KTP NO</th>
                                                        <th>CORRECT KK NO</th>
                                                        <th>NAMA JAWARA</th>
                                                        <th>KTP TAMPAK JELAS</th>
                                                        <th>KK TAMPAK JELAS</th>
                                                        <th>STORE TAMPAK JELAS</th>
                                                        <th>STATUS DATA</th>
                                                        <th>FILE KTP</th>
                                                        <th>FILE KTP PASANGAN</th>
                                                        <th>FILE KK</th>
                                                        <th>FILE STORE</th>
                                                        <th>NIK KK</th>
                                                        <th>NOTES</th>
                                                        <th>DATA SUBMIT CHECK</th>
                                                        <th>DOKUMEN PENDUKUNG </th>
                                                        <th>TOTAL SCORE</th>
                                                        <th>USER GRADING KYC</th>
                                                        <th>TIMESTAMP</th>
                                                        <th>DONE DIGITAL INSPECTION</th>
                                                        <th>ACTION</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="rh">
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table id="tbl-kyc-reject" class="table table-striped table-bordered table-td-valign-middle" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>NO</th>
                                                        <th>EMAIL VERIFIKASI</th>
                                                        <th>JAWARA ID</th>
                                                        <th>NIK KTP</th>
                                                        <th>NAME</th>
                                                        <th>PHONE</th>
                                                        <th>NAMA BANK</th>
                                                        <th>NOMOR REKENING</th>
                                                        <th>DELIVERED EMAIL</th>
                                                        <th>ACTIVE PHONE NUMBER</th>
                                                        <th>AVAILABLE IN WHATSAPP</th>
                                                        <th>CORRECT KTP NO</th>
                                                        <th>CORRECT KK NO</th>
                                                        <th>NAMA JAWARA</th>
                                                        <th>KTP TAMPAK JELAS</th>
                                                        <th>KK TAMPAK JELAS</th>
                                                        <th>STORE TAMPAK JELAS</th>
                                                        <th>STATUS DATA</th>
                                                        <th>FILE KTP</th>
                                                        <th>FILE KTP PASANGAN</th>
                                                        <th>FILE KK</th>
                                                        <th>FILE STORE</th>
                                                        <th>NIK KK</th>
                                                        <th>NOTES</th>
                                                        <th>DATA SUBMIT CHECK</th>
                                                        <th>DOKUMEN PENDUKUNG </th>
                                                        <th>TOTAL SCORE</th>
                                                        <th>USER GRADING KYC</th>
                                                        <th>TIMESTAMP</th>
                                                        <th>ACTION</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end panel-body -->
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-10 -->
    </div>

    <!-- Modal -->
    <div class="modal fade" id="ubahkyc" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Jawara KYC</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="kyc/ubahKYC" enctype="multipart/form-data" method="POST">
                        
                        <div class="row">
                            <input type="hidden" name="autocomplete" id="field-autocomplete">
                            <input class="form-control" type="hidden" name="id" id="idx" />
                            <input class="form-control" type="hidden" name="file_ktpx" id="file_ktpx" />
                            <input class="form-control" type="hidden" name="file_ktp_pasanganx" id="file_ktp_pasanganx" />
                            <input class="form-control" type="hidden" name="file_kkx" id="file_kkx" />
                            <input class="form-control" type="hidden" name="file_storex" id="file_storex" />

                            <div class="form-group col-xl-12">
                                <label class="col-form-label">Email Verifikator <span class="text-danger">*</span></label>
                                <input type="email" class="form-control" name="email" id="email" placeholder="Email Verifikator" autocomplete="off" value="<?= $this->session->userdata('email_address'); ?>" required readonly>
                            </div>

                            <div class="form-group col-xl-12">
                                <label class="col-form-label">No KTP Pemilik <span class="text-danger">*</span></label>
                                <input type="text" pattern="^[0-9]{16,}$" class="form-control" name="nik_ktp" id="nik_ktp" placeholder="No KTP Pemilik" autocomplete="off" required>
                            </div>

                            <div class="form-group col-xl-12">
                                <label class="col-form-label">No KK Pemilik <span class="text-danger">*</span></label>
                                <input type="text" pattern="^[0-9]{16,}$" class="form-control" name="nik_kk" id="nik_kk" placeholder="No KK Pemilik" autocomplete="off" required>
                            </div>

                            <div class="form-group col-xl-6">
                                <label class="col-form-label">Nama Jawara yang di registrasikan <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="Nama jawara" autocomplete="off" required>
                            </div>

                            <div class="form-group col-xl-6">
                                <label class="col-form-label">No Telp Pemilik <span class="text-danger">*</span></label>
                                <input type="tel" pattern="^628[0-9]{8,}$" class="form-control" name="phone" id="phone" placeholder="628xxxxxxxxx" autocomplete="off" required>
                                <span style="font-size: 12px;">Note: 628xxxxxxxxx (Terdiri dari panjang minimal 10 dan selalu dimulai dengan 628)</span>
                            </div>

                            <div class="form-group col-xl-6">
                                <label class="col-form-label">Nama Bank <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="bank_code" id="bank_code" placeholder="Nama Bank" autocomplete="off" required>
                            </div>

                            <div class="form-group col-xl-6">
                                <label class="col-form-label">Nomor Rekening <span class="text-danger">*</span></label>
                                <input type="text" pattern="^[0-9]{5,}$" class="form-control" name="nomor_rekening" id="nomor_rekening" placeholder="Nomor Rekening" autocomplete="off" required>
                            </div>

                            <div class="form-group col-xl-12">
                                <br>
                                <h2>KYC</h2>
                                <h5>Data Submit Check</h5>
                                <br>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Nama Jawara <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getDSC as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="nama_jawara" id="nama_jawara<?= $r->status; ?>" value="<?= $r->status; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Delivered Email <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getDSC as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="delivered_email" id="delivered_email<?= $r->status; ?>" value="<?= $r->status; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Active Phone <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getDSC as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="active_phone" id="active_phone<?= $r->status; ?>" value="<?= $r->status; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Available in Whatsapp <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getDSC as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="active_whatsapp" id="active_whatsapp<?= $r->status; ?>" value="<?= $r->status; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Correct KTP No <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getDSC as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="correct_ktp" id="correct_ktp<?= $r->status; ?>" value="<?= $r->status; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
       
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Correct KK No <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getDSC as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="correct_kk" id="correct_kk<?= $r->status; ?>" value="<?= $r->status; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                            </div>

                            <div class="form-group col-xl-12">
                                <h5>Dokumen Pendukung</h5>
                                <br>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">KTP tampak jelas <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getDSC as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="ktp_jelas" id="ktp_jelas<?= $r->status; ?>" value="<?= $r->status; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>

                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">KK tampak jelas <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getDSC as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="kk_jelas" id="kk_jelas<?= $r->status; ?>" value="<?= $r->status; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>

                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Store tampak jelas <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getDSC as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="store_jelas" id="store_jelas<?= $r->status; ?>" value="<?= $r->status; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                            </div>

                            <div class="form-group col-xl-12">
                                <h5>Update Dokumen</h5>
                                <br>
                                <!-- begin form-group -->
                                <div class="form-group row m-b-10">
                                    <label class="col-lg-3 text-lg-right col-form-label">Update KTP</label>
                                    <div class="col-lg-9 col-xl-6">
                                        <input type="file" name="file_ktp" accept="image/png, image/jpg, image/jpeg" class="form-control"/>
                                        <span style="font-size: 12px;">Note : Ukuran Foto KTP max. 2000kb.</span>
                                    </div>
                                </div>
                                <!-- end form-group -->
                                <!-- begin form-group -->
                                <div class="form-group row m-b-10">
                                    <label class="col-lg-3 text-lg-right col-form-label">Update KK</label>
                                    <div class="col-lg-9 col-xl-6">
                                        <input type="file" name="file_kk" accept="image/png, image/jpg, image/jpeg" class="form-control"/>
                                        <span style="font-size: 12px;">Note : Ukuran Foto KK max. 2000kb.</span>
                                    </div>
                                </div>
                                <!-- end form-group -->
                                <!-- begin form-group -->
                                <div class="form-group row m-b-10">
                                    <label class="col-lg-3 text-lg-right col-form-label">Update Toko Tampak Depan</label>
                                    <div class="col-lg-9 col-xl-6">
                                        <input type="file" name="file_store" accept="image/png, image/jpg, image/jpeg"  class="form-control"/>
                                        <span style="font-size: 12px;">Note : Ukuran Foto max. 2000kb.</span>
                                    </div>
                                </div>
                                <!-- end form-group -->
                            </div>

                            <div class="form-group col-xl-12">
                                <h5>Dokumen Pendukung Lainnya</h5>
                                <br>
                                <!-- begin form-group -->
                                <div class="form-group row m-b-10">
                                    <label class="col-lg-3 text-lg-right col-form-label">KTP Pasangan </label>
                                    <div class="col-lg-9 col-xl-6">
                                        <input type="file" name="file_ktp_pasangan" accept="image/png, image/jpg, image/jpeg" class="form-control"/>
                                        <span style="font-size: 12px;">Note : Ukuran Foto KTP max. 2000kb.</span>
                                    </div>
                                </div>
                                <!-- end form-group -->
                            </div>

                            <div class="form-group col-xl-12">
                                <h5>Status Jawara</h5>
                                <br>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Status Data <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getSD as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="status" id="status<?= $r->status; ?>" value="<?= $r->status; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                            </div>
                            <div class="form-group col-xl-12">
                                <label class="col-form-label">Notes:</label>
                                <textarea class="form-control" name="notes" id="notes"></textarea>                                
                            </div>                    
                        </div>
                        
                        </br>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="reset" name="reset" class="btn btn-info"><i class="fa fa-cut"></i> Reset</button>
                            <button type="submit" class="btn btn-primary" value="Cek"><i class="fa fa-save"></i> Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="digital_inspection" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Digital Inspection</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="kyc/createDI" enctype="multipart/form-data" method="POST">
                        
                        <div class="row">
                            <input type="hidden" name="autocomplete" id="field-autocomplete">
                            <input class="form-control" type="hidden" name="id" id="idx" />

                            <div class="form-group col-xl-12">
                                <label class="col-form-label">Email Verifikator <span class="text-danger">*</span></label>
                                <input type="email" class="form-control" name="email" id="email" placeholder="Email Verifikator" autocomplete="off" value="<?= $this->session->userdata('email_address'); ?>" required readonly>
                            </div>

                            <div class="form-group col-xl-12">
                                <label class="col-form-label">No KTP Pemilik <span class="text-danger">*</span></label>
                                <input type="text" pattern="^[0-9]{16,}$" class="form-control" name="nik_ktp" id="nik_ktp" placeholder="No KTP Pemilik" autocomplete="off" required>
                            </div>

                            <div class="form-group col-xl-6">
                                <label class="col-form-label">Nama Jawara yang di registrasikan <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="Nama jawara" autocomplete="off" required>
                            </div>

                            <div class="form-group col-xl-6">
                                <label class="col-form-label">No Telp Pemilik <span class="text-danger">*</span></label>
                                <input type="tel" pattern="^628[0-9]{8,}$" class="form-control" name="phone" id="phone" placeholder="628xxxxxxxxx" autocomplete="off" required>
                                <span style="font-size: 12px;">Note: 628xxxxxxxxx (Terdiri dari panjang minimal 10 dan selalu dimulai dengan 628)</span>
                            </div>

                            <div class="form-group col-xl-12">
                                <br>
                                <h2>KYC</h2>
                                <h5>kyc Check</h5>
                                <br>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Npwp Status <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getDSC as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="cek_npwp" id="cek_npwp" value="<?= $r->status; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Get Contact Status <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getDSC as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="cek_getcontact" id="cek_getcontact" value="<?= $r->status; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Google Earth Status <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getDSC as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="cek_gearth" id="cek_gearth" value="<?= $r->status; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Phone Survey Status <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getDSC as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="cek_phone" id="cek_phone" value="<?= $r->status; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                            </div>

                            <div class="form-group col-xl-12">
                                <br>
                                <h2>Busniess Aspects</h2>
                                <p>review of personal capabilities based on business experience</p>

                                <div class="form-group col-xl-12">
                                    <label class="col-form-label">No Npwp <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="npwp" id="npwp" placeholder="No Npwp" autocomplete="off" required>
                                </div>
                                <br>
                                <h5>Google Findings</h5>
                                <br>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Competitor in area <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getBA as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="gf_competitor" id="gf_competitor" value="<?= $r->status; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Kelayakan Tempat <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getBA as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="gf_kelayakan" id="gf_kelayakan" value="<?= $r->status; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Posisi Lokasi <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getBA as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="gf_lokasi" id="gf_lokasi" value="<?= $r->status; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                            </div>

                            <div class="form-group col-xl-12">
                                <label class="col-form-label">Copy Paste Gmaps Lattitude & Longtitude <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="gmaps" id="gmaps" placeholder="Gmaps Lattitude & Longtitude" autocomplete="off" required>
                            </div>

                            <div class="form-group col-xl-12">
                                <!-- begin form-group -->
                                <h5>Screen Capture Google Earth Store</h5>
                                <br>
                                <div class="form-group row m-b-10">
                                    <label class="col-lg-3 text-lg-right col-form-label">Screen Capture Google Earth Store <span class="text-danger">*</span></label>
                                    <div class="col-lg-9 col-xl-6">
                                        <input type="file" name="file_gearth" accept="image/png, image/jpg, image/jpeg" class="form-control" required/>
                                        <span style="font-size: 12px;">Note : Ukuran Foto max. 2000kb.</span>
                                    </div>
                                </div>
                                <!-- end form-group -->
                            </div>

                            <div class="form-group col-xl-12">
                                <br>
                                <h5>Get Contact Findings</h5>
                                <br>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Jumlah Tags <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getBA as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="gcf_tags" id="gcf_tags" value="<?= $r->status; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Penilaian Keseluruhan <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getBA as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="gcf_penilaian" id="gcf_penilaian" value="<?= $r->status; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Aktif dalam kegiatan usaha/organisasi <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getBA as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="gcf_aktif" id="gcf_aktif" value="<?= $r->status; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                            </div>

                            <div class="form-group col-xl-12">
                                <h5>Screen Capture Get Contact Finding</h5>
                                <br>
                                <!-- begin form-group -->
                                <div class="form-group row m-b-10">
                                    <label class="col-lg-3 text-lg-right col-form-label">Screen Capture Get Contact Finding <span class="text-danger">*</span></label>
                                    <div class="col-lg-9 col-xl-6">
                                        <input type="file" name="file_gcf" accept="image/png, image/jpg, image/jpeg" class="form-control" required/>
                                        <span style="font-size: 12px;">Note : Ukuran Foto max. 2000kb.</span>
                                    </div>
                                </div>
                                <!-- end form-group -->
                            </div>

                            <div class="form-group col-xl-12">
                                <br>
                                <h5>Score</h5>
                                <br>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Get Contact Status <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getBA as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="score_getcontact" id="score_getcontact" value="<?= $r->status; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                                <fieldset class="form-group">
                                    <div class="row">
                                        <legend class="col-form-label col-sm-4 pt-0">Google Earth Status <span class="text-danger">*</span></legend>
                                        <div class="col-sm-8">
                                                
                                            <?php foreach ($getBA as $r) { ?>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="score_gearth" id="score_gearth" value="<?= $r->status; ?>" required>
                                                <label class="form-check-label">
                                                    <?= $r->name; ?>
                                                </label>
                                            </div>
                                                        
                                            <?php } ?>
                                        </div>      
                                    </div>         
                                </fieldset>
                            </div>

                            <div class="form-group col-xl-12">
                                <label class="col-form-label">Notes:</label>
                                <textarea class="form-control" name="notes" id="notes"></textarea>
                                <!-- <input type="tel" pattern="^628[0-9]{8,}$" class="form-control" name="notes" id="notes" placeholder="628xxxxxxxxx" autocomplete="off" required> -->
                            </div>                    
                        </div>
                        
                        </br>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="reset" name="reset" class="btn btn-info"><i class="fa fa-cut"></i> Reset</button>
                            <button type="submit" class="btn btn-primary" value="Cek"><i class="fa fa-save"></i> Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Manage -->
    <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/app-manage.js');?>"></script>

    <script type="text/javascript">

        $(document).ready(function() {

            $('#select-all').click(function(event) {
                if (this.checked) {
                    // Iterate each checkbox
                    $(':checkbox').each(function() {
                        this.checked = true;
                    });
                } else {
                    $(':checkbox').each(function() {
                        this.checked = false;
                    });
                }
            });

            $('.approve_checkbox').click(function() {
                if($(this).is(':checked')){
                    $('.approve_checkbox').prop('checked', true);
                }else{
                    $('.approve_checkbox').prop('checked', false);
                }
            });
 

            $('#approve_all').click(function() {

                var checkbox_value = [];
                var oTable = $('#tbl-kyc').dataTable();
                var rowcollection =  oTable.$(".approve_checkbox:checked", {"page": "all"});
                rowcollection.each(function(index,elem){
                    checkbox_value.push($(elem).val());

                });

                if (checkbox_value.length > 0) {

                    var yakin = confirm("Apakah kamu yakin akan Mengapprove ?");

                    console.log(checkbox_value)

                    if (yakin) {
                        $.ajax({
                            url: "kyc/approve_all",
                            method: "POST",
                            data: {
                                checkbox_value: checkbox_value
                            },
                            success: function() {
                                setInterval('location.reload()', 2000);
                            }
                        })
                        Swal.fire({
                            title: 'Informasi!',
                            text: 'Data Berhasil di Approve',
                            imageUrl: '<?= base_url('assets/svg/completed-animate.svg') ?>',
                            imageWidth: 400,
                            imageHeight: 250,
                            imageAlt: 'Custom image',
                        });
                    } else {
                        Swal.fire({
                            title: 'Informasi!',
                            text: 'Data Gagal di Approve, Silahkan Coba Lagi',
                            imageUrl: '<?= base_url('assets/svg/questions-animate.svg') ?>',
                            imageWidth: 400,
                            imageHeight: 250,
                            imageAlt: 'Custom image',
                        });
                    }
                } else {
                    alert('Tidak ada data yang ingin di Pilih, silahkan pilih data terlebih dahulu.');
                }
            });

        });
      
    </script>

    
