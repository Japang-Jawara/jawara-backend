<?php
class Api_model extends CI_Model{

    public function getUsers($email, $pass){
		return $this->db->get_where('jawara_users', ['email_address' => $email, 'password' => $pass])->result();
	}

    public function keys($input_data){
		return $this->db->insert('keys', $input_data);
	}

    public function getKey($apiKey){
		return $this->db->get_where('keys', ['key' => $apiKey])->result();
	}

    public function price($input_data){
		return $this->db->insert('tbl_price', $input_data);
	}

}