<?php
use chriskacerguis\RestServer\RestController;

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Price extends REST_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->model('Api_model', 'api_server');
    }

    public function index_post(){

        $apiKey = $this->input->get('api_key');
		$apiKeyx = $this->api_server->getKey($apiKey);
		
		if($apiKeyx){
			if(date('Y-m-d H:i:s') <= $apiKeyx[0]->expire_date){

                $input_data['sku'] = $this->input->post('sku');
                $input_data['date'] = $this->input->post('date');
                $input_data['price'] = $this->input->post('price');

                $this->api_server->price($input_data);

				$this->response([
                    'status' => true,
                    'message' => 'Data Price Berhasil Ditambahkan',
                    'data' => $input_data
                ], REST_Controller::HTTP_OK); 
			}else{
				$this->response([
					'status' => false,
					'message' => 'token expired'
				], REST_Controller::HTTP_NOT_FOUND);
			}
			
		}else{
			$this->response([
				'status' => false,
				'error' => 'Invalid API key'
			], REST_Controller::HTTP_NOT_FOUND);
		}
                
    }
}