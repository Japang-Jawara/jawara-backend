<?php
use chriskacerguis\RestServer\RestController;

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Token extends REST_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->model('Api_model', 'api_server');
    }

    public function index_post(){

        $email = $this->input->post('email');
        $password = MD5($this->input->post('password'));

        $users = $this->api_server->getUsers($email, $password);
        
        if($users){
            
            $input_data['user_id'] = $users[0]->user_id;
            $input_data['key'] = MD5(date('Y-m-d H:i:s'));
            $input_data['expire_date'] = date("Y-m-d H:i:s", strtotime('+5 hours'));
            $input_data['created_at'] = date('Y-m-d H:i:s');

            $this->api_server->keys($input_data);

            $this->response([
                'status' => true,
                'token' => $input_data['key']
            ], REST_Controller::HTTP_OK);

        }else{
            $this->response([
                'status' => false,
                'error' => 'Username atau Password TIdak Ditemukan'
            ], REST_Controller::HTTP_NOT_FOUND);
        }
                
    }
}