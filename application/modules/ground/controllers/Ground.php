<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ground extends CI_Controller{

     function __construct(){
          parent::__construct();

          $this->load->model('Ground_model', 'modelGround');
     }

     function index(){
          if($this->session->userdata('is_login') == TRUE){

               // $x['getDSC'] = $this->modelGround->getDSC();
               // $x['getSD'] = $this->modelGround->getSD();
               // $x['getBA'] = $this->modelGround->getBA();

               $this->load->view("include/head");
               $this->load->view("include/top-header");
               $this->load->view('ground');
               $this->load->view("include/sidebar");
               $this->load->view("include/panel");
               $this->load->view("include/footer");
               $this->load->view("include/alert");
          }else{
               $this->session->set_flashdata('success', 'Upsss!!!, Login dulu ya.');
               redirect('index.php/login');
          }
     }

     function tableGround() {
          
          $data = $this->modelGround->getGround();

          $no = 1;
          $jawara = array();

          foreach($data as $r) {

               $email_verifikasi = $r->email_verifikasi_sc;
               $jawara_id = $r->id;
               $nik_ktp = $r->nik_ktp_sc;
               $name = $r->name_sc;
               $phone = $r->phone_sc;
               $shop_name = $r->shop_name_sc;
               $shop_location = $r->shop_location_sc;
               $sa_pengalaman = $r->sa_pengalaman_sc;
               $sa_karyawan = $r->sa_karyawan_sc;

               $sa_keluarga = $r->sa_keluarga_sc;
               $sa_aktif = $r->sa_aktif_sc;

               $ba_jusaha = $r->ba_jusaha_sc;
               $ba_musaha = $r->ba_musaha_sc;

               $ca_location = $r->ca_location_sc;
               $ca_kompetitor = $r->ca_kompetitor_sc;

               $frma_kendaraan = $r->frma_kendaraan_sc;
               $frma_aset = $r->frma_aset_sc;
               $frma_pendapatan = $r->frma_pendapatan_sc;

               $notes = $r->notes_sc == NULL ? "<font>Catatan Tidak Ada</font>" : $r->notes_sc;


               if($r->file_selfie_sc != NULL){
                    $file_selfie = '<a href="'.$r->file_selfie_sc.'" target="_blank" class="btn btn-sm btn-primary">'.$r->file_selfie_sc.'</a>';
               }else{
                    $file_selfie = 'Tidak ada File KTP';
               }

               $ts_sa = $r->ts_sa_sc;
               $ts_ba = $r->ts_ba_sc;
               $ts_ca = $r->ts_ca_sc;
               $ts_frma = $r->ts_frma_sc;

               $percent_sa = $r->percent_sa_sc;
               $percent_ba = $r->percent_ba_sc;
               $percent_ca = $r->percent_ca_sc;
               $percent_frma = $r->percent_frma_sc;

               $total_score = $r->total_score_sc;
               $user_grading = $r->user_grading_sc;
               $created_at = $r->created_at_sc;
               

               $jawara[] = array(
                    $no++,
                    $email_verifikasi,
                    $jawara_id,
                    $nik_ktp,
                    $name,
                    $phone,
                    $shop_name,
                    $shop_location,

                    $sa_pengalaman,
                    $sa_karyawan,
                    $sa_keluarga,
                    $sa_aktif,

                    $ba_jusaha,
                    $ba_musaha,

                    $ca_location,
                    $ca_kompetitor,

                    $frma_kendaraan,
                    $frma_aset,
                    $frma_pendapatan,

                    $notes,
                    $file_selfie,

                    $ts_sa,
                    $ts_ba,
                    $ts_ca,
                    $ts_frma,

                    $percent_sa,
                    $percent_ba,
                    $percent_ca,
                    $percent_frma,
                    
                    $total_score,
                    $user_grading,
                    $created_at
               );
               
          }
          
          echo json_encode($jawara);
     }

     function tableGroundrendah() {
          
          $data = $this->modelGround->getGroundRendah();

          $no = 1;
          $jawara = array();

          foreach($data as $r) {

               $email_verifikasi = $r->email_verifikasi_sc;
               $jawara_id = $r->id;
               $nik_ktp = $r->nik_ktp_sc;
               $name = $r->name_sc;
               $phone = $r->phone_sc;
               $shop_name = $r->shop_name_sc;
               $shop_location = $r->shop_location_sc;
               $sa_pengalaman = $r->sa_pengalaman_sc;
               $sa_karyawan = $r->sa_karyawan_sc;

               $sa_keluarga = $r->sa_keluarga_sc;
               $sa_aktif = $r->sa_aktif_sc;

               $ba_jusaha = $r->ba_jusaha_sc;
               $ba_musaha = $r->ba_musaha_sc;

               $ca_location = $r->ca_location_sc;
               $ca_kompetitor = $r->ca_kompetitor_sc;

               $frma_kendaraan = $r->frma_kendaraan_sc;
               $frma_aset = $r->frma_aset_sc;
               $frma_pendapatan = $r->frma_pendapatan_sc;

               $notes = $r->notes_sc == NULL ? "<font>Catatan Tidak Ada</font>" : $r->notes_sc;


               if($r->file_selfie_sc != NULL){
                    $file_selfie = '<a href="'.$r->file_selfie_sc.'" target="_blank" class="btn btn-sm btn-primary">'.$r->file_selfie_sc.'</a>';
               }else{
                    $file_selfie = 'Tidak ada File KTP';
               }

               $ts_sa = $r->ts_sa_sc;
               $ts_ba = $r->ts_ba_sc;
               $ts_ca = $r->ts_ca_sc;
               $ts_frma = $r->ts_frma_sc;

               $percent_sa = $r->percent_sa_sc;
               $percent_ba = $r->percent_ba_sc;
               $percent_ca = $r->percent_ca_sc;
               $percent_frma = $r->percent_frma_sc;

               $total_score = $r->total_score_sc;
               $user_grading = $r->user_grading_sc;
               $created_at = $r->created_at_sc;
               

               $jawara[] = array(
                    $no++,
                    $email_verifikasi,
                    $jawara_id,
                    $nik_ktp,
                    $name,
                    $phone,
                    $shop_name,
                    $shop_location,

                    $sa_pengalaman,
                    $sa_karyawan,
                    $sa_keluarga,
                    $sa_aktif,

                    $ba_jusaha,
                    $ba_musaha,

                    $ca_location,
                    $ca_kompetitor,

                    $frma_kendaraan,
                    $frma_aset,
                    $frma_pendapatan,

                    $notes,
                    $file_selfie,

                    $ts_sa,
                    $ts_ba,
                    $ts_ca,
                    $ts_frma,

                    $percent_sa,
                    $percent_ba,
                    $percent_ca,
                    $percent_frma,
                    
                    $total_score,
                    $user_grading,
                    $created_at
               );
               
          }
          
          echo json_encode($jawara);
     }

}