<?php
class Ground_model extends CI_Model{

    public function getDSC(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'DSC')->get()->result();
    }

    public function getSD(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'SD')->get()->result();
    }

    public function getBA(){
        return $this->db->select('*')->from('jawara_config')->where('code', 'BA')->get()->result();
    }

    public function getGround(){
        return $this->db->select('*')->from('jawara_leads')->where('status_score_sc', '1')->order_by('id', 'DESC')->get()->result();
    }

    public function getGroundRendah(){
        return $this->db->select('*')->from('jawara_leads')->where('status_score_sc', '0')->order_by('id', 'DESC')->get()->result();
    }
}
?>