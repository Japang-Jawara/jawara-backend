<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jawara extends CI_Controller{
     function __construct(){
          parent::__construct();

          $this->load->model('Jawara_model', 'modelJawara');
     }

     function uploadKTP()
	{
		foreach ($_FILES as $name => $fileInfo) {
			$filename = $_FILES['file_ktp']['name'];
			$tmpname = $_FILES['file_ktp']['tmp_name'];
			$exp = explode('.', $filename);
			$ext = end($exp);
			$newname =  'KTP'.date('Ymdhis').".". $ext;
			$config['upload_path'] = './assets/berkas/ktp/';
			$config['upload_url'] =  base_url() . 'assets/berkas/ktp/';
			$config['allowed_types'] = "png|jpg|jpeg";
			$config['max_size'] = '2000';
			$config['file_name'] = $newname;
			$this->load->library('upload', $config);
			move_uploaded_file($tmpname, "assets/berkas/ktp/" . $newname);
			return $newname;
		}
	}

     function uploadKK()
	{
		foreach ($_FILES as $name => $fileInfo) {
			$filename = $_FILES['file_kk']['name'];
			$tmpname = $_FILES['file_kk']['tmp_name'];
			$exp = explode('.', $filename);
			$ext = end($exp);
			$newname =  'KK'.date('Ymdhis')."." . $ext;
			$config['upload_path'] = './assets/berkas/kk/';
			$config['upload_url'] =  base_url() . 'assets/berkas/kk/';
			$config['allowed_types'] = "png|jpg|jpeg";
			$config['max_size'] = '2000';
			$config['file_name'] = $newname;
			$this->load->library('upload', $config);
			move_uploaded_file($tmpname, "assets/berkas/kk/" . $newname);
			return $newname;
		}
	}

     function uploadStore()
	{
		foreach ($_FILES as $name => $fileInfo) {
			$filename = $_FILES['file_store']['name'];
			$tmpname = $_FILES['file_store']['tmp_name'];
			$exp = explode('.', $filename);
			$ext = end($exp);
			$newname =  'STORE'.date('Ymdhis')."." . $ext;
			$config['upload_path'] = './assets/berkas/store/';
			$config['upload_url'] =  base_url() . 'assets/berkas/store/';
			$config['allowed_types'] = "png|jpg|jpeg";
			$config['max_size'] = '2000';
			$config['file_name'] = $newname;
			$this->load->library('upload', $config);
			move_uploaded_file($tmpname, "assets/berkas/store/" . $newname);
			return $newname;
		}
	}

     function index(){
          $x['provinsi'] = $this->modelJawara->get_provinsi();

          $this->load->view("include/head");
          $this->load->view("include/top-header");
          $this->load->view('jawara', $x);
          $this->load->view("include/sidebar");
          $this->load->view("include/panel");
          $this->load->view("include/footer");
          $this->load->view("include/alert");
     }

     function get_kabupaten()
     {
          if ($this->input->post('provinsi_id')) {
               echo $this->modelJawara->get_kabupaten($this->input->post('provinsi_id'));
          }
     }

     public function leds(){
          $ktp = '';
          $kk = '';
          $store = '';

          if (!empty($_FILES['file_ktp']['name'])) {
               $newname = $this->uploadKTP();
               $data['file_ktp'] = $newname;
               $ktp = $newname;
          }

          if (!empty($_FILES['file_kk']['name'])) {
               $newname = $this->uploadKK();
               $data['file_kK'] = $newname;
               $kk = $newname;
          }

          if (!empty($_FILES['file_store']['name'])) {
               $newname = $this->uploadStore();
               $data['file_store'] = $newname;
               $store = $newname;
          }
          
          $x['email'] = $this->input->post('email');
          $x['name'] = $this->input->post('name');
          $x['phone'] = $this->input->post('phone');
          $x['nik_ktp'] = $this->input->post('nik_ktp');
          $x['nik_kk'] = $this->input->post('nik_kk');
          $x['alamat_rumah'] = $this->input->post('alamat_rumah');
          
          $x['file_ktp'] = base_url('assets/berkas/ktp/').$ktp;
          $x['file_kk'] = base_url('assets/berkas/kk/').$kk;

          $x['alamat_usaha'] = $this->input->post('alamat_usaha');
          $x['provinsi'] = $this->modelJawara->cekProvinsi($this->input->post('provinsi'));
          $x['kab_kota'] = $this->input->post('kab_kota');
          $x['kodepos'] = $this->input->post('kodepos');
          $x['file_store'] = base_url('assets/berkas/store/').$store;
          $x['kode_referral'] = $this->input->post('kode_referral');

          $x['created_at'] = date('Y-m-d H:i:s');

          $result = $this->modelJawara->leds($x);

          redirect('index.php/register/jawara');

     }

}