<?php
class Jawara_model extends CI_Model{

    public function getJawaraLeds(){
        return $this->db->select('*')->from('jawara_leds')->get()->result();
    }

    public function cekNIK($nik){
        return $this->db->where('nik_ktp', $nik)->get('jawara_leds')->result();
    }

    public function cekEmail($email){
        return $this->db->where('email', $email)->get('jawara_leds')->result();
    }

    public function leds($data){

        $cekNik = $this->cekNik($data['nik_ktp']);
        $cekEmail = $this->cekEmail($data['email']);

        if($cekNik == NULL){
            if($cekEmail == NULL){
                $this->db->insert('jawara_leds', $data);
                $this->session->set_flashdata('success','REGISTRASI BERHASIL, MOHON MENUNGGU TINDAK LANJUTNYA DARI TEAM KAMI');
                return TRUE;
            }else{
                $this->session->set_flashdata('success','REGISTRASI GAGAL, EMAIL YANDA ANDA MASUKKAN SUDAH TERDAFTAR');
                return FALSE;
            }
        }else{
            $this->session->set_flashdata('success','REGISTRASI GAGAL, NIK KTP ANDA SUDAH TERDAFTAR');
            return FALSE;
        }
    }

    function get_provinsi()
    {
        $this->db->order_by('name', 'ASC');
        $query = $this->db->get('provinces');
        return $query->result();
    }

    function get_kabupaten($provinsi_id)
    {
        //ambil data kabupaten berdasarkan id provinsi yang dipilih
        $this->db->where('province_id', $provinsi_id);
        $this->db->order_by('name', 'ASC');
        $query = $this->db->get('regencies');

        $output = '<option value="">-- Pilih Kabupaten --</option>';

        //looping data
        foreach ($query->result() as $row) {
            $output .= '<option value="' . $row->name . '">' . $row->name . '</option>';
        }
        //return data kabupaten
        return $output;
    }

    function cekProvinsi($prov){
        return $this->db->where('id', $prov)->get('provinces')->result()[0]->name;
    }

}
?>