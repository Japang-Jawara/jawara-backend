<div id="content" class="content content-full-width">
    <!-- begin profile -->
    <div class="profile">
        <div class="profile-header">
            <!-- BEGIN profile-header-cover -->
            <div class="profile-header-cover"></div>
            <!-- END profile-header-cover -->
            <!-- BEGIN profile-header-content -->
            <div class="profile-header-content">
                <!-- BEGIN profile-header-img -->
                <div class="profile-header-img">
                    <img src="<?= $data->profile_picture ?>" alt="">
                </div>
                <!-- END profile-header-img -->
                <!-- BEGIN profile-header-info -->
                <div class="profile-header-info">
                    <h4 class="mt-0 mb-1"><?= $data->first_name ?></h4>
                    <p class="mb-2"><?= $data->last_name ?></p>
                    <a href="#" class="btn btn-xs btn-yellow">Form Edit Profile</a>
                </div>
                <!-- END profile-header-info -->
            </div>
            <!-- END profile-header-content -->
            <!-- BEGIN profile-header-tab -->
            <ul class="profile-header-tab nav nav-tabs">
                <li class="nav-item"><a href="#profile-post" class="nav-link active" data-toggle="tab">POSTS</a></li>
            </ul>
            <!-- END profile-header-tab -->
        </div>
    </div>
    <!-- end profile -->
    <!-- begin profile-content -->
    <div class="profile-content">
        <!-- begin tab-content -->
        <div class="tab-content p-0">
            <!-- begin #profile-post tab -->
            <div class="tab-pane fade show active" id="profile-post">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                        </h4>
                        <div class="panel-heading-btn">
                            <!-- <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a> -->
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="timeline-body">
                            <form class="form-horizontal form-bordered" action="user/update" enctype="multipart/form-data" method="POST">
                                <div class="row">
                                    <div class="form-group col-xl-6">
                                        <label class="col-lg-4">Nama Depan <span style="color:red"> *</span></label>
                                        <div class="col-lg-12">
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="first_name" value="<?php echo $data->first_name; ?>" placeholder="Nama Depan..." required/>
                                                <div class="input-group-addon">
                                                    <i class="fa fa-user-circle"></i>
                                                </div>
                                            </div>
                                        </div>

                                        <label class="col-lg-4">Nama Belakang <span style="color:red"> *</span></label>
                                        <div class="col-lg-12">
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="last_name" value="<?php echo $data->last_name; ?>" placeholder="Nama Belakang..." required/>
                                                <div class="input-group-addon">
                                                    <i class="fa fa-users"></i>
                                                </div>
                                            </div>
                                        </div>

                                        <label class="col-lg-4">Ganti Foto</label>
                                        <div class="col-lg-12">
                                            <input id="fileUpload" class="upload btn-primary" name="profile_pic" type="file"/><br />
                                            <input type="hidden" name="fileOld" value="<?php echo $this->session->userdata('profile_picture') != NULL ? $this->session->userdata('profile_picture'):'';?>" />
                                        </div>
                                    </div>

                                    <div class="form-group col-xl-6">
                                        <label class="col-lg-4">Password Baru</label>
                                        <div class="input-group" id="show_hide_password">
                                            <input type="password" data-toggle="password" data-placement="after" class="form-control" name="password" id="password" placeholder="Password baru" aria-label="Password baru" aria-describedby="password-addon" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Harus berisi setidaknya satu angka dan satu huruf besar dan kecil, dan setidaknya 8 karakter atau lebih">
                                            <div class="input-group-addon">
                                                <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                                            </div>
                                        </div>

                                        <label class="col-lg-4">Konfirmasi Password Baru</label>
                                        <div class="input-group" id="show_hide_konfirmasi_password">
                                            <input type="password" class="form-control" name="confirm_password" id="confirm_password" placeholder="Konfirmasi Password" aria-label="Konfirmasi Password" aria-describedby="password-addon">
                                            <div class="input-group-addon">
                                                <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                                            </div>
                                        </div>

                                        <div class="card-body" id="message">
                                            <h5>Syarat password baru:</h5>
                                            <span id="letter" class="invalid">- Mengandung <b>huruf kecil</b></span><br>
                                            <span id="capital" class="invalid">- Mengandung <b>huruf kapital (huruf besar)</b></span><br>
                                            <span id="number" class="invalid">- Mengandung <b>nomor</b></span><br>
                                            <span id="length" class="invalid">- Minimal <b>8 karakter</b></span><br>
                                        </div>
                                    </div>        
                                </div>  
                                <button type="submit" class="btn btn-primary f-s-12 rounded-corner" type="button" style="float: right;">Simpan</button>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- begin timeline -->
                
            </div>
            <!-- end #profile-post tab -->
        </div>
        <!-- end tab-content -->
    </div>
    <!-- end profile-content -->
</div>

<style>
    #message {
        display: none;
        background: #f1f1f1;
        color: #000;
        position: relative;
        padding: 20px;
        margin-top: 10px;
    }

    #message p {
        padding: 10px 35px;
        font-size: 18px;
    }

    .valid {
        color: rgb(3, 184, 190);
    }

    .valid:before {
        position: relative;
        left: -35px;
    }

    .invalid {
        color: red;
    }

    .invalid:before {
        position: relative;
        left: -35px;
    }
</style>

<script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
<script>

    $(document).ready(function() {
        $("#show_hide_password a").on('click', function(event) {
            event.preventDefault();
            if($('#show_hide_password input').attr("type") == "text"){
                $('#show_hide_password input').attr('type', 'password');
                $('#show_hide_password i').addClass( "fa-eye-slash" );
                $('#show_hide_password i').removeClass( "fa-eye" );
            }else if($('#show_hide_password input').attr("type") == "password"){
                $('#show_hide_password input').attr('type', 'text');
                $('#show_hide_password i').removeClass( "fa-eye-slash" );
                $('#show_hide_password i').addClass( "fa-eye" );
            }
        });
    });

    $(document).ready(function() {
        $("#show_hide_konfirmasi_password a").on('click', function(event) {
            event.preventDefault();
            if($('#show_hide_konfirmasi_password input').attr("type") == "text"){
                $('#show_hide_konfirmasi_password input').attr('type', 'password');
                $('#show_hide_konfirmasi_password i').addClass( "fa-eye-slash" );
                $('#show_hide_konfirmasi_password i').removeClass( "fa-eye" );
            }else if($('#show_hide_konfirmasi_password input').attr("type") == "password"){
                $('#show_hide_konfirmasi_password input').attr('type', 'text');
                $('#show_hide_konfirmasi_password i').removeClass( "fa-eye-slash" );
                $('#show_hide_konfirmasi_password i').addClass( "fa-eye" );
            }
        });
    });

    var myInput = document.getElementById("password");
    var letter = document.getElementById("letter");
    var capital = document.getElementById("capital");
    var number = document.getElementById("number");
    var length = document.getElementById("length")

    myInput.onfocus = function() {
        document.getElementById("message").style.display = "block";
    }

    myInput.onblur = function() {
        document.getElementById("message").style.display = "none";
    }

    myInput.onkeyup = function() {
        var lowerCaseLetters = /[a-z]/g;
        if (myInput.value.match(lowerCaseLetters)) {
            letter.classList.remove("invalid");
            letter.classList.add("valid");
        } else {
            letter.classList.remove("valid");
            letter.classList.add("invalid");
        }

        var upperCaseLetters = /[A-Z]/g;
        if (myInput.value.match(upperCaseLetters)) {
            capital.classList.remove("invalid");
            capital.classList.add("valid");
        } else {
            capital.classList.remove("valid");
            capital.classList.add("invalid");
        }

        var numbers = /[0-9]/g;
        if (myInput.value.match(numbers)) {
            number.classList.remove("invalid");
            number.classList.add("valid");
        } else {
            number.classList.remove("valid");
            number.classList.add("invalid");
        }

        if (myInput.value.length >= 8) {
            length.classList.remove("invalid");
            length.classList.add("valid");
        } else {
            length.classList.remove("valid");
            length.classList.add("invalid");
        }
    }

    var win = navigator.platform.indexOf('Win') > -1;
    if (win && document.querySelector('#sidenav-scrollbar')) {
        var options = {
            damping: '0.5'
        }
        Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
    }

    // Validate Password
    window.onload = function () {
        document.getElementById("password").onchange = validatePassword;
        document.getElementById("confirm_password").onchange = validatePassword;
    }

    function validatePassword(){
      var pass2=document.getElementById("confirm_password").value;
      var pass1=document.getElementById("password").value;
      console.log(pass2);
      console.log(pass1);
      console.log(pass1!=pass2);
      
      if(pass1.length >= 6)
        document.getElementById("password").setCustomValidity('');
      else
        document.getElementById("password").setCustomValidity("Password Kurang dari 6 character");
      

      if(pass1!=pass2)
          document.getElementById("confirm_password").setCustomValidity("Password Tidak Sama, Coba Lagi");
      else
          document.getElementById("confirm_password").setCustomValidity('');
    }
</script>
<!-- Github buttons -->