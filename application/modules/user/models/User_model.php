<?php
class User_model extends CI_Model {

    public function getUser(){
        $email_address = $this->session->userdata('email_address');
        return $this->db->select('*')->from('jawara_users')->where('email_address', $email_address)->get()->result();
    }

    function update($input_data){              
        $email_address = $this->session->userdata('email_address');
        return $this->db->where('email_address', $email_address)->update('jawara_users', $input_data);         
    }

}
?>