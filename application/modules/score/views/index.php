<div id="content" class="content">
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="<?php echo base_url('home');?>">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="<?php echo base_url('kepegawaian/thl');?>">SELF ASSESSMENT</a></li>
    </ol>
    <h1 class="page-header"><b>SELF ASSESSMENT</b></h1>
    <div class="note note-info note-with-right-icon">
        <div class="note-icon"><i class="fa fa-lightbulb"></i></div>
        <div class="note-content text-right">
            <h2><b>SELF ASSESSMENT</b></h2>
            <p>Score nilai jawara.</p>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <?php if($this->session->userdata('role') == 'Admin' || $this->session->userdata('role') == 'SuperAdmin' || $this->session->userdata('role') == 'Kepegawaian'){?>
                        <a href="" class="btn btn-icon btn-sm btn-inverse" data-toggle="modal" data-target="#addthl"><i class="fa fa-plus-square"></i></a>
                        <?php } ?>
                    </h4>
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                </div>

                <div class="panel-body">
                    <div class ="table-responsive">
                        <div class="col-xl-12">
                            <ul class="nav nav-tabs nav-tabs-inverse nav-justified nav-justified-mobile" data-sortable-id="index-2">
                                <li class="nav-item">
                                    <a href="#bm" data-toggle="tab" class="nav-link active">
                                        <span><i class="fa fa-check-circle fa-lg m-r-5"></i> NILAI A+ S/D C</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#dt" data-toggle="tab" class="nav-link">
                                        <span><i class="fa fa-close fa-lg m-r-5"></i> NILAI C- S/D F</span>
                                    </a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane fade active show" id="bm">
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table id="tbl-score-tinggi" class="table table-striped table-bordered table-td-valign-middle" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>NO</th>
                                                        <th>EMAIL JAWARA</th>
                                                        <th>NIK KTP</th>
                                                        <th>NAME</th>
                                                        <th>PHONE</th>
                                                        <th>NAMA TOKO</th>
                                                        <th>LOKASI (ALAMAT TOKO)</th>
                                                        <th>AS (PENGALAN BERDAGANG)</th>
                                                        <th>BPS (JUMLAH KARYAWAN)</th>
                                                        <th>BPS (JUMLAH KELUARGA)</th>
                                                        <th>BPS (AKTIF KEGIATAN)</th>
                                                        <th>BE (JENIS USAHA)</th>
                                                        <th>BE (MODAL USAHA)</th>
                                                        <th>MS (POSISI LOKASI)</th>
                                                        <th>MS (KOMPETITOR)</th>
                                                        <th>PFS (ASET PRIBADI)</th>
                                                        <th>PFS (PENDAPATAN LAIN)</th>
                                                        <th>NOTES</th>
                                                        <th>FILE SELFIE</th>
                                                        <th>TOTAL SA</th>
                                                        <th>TOTAL BA</th>
                                                        <th>TOTAL CA</th>
                                                        <th>TOTAL FRMS</th>
                                                        <th>PERCENT SA</th>
                                                        <th>PERCENT BA</th>
                                                        <th>PERCENT CA</th>
                                                        <th>PERCENT FRMS</th>
                                                        <th>TOTAL SCORE</th>
                                                        <th>USER GRADING SELF ASSESSMENT</th>
                                                        <th>TIMESTAMP</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="dt">
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table id="tbl-score-rendah" class="table table-striped table-bordered table-td-valign-middle" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>NO</th>
                                                        <th>EMAIL JAWARA</th>
                                                        <th>NIK KTP</th>
                                                        <th>NAME</th>
                                                        <th>PHONE</th>
                                                        <th>NAMA TOKO</th>
                                                        <th>LOKASI (ALAMAT TOKO)</th>
                                                        <th>AS (PENGALAN BERDAGANG)</th>
                                                        <th>BPS (JUMLAH KARYAWAN)</th>
                                                        <th>BPS (JUMLAH KELUARGA)</th>
                                                        <th>BPS (AKTIF KEGIATAN)</th>
                                                        <th>BE (JENIS USAHA)</th>
                                                        <th>BE (MODAL USAHA)</th>
                                                        <th>MS (POSISI LOKASI)</th>
                                                        <th>MS (KOMPETITOR)</th>
                                                        <th>PFS (ASET PRIBADI)</th>
                                                        <th>PFS (PENDAPATAN LAIN)</th>
                                                        <th>NOTES</th>
                                                        <th>FILE SELFIE</th>
                                                        <th>TOTAL SA</th>
                                                        <th>TOTAL BA</th>
                                                        <th>TOTAL CA</th>
                                                        <th>TOTAL FRMS</th>
                                                        <th>PERCENT SA</th>
                                                        <th>PERCENT BA</th>
                                                        <th>PERCENT CA</th>
                                                        <th>PERCENT FRMS</th>
                                                        <th>TOTAL SCORE</th>
                                                        <th>USER GRADING SELF ASSESSMENT</th>
                                                        <th>TIMESTAMP</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end panel-body -->
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-10 -->
    </div>

    <!-- Manage -->
    <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/app-manage.js');?>"></script>

    
