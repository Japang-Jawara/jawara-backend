<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		// LOAD MODELS
		$this->load->model('glogin_model');
	}

	function index()
	{
		// include_once APPPATH . "../vendor/autoload.php";

		// $google_client = new Google_Client();
		// $google_client->setClientId('363675125629-pdlt78ins6ov8r19tc7ag7q4i9s6q1h7.apps.googleusercontent.com');
		// $google_client->setClientSecret('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');
		// $google_client->setRedirectUri('https://dashboard.japangjawara.com/index.php/login');
		// $google_client->addScope('email');
		// $google_client->addScope('profile');

        // $direct_google = '';

		// if (isset($_GET["code"])) {

		// 	$token = $google_client->fetchAccessTokenWithAuthCode($_GET["code"]);

		// 	if (!isset($token["error"])) {

		// 		$google_client->setAccessToken($token['access_token']);
		// 		$this->session->set_userdata('access_token', $token['access_token']);

		// 		$google_service = new Google_Service_Oauth2($google_client);
		// 		$data = $google_service->userinfo->get();
		// 		$current_datetime = date('Y-m-d H:i:s');

        //         $email = explode("@", $data['email']);

        //         if($email[1] == 'jaringpangan.com'){
        //             if ($this->glogin_model->already($data['id'])) {

        //                 $user_data = array(
        //                     'first_name' => $data['given_name'],
        //                     'last_name'  => $data['family_name'],
        //                     'email_address' => $data['email'],
        //                     'profile_picture' => $data['picture'],
        //                     'updated_at' => $current_datetime
        //                 );
    
        //                 $this->glogin_model->update_google($user_data, $data['id']);

        //                 $this->session->set_flashdata('success', 'Akun google yang anda gunakan sudah terdaftar.');
        //             } else {
    
        //                 $user_data = array(
        //                     'login_oauth_uid' => $data['id'],
        //                     'first_name'  => $data['given_name'],
        //                     'last_name'   => $data['family_name'],
        //                     'email_address'  => $data['email'],
        //                     'profile_picture' => $data['picture'],
        //                     'created_at'  => $current_datetime,
        //                     'password'  => MD5('@Japang2022')
        //                 );
    
        //                 $this->glogin_model->insert_google($user_data);

        //                 $this->session->set_flashdata('success', 'Register telah berhasil, Harap gunakan email yang telah terdaftar dan gunakan password default @Japang2022.');
        //             }
    
        //             // $this->session->set_userdata('user_data', $user_data);
                    
        //         }else{
        //             $this->session->set_flashdata('success', 'Register Gagal, Harap gunakan email jaringpangan.com.');
        //         }
				
		// 	}
		// }
	
        // $direct_google = $google_client->createAuthUrl();
        // $x['direct_google'] = $direct_google;

        // $this->load->view('login', $x);
        // $this->load->view('include/alert');
        redirect();
	}

    public function proses(){
		$email = $this->input->post('email', TRUE);
		$password = MD5($this->input->post('password', TRUE));

        if ($this->glogin_model->proses($email, $password)) {
            if($this->session->userdata('is_p2p') == TRUE){
                redirect('index.php/p2p');
            }else if($this->session->userdata('is_surveyor') == TRUE){
                redirect('index.php/surveyor');
            }else if($this->session->userdata('is_sales') == TRUE){
                redirect('index.php/sales');
            }else{
                redirect('index.php/home');
            }
        } else {
            redirect('index.php/login');
        }		
	}

	function logout()
	{
		$this->session->unset_userdata('code');
		$this->session->unset_userdata('access_token');
		$this->session->unset_userdata('user_data');
        $this->session->sess_destroy();
		$this->session->set_flashdata('success', 'Anda Berhasil Logout!');
		redirect('index.php/login');
	}
}
