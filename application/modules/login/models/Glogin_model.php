<?php
class Glogin_model extends CI_Model {
    
    function already($id)
    {
        $this->db->where('login_oauth_uid', $id);
        $query = $this->db->get('jawara_users');
        if($query->num_rows() > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function update_google($data, $id)
    {
        $this->db->where('login_oauth_uid', $id);
        $this->db->update('jawara_users', $data);
    }

    function insert_google($data)
    {
        $this->db->insert('jawara_users', $data);
    }

    public function proses($email, $password){
        $this->db->select('*');
        $this->db->from('jawara_users');
        $this->db->where('email_address', $email);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $data_user = $query->row();

            $this->db->where('email_address', $email);
            $this->db->where("password='$password'");

            $result = $this->db->get('jawara_users')->result();

            if (!empty($result)) {
                $this->session->set_userdata('first_name', $data_user->first_name);
                $this->session->set_userdata('last_name', $data_user->last_name);
                $this->session->set_userdata('email_address', $data_user->email_address);
                $this->session->set_userdata('profile_picture', $data_user->profile_picture);
                $this->session->set_userdata('role', $data_user->role);
                
                if($data_user->role < '1111'){
                    if($data_user->role == '999'){
                        $this->session->set_userdata('is_surveyor', TRUE);
                    }else if($data_user->role == '998'){
                        $this->session->set_userdata('is_sales', TRUE);
                    }else{
                        $this->session->set_userdata('is_login', TRUE);
                    }
                }else{
                    $this->session->set_userdata('is_p2p', TRUE);
                }
                

                $this->session->set_flashdata('success', 'Welcome to Dashboard Japang Jawara');
                return TRUE;
            } else {
                $this->session->set_flashdata('success', 'Password Anda Salah!!!');
                return FALSE;
            }
        } else {
            $this->session->set_flashdata('success', 'Email Anda Salah!!!');
            return FALSE;
        }

    }
}
?>
