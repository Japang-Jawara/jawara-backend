<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Eligible extends CI_Controller{

     function __construct(){
          parent::__construct();

          $this->load->model('Eligible_model', 'modelEligible');
     }

     function index(){
          if($this->session->userdata('is_login') == TRUE){
               $this->load->view("include/head");
               $this->load->view("include/top-header");
               $this->load->view('index');
               $this->load->view("include/sidebar");
               $this->load->view("include/panel");
               $this->load->view("include/footer");
               $this->load->view("include/alert");
          }else{
               $this->session->set_flashdata('success', 'Upsss!!!, Login dulu ya');
               redirect('index.php/login');
          }
     }

     function rekap(){
          if($this->session->userdata('is_login') == TRUE){
               $this->load->view("include/head");
               $this->load->view("include/top-header");
               $this->load->view('rekap');
               $this->load->view("include/sidebar");
               $this->load->view("include/panel");
               $this->load->view("include/footer");
               $this->load->view("include/alert");
          }else{
               $this->session->set_flashdata('success', 'Upsss!!!, Login dulu ya');
               redirect('index.php/login');
          }
     }

     function getRekap(){

          $data = $this->modelEligible->getRekap();

          $no = 1;
          $jawara = array();

          foreach($data as $r) {

               $nik_ktp = $r->nik_ktp == NULL ? "<font>NIK KTP Tidak Ada</font>" : $r->nik_ktp;

               if($r->file_ktp != NULL){
                    $file_ktp = '<a href="'.$r->file_ktp.'" target="_blank" class="btn btn-sm btn-primary">'.$r->file_ktp.'</a>';
               }else{
                    $file_ktp = 'Tidak ada File KTP';
               }

               $name = $r->name;
               $phone = $r->phone;
               $alamat_rumah = $r->alamat_rumah;
               $alamat_usaha = $r->alamat_usaha;
               $area_jawara = $r->area_jawara;
               $user_grading_sc = $r->user_grading;

               $jawara[] = array(
                    $no++,
                    $nik_ktp,
                    $name,
                    $phone,
                    $alamat_rumah,
                    $alamat_usaha,
                    $user_grading_sc,
                    $area_jawara,
                    $file_ktp
               );
               
          }
          
          echo json_encode($jawara);
     }

     function getAlldata(){

          $data = $this->modelEligible->getAlldata();

          $no = 1;
          $jawara = array();

          foreach($data as $r) {

               $input = "<input type='checkbox' class='approve_checkbox' value='$r->jawara_id'>";
               $nik_ktp = $r->nik_ktp_sc == NULL ? "<font>NIK KTP Tidak Ada</font>" : $r->nik_ktp_sc;

               if($r->file_ktp != NULL){
                    $file_ktp = '<a href="'.$r->file_ktp.'" target="_blank" class="btn btn-sm btn-primary">'.$r->file_ktp.'</a>';
               }else{
                    $file_ktp = 'Tidak ada File KTP';
               }

               $name_sc = $r->name_sc;
               $alamat_rumah = $r->alamat_rumah;
               $alamat_usaha = $r->alamat_usaha;
               $area_jawara = $r->area_jawara;
               $user_grading_sc = $r->user_grading_sc;


               $jawara[] = array(
                    $no++,
                    $input,
                    $nik_ktp,
                    $name_sc,
                    $alamat_rumah,
                    $alamat_usaha,
                    $user_grading_sc,
                    $area_jawara,
                    $file_ktp,
               );
               
          }
          
          echo json_encode($jawara);
     }

     function getApprove(){

          $data = $this->modelEligible->getApprove();

          $no = 1;
          $jawara = array();

          foreach($data as $r) {

               $nik_ktp = $r->nik_ktp_sc == NULL ? "<font>NIK KTP Tidak Ada</font>" : $r->nik_ktp_sc;

               if($r->file_ktp != NULL){
                    $file_ktp = '<a href="'.$r->file_ktp.'" target="_blank" class="btn btn-sm btn-primary">'.$r->file_ktp.'</a>';
               }else{
                    $file_ktp = 'Tidak ada File KTP';
               }

               $name_sc = $r->name_sc;
               $alamat_rumah = $r->alamat_rumah;
               $alamat_usaha = $r->alamat_usaha;
               $area_jawara = $r->area_jawara;
               $user_grading_sc = $r->user_grading_sc;


               $jawara[] = array(
                    $no++,
                    $nik_ktp,
                    $name_sc,
                    $alamat_rumah,
                    $alamat_usaha,
                    $user_grading_sc,
                    $area_jawara,
                    $file_ktp
               );
               
          }
          
          echo json_encode($jawara);
     }

     function getReject(){

          $data = $this->modelEligible->getReject();

          $no = 1;
          $jawara = array();

          foreach($data as $r) {

               $nik_ktp = $r->nik_ktp_sc == NULL ? "<font>NIK KTP Tidak Ada</font>" : $r->nik_ktp_sc;

               if($r->file_ktp != NULL){
                    $file_ktp = '<a href="'.$r->file_ktp.'" target="_blank" class="btn btn-sm btn-primary">'.$r->file_ktp.'</a>';
               }else{
                    $file_ktp = 'Tidak ada File KTP';
               }

               $name_sc = $r->name_sc;
               $alamat_rumah = $r->alamat_rumah;
               $alamat_usaha = $r->alamat_usaha;
               $area_jawara = $r->area_jawara;
               $user_grading_sc = $r->user_grading_sc;


               $jawara[] = array(
                    $no++,
                    $nik_ktp,
                    $name_sc,
                    $alamat_rumah,
                    $alamat_usaha,
                    $user_grading_sc,
                    $area_jawara,
                    $file_ktp
               );
               
          }
          
          echo json_encode($jawara);
     }

     public function approve_all()
	{
          if ($this->input->post('checkbox_value')) {
               $jawara_id = $this->input->post('checkbox_value');
               
               for ($count = 0; $count < count($jawara_id); $count++) {
                    $this->modelEligible->approve_all($jawara_id[$count]);
               }
          }
	}

	public function tolak_all()
	{
		if ($this->input->post('checkbox_value')) {
			$id = $this->input->post('checkbox_value');
			for ($count = 0; $count < count($id); $count++) {
				$this->modelEligible->tolak_all($id[$count]);
			}
		}
	}

     

}