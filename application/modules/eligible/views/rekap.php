<div id="content" class="content">
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="#">FINANCIAL INSTITUTION</a></li>
    </ol>
    <h1 class="page-header"><B>FINANCIAL INSTITUTION</b></h1>
    <div class="row">
        <div class="col-xl-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">
                    </h4>
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                </div>

                <div class="panel-body">
                    <div class ="table-responsive">
                        <table id="tbl-eligible-rekap" class="table table-striped table-bordered table-td-valign-middle" width="100%">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>NIK KTP</th>
                                    <th>NAMA</th>
                                    <th>NOMOR HP</th>
                                    <th>ALAMAT LENGKAP RUMAH</th>
                                    <th>ALAMAT LENGKAP USAHA</th>
                                    <th>USER GRADING</th>
                                    <th>AREA JAWARA</th>
                                    <th>FILE KTP</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            <!-- end panel -->
        </div>
        <!-- end col-10 -->
    </div>


    <!-- Manage -->
    <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/app-manage.js');?>"></script>

    
