<div id="content" class="content">
    <ol class="breadcrumb float-xl-right">
        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="#">FINANCIAL INSTITUTION</a></li>
    </ol>
    <h1 class="page-header"><B>FINANCIAL INSTITUTION</b></h1>
    <div class="row">
        <div class="col-xl-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">
                    </h4>
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class ="table-responsive">
                        <div class="col-xl-12">
                            <ul class="nav nav-tabs nav-tabs-inverse nav-justified nav-justified-mobile" data-sortable-id="index-2">
                                <li class="nav-item">
                                    <a href="#bm" data-toggle="tab" class="nav-link active">
                                        <span><i class="fa fa-list fa-lg m-r-5"></i> Approve/Reject</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#dt" data-toggle="tab" class="nav-link">
                                        <span><i class="fa fa-check-circle fa-lg m-r-5"></i> Approve</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#og" data-toggle="tab" class="nav-link">
                                        <span><i class="fa fa-stop fa-lg m-r-5"></i> Reject</span>
                                    </a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane fade active show" id="bm">
                                    <div class="panel-body">
                                        <div class ="table-responsive">
                                            <p>Action Approve or Reject Jawara</p>
                                            <button type="button" name="approve_all" id="approve_all" class="btn btn-success btn-xl">Approve</button>
                                            <button typr="button" name="tolak_all" id="tolak_all" class="btn btn-warning btn-xl">Reject</button>
                                            <br><br>
                                            <table id="tbl-eligiblee" class="table table-striped table-bordered table-td-valign-middle" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>NO</th>
                                                        <th>
                                                            <input type="checkbox" name="select-all" id="select-all" />
                                                        </th>
                                                        <th>NIK KTP</th>
                                                        <th>NAME</th>
                                                        <th>ALAMAT LENGKAP RUMAH</th>
                                                        <th>ALAMAT LENGKAP USAHA</th>
                                                        <th>USER GRADING</th>
                                                        <th>AREA JAWARA</th>
                                                        <th>FILE KTP</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="dt">
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table id="tbl-eligible-approve" class="table table-striped table-bordered table-td-valign-middle" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>NO</th>
                                                        <th>NIK KTP</th>
                                                        <th>NAME</th>
                                                        <th>ALAMAT LENGKAP RUMAH</th>
                                                        <th>ALAMAT LENGKAP USAHA</th>
                                                        <th>USER GRADING</th>
                                                        <th>AREA JAWARA</th>
                                                        <th>FILE KTP</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="og">
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table id="tbl-eligible-reject" class="table table-striped table-bordered table-td-valign-middle" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>NO</th>
                                                        <th>NIK KTP</th>
                                                        <th>NAME</th>
                                                        <th>ALAMAT LENGKAP RUMAH</th>
                                                        <th>ALAMAT LENGKAP USAHA</th>
                                                        <th>USER GRADING</th>
                                                        <th>AREA JAWARA</th>
                                                        <th>FILE KTP</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-10 -->
    </div>


    <!-- Manage -->
    <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/app-manage.js');?>"></script>

    <script type="text/javascript">

        $(document).ready(function() {

            $('#select-all').click(function(event) {
                if (this.checked) {
                    // Iterate each checkbox
                    $(':checkbox').each(function() {
                        this.checked = true;
                    });
                } else {
                    $(':checkbox').each(function() {
                        this.checked = false;
                    });
                }
            });

            $('.approve_checkbox').click(function() {
                if($(this).is(':checked')){
                    $('.approve_checkbox').prop('checked', true);
                }else{
                    $('.approve_checkbox').prop('checked', false);
                }
            });
 

            $('#approve_all').click(function() {

                var checkbox_value = [];
                var oTable = $('#tbl-eligiblee').dataTable();
                var rowcollection =  oTable.$(".approve_checkbox:checked", {"page": "all"});
                rowcollection.each(function(index,elem){
                    checkbox_value.push($(elem).val());

                });

                if (checkbox_value.length > 0) {

                    var yakin = confirm("Apakah kamu yakin akan Mengapprove ?");

                    console.log(checkbox_value)

                    if (yakin) {
                        $.ajax({
                            url: "eligible/approve_all",
                            method: "POST",
                            data: {
                                checkbox_value: checkbox_value
                            },
                            success: function() {
                                setInterval('location.reload()', 2000);
                            }
                        })
                        Swal.fire({
                            title: 'Informasi!',
                            text: 'Data Berhasil di Approve',
                            imageUrl: '<?= base_url('assets/svg/completed-animate.svg') ?>',
                            imageWidth: 400,
                            imageHeight: 250,
                            imageAlt: 'Custom image',
                        });
                    } else {
                        Swal.fire({
                            title: 'Informasi!',
                            text: 'Data Gagal di Approve, Silahkan Coba Lagi',
                            imageUrl: '<?= base_url('assets/svg/questions-animate.svg') ?>',
                            imageWidth: 400,
                            imageHeight: 250,
                            imageAlt: 'Custom image',
                        });
                    }
                } else {
                    alert('Tidak ada data yang ingin di Approve, silahkan pilih data terlebih dahulu.');
                }
            });

            $('#tolak_all').click(function() {
                var checkbox_value = [];
                var oTable = $('#tbl-eligiblee').dataTable();
                var rowcollection =  oTable.$(".approve_checkbox:checked", {"page": "all"});
                rowcollection.each(function(index,elem){
                    checkbox_value.push($(elem).val());

                });

                if (checkbox_value.length > 0) {
                   
                    var yakin = confirm("Apakah kamu yakin akan Mengreject ?");

                    if (yakin) {
                        $.ajax({
                            url: "eligible/tolak_all",
                            method: "POST",
                            data: {
                                checkbox_value: checkbox_value
                            },
                            success: function() {
                                setInterval('location.reload()', 2000);
                            }
                        })
                        Swal.fire({
                            title: 'Informasi!',
                            text: 'Data Berhasil di Reject',
                            imageUrl: '<?= base_url('assets/svg/empty-animate.svg') ?>',
                            imageWidth: 400,
                            imageHeight: 250,
                            imageAlt: 'Custom image',
                        });
                    } else {
                        Swal.fire({
                            title: 'Informasi!',
                            text: 'Data Gagal di Reject, Silahkan Coba Lagi',
                            imageUrl: '<?= base_url('assets/svg/questions-animate.svg') ?>',
                            imageWidth: 400,
                            imageHeight: 250,
                            imageAlt: 'Custom image',
                        });
                    }
                } else {
                    alert('Tidak ada data yang ingin di Reject, silahkan pilih data terlebih dahulu.');
                }
            });


        });
      
    </script>

    
