<?php
class Eligible_model extends CI_Model{

    public function getLend(){
        return $this->db->select('*')->from('lend_stats')->where('code', 1116)->where('is_active', 1)->get()->result();
    }

    public function getData($jawara_id){
        return $this->db->query("SELECT a.user_grading_sc, b.*
        FROM jawara_score as a
        JOIN jawara_leads as b ON a.jawara_id = b.id
        WHERE b.id = '$jawara_id'")
        ->result();
    }

    public function getRekap(){
        return $this->db->select('a.*, b.phone')->from('jawara_score_eligible as a')
        ->join('jawara_leads as b', 'a.jawara_id = b.id')->get()->result();
    }
    
    public function getAlldata(){
        return $this->db->query("SELECT a.*, b.file_ktp, b.alamat_rumah, b.alamat_usaha, b.provinsi, b.kab_kota, b.area_jawara
        FROM jawara_score as a
        JOIN jawara_leads as b ON a.jawara_id = b.id
        WHERE a.status_score_sc = 1 AND a.status_eligible IS NULL")
        ->result();
    }

    public function getApprove(){
        return $this->db->query("SELECT a.*, b.file_ktp, b.alamat_rumah, b.alamat_usaha, b.provinsi, b.kab_kota, b.area_jawara
        FROM jawara_score as a
        JOIN jawara_leads as b ON a.jawara_id = b.id
        WHERE a.status_score_sc = 1 AND a.status_eligible = 1")
        ->result();
    }

    public function getReject(){
        return $this->db->query("SELECT a.*, b.file_ktp, b.alamat_rumah, b.alamat_usaha, b.provinsi, b.kab_kota, b.area_jawara
        FROM jawara_score as a
        JOIN jawara_leads as b ON a.jawara_id = b.id
        WHERE a.status_score_sc = 1 AND a.status_eligible = 2")
        ->result();
    }

    function approve_all($jawara_id){

        $data = array(
            'status_eligible' => 1,
            'created_at_eligible'  => date('Y-m-d H:i:s')
        );
        
        $this->db->where('jawara_id', $jawara_id);
        $result = $this->db->update('jawara_score', $data);

        if($result){
            $getDataa = $this->getData($jawara_id);

            $dataa = array(
                'jawara_id' => $jawara_id,
                'nik_ktp' => $getDataa[0]->nik_ktp,
                'name' => $getDataa[0]->name,
                'alamat_rumah' => $getDataa[0]->alamat_rumah,
                'alamat_usaha' => $getDataa[0]->alamat_usaha,
                'user_grading' => $getDataa[0]->user_grading_sc,
                'area_jawara' => $getDataa[0]->area_jawara,
                'file_ktp' => $getDataa[0]->file_ktp,
                'created_at'  => date('Y-m-d H:i:s')
            );
            
            $this->db->insert('jawara_score_eligible', $dataa);
    
        }
        
	}

    function tolak_all($jawara_id){
		$input_data['status_eligible'] = '2';
        $input_data['created_at_eligible'] = date('Y-m-d H:i:s');
		$this->db->where('jawara_id', $jawara_id);
		$this->db->update('jawara_score', $input_data);
	}
}
?>